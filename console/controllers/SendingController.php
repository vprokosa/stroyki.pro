<?php

namespace console\controllers;
use common\enum\City;
use common\models\Realty;
use common\models\RealtyAttr;
use common\models\RealtyDesc;
use common\models\SubscriptionSearch;
use yii\console\Controller;
use yii\db\Query;

/**
 * Рассылка
 * Запуск, через команду [php yii sending/finder]
 */
class SendingController extends Controller
{
    /**
     * По поиску
     */
    public function actionFinder()
    {
        //1475533204
        $yesterday = date('d.m.Y', strtotime('-1 day'));

        foreach (SubscriptionSearch::find()->with('user')->each(10) as $subscription) {
            $rooms = [];
            $room = [];
            if($subscription->room4==1) {
                $room[] = '+4';
                $rooms[] = 4;
                $rooms[] = 5;
                $rooms[] = 6;
                $rooms[] = 7;
                $rooms[] = 8;
                $rooms[] = 9;
                $rooms[] = 10;
            }
            if($subscription->room3==1) {
                $room[] = 3;
                $rooms[] = 3;
            }
            if($subscription->room2==1) {
                $room[] = 2;
                $rooms[] = 2;
            }
            if($subscription->room1==1) {
                $room[] = 1;
                $rooms[] = 1;
            }

            $query = (new Query())
                ->select(['r.alias','rd.title'])
                ->from(Realty::tableName().' AS r')
                ->innerJoin(RealtyAttr::tableName().' AS ra', 'ra.realty_id=r.id')
                ->innerJoin(RealtyDesc::tableName().' AS rd', 'rd.realty_id=r.id')
                ->where(['FROM_UNIXTIME(r.created_at, "%d.%m.%Y")' => $yesterday])

                ->andFilterWhere(['in', 'ra.rooms', $rooms])
                ->andFilterWhere(['>=', 'ra.area_all', $subscription->start_area])
                ->andFilterWhere(['<=', 'ra.area_all', $subscription->end_area])
                ->andFilterWhere(['>=', 'r.price', $subscription->start_price])
                ->andFilterWhere(['<=', 'r.price', $subscription->end_price])
                ->andFilterWhere(['ra.city_id'=>$subscription->city_id])

                ->limit(10)
                ->all();
            
            if(count($query)) {
                \Yii::$app->mailer->compose(['html'=>'sending'], [
                    'full_name'=>$subscription->user->full_name,
                    'date'=>$yesterday,
                    'rooms'=>implode(',', $room),
                    'start_area'=>$subscription->start_area,
                    'end_area'=>$subscription->end_area,
                    'start_price'=>$subscription->start_price,
                    'end_price'=>$subscription->end_price,
                    'city'=>City::Lists($subscription->city_id),
                    'realty'=>$query
                ])
                    ->setFrom([\Yii::$app->params['supportEmail'] => 'Рассылка объявлений'])
                    ->setTo($subscription->user->email)
                    ->setSubject('Рассылка объявлений за '.$yesterday.', номер заказа №'.$subscription->id.', с сайта '.\Yii::$app->params['sitename'])
                    ->send();
            }
        }
        
        self::EXIT_CODE_NORMAL;
    }
}