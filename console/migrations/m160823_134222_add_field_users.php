<?php

use yii\db\Migration;

class m160823_134222_add_field_users extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'bg_header', \yii\db\Schema::TYPE_STRING.'(64) DEFAULT "nopic-bg-header.png" AFTER avatar');
    }

    public function down()
    {
        echo "m160823_134222_add_field_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
