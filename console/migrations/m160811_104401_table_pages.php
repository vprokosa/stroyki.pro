<?php

use yii\db\Migration;
use yii\db\Schema;

class m160811_104401_table_pages extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=MYISAM';
        }

        $this->createTable('{{%pages}}', [
            'id' => Schema::TYPE_PK,
            'status' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',
            'type' => Schema::TYPE_BOOLEAN.' DEFAULT NULL',
            'title' => Schema::TYPE_STRING.'(254) DEFAULT NULL',
            'text' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
    }

    public function down()
    {
        echo "m160811_104401_table_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
