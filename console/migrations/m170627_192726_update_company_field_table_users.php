<?php

use yii\db\Migration;

class m170627_192726_update_company_field_table_users extends Migration
{
    public function up()
    {
        $this->alterColumn('users', 'company', $this->string(5000)->defaultValue(null));
    }

    public function down()
    {
        echo "m170627_192726_update_company_field_table_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
