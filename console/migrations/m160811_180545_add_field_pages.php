<?php

use yii\db\Migration;

class m160811_180545_add_field_pages extends Migration
{
    public function up()
    {
        $this->addColumn('pages', 'alias', \yii\db\Schema::TYPE_STRING.'(254) DEFAULT NULL AFTER title');
        $this->createIndex('UK_pages__alias', 'pages', 'alias', true);
    }

    public function down()
    {
        echo "m160811_180545_add_field_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
