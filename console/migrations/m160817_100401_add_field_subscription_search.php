<?php

use yii\db\Migration;
use yii\db\Schema;

class m160817_100401_add_field_subscription_search extends Migration
{
    public function up()
    {
        $this->dropColumn('subscription_search', 'filter');
        $this->addColumn('subscription_search', 'room1', Schema::TYPE_BOOLEAN);
        $this->addColumn('subscription_search', 'room2', Schema::TYPE_BOOLEAN);
        $this->addColumn('subscription_search', 'room3', Schema::TYPE_BOOLEAN);
        $this->addColumn('subscription_search', 'room4', Schema::TYPE_BOOLEAN);
        $this->addColumn('subscription_search', 'start_area', Schema::TYPE_DECIMAL.'(10, 2)');
        $this->addColumn('subscription_search', 'end_area', Schema::TYPE_DECIMAL.'(10, 2)');
        $this->addColumn('subscription_search', 'start_price', Schema::TYPE_DECIMAL.'(10, 2)');
        $this->addColumn('subscription_search', 'end_price', Schema::TYPE_DECIMAL.'(10, 2)');
        $this->addColumn('subscription_search', 'option', Schema::TYPE_SMALLINT.'(1)');
        $this->addColumn('subscription_search', 'city_id', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m160817_100401_add_field_subscription_search cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
