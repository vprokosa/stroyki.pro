<?php

use yii\db\Migration;
use yii\db\Schema;

class m160811_114058_table_stat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%stat_realty}}', [
            'id' => Schema::TYPE_PK,
            'realty_id' => Schema::TYPE_INTEGER,
            'action' => Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_stat_realty__realty_id', '{{%stat_realty}}', 'realty_id');
        $this->createIndex('IX_stat_realty__action_created_at', '{{%stat_realty}}', ['action','created_at']);

        $this->addForeignKey('FK_stat_realty__realty_id', '{{%stat_realty}}', 'realty_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('{{%stat_banners}}', [
            'id' => Schema::TYPE_PK,
            'banner_id' => Schema::TYPE_INTEGER,
            'action' => Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_stat_banners__banner_id', '{{%stat_banners}}', 'banner_id');
        $this->createIndex('IX_stat_banners__action_created_at', '{{%stat_banners}}', ['action','created_at']);

        $this->addForeignKey('FK_stat_banners__banners', '{{%stat_banners}}', 'banner_id', '{{%banners}}', 'id', 'RESTRICT', 'CASCADE');


        //проиндекс. для виджетов статистики
        $this->createIndex('IX_realty__created_at', '{{%realty}}', 'created_at');
        $this->createIndex('IX_users__created_at', '{{%users}}', 'created_at');
        $this->createIndex('IX_payment__created_at', '{{%payment}}', 'created_at');
        $this->createIndex('IX_subscription_price__created_at', '{{%subscription_price}}', 'created_at');
        $this->createIndex('IX_subscription_search__created_at', '{{%subscription_search}}', 'created_at');
    }

    public function down()
    {
        echo "m160811_114058_table_stat cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
