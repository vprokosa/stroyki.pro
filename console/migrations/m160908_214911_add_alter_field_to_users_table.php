<?php

use yii\db\Migration;

class m160908_214911_add_alter_field_to_users_table extends Migration
{
    public function up()
    {
        $this->alterColumn('users', 'avatar', \yii\db\Schema::TYPE_STRING.'(20) DEFAULT NULL');
        $this->alterColumn('users', 'bg_header', \yii\db\Schema::TYPE_STRING.'(20) DEFAULT NULL');
        $this->renameColumn('users', 'bg_header', 'header');
    }

    public function down()
    {
        echo "m160908_214911_add_alter_field_to_users_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
