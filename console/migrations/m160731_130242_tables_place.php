<?php

use yii\db\Migration;
use yii\db\Schema;

class m160731_130242_tables_place extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%place_okrug}}', [
            'id' => Schema::TYPE_PK,
            'area_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->createTable('{{%place_city}}', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'okrug_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'default' => Schema::TYPE_BOOLEAN . ' DEFAULT 0',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_place_city_okrug_id', '{{%place_city}}', 'okrug_id');
        
        $this->addForeignKey('FK_place_city_okrug', '{{%place_city}}', 'okrug_id', '{{%place_okrug}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('{{%place_district}}', [
            'id' => Schema::TYPE_PK,
            'city_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_place_district_city_id', '{{%place_district}}', 'city_id');
        
        $this->addForeignKey('FK_place_district_city', '{{%place_district}}', 'city_id', '{{%place_city}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        echo "m160731_130242_tables_place cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
