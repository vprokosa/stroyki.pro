<?php

use yii\db\Migration;

class m160811_191550_add_index_pages extends Migration
{
    public function up()
    {
        $this->createIndex('UK_pages__type', 'pages', 'type', true);
    }

    public function down()
    {
        echo "m160811_191550_add_index_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
