<?php

use yii\db\Migration;
use yii\db\Schema;

class m160731_130301_tables_realestate extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%realty}}', [
            'id' => Schema::TYPE_PK,
            'status'=>Schema::TYPE_SMALLINT,
            'type'=>Schema::TYPE_SMALLINT,
            'price'=>Schema::TYPE_INTEGER,
            'price_update'=>Schema::TYPE_DATE.' COMMENT "Для рассылки"',
            'user_id'=>Schema::TYPE_INTEGER.' NOT NULL',
            'geo_lat' => Schema::TYPE_DECIMAL.'(10, 8) COMMENT "Координаты широты"',
            'geo_lng' => Schema::TYPE_DECIMAL.'(10, 8) COMMENT "Координата долготы"',
            'alias'=> Schema::TYPE_STRING,
            'deadline' => Schema::TYPE_INTEGER.' COMMENT "Срок сдачи"',
            'vip' => Schema::TYPE_BOOLEAN.' DEFAULT 0',
            'parent_id' => Schema::TYPE_INTEGER.' DEFAULT 0 COMMENT "В составе" ',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_realty_type', '{{%realty}}', 'type');
        $this->createIndex('IX_realty_price', '{{%realty}}', 'price');
        $this->createIndex('IX_realty_user_id', '{{%realty}}', 'user_id');
        $this->createIndex('IX_realty_geo_lat_geo_lng', '{{%realty}}', ['geo_lat','geo_lng']);
        $this->createIndex('IX_realty_alias', '{{%realty}}', 'alias');
        $this->createIndex('IX_realty_vip', '{{%realty}}', 'vip');
        $this->createIndex('IX_realty_parent_id', '{{%realty}}', 'parent_id');

        $this->addForeignKey('FK_realty_users', '{{%realty}}', 'user_id', '{{%users}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_realty_parent_id', '{{%realty}}', 'parent_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');

        //Атрибуты недвижимости
        $this->createTable('{{%realty_attr}}', [
            'id' => Schema::TYPE_PK,
            'realty_id'=> Schema::TYPE_INTEGER.' NOT NULL',
            'city_id' => Schema::TYPE_INTEGER.' NOT NULL COMMENT "Город"',
            'district_id' => Schema::TYPE_INTEGER.' NOT NULL COMMENT "Район"',
            'street' => Schema::TYPE_STRING.'(120) COMMENT "Улица"',
            'house' => Schema::TYPE_STRING.'(36) COMMENT "Дом"',
            'housing' => Schema::TYPE_STRING.'(36) COMMENT "Корпус"',
            'building' => Schema::TYPE_STRING.'(36) COMMENT "Строение"',
            'around' => Schema::TYPE_STRING.'(36) COMMENT "Рядом"',
            'liter' => Schema::TYPE_STRING.'(36) COMMENT "Литер"',
            'gkh_name' => Schema::TYPE_STRING.'(36) COMMENT "ЖКХ"',
            'developer' => Schema::TYPE_STRING.'(36) COMMENT "Застройщик"',
            'housing_estate' => Schema::TYPE_STRING.'(36) COMMENT "Жилой комплекс"',
            'rooms' => Schema::TYPE_SMALLINT.' COMMENT "Комнаты"',
            'floor' => Schema::TYPE_SMALLINT.' COMMENT "Этаж"',
            'floors' => Schema::TYPE_SMALLINT.' COMMENT "Этажей"',
            'area_all' => Schema::TYPE_DECIMAL.'(5,2) COMMENT "Общая площадь"',
            'area_living' => Schema::TYPE_DECIMAL.'(5,2) COMMENT "Жилая площадь"',
            'area_kitchen' => Schema::TYPE_DECIMAL.'(5,2) COMMENT "Площадь кухни"',
        ], $tableOptions);
        $this->createIndex('IX_realty_attr_realty_id', '{{%realty_attr}}', 'realty_id');
        $this->createIndex('IX_realty_attr_city_id', '{{%realty_attr}}', 'city_id');
        $this->createIndex('IX_realty_attr_district_id', '{{%realty_attr}}', 'district_id');
        $this->createIndex('IX_realty_attr_rooms', '{{%realty_attr}}', 'rooms');
        $this->createIndex('IX_realty_attr_area_all', '{{%realty_attr}}', 'area_all');

        $this->addForeignKey('FK_realty_attr_realty', '{{%realty_attr}}', 'realty_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_realty_attr_place_city', '{{%realty_attr}}', 'city_id', '{{%place_city}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_realty_attr_place_district', '{{%realty_attr}}', 'district_id', '{{%place_district}}', 'id', 'RESTRICT', 'CASCADE');

        //Описание
        $this->createTable('{{%realty_desc}}', [
            'id' => Schema::TYPE_PK,
            'realty_id'=> Schema::TYPE_INTEGER.' NOT NULL',
            'title'=> Schema::TYPE_STRING,
            'details'=> Schema::TYPE_TEXT,
        ], $tableOptions);
        $this->createIndex('IX_realty_desc_realty_id', '{{%realty_desc}}', 'realty_id');

        $this->addForeignKey('FK_realty_desc_realty', '{{%realty_desc}}', 'realty_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');

        //Картинки
        $this->createTable('{{%realty_image}}', [
            'id' => Schema::TYPE_PK,
            'realty_id'=> Schema::TYPE_INTEGER.' NOT NULL',
            'image' => Schema::TYPE_STRING.'(60)',
            'desc' => Schema::TYPE_STRING.'(120)',
            'sort' => Schema::TYPE_SMALLINT,
        ], $tableOptions);
        $this->createIndex('IX_realty_image_realty_id', '{{%realty_image}}', 'realty_id');

        $this->addForeignKey('FK_realty_image_realty', '{{%realty_image}}', 'realty_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');

        //Документы акты,схемы, ...
        $this->createTable('{{%realty_doc}}', [
            'id' => Schema::TYPE_PK,
            'realty_id'=> Schema::TYPE_INTEGER.' NOT NULL',
            'name'=> Schema::TYPE_STRING.'(60) COMMENT "Название"',
            'file'=> Schema::TYPE_STRING.'(254)',
            'sort' => Schema::TYPE_SMALLINT,
        ], $tableOptions);
        $this->createIndex('IX_realty_doc_realty_id', '{{%realty_doc}}', 'realty_id');

        $this->addForeignKey('FK_realty_doc_realty', '{{%realty_doc}}', 'realty_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');

        //Избраное
        $this->createTable('{{%realty_favorite}}', [
            'id' => Schema::TYPE_PK,
            'realty_id'=> Schema::TYPE_INTEGER.' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER.' NOT NULL COMMENT "Пользователь который добавил"',
            'created_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_realty_favorite_realty_id', '{{%realty_favorite}}', 'realty_id');
        $this->createIndex('IX_realty_favorite_user_id', '{{%realty_favorite}}', 'user_id');

        $this->addForeignKey('FK_realty_favorite_realty', '{{%realty_favorite}}', 'realty_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_realty_favorite_users', '{{%realty_favorite}}', 'user_id', '{{%users}}', 'id', 'RESTRICT', 'CASCADE');

        //Vip
        $this->createTable('{{%realty_vip}}', [
            'id' => Schema::TYPE_PK,
            'realty_id'=> Schema::TYPE_INTEGER.' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'end_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_realty_vip_realty_id', '{{%realty_vip}}', 'realty_id');

        $this->addForeignKey('FK_realty_vip_realty', '{{%realty_vip}}', 'realty_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        echo "m160731_130301_tables_realestate cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
