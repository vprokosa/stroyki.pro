<?php

use yii\db\Migration;

/**
 * Handles adding comment to table `payment`.
 */
class m160828_134010_add_comment_column_to_payment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('payment', 'comment', $this->text(). ' AFTER user_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('payment', 'comment');
    }
}
