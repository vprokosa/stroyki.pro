<?php

use yii\db\Migration;
use yii\db\Schema;

class m160731_130351_tables_payment extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Оплата
        $this->createTable('{{%payment}}', [
            'id' => Schema::TYPE_PK,
            'status'=>Schema::TYPE_SMALLINT,
            'system_pay'=> Schema::TYPE_SMALLINT.' COMMENT "Платежная система"',
            'pay'=>Schema::TYPE_DECIMAL.'(10,2)',
            'user_id'=>Schema::TYPE_INTEGER.' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_payment_user_id', '{{%payment}}', 'user_id');

        $this->addForeignKey('FK_payment_users', '{{%payment}}', 'user_id', '{{%users}}', 'id', 'RESTRICT', 'CASCADE');

        //Vip
        $this->createTable('{{%payment_vip}}', [
            'id' => Schema::TYPE_PK,
            'payment_id'=> Schema::TYPE_INTEGER.' NOT NULL',
            'realty_vip_id'=> Schema::TYPE_INTEGER,
            'period' => Schema::TYPE_SMALLINT,
        ], $tableOptions);
        $this->createIndex('IX_payment_vip_payment_id', '{{%payment_vip}}', 'payment_id');
        $this->createIndex('IX_payment_vip_realty_vip_id', '{{%payment_vip}}', 'realty_vip_id');

        $this->addForeignKey('FK_payment_vip_payment', '{{%payment_vip}}', 'payment_id', '{{%payment}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_payment_vip_realty_vip', '{{%payment_vip}}', 'realty_vip_id', '{{%realty_vip}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        echo "m160731_130351_tables_payment cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
