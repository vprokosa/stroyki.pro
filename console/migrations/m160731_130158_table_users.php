<?php

use yii\db\Migration;
use yii\db\Schema;

class m160731_130158_table_users extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_BOOLEAN.' DEFAULT NULL',
            'status' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',
            'role' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',
            'username' => Schema::TYPE_STRING . '(36) NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(64) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . '(64) NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING. '(64) NOT NULL',
            'email' => Schema::TYPE_STRING . '(80) NOT NULL',
            'full_name' => Schema::TYPE_STRING.'(64) DEFAULT NULL',
            'phone' => Schema::TYPE_STRING.'(32) DEFAULT NULL',
            'avatar' => Schema::TYPE_STRING.'(64) DEFAULT "nopic-user.png"',
            'company' => Schema::TYPE_STRING.'(84) DEFAULT NULL',
            'address' => Schema::TYPE_STRING.' DEFAULT NULL',
            'sending' => Schema::TYPE_BOOLEAN.' DEFAULT 0',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('UK_users_username', '{{%users}}', 'username', true);
        $this->createIndex('UK_users_email', '{{%users}}', 'email', true);
    }

    public function down()
    {
        echo "m160731_130158_table_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
