<?php

use yii\db\Migration;

class m160808_060929_data_realty extends Migration
{
    public function up()
    {
        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias1',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира1',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица1',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);



        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias2',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира2',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица2',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);


        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias3',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира3',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица3',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);



        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias4',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира4',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица4',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);


        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias5',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира5',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица5',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);




        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias6',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира6',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица6',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);




        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias7',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира7',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица7',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);




        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias8',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира8',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица8',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);



        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias9',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира9',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица9',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);



        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias10',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира10',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица10',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);

        $this->insert('realty', [
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'type'=>\common\enum\Realty::TYPE_FLAT,
            'price'=>12345,
            'price_update'=>'',
            'user_id'=>1,
            'geo_lat'=>'',
            'geo_lng'=>'',
            'alias'=>'alias11',
            'deadline'=>'',
            'vip'=>0,
            'parent_id'=>'',
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $realtyId = $this->db->lastInsertID;
        $this->insert('realty_desc', [
            'realty_id'=>$realtyId,
            'title'=>'Квартира11',
            'details'=>'',
        ]);
        $this->insert('realty_attr', [
            'realty_id'=>$realtyId,
            'city_id'=>1,
            'district_id'=>1,
            'street'=>'Улица11',
            'house'=>'1',
            'housing'=>'',
            'building'=>'',
            'around'=>'',
            'liter'=>'',
            'gkh_name'=>'',
            'developer'=>'',
            'housing_estate'=>'',
            'rooms'=>'',
            'floor'=>'',
            'floors'=>'',
            'area_all'=>'',
            'area_living'=>'',
            'area_kitchen'=>'',
        ]);

    }

    public function down()
    {
        echo "m160808_060929_data_realty cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
