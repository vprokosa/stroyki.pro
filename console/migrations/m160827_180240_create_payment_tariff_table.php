<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `payment_tariff`.
 */
class m160827_180240_create_payment_tariff_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('payment_tariff', [
            'id' => $this->primaryKey(),
            'type' => Schema::TYPE_BOOLEAN,
            'title' => Schema::TYPE_STRING.'(36)',
            'tariff' => Schema::TYPE_SMALLINT,
            'desc' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('payment_tariff');
    }
}
