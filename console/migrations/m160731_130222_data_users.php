<?php

use yii\db\Migration;

class m160731_130222_data_users extends Migration
{
    public function up()
    {
        $this->insert('users', [
            'full_name'=>'Пользователь 1',
            'username'=>'user1',
            'role'=>\common\enum\User::ROLE_USER,
            'status'=>\common\enum\User::STATUS_ACTIVE,
            'email'=>'user1@mail.ru',
            'password_hash'=>\Yii::$app->security->generatePasswordHash(12345678),
            'auth_key'=>\Yii::$app->security->generateRandomString(),
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $this->insert('users', [
            'full_name'=>'Пользователь 2',
            'username'=>'user2',
            'role'=>\common\enum\User::ROLE_USER,
            'status'=>\common\enum\User::STATUS_ACTIVE,
            'email'=>'user2@mail.ru',
            'password_hash'=>\Yii::$app->security->generatePasswordHash(12345678),
            'auth_key'=>\Yii::$app->security->generateRandomString(),
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $this->insert('users', [
            'full_name'=>'Пользователь 3',
            'username'=>'user3',
            'role'=>\common\enum\User::ROLE_USER,
            'status'=>\common\enum\User::STATUS_ACTIVE,
            'email'=>'user3@mail.ru',
            'password_hash'=>\Yii::$app->security->generatePasswordHash(12345678),
            'auth_key'=>\Yii::$app->security->generateRandomString(),
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
        $this->insert('users', [
            'full_name'=>'Арташес',
            'username'=>'kriartds',
            'role'=>\common\enum\User::ROLE_ADMIN,
            'status'=>\common\enum\User::STATUS_ACTIVE,
            'email'=>'admin@mail.ru',
            'password_hash'=>\Yii::$app->security->generatePasswordHash(12345678),
            'auth_key'=>\Yii::$app->security->generateRandomString(),
            'created_at'=>time(),
            'updated_at'=>time(),
        ]);
    }

    public function down()
    {
        echo "m160731_130222_data_users cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
