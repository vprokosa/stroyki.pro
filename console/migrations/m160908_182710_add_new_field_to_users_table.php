<?php

use yii\db\Migration;

class m160908_182710_add_new_field_to_users_table extends Migration
{
    public function up()
    {
        $this->addColumn('users', 'logo', \yii\db\Schema::TYPE_STRING.'(20) AFTER sending');
        $this->addColumn('users', 'site', \yii\db\Schema::TYPE_STRING.'(64) AFTER logo');
    }

    public function down()
    {
        echo "m160908_182710_add_new_field_to_users_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
