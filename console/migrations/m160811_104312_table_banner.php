<?php

use yii\db\Migration;
use yii\db\Schema;

class m160811_104312_table_banner extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%banners}}', [
            'id' => Schema::TYPE_PK,
            'status' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1',
            'type' => Schema::TYPE_BOOLEAN.' DEFAULT NULL',
            'city_id' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING.'(160) DEFAULT NULL',
            'image' => Schema::TYPE_STRING.'(60) DEFAULT NULL',
            'link' => Schema::TYPE_STRING.'(254) DEFAULT NULL',
            'sort' => Schema::TYPE_SMALLINT,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_banners_city_id', '{{%banners}}', 'city_id');
        $this->addForeignKey('FK_banners_city', '{{%banners}}', 'city_id', '{{%place_city}}', 'id', 'RESTRICT', 'CASCADE');

    }

    public function down()
    {
        echo "m160811_104312_table_banner cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
