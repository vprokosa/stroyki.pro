<?php

use yii\db\Migration;

class m161003_085938_add_field_table_subscription_price extends Migration
{
    public function up()
    {
        $this->addColumn(\common\models\SubscriptionPrice::tableName(), 'show', \yii\db\Schema::TYPE_BOOLEAN.' DEFAULT 0 AFTER user_id');
    }

    public function down()
    {
        echo "m161003_085938_add_field_table_subscription_price cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
