<?php

use yii\db\Migration;
use yii\db\Schema;

class m160731_130320_tables_subscription extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        //Подписка на поиск
        $this->createTable('{{%subscription_search}}', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'filter' => Schema::TYPE_TEXT.' COMMENT "Пользовательский фильтр на поиск"',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_subscription_search_user_id', '{{%subscription_search}}', 'user_id');

        $this->addForeignKey('FK_subscription_search_users', '{{%subscription_search}}', 'user_id', '{{%users}}', 'id', 'RESTRICT', 'CASCADE');

        //Подписка на объявления если меняется цена
        $this->createTable('{{%subscription_price}}', [
            'id' => Schema::TYPE_PK,
            'realty_id'=> Schema::TYPE_INTEGER.' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER,
            'created_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
        $this->createIndex('IX_subscription_price_realty_id', '{{%subscription_price}}', 'realty_id');
        $this->createIndex('IX_subscription_price_user_id', '{{%subscription_price}}', 'user_id');

        $this->addForeignKey('FK_subscription_price_realty', '{{%subscription_price}}', 'realty_id', '{{%realty}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_subscription_price_users', '{{%subscription_price}}', 'user_id', '{{%users}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        echo "m160731_130320_tables_subscription cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
