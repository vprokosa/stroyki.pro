<?php

use yii\db\Migration;

class m170628_045640_add_realty_image_sort_field_table extends Migration
{
    public function up()
    {
        $this->addColumn('realty_image', 'sort', $this->integer(11));
    }

    public function down()
    {
        echo "m170628_045640_add_realty_image_sort_field_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
