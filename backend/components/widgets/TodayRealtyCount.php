<?php

namespace backend\components\widgets;


use common\models\Realty;
use yii\base\Widget;

class TodayRealtyCount extends Widget
{
    public function run()
    {
        $count = Realty::find()->where([
            'status'=>\common\enum\Realty::STATUS_PUBLIC,
            'FROM_UNIXTIME(created_at, "%d.%m.%Y")' => date('d.m.Y', time()),
        ])->count();
        return $this->render('today-realty-count', ['count'=>$count]);
    }
}