<?php

namespace backend\components\widgets;


use common\models\User;
use yii\base\Widget;

class UserCount extends Widget
{
    public function run()
    {
        $count = User::find()->where(['role'=>\common\enum\User::ROLE_USER])->count();
        return $this->render('user-count', ['count'=>$count]);
    }
}