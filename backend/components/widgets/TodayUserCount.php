<?php

namespace backend\components\widgets;


use common\models\User;
use yii\base\Widget;

class TodayUserCount extends Widget
{
    public function run()
    {
        $count = User::find()->where([
            'status'=>\common\enum\User::STATUS_ACTIVE,
            'role'=>\common\enum\User::ROLE_USER,
            'FROM_UNIXTIME(created_at, "%d.%m.%Y")' => date('d.m.Y', time()),
        ])->count();
        return $this->render('today-user-count', ['count'=>$count]);
    }
}