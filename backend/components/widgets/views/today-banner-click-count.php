<div class="small-box bg-aqua">
    <div class="inner">
        <h3><?= $count ?></h3>
        <p>За сегодня</p>
    </div>
    <div class="icon">
        <i class="ion ion-stats-bars"></i>
    </div>
    <a class="small-box-footer" href="<?= \yii\helpers\Url::to(['/realty/index']) ?>">Все <i class="fa fa-arrow-circle-right"></i></a>
</div>