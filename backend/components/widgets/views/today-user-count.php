<div class="small-box bg-red">
    <div class="inner">
        <h3><?= $count ?></h3>
        <p>За сегодня</p>
    </div>
    <div class="icon">
        <i class="ion ion-pie-graph"></i>
    </div>
    <a class="small-box-footer" href="<?= \yii\helpers\Url::to(['/users/index']) ?>">Все <i class="fa fa-arrow-circle-right"></i></a>
</div>