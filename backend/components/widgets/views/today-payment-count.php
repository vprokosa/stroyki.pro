<div class="small-box bg-yellow">
    <div class="inner">
        <h3><?= $count ?></h3>
        <p>За сегодня</p>
    </div>
    <div class="icon">
        <i class="ion ion-person-add"></i>
    </div>
    <a class="small-box-footer" href="<?= \yii\helpers\Url::to(['/payment/index']) ?>">Все <i class="fa fa-arrow-circle-right"></i></a>
</div>