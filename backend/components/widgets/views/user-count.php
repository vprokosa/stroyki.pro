<div class="info-box">
    <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Пользователей</span>
        <span class="info-box-number"><?= $count ?></span>
    </div>
</div>