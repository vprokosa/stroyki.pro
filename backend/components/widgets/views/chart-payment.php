<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Платежи</h3>
    </div>
    <div class="box-body">
        <div class="chart">
            <?= \dosamigos\chartjs\ChartJs::widget([
                'type' => 'line',
                'options' => [
                    'height' => 250,
                ],
                'data' => [
                    'labels' => $month,
                    'datasets' => [
                        [
                            'label'=> $year,
                            'fillColor' => "rgba(151,187,205,0.5)",
                            'strokeColor' => "rgba(151,187,205,1)",
                            'pointColor' => "rgba(151,187,205,1)",
                            'pointStrokeColor' => "#fff",
                            'data' => $data
                        ]
                    ]
                ]
            ]);
            ?>
        </div>
    </div>
</div>