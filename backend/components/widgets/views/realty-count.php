<div class="info-box">
    <span class="info-box-icon bg-green"><i class="fa fa-institution"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Объявлений</span>
        <span class="info-box-number"><?= $count ?></span>
    </div>
</div>