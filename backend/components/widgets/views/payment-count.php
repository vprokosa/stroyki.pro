<div class="info-box">
    <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Платежей</span>
        <span class="info-box-number"><?= $count ?></span>
    </div>
</div>