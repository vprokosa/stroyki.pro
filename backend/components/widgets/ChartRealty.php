<?php

namespace backend\components\widgets;


use common\models\Realty;
use yii\base\Widget;
use yii\db\Query;

class ChartRealty extends Widget
{
    public function run()
    {
        $currentYear = date('Y', time());
        $currentMonth = date('m', time());
        $month = [
            '01'=>'Январь',
            '02'=>'Феврать',
            '03'=>'Март',
            '04'=>'Апрель',
            '05'=>'Май',
            '06'=>'Июнь',
            '07'=>'Июль',
            '08'=>'Август',
            '09'=>'Сентябрь',
            '10'=>'Октябрь',
            '11'=>'Ноябрь',
            '12'=>'Декабрь'
        ];
        $dataYear = $currentYear;
        $dataMonth = [];
        $data = [];

        foreach($month as $number=>$name) {
            if($number <= $currentMonth) {
                $count = (new Query())->from(Realty::tableName())->where([
                    'FROM_UNIXTIME(created_at, "%m")'=>$number
                ])->count();
                $dataMonth[] = $name;
                $data[] = $count;
            }
        }

        return $this->render('chart-realty', [
            'year'=>$dataYear,
            'month'=>$dataMonth,
            'data'=>$data,
        ]);
    }
}