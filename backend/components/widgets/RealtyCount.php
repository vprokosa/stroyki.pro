<?php

namespace backend\components\widgets;


use common\models\Realty;
use yii\base\Widget;

class RealtyCount extends Widget
{
    public function run()
    {
        $count = Realty::find()->count();
        return $this->render('realty-count', ['count'=>$count]);
    }
}