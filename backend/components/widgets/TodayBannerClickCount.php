<?php

namespace backend\components\widgets;


use common\enum\Stat;
use common\models\StatBanners;
use yii\base\Widget;

class TodayBannerClickCount extends Widget
{
    public function run()
    {
        $count = StatBanners::find()->where([
            'action'=>Stat::ACTION_CLICK,
            'FROM_UNIXTIME(created_at, "%d.%m.%Y")' => date('d.m.Y', time()),
        ])->count();
        return $this->render('today-banner-click-count', ['count'=>$count]);
    }
}