<?php

namespace backend\components\widgets;


use common\models\Payment;
use yii\base\Widget;

class TodayPaymentCount extends Widget
{
    public function run()
    {
        $count = Payment::find()->where([
            'status'=>\common\enum\Payment::STATUS_SUCCESS,
            'FROM_UNIXTIME(created_at, "%d.%m.%Y")' => date('d.m.Y', time()),
        ])->count();
        return $this->render('today-payment-count', ['count'=>$count]);
    }
}