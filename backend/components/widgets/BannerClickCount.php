<?php

namespace backend\components\widgets;


use common\enum\Stat;
use common\models\StatBanners;
use yii\base\Widget;

class BannerClickCount extends Widget
{
    public function run()
    {
        $count = StatBanners::find()->where(['action'=>Stat::ACTION_CLICK])->count();
        return $this->render('banner-click-count', ['count'=>$count]);
    }
}