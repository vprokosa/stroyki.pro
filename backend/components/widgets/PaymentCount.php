<?php

namespace backend\components\widgets;


use common\models\Payment;
use yii\base\Widget;

class PaymentCount extends Widget
{
    public function run()
    {
        $count = Payment::find()->count();
        return $this->render('payment-count', ['count'=>$count]);
    }
}