<?php

namespace backend\components;
 
use common\traits\UrlTrait;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

class AppController extends \yii\web\Controller
{
    public $disableFooter = false;
    
    use UrlTrait;
    
    public $bodyClass = 'hold-transition skin-blue sidebar-mini';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['error'],
                        'allow' => true,
                        'roles' => ['?','@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return \Yii::$app->user->identity->isAdmin();
                        }
                    ],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($action->id=='error') {
                $this->layout ='@frontend/views/layouts/total';
            }
            return true;
        } else {
            return false;
        }
    }
    
    protected function getModelOr404($modelClass, $id)
    {
        if(is_numeric($id)) {
            $model = $modelClass::findOne($id);

            if ($model) {
                return $model;
            }
        }
        throw new NotFoundHttpException();
    }
}