<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'thumbnail'],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
				'name' => '_backendUser', 
				'path'=>'/backend/web',
				'httpOnly' => true
			],
        ],
        'session' => [
            'name' => '_backendSessionId', // unique for backend
			'savePath' => __DIR__ . '/../runtime', // a temporary folder on backend
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'enableStrictParsing' => false,
			'rules' => [
				
				'<controller>' => '<controller>/index',
				'<controller>/<action>' => '<controller>/<action>',
				'<modules>/<controller>/<action>' => '<modules>/<controller>/<action>',
			],
		],
        'urlManagerFrontEnd' => [require(Yii::getAlias('@common/config/urlManager.php')),'class' => \frontend\components\RealtyUrlRule::class,],
    ],
    'params' => $params,
];
