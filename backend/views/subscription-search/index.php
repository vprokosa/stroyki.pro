<style>
    .table th{width:150px;}
    #w0-kvdate{width:150px;}
</style>
<?php

$this->title = 'Подписка на поиск, всего '.$dataProvider->getTotalCount();

$this->params['breadcrumbs'][] = 'Список подписки';

?>

<div class="box">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
        </div>
    </div>

    <div class="box-body">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => 'Ничего не найдено',
            'summary'=>'',
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'columns' => [
                'id',
                [
                    'attribute'=>'user_id',
                    'content'=>function($data) {
                        return \common\enum\User::Lists($data->user_id);
                    },
                ],
                [
                    'attribute'=>'room1',
                    'content'=>function($data) {
                        return $data->room1 ? 'Да' : '';
                    },
                ],
                [
                    'attribute'=>'room2',
                    'content'=>function($data) {
                        return $data->room2 ? 'Да' : '';
                    },
                ],
                [
                    'attribute'=>'room3',
                    'content'=>function($data) {
                        return $data->room3 ? 'Да' : '';
                    },
                ],
                [
                    'attribute'=>'room4',
                    'content'=>function($data) {
                        return $data->room4 ? 'Да' : '';
                    },
                ],
                [
                    'attribute'=>'start_area',
                    'content'=>function($data) {
                        return Yii::$app->formatter->asInteger($data->start_area);
                    },
                ],
                [
                    'attribute'=>'end_area',
                    'content'=>function($data) {
                        return Yii::$app->formatter->asInteger($data->end_area);
                    },
                ],
                [
                    'attribute'=>'start_price',
                    'content'=>function($data) {
                        return Yii::$app->formatter->asInteger($data->start_price);
                    },
                ],
                [
                    'attribute'=>'end_price',
                    'content'=>function($data) {
                        return Yii::$app->formatter->asInteger($data->end_price);
                    },
                ],
                [
                    'attribute'=>'city_id',
                    'content'=>function($data) {
                        return \common\enum\City::Lists($data->city_id);
                    }
                ],
                [
                    'attribute'=>'created_at',
                    'content'=>function($data) {
                        return date('d.m.Y', $data->created_at);
                    },
                ],

//                [
//                    'class' => 'yii\grid\ActionColumn',
//                    'template' => '{view}',//{update} {delete}
//                    'buttons' => [
//                        'view' => function ($url, $model){
//                            return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', $url, [
//                                'class' => 'btn-xs btn-info',
//                                'title' => 'Просмотр',
//                            ]);
//                        },
//                        'update' => function ($url, $model){
//                            return \yii\helpers\Html::a('<i class="fa fa-pencil-square-o"></i>', $url, [
//                                'class' => 'btn-xs btn-warning',
//                                'title' => 'Обновить',
//                            ]);
//                        },
//                        'delete' => function ($url, $model){
//                            return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', $url, [
//                                'class' => 'btn-xs btn-danger',
//                                'title' => 'Удалить',
//                                'data-confirm' => 'Удалить ?'
//                            ]);
//                        },
//                    ]
//                ],
            ],
        ]); ?>
    </div>

</div>


