<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

    <div class="box-body">

        <?= $form->field($model, 'username')->textInput() ?>
    
        <?= $form->field($model, 'email')->input('email') ?>
    
        <?= $form->field($model, 'full_name')->textInput() ?>
		
        <?= $form->field($model, 'buildcompany')->textInput() ?>
    
        <?= $form->field($model, 'type')->dropDownList(\common\enum\User::Type(), ['prompt'=>'']) ?>
    
        <?php if($model->scenario == \common\enum\Scenario::CREATE): ?>
            <?= $form->field($model, 'password_hash')->passwordInput([]) ?>
        <?php endif ?>
    </div>

    <div class="box-footer">
        <?= yii\helpers\Html::submitButton(!is_numeric($model->id) ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary']) ?>
        <?= yii\helpers\Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>


<?php ActiveForm::end(); ?>

