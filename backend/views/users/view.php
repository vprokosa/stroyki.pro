<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = "Просмотр пользователя $model->full_name";

$this->params['breadcrumbs'][] = ['label' => 'Список пользователей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->full_name;
?>

<div class="box box-info">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn-sm btn-warning']) ?>
            <?= $model->lockAndUnlock() ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn-sm btn-danger']) ?>
        </div>
    </div>

    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'attributes' => [
                [
                    'attribute'=> 'avatar',
                    'format'=>'html',
                    'value' => $model->avatar(50,50),
                ],
                [
                    'attribute'=> 'status',
                    'value' => \common\enum\User::Status($model->status)
                ],
                [
                    'attribute'=> 'type',
                    'value' => $model->type != 0 ? \common\enum\User::Type($model->type) : ''
                ],
                'full_name',
                'username',
                'email:email',
                'phone',
                'company',
                'buildcompany',
                'address',
                [
                    'attribute'=> 'sending',
                    'value' => \common\enum\User::Subscription($model->sending)
                ],
                [
                    'attribute'=> 'created_at',
                    'value' => date('d.m.Y', $model->created_at)
                ]
            ],
        ]) ?>
    </div>

	<div class="box-footer">
		<?= Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
	</div>

</div>
