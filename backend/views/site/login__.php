<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\assets\IcheckAsset;
use common\models\Realty;
use common\models\City;
use common\models\District;
use common\models\base\Users;
use common\models\User;
use common\models\RealEstateImages;
use yii\db\Connection;
use yii\base\Security;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
header('Content-Type: text/html; charset=utf-8');

$this->title = 'Авторизация';
?>
<div class="login-box">
    <div class="login-logo">
        Авторизация
    </div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin() ?>

        <?= $form->field($model, 'username',[
            'options'=>[
                'class'=>'form-group has-feedback',
            ],
            'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}'
        ])->textInput([
            'class'=>'form-control',
            'placeholder'=>$model->getAttributeLabel('username'),
            'autofocus' => true
        ]) ?>

        <?= $form->field($model, 'password', [
            'options'=>[
                'class'=>'form-group has-feedback',
            ],
            'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
        ])->passwordInput([
            'class'=>'form-control',
            'placeholder'=>$model->getAttributeLabel('password'),
        ]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe',[
                    'template' => "<div class=\"checkbox icheck\"><label>{input} {label}</label></div>{error}",
                ])->checkbox() ?>
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('Вход', [
                    'class' => 'btn btn-primary btn-block btn-flat',
                    'name' => 'login-button'
                ]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

        <!-- <a href="#">I forgot my password</a><br>-->
        <!-- <a href="register.html" class="text-center">Register a new membership</a>-->

    </div>
</div>
<?php 
		$ids = array(); 
		$fileIds = __DIR__ . '/../../../ids.json';
		$f = __DIR__ . '/../../../exp.xml';
		if (file_exists(__DIR__ . '/../../../ids.json')){
			$ids = json_decode(file_get_contents($fileIds));
		}
		$data = simplexml_load_file($f);
		foreach($data->items->item as $item){
print_r($item->images->image);
die();
			if(in_array((string)$item['id'], $ids)){continue;}
			$ids[] = (string)$item['id'];
			file_put_contents($fileIds, json_encode($ids));
			print_r($ids);
			die();
			$city = Cities::findOne(['title' => (string)$item['ad_city']]);
			$district = Districts::findOne(['city_id' => $city->id]);
			$user = Users::findOne(['username' => (string)$item['ad_phone']]);
			if(!$user){
				$result = Yii::$app->db->createCommand('INSERT INTO `users` (`username`,`email`,`full_name`) VALUES (:username,:email,:full_name)', [':username' => (string)$item['ad_phone'], ':email' => (string)$item['email'], ':full_name' => (string)$item['name']])->execute();
				if($result){
					$user = Users::findOne(['username' => (string)$item['ad_phone']]);
				} else {
					continue;
				}
			}
			if(!$city){
				$city = new stdClass();
				$city->id = 1036;
			}
			if(!$district){
				$district = new stdClass();
				$district->id = 4;
			}
			$obj = new RealEstates([
				'published' => (string)$item['published'],
				'user_id' => $user->id,
				'street' => (string)$item['ad_street'], 
				'city_id' => $city->id,
				'house' => (string)$item['ad_nomerdoma'], 
				'geo_lat' => (string)$item['lat'], 
				'geo_lng' => (string)$item['lng'], 
				'deadline' => (string)$item['expiration_date'], 
				'rooms' => (string)$item['ad_room'], 
				'floor' => (string)$item['ad_etach'], 
				'floors' => (string)$item['ad_etagei'], 
				'area_all' => (string)$item['ad_obshajas'], 
				'area_living' => (string)$item['ad_zhilayas'], 
				'area_kitchen' => (string)$item['ad_kyxnyas'],
				'price' => (string)$item['ad_price'],
				'details' => (string)$item['ad_text'],
				'created_at' => strtotime((string)$item['date_created']), 
				'updated_at' => strtotime((string)$item['date_up']),
				'housing_estate' => (string)$item['profile_name'],
				'activated_at' => (string)$item['date_created'],
				'stats' => (string)$item['views']
			]);
			echo '<pre>';
			//$obj->load();
			$obj->validate();
			//var_dump($obj->errors);
			if(!$obj->save(false)){
				print_r($obj->getErrors());
			}
			$security = new Security();
			$image_path = Yii::getAlias('@images') . '/' . $obj->id;
			if (!file_exists($image_path)){
				$old_umask = umask(0);
				mkdir($image_path, 0777, true);
				umask($old_umask);
				//mkdir($image_path, 0777);
			}

/* print_r($item->images);
die(); */
			foreach($item->images->image as $imageStr){
				foreach($imageStr->attributes() as $key=>$attr){
					if($key == 'src'){
						$filename = end(explode('/', $attr));
						$exp = end(explode('.', $attr));
						//$UploadedFile = new UploadedFile($attr);
						//$file = new RealEstateImages(['imageFiles'=>$attr]);
						$imageNew = $security->generateRandomString(64) . '.' . $exp;
						$existImg = '/var/www/icecalls/data/www/zoloz.ru/frontend/web/uploads/ads/' . $filename;
						$image_path_img = $image_path . '/' . $imageNew;
						//$full_path = $image_path_img . '/' . $image;
						//shell_exec("cp -r $attr $image_path_img");
						if(
						rename($existImg, $image_path_img)){ 
							$result = Yii::$app->db->createCommand('INSERT INTO `real_estate_images` (`real_estate_id`,`image`) VALUES (:real_estate_id,:image)', [':real_estate_id' => $obj->id, ':image' => $imageNew])->execute();
						} else {
							print_r($filename.' не удалось скопировать!');
						}
						//$filename = end(explode('/', $attr));
						//$result = Yii::$app->db->createCommand('INSERT INTO `real_estate_images` (`real_estate_id`,`image`) VALUES (:real_estate_id,:image)', [':real_estate_id' => $obj->id, ':image' => $filename])->execute();

						print_r($attr);
					}
		 			//print_r($UploadedFile->getExtension());
				}
			}
			//print_r($image_path);
			echo '</pre>';
			die();
			//$obj->save();
			//print_r($obj);
			//print_r($city);
			//print_r($item); 
			echo '</pre>';
			die();
			/* 
			
			 `full_name`,
                            `username`,
                            `email`
							( 8M8x3L1d
			`published`,
			`archived`,
			`user_id`, 
			`city_id`,
			`district_id`,
			`street`,
			`house`,
			`housing`,
			`building`,
			`around`,
			`liter`, 
			`geo_lat`,
			`geo_lng`,
			`gkh_name`,
			`developer`,
			`deadline`,
			`rooms`,
			`floor`,
			`floors`,
			`area_all`,
			`area_living`,
			`area_kitchen`,
			`price`,
			`details`,
			`created_at`,
			`updated_at`,
			`housing_estate`,
			`activated_at`,
			`stats`
			) */
			//
			foreach($item->attributes() as $key=>$attr){
				switch((string)$key){
					case 'published':
					$obj->$key = (string)$attr;
					break;
					case 'userid':
					$obj->user_id = (string)$attr;
					break;
					case 'ad_street':
					$obj->street = (string)$attr;
					break;
					case 'ad_city':
					$obj->city_id = 1036;
					break;
					case 'ad_nomerdoma':
					$obj->house = (string)$attr;
					break;
					
					case 'lat':
					$obj->geo_lat = (string)$attr;
					break;
					case 'lng':
					$obj->geo_lng = (string)$attr;
					break;
					case 'expiration_date':
					$obj->deadline = (string)$attr;
					break;
					case 'ad_room':
					$obj->rooms = (string)$attr;
					break;
					case 'ad_etach':
					$obj->floor = (string)$attr;
					break;
					
					case 'ad_etagei':
					$obj->floors = (string)$attr;
					break;
					case 'ad_obshajas':
					$obj->area_all = (string)$attr;
					break;
					case 'area_living':
					$obj->ad_zhilayas = (string)$attr;
					break;
					case 'ad_kyxnyas':
					$obj->area_kitchen = (string)$attr;
					break;
					case 'ad_price':
					$obj->price = (string)$attr;
					break;
					
					case 'ad_text':
					$obj->details = (string)$attr;
					break;
					case 'date_created':
					$obj->created_at = strtotime((string)$attr);
					break;
					case 'date_up':
					$obj->updated_at = strtotime((string)$attr);
					break;
					case 'profile_name':
					$obj->housing_estate = (string)$attr;
					break;
					case 'date_created':
					$obj->activated_at = strtotime((string)$attr);
					break;
					case 'views':
					$obj->stats = (string)$attr;
					break;
				}

			//$vars = get_object_vars($item['@attributes']);
			//while (list($key, $value) = each($vars)) :
			$obj->$key = (string)$attr;
			//endwhile;дальнобой 15 канал CB  0673290780 мария
# цена договорная ( тел. +380955040111, тел.+380677857172, тел. +380999636926 )

			}
			print_r($item);
			unset($obj->id);
			unset($obj->images);
			unset($obj->typevalues);
			unset($obj->komments);
			unset($obj->catss);
				$result = JFactory::getDbo()->insertObject('asuw_objects', $obj);
				$insertid = JFactory::getDbo()->insertid();
				print_r($result);
				print_r($insertid);
			if($item->images->image){
				foreach($item->images->image as $img){
					if($img['src']){
						$i=1;
						$imageToDb = new stdClass();
						$imageToDb->idobj = $insertid;
						$imageToDb->filename = (string)$img['src'];
						$imageToDb->ordering = $i;
						$resultImg = JFactory::getDbo()->insertObject('asuw_obj_images', $imageToDb);
						$i++;
					}
					print_r($img);
					
				}
				
			}
			if($item->types->type){
				foreach($item->types->type as $type){
						$typeToDb = new stdClass();
						$typeToDb->idobj = $insertid;
						$typeToDb->value_id = (string)$type['value_id'];
						$typeToDb->type = (string)$type['type'];
						$typeToDb->title = (string)$type['title'];
						$resultImg = JFactory::getDbo()->insertObject('asuw_obj_type', $typeToDb);
					print_r($type);
					
				}
			}
			
			if($item->cats->cat){
				foreach($item->cats->cat as $cat){
						$catToDb = new stdClass();
						$catToDb->idobj = (string)$cat['idobj'];
						$catToDb->idcat = (string)$cat['idcat'];
						$catToDb->idobjlocal = $insertid;
						$resultCat = JFactory::getDbo()->insertObject('asuw_obj_cats', $catToDb);
					print_r($cat); 
					
				}
			}
			if($item->comments->comment){
				foreach($item->comments->comment as $comment){
						$commentToDb = new stdClass();
						foreach($comment->attributes() as $key=>$attr){
							$commentToDb->$key = (string)$attr;
						}
						$commentToDb->idobj = $insertid;
						$resultCat = JFactory::getDbo()->insertObject('asuw_comment', $commentToDb);
					print_r($type);
					
				}
			}
			echo '</pre>';
		}
			die();

$city = Cities::findOne(['title' => 'Москва']);
//$cityesList = Cities::find()->all();
			echo '<pre>';
			print_r($city->id);
			echo '</pre>';
			die();
