<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\assets\IcheckAsset;

use common\models\User;
use common\models\Realty;
use common\models\RealtyAttr;
use common\models\RealtyDesc;
use common\models\RealtyImage;
use yii\helpers\FileHelper;
use yii\imagine\Image;

use yii\base\Action;
use backend\models\CitySearch;
use backend\models\DistrictSearch;
use backend\models\OkrugSearch;
use backend\models\UsersSearch;

$this->title = 'Авторизация';
header('Content-Type: text/html; charset=utf-8');
?>

<div class="login-box">
    <div class="login-logo">
        Авторизация
    </div>
    <div class="login-box-body">
        <?php $form = ActiveForm::begin() ?>

        <?= $form->field($model, 'username',[
            'options'=>[
                'class'=>'form-group has-feedback',
            ],
            'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}'
        ])->textInput([
            'class'=>'form-control',
            'placeholder'=>$model->getAttributeLabel('username'),
            'autofocus' => true
        ]) ?>

        <?= $form->field($model, 'password', [
            'options'=>[
                'class'=>'form-group has-feedback',
            ],
            'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
        ])->passwordInput([
            'class'=>'form-control',
            'placeholder'=>$model->getAttributeLabel('password'),
        ]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe',[
                    'template' => "<div class=\"checkbox icheck\"><label>{input} {label}</label></div>{error}",
                ])->checkbox() ?>
            </div>
            <div class="col-xs-4">
                <?= Html::submitButton('Вход', [
                    'class' => 'btn btn-primary btn-block btn-flat',
                    'name' => 'login-button'
                ]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

        <!-- <a href="#">I forgot my password</a><br>-->
        <!-- <a href="register.html" class="text-center">Register a new membership</a>-->

    </div>
</div>
<?php  
		$ids = array(); 
		/* $usersFile = __DIR__ . '/../../../users.json';
		$users = json_decode(file_get_contents($usersFile));
		
		foreach($users as $user){
			$userFromDb = UsersSearch::findOne(['email' => $user->email]);
			if(!$userFromDb){
				$pass = Yii::$app->security->generatePasswordHash('654321');
				$result = Yii::$app->db->createCommand(
				'INSERT INTO `users` (
				`type`,`status`,`role`,`username`,`password_hash`,`email`,`full_name`, `phone`,`company`,`address`)
				VALUES 
				(:type,:status,:role,:username,:password_hash,:email,:full_name,:phone,:company,:address)',
				[
				':type' => 1,
				':status' => 1,
				':role' => 1,
				':username' => $user->username,
				':password_hash' => $pass,
				':email' => $user->email,
				':full_name' => $user->name,
				':phone' => $user->ad_phone,
				':company' => $user->ad_company.' -- '.$user->ad_company,
				':address' => $user->ad_agencyaddress
				])->execute();
				if($result){
					
				}
			}
		}
		die(); */
		/* $user = UsersSearch::findOne(['phone' => '']);
		echo '<pre>'; 
		print_r($user);
		echo '</pre>';

		die(); */
		/* $fileIds = __DIR__ . '/../../../ids.json';
		$f = __DIR__ . '/../../../exp.xml';
		if (file_exists(__DIR__ . '/../../../ids.json')){
			$ids = json_decode(file_get_contents($fileIds));
		}
		$data = simplexml_load_file($f);
		$i=0;
		foreach($data->items->item as $item){
				echo '<pre>'; 
				print_r('i='.$i);
				echo '</pre>';
			$i++;
			$user = UsersSearch::findOne(['email' => (string)$item['user_email']]);
			if(!$user){
				$pass = Yii::$app->security->generatePasswordHash('654321');
				$result = Yii::$app->db->createCommand(
				'INSERT INTO `users` (
				`username`,`email`,`full_name`, `phone`,`company`,`address`,`password_hash`)
				VALUES 
				(:username,:email,:full_name,:phone,:company,:address,:password_hash)',
				[
				':username' => (string)$item['user_email'],
				':email' => (string)$item['user_email'],
				':full_name' => (string)$item['name'],
				':phone' => (string)$item['profile_phone'],
				':company' => (string)$item['profile_company'],
				':password_hash' => $pass,
				':address' => (string)$item['profile_agentstvo']
				])->execute();
				if($result){
					$user = User::findOne(['email' => (string)$item['user_email']]);
				}
				echo '<pre>'; 
				print_r($item);
				echo '</pre>';
				echo '<pre>';
				print_r($user);
				echo '</pre>';
			die('1');
			}
			if((empty($user->phone)) || (empty($user->email))){
				$user->username = (string)$item['user_email'];
				$user->full_name = (string)$item['name'];
				$user->email = (string)$item['user_email'];
				$user->phone = (string)$item['ad_phone'];
				$user->validate();
				$user->update();
				echo '<pre>'; 
				print_r($item);
				echo '</pre>';
				echo '<pre>';
				print_r($user);
				echo '</pre>';
				//continue;
			die('2');
			} else {
				echo '<pre>'; 
				print_r($item);
				echo '</pre>';
				echo '<pre>';
				print_r($user);
				echo '</pre>';
				continue;
				die('3');
			}
			die();
			if(in_array((string)$item['id'], $ids)){continue;} 
			$ids[] = (string)$item['id'];
			$rooms = substr((string)$item['cat'], 0, 1);
			//$item['ad_sroksdachi'] = "не указан";
			$sroksdachi = explode(' ', (string)$item['ad_sroksdachi']);
			//print_r((string)$sroksdachi[1]);
			//$srok = array();
			$deadline = 2038;
			if(isset($sroksdachi[1]) && preg_match("/^(\d+)?\d+$/", $sroksdachi[1], $srok)){
				if(isset($srok[0])){
					$deadline = $srok[0].'-01-01';
				}
			}

			$city = CitySearch::find();
			$district = DistrictSearch::find();
			$okrug = OkrugSearch::find();
			if(!empty((string)$item['ad_city'])){
				$city = CitySearch::findOne(['title' => (string)$item['ad_city']]);
				if(!$city){
					continue;
				}
				$district = DistrictSearch::findOne(['city_id' => $city->id]);
				if(!$district){
					$district = new stdClass();
					$district->id = 2;
				}
				$okrug = OkrugSearch::findOne(['id' => $city->okrug_id]);
			} else {
				$city = new stdClass();
				$city->id = 1036;
				$district = new stdClass();
				$district->id = 2;
				$okrug = new stdClass();
				$okrug->id = 83;
			}
			$user = UsersSearch::findOne(['email' => (string)$item['email']]);
			if(!$user){
				$user = UsersSearch::findOne(['username' => (string)$item['ad_phone']]);
			}
			if(!$user){
				$pass = Yii::$app->security->generatePasswordHash('654321');
				$result = Yii::$app->db->createCommand(
				'INSERT INTO `users` (
				`username`,`email`,`full_name`, `phone`,`company`,`address`,`password_hash`)
				VALUES 
				(:username,:email,:full_name,:phone,:company,:address,:password_hash)',
				[
				':username' => (string)$item['ad_phone'],
				':email' => (string)$item['email'],
				':full_name' => (string)$item['name'],
				':phone' => (string)$item['profile_phone'],
				':company' => (string)$item['profile_company'],
				':password_hash' => $pass,
				':address' => (string)$item['profile_agentstvo']
				])->execute();
				if($result){
					$user = User::findOne(['email' => (string)$item['email']]);
				} else {
					continue;
				}
			}
			
			$obj = new Realty([
				'status' => 2,
				'type' => 1,
				'price' => (string)$item['ad_price'],
				'user_id' => $user->id,
				'geo_lat' => (string)$item['lat'], 
				'geo_lng' => (string)$item['lng'], 
				'deadline' => strtotime($deadline), 
				'created_at' => strtotime((string)$item['date_created']), 
				'parent_id' => NULL, 
				'updated_at' => strtotime((string)$item['date_up'])
			]);
			$obj->validate();
			if(!$obj->save(false)){
				print_r($obj->getErrors());
			}
			$objAttrs = new RealtyAttr([
				'realty_id' => $obj->id,
				'city_id' => $city->id,
				'district_id' => $district->id,
				'street' => (string)$item['ad_street'], 
				'house' => (string)$item['ad_nomerdoma'], 
				'housing_estate' => (string)$item['profile_name'],
				'rooms' => $rooms, 
				'floor' => (string)$item['ad_etach'], 
				'floors' => (string)$item['ad_etagei'], 
				'area_all' => (string)$item['ad_obshajas'], 
				'area_living' => (string)$item['ad_zhilayas'], 
				'area_kitchen' => (string)$item['ad_kyxnyas'],
				'housing_estate' => (string)$item['ad_Kompleks']
			]);
			$objAttrs->validate();
			if(!$objAttrs->save(false)){
				print_r($objAttrs->getErrors());
			}
			
			$objDesc = new RealtyDesc([
				'realty_id' => $obj->id,
				'title' => (string)$item['id'].'--'.(string)$item['ad_headline'],
				'details' => (string)$item['ad_text']
			]);
			$objDesc->validate();
			if(!$objDesc->save(false)){
				print_r($objDesc->getErrors());
			}
			$dir = \Yii::getAlias('@common').'/upload/realty/picture/'.$obj->id;
			if(!file_exists($dir)) {
				FileHelper::createDirectory($dir);
			}

			foreach($item->images->image as $imageStr){
				foreach($imageStr->attributes() as $key=>$attr){
					if($key == 'src'){
						$filename = end(explode('/', $attr));
						$ext = end(explode('.', $attr));
						
						$name = \Yii::$app->security->generateRandomString().'.'.$ext;
						//
						$image = new RealtyImage();
						$image->realty_id = $obj->id;
						$image->image = $name;
						// 
						$existImg = '/var/www/stroyki/data/www/stroyki.pro.global/frontend/ads/' . $filename;
						if(file_exists($existImg)){
							if((copy($existImg,"$dir/$name")) && ($image->save())) {
								print_r($image);
							} else {
								print_r($name.' не удалось скопировать!');
							} 
							
						} 
					}
				}
			}

			file_put_contents($fileIds, json_encode($ids));




		} */


