<!-- Пользователь -->
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <?= Yii::$app->user->identity->avatar(25, 25) ?>
        <span class="hidden-xs">
            <?= Yii::$app->user->identity->full_name ?>
        </span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            <?= Yii::$app->user->identity->avatar(50, 50) ?>
            <p>
                <?= Yii::$app->user->identity->full_name ?>
                <small>Администратор</small>
            </p>
        </li>

        <?php /*
        <!-- Menu Body -->
        <li class="user-body">
            <div class="row">
                <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                </div>
            </div>
            <!-- /.row -->
        </li>
        */ ?>

        <!-- Menu Footer-->
        <li class="user-footer">

            <?php /*
            <div class="pull-left">
                <a href="#" class="btn btn-default btn-flat">Profile</a>
            </div>
            */ ?>
            <div class="pull-left">
                <?= \yii\helpers\Html::a('Настройки', ['users/update?id=' . Yii::$app->user->getId()], ['class'=>'btn btn-default btn-flat']) ?>
            </div>
            <div class="pull-right">
                <?= \yii\helpers\Html::a('Выход', ['site/logout'], ['class'=>'btn btn-default btn-flat']) ?>
            </div>
        </li>
    </ul>
</li>