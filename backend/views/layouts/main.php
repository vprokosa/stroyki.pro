<?php $this->beginContent('@backend/views/layouts/base.php') ?>

    <div class="wrapper">

        <?= $this->render('_header') ?>

        <?= $this->render('_menu') ?>

        <div class="content-wrapper">
            
            <?= $this->render('_breadcrumb') ?>

            <section class="content">
                <br>
                <?= $this->render('@frontend/views/layouts/_alert') ?>
                <?= $content ?>
            </section>
            
        </div>

        <?= $this->render('_footer') ?>

        <?= $this->render('_control') ?>

<?php $this->endContent() ?>