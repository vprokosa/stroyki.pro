<section class="content-header">

    <?= \yii\widgets\Breadcrumbs::widget([
        'id' => 'breadcrumb',
        'homeLink' => [
            'label' => 'Панель',
            'url' => '/',
            'template' => '<li><i class="fa fa-dashboard"></i> {link}</li>',
        ],
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : null,
        'options' => [
            'class'=>'breadcrumb'
        ]
    ]) ?>

</section>
<br>