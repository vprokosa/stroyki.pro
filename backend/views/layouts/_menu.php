<?php

use common\traits\UrlTrait;

?>

<aside class="main-sidebar">

     <section class="sidebar">

         <div class="user-panel">
            <div class="pull-left image">
                <?= Yii::$app->user->identity->avatar(50, 50) ?>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->full_name ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>



         <ul class="sidebar-menu">
             <li>
                 <a href="<?= $this->context->getDomain() ?>"><i class="fa fa-star"></i>
                     <span>Сайт</span>
                 </a>
             </li>
             <li>
                 <a href="/"><i class="fa fa-dashboard"></i>
                     <span>Панель</span>
                 </a>
             </li>

             <!-- Объявления -->
             <li class="header">Объявления</li>
             <li>
                 <a href="<?= \yii\helpers\Url::to(['/realty/index']) ?>"><i class="fa fa-institution"></i>
                     <span>Недвижимость</span>
                 </a>
             </li>
             <li class="treeview">
                 <a href="#">
                     <i class="fa  fa-money"></i> <span>Платежи</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                 </a>
                 <ul class="treeview-menu">
                     <li>
                         <a href="<?= \yii\helpers\Url::to(['/payment/index']) ?>"><i class="fa fa-genderless"></i>
                             <span>Все</span>
                         </a>
                     </li>
                     <li>
                         <a href="<?= \yii\helpers\Url::to(['/tariff/index']) ?>"><i class="fa fa-genderless"></i>
                             <span>Тарифы</span>
                         </a>
                     </li>
                 </ul>
             </li>
             <li class="treeview">
                 <a href="#">
                    <i class="fa fa-rss"></i> <span>Подписка</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                 </a>
                 <ul class="treeview-menu">
                     <li>
                         <a href="<?= \yii\helpers\Url::to(['/subscription-search/index']) ?>"><i class="fa fa-genderless"></i>
                             На поиск
                         </a>
                     </li>
                     <li>
                         <a href="<?= \yii\helpers\Url::to(['/subscription-price/index']) ?>"><i class="fa fa-genderless"></i>
                             На цену
                         </a>
                     </li>
                 </ul>
             </li>
             <!-- #Объявления -->


             <!-- Инфо -->
             <li class="header">Инфо</li>
             <li>
                 <a href="<?= \yii\helpers\Url::to(['/page/index']) ?>"><i class="fa fa-file"></i>
                     <span>Страницы</span>
                 </a>
             </li>
             <li>
                 <a href="<?= \yii\helpers\Url::to(['/page/social']) ?>"><i class="fa fa-share-square"></i>
                     <span>Соц. иконки</span>
                 </a>
             </li>
             <!-- #Инфо -->

			 
             <!-- Инфо -->
             <li class="header">Статьи</li>
             <li>
                 <a href="<?= \yii\helpers\Url::to(['/article/index']) ?>"><i class="fa fa-file"></i>
                     <span>Статьи</span>
                 </a>
             </li>
             <!-- #Инфо -->


             <!-- #Реклама -->
             <li class="header">Реклама</li>
             <li>
                 <a href="<?= \yii\helpers\Url::to(['/banners/index']) ?>"><i class="fa fa-paw"></i>
                     <span>Баннеры</span>
                 </a>
             </li>
             <!-- #Реклама -->

             <!-- Статистика -->
             <li class="header">Статистика</li>
             <li>
                 <a href="<?= \yii\helpers\Url::to(['/stat/today']) ?>"><i class="fa fa-bar-chart"></i>
                     <span>За сегодня</span>
                 </a>
             </li>
             <li>
                 <a href="<?= \yii\helpers\Url::to(['/stat/banner']) ?>"><i class="fa fa-bar-chart"></i>
                     <span>Баннеры</span>
                 </a>
             </li>
             <!-- #Статистика -->


             <li class="header">Разное</li>
             <li>
                 <a href="<?= \yii\helpers\Url::to(['/users/index']) ?>">
                     <i class="fa fa-users"></i>
                     <span>Пользователи</span>
                 </a>
             </li>
             <li class="treeview">
                <a href="#">
                    <i class="fa fa-map"></i> <span>Территория</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <?= \yii\helpers\Html::a('<i class="fa fa-genderless"></i> Округи', ['/okrug/index']) ?>
                    </li>
                    <li>
                        <?= \yii\helpers\Html::a('<i class="fa fa-genderless"></i> Города', ['/city/index']) ?>
                    </li>
                    <li>
                        <?= \yii\helpers\Html::a('<i class="fa fa-genderless"></i> Районы', ['/district/index']) ?>
                    </li>
                </ul>
             </li>


        </ul>
    </section>
 </aside>