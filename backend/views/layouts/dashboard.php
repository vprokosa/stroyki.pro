<?php $this->title = 'Панель' ?>
<?php $this->beginContent('@backend/views/layouts/base.php') ?>

    <div class="wrapper">

<?= $this->render('_header') ?>

<?= $this->render('_menu') ?>

    <div class="content-wrapper">

        <section class="content-header">

        </section>

        <section class="content">

            <div class="row">

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?= \backend\components\widgets\RealtyCount::widget() ?>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?= \backend\components\widgets\PaymentCount::widget() ?>
                </div>

                 <div class="col-md-3 col-sm-6 col-xs-12">
                    <?= \backend\components\widgets\UserCount::widget() ?>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <?= \backend\components\widgets\BannerClickCount::widget() ?>
                </div>

            </div>

            <div class="row">

                <div class="col-lg-3 col-xs-6">
                    <?= \backend\components\widgets\TodayRealtyCount::widget() ?>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <?= \backend\components\widgets\TodayPaymentCount::widget() ?>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <?= \backend\components\widgets\TodayUserCount::widget() ?>
                </div>

                <div class="col-lg-3 col-xs-6">
                    <?= \backend\components\widgets\TodayBannerClickCount::widget() ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= \backend\components\widgets\ChartRealty::widget() ?>
                </div>
                <div class="col-md-6">
                    <?= \backend\components\widgets\ChartPayment::widget() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= \backend\components\widgets\ChartUser::widget() ?>
                </div>
                <div class="col-md-6">
                    <?= \backend\components\widgets\ChartBannerClick::widget() ?>
                </div>
            </div>

        </section>

<!--        <footer class="main-footer">-->
<!---->
<!--        </footer>-->
    </div>

<?= $this->render('_footer') ?>

<?= $this->render('_control') ?>

<?php $this->endContent() ?>