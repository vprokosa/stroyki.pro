<style>
    .table th{width:150px;}
    #w0-kvdate{width:150px;}
</style>
<?php

$this->title = 'Платежи, всего '.$dataProvider->getTotalCount();

$this->params['breadcrumbs'][] = 'Список платежей';

?>

<div class="box">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
        </div>
    </div>

    <div class="box-body">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => 'Ничего не найдено',
            'summary'=>'',
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'columns' => [
                'id',
                [
                    'attribute'=>'status',
                    'content'=>function($data) {
                        return \common\enum\Payment::Status($data->status);
                    },
                    'filter' => \common\enum\Payment::Status()
                ],
                [
                    'attribute'=>'system_pay',
                    'content'=>function($data) {
                        return \common\enum\Payment::System($data->system_pay);
                    },
                ],
                [
                    'attribute'=>'pay',
                    'content'=>function($data) {
                        return Yii::$app->formatter->asInteger($data->pay).'руб';
                    },
                ],
                [
                    'attribute'=>'user_id',
                    'content'=>function($data) {
                        return \common\enum\User::Lists($data->user_id);
                    },
                ],
                [
                    'attribute'=>'created_at',
                    'content'=>function($data) {
                        return date('d.m.Y', $data->created_at);
                    },
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',//{update} {delete}
                    'buttons' => [
                        'view' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn-xs btn-info',
                                'title' => 'Просмотр',
                            ]);
                        },
//                        'update' => function ($url, $model){
//                            return \yii\helpers\Html::a('<i class="fa fa-pencil-square-o"></i>', $url, [
//                                'class' => 'btn-xs btn-warning',
//                                'title' => 'Обновить',
//                            ]);
//                        },
//                        'delete' => function ($url, $model){
//                            return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', $url, [
//                                'class' => 'btn-xs btn-danger',
//                                'title' => 'Удалить',
//                                'data-confirm' => 'Удалить ?'
//                            ]);
//                        },
                    ]
                ],
            ],
        ]); ?>
    </div>

</div>


