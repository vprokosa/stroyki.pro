<?php

use yii\helpers\Html;

$this->title = 'Создать';

$this->params['breadcrumbs'][] = ['label' => 'Список тарифов', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';

?>

<div class="box box-success">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>
    </div>

    <div class="box-body">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>    
