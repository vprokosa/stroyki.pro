<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

    <div class="box-body">

        <?= $form->field($model, 'type')->dropDownList(\common\enum\PaymentTariff::Type(), ['prompt'=>'']) ?>

        <?= $form->field($model, 'title')->textInput() ?>

        <?= $form->field($model, 'tariff')->textInput() ?>

        <?= $form->field($model, 'desc')->textarea() ?>

    </div>

    <div class="box-footer">
        <?= yii\helpers\Html::submitButton(!is_numeric($model->id) ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary']) ?>
        <?= yii\helpers\Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

