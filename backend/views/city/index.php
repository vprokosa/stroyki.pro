<style>
    .table th{width:150px;}
    #w0-kvdate{width:150px;}
</style>
<?php

$this->title = 'Города, всего '.$dataProvider->getTotalCount();

$this->params['breadcrumbs'][] = 'Список городов';

?>

<div class="box">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
            <?= \yii\helpers\Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn-sm btn-success']) ?>
        </div>
    </div>

    <?php \yii\widgets\Pjax::begin([
        'timeout'=>10000,
        'scrollTo'=>new \yii\web\JsExpression('$(".box").position().top')
    ]) ?>

    <div class="box-body">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => 'Ничего не найдено',
            'summary'=>'',
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions'=>[
                        'style'=>'width:40px'
                    ]
                ],
                'title',
                [
                    'attribute'=>'okrug_id',
                    'content'=>function($data) {
                        return \common\enum\Okrug::Lists($data->okrug_id);
                    },
                    'filter' => \common\enum\Okrug::Lists()
                ],
                [
                    'attribute'=>'default',
                    'content'=>function($data) {
                        return \common\enum\City::Defaults($data->default);
                    },
                    'filter' => \common\enum\City::Defaults()
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn-xs btn-info',
                                'title' => 'Просмотр',
                            ]);
                        },
                        'update' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-pencil-square-o"></i>', $url, [
                                'class' => 'btn-xs btn-warning',
                                'title' => 'Обновить',
                            ]);
                        },
                        'delete' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', $url, [
                                'class' => 'btn-xs btn-danger',
                                'title' => 'Удалить',
                                'data-confirm' => 'Удалить ?'
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>

    <?php \yii\widgets\Pjax::end() ?>

</div>


