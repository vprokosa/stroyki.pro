<style>
    .table th{width:150px;}
    #w0-kvdate{width:150px;}
</style>
<?php

$this->title = 'Статьи, всего '.$dataProvider->getTotalCount();

$this->params['breadcrumbs'][] = 'Список статей';

?>

<div class="box">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
            <?= \yii\helpers\Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn-sm btn-success']) ?>
        </div>
    </div>

    <?php \yii\widgets\Pjax::begin([
        'timeout'=>10000,
        'scrollTo'=>new \yii\web\JsExpression('$(".box").position().top')
    ]) ?>
    
    <div class="box-body">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => 'Ничего не найдено',
            'summary'=>'',
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'columns' => [
                'id',
                [
                    'attribute'=>'status',
                    'content'=>function($data) {
                        return \common\enum\Article::Status($data->status);
                    },
                    'filter' => \common\enum\Article::Status()
                ],
                'title',
                [
                    'attribute'=>'city_id',
                    'content'=>function($data) {
                        return !empty($data->city_id) ?  \common\enum\City::Lists($data->city_id): 'Главная' ;
                    },
                    'filter' => \common\enum\City::Lists()
                ],
                [
                    'attribute'=>'position',
                    'content'=>function($data) {
                        return \common\enum\Article::Position($data->position);
                    },
                    'filter' => \common\enum\Article::Position()
                ],
                [
                    'attribute'=>'created_at',
                    'content'=>function($data) {
                        return date('d.m.Y', $data->created_at);
                    }, 
                    'filter'=>\kartik\widgets\DatePicker::widget([
                        'language' => 'en',
                        'name' => 'ArticlesSearch[created_at]',
                        'value'=> $searchModel->created_at,
                        'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
                        'layout'=>'{input}{remove}',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn-xs btn-info',
                                'title' => 'Просмотр',
                            ]);
                        },
                        'update' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-pencil-square-o"></i>', $url, [
                                'class' => 'btn-xs btn-warning',
                                'title' => 'Обновить',
                            ]);
                        },
                        'delete' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', $url, [
                                'class' => 'btn-xs btn-danger',
                                'title' => 'Удалить',
                                'data-confirm' => 'Удалить ?'
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>

    <?php \yii\widgets\Pjax::end() ?>

</div>


