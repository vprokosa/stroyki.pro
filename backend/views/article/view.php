<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = "Просмотр статьи $model->title";
/* @var $this yii\web\View */

$this->params['breadcrumbs'][] = ['label' => 'Список статей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>
 
<div class="box box-info">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn-sm btn-warning']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn-sm btn-danger']) ?>
        </div>
    </div>

    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'attributes' => [
                'id',
                [
                    'attribute'=> 'status',
                    'value' => \common\enum\Article::Status($model->status),
                ],
                [
                    'attribute'=> 'position',
                    'value' => \common\enum\Article::Position($model->position),
                ],
                [
                    'attribute'=> 'city_id',
                    'value' => isset($model->city_id) ? \common\enum\City::Lists($model->city_id) : '',
                ],
                'title',
                [
                    'attribute'=> 'text',
                    'format'=>'html'
                ],
                [
                    'attribute'=> 'created_at',
                    'value' => date('d.m.Y', $model->created_at),
                ],
                [
                    'attribute'=> 'updated_at',
                    'value' => date('d.m.Y', $model->updated_at),
                ],
            ],
        ]) ?>
    </div>

	<div class="box-footer">
		<?= Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
	</div>

</div>
