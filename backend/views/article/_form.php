<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
 

/* @var $this yii\web\View */
/* @var $model common\models\Article */ 
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
  
    <div class="box-body">
        <?= $form->field($model, 'status')->dropDownList(\common\enum\Article::Status(), ['prompt'=>'']) ?>
        <?= $form->field($model, 'title')->textInput() ?>
        <?= $form->field($model, 'city_id')->dropDownList(\common\enum\City::Lists(), ['prompt'=>'']) ?>
        <?= $form->field($model, 'position')->dropDownList(\common\enum\Article::Position(), ['prompt'=>'']) ?>
		<?= $form->field($model, 'text')->widget(TinyMce::className(), [
			'options' => ['rows' => 6],
			'language' => 'ru',
			'clientOptions' => [
				'plugins' => [
					"advlist autolink lists link charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
				'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
			]
		]);?>
    </div>

    <div class="box-footer">
        <?= yii\helpers\Html::submitButton(!is_numeric($model->id) ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary']) ?>
        <?= yii\helpers\Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>


<?php ActiveForm::end(); ?>

