<?php

$this->title = "Обновить район $model->title";

$this->params['breadcrumbs'][] = ['label' => 'Список районов', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>

<div class="box box-warning">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>  
