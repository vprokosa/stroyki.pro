<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */

$this->title = "Просмотр района $model->title";

$this->params['breadcrumbs'][] = ['label' => 'Список районов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->title;
?>

<div class="box box-info">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn-sm btn-warning']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn-sm btn-danger']) ?>
        </div>
    </div>

    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'attributes' => [
                'title',
                [
                    'attribute'=> 'city_id',
                    'value' => \common\enum\City::Lists($model->city_id),
                ],
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>

</div>
