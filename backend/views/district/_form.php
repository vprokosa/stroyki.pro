<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

    <div class="box-body">
        <?= $form->field($model, 'title')->textInput() ?>

        <?= $form->field($model, 'city_id')->dropDownList(\common\enum\City::Lists(), ['prompt'=>'']) ?>

    </div>

    <div class="box-footer">
        <?= yii\helpers\Html::submitButton(!is_numeric($model->id) ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary']) ?>
        <?= yii\helpers\Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

