<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use common\components\Social;

/* @var $this yii\web\View */

$this->title = "Социальные кнопки";

$this->params['breadcrumbs'][] = ['label' => 'Социальные кнопки', 'url' => '/'];
?>

<div class="box box-info">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">

        </div>
    </div>	
	<form action="/page/social" method="get">
		<div class="box-body">
			<div class="form-group required">
				<label class="control-label" for="soc_1">Вконтакте</label>
				<input type="text" id="soc_1" class="form-control" name="vk" value="<?= Social::get('vk'); ?>" aria-required="true">
			</div>
			<div class="form-group required">
				<label class="control-label" for="soc_2">Одноклассники</label>
				<input type="text" id="soc_2" class="form-control" name="ok" value="<?= Social::get('ok'); ?>" aria-required="true">
			</div>
			<div class="form-group required">
				<label class="control-label" for="soc_3">Facebook</label>
				<input type="text" id="soc_3" class="form-control" name="fb" value="<?= Social::get('fb'); ?>" aria-required="true">
			</div>
			<div class="form-group required">
				<label class="control-label" for="soc_4">Twitter</label>
				<input type="text" id="soc_4" class="form-control" name="tw" value="<?= Social::get('tw'); ?>" aria-required="true">
			</div>
			<div class="form-group required">
				<label class="control-label" for="soc_5">Instagram</label>
				<input type="text" id="soc_5" class="form-control" name="ig" value="<?= Social::get('ig'); ?>" aria-required="true">
			</div>			
		</div>
		<div class="box-footer">
			<button type="submit" class="btn btn-primary">Обновить</button>
			<a class="btn btn-primary" href="/">Закрыть</a>
		</div>
		<input type="hidden" name="do" value="save">
	</form>
</div>
