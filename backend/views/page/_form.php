<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

\backend\assets\CkeditorAsset::register($this);

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
    $(document).ready(function () {
        $('#pageform-title').liTranslit({
            elAlias: $('#pageform-alias')
        });
        
        CKEDITOR.replace('PageForm[text]', {
            toolbar : [
                {name: "basicstyles",  items: ["Bold","Italic"]}
            ]
//            'filebrowserBrowseUrl':'/plugins/ckeditor/kcfinder/browse.php?type=files',
//            'filebrowserImageBrowseUrl':'/plugins/ckeditor/kcfinder/browse.php?type=images',
//            'filebrowserFlashBrowseUrl':'/plugins/ckeditor/kcfinder/browse.php?type=flash',
//            'filebrowserUploadUrl':'/plugins/ckeditor/kcfinder/upload.php?type=files',
//            'filebrowserImageUploadUrl':'/plugins/ckeditor/kcfinder/upload.php?type=images',
//            'filebrowserFlashUploadUrl':'/plugins/ckeditor/kcfinder/upload.php?type=flash'
        });
    });
</script>

<?php $form = ActiveForm::begin(); ?>

    <div class="box-body">

        <?= $form->field($model, 'status')->dropDownList(\common\enum\Pages::Status(), ['prompt'=>'']) ?>

        <?= $form->field($model, 'type')->dropDownList(\common\enum\Pages::Type(), ['prompt'=>'']) ?>

        <?= $form->field($model, 'title')->textInput() ?>

        <?= $form->field($model, 'alias')->textInput() ?>

        <?= $form->field($model, 'text')->textarea() ?>

    </div>

    <div class="box-footer">
        <?= yii\helpers\Html::submitButton(!is_numeric($model->id) ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary']) ?>
        <?= yii\helpers\Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

