<style>
    .table th{width:150px;}
    #w0-kvdate{width:150px;}
</style>
<?php

$this->title = 'Страниц, всего '.$dataProvider->getTotalCount();

$this->params['breadcrumbs'][] = 'Список страниц';

?>

<div class="box">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <?php /*
        <div class="box-tools pull-right">
            <?= \yii\helpers\Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn-sm btn-success']) ?>
        </div>
        */ ?>
    </div>

    <?php \yii\widgets\Pjax::begin([
        'timeout'=>10000,
        'scrollTo'=>new \yii\web\JsExpression('$(".box").position().top')
    ]) ?>

    <div class="box-body">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => 'Ничего не найдено',
            'summary'=>'',
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'columns' => [
                'id',
                [
                    'attribute'=>'status',
                    'content'=>function($data) {
                        return \common\enum\Pages::Status($data->status);
                    },
                    'filter' => \common\enum\Pages::Status()
                ],
                [
                    'attribute'=>'type',
                    'content'=>function($data) {
                        return \common\enum\Pages::Type($data->type);
                    },
                    'filter' => \common\enum\Pages::Type()
                ],
                'title',
                [
                    'attribute'=>'created_at',
                    'content'=>function($data) {
                        return date('d.m.Y',$data->created_at);
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn-xs btn-info',
                                'title' => 'Просмотр',
                            ]);
                        },
                        'update' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-pencil-square-o"></i>', $url, [
                                'class' => 'btn-xs btn-warning',
                                'title' => 'Обновить',
                            ]);
                        },
                        'delete' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', $url, [
                                'class' => 'btn-xs btn-danger',
                                'title' => 'Удалить',
                                'data-confirm' => 'Удалить ?'
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>

    <?php \yii\widgets\Pjax::end() ?>

</div>


