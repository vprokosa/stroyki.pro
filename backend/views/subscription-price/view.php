<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */

$this->title = 'Просмотр платежа';

$this->params['breadcrumbs'][] = ['label' => 'Список платежей', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'просмотр';
?>

<div class="box box-info">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
        </div>
    </div>

    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'attributes' => [
                'id',
                [
                    'attribute'=> 'status',
                    'value' => \common\enum\Payment::Status($model->status),
                ],
                [
                    'attribute'=> 'system_pay',
                    'value' => \common\enum\Payment::System($model->system_pay),
                ],
                [
                    'attribute'=> 'pay',
                    'value' => Yii::$app->formatter->asInteger($model->pay).'руб',
                ],
                'comment',
                [
                    'attribute'=> 'paymentVip.period',
                    'value' => $model->paymentVip->period,
                ],

                [
                    'attribute'=> 'created_at',
                    'value' => date('d.m.Y', $model->created_at),
                ],
                [
                    'attribute'=> 'updated_at',
                    'value' => date('d.m.Y', $model->updated_at),
                ],
            ],
        ]) ?>
    </div>

    <div class="box-footer">
        <?= Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>

</div>
