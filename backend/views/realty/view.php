<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Просмотр объявления: '.(isset($model->desc->title) ? $model->desc->title : '');

$this->params['breadcrumbs'][] = ['label' => 'Список объявлений', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-info">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
            <?= Html::a('<i class="fa fa-edit"></i>', ['update', 'id' => $model->id], ['class' => 'btn-sm btn-warning']) ?>
            <?= Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn-sm btn-danger']) ?>
        </div>
    </div>

    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'attributes' => [
                'id',
                [
                    'attribute'=> 'vip',
                    'format'=>'html',
                    'value' => \common\enum\Realty::Vip($model->vip, true)
                ],
                [
                    'attribute'=> 'desc.title',
                    'value' => isset($model->desc->title) ? $model->desc->title : '',
                ],
                [
                    'attribute'=> 'status',
                    'value' => !empty($model->status) ? \common\enum\Realty::Status($model->status) : '',
                ],
                [
                    'attribute'=> 'type',
                    'value' => !empty($model->type) ? \common\enum\Realty::Type($model->type) : '',
                ],
                'price',
                'price_update',
                [
                    'attribute'=> 'user_id',
                    'value' => \common\enum\User::Lists($model->user_id),
                ],
                'geo_lat',
                'geo_lng',
                'alias',
                [
                    'attribute'=> 'deadline',
                    'value' => date('d.m.Y', $model->deadline),
                ],
                [
                    'attribute'=> 'parent_id',
                    'format'=>'html',
                    'value' => is_numeric($model->parent_id) ? \common\enum\Realty::MultipleHouse($model->parent_id) : '',
                ],
                [
                    'attribute'=> 'attr.city_id',
                    'value' => isset($model->attr->city_id) ? \common\enum\City::Lists($model->attr->city_id) : '',
                ],
                [
                    'attribute'=> 'attr.district_id',
                    'value' => isset($model->attr->district_id) ? \common\enum\District::Lists($model->attr->district_id) : '' ,
                ],
                'attr.street',
                'attr.house',
                'attr.housing',
                'attr.building',
                'attr.around',
                'attr.liter',
                'attr.gkh_name',
                'attr.developer',
                'attr.housing_estate',
                'attr.rooms',
                'attr.floor',
                'attr.floors',
                'attr.area_all',
                'attr.area_living',
                'attr.area_kitchen',
                'desc.details',
                [
                    'attribute'=> 'created_at',
                    'value' => date('d.m.Y', $model->created_at),
                ],
                [
                    'attribute'=> 'updated_at',
                    'value' => date('d.m.Y', $model->updated_at),
                ],
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>

</div>
