<style>
    .table th{width:150px;}
    #w0-kvdate{width:150px;}
</style>
<?php

$this->title = 'Объявления, всего '.$dataProvider->getTotalCount();

$this->params['breadcrumbs'][] = 'Список объявлений';

?>

<div class="box">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div class="box-tools pull-right">
            <?= \yii\helpers\Html::a('<i class="fa fa-plus"></i>', ['create'], ['class' => 'btn-sm btn-success']) ?>
        </div>
    </div>

    <?php \yii\widgets\Pjax::begin([
        'timeout'=>10000,
        'scrollTo'=>new \yii\web\JsExpression('$(".box").position().top')
    ]) ?>

    <div class="box-body">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'emptyText' => 'Ничего не найдено',
            'summary'=>'',
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'columns' => [
//                [
//                    'class' => 'yii\grid\SerialColumn',
//                    'headerOptions'=>[
//                        'style'=>'width:40px'
//                    ]
//                ],
                'id',
                [
                    'attribute'=>'desc.title',
                    'content'=>function($data) {
                        return isset($data->desc->title) ? $data->desc->title : '';
                    },
                ],
                [
                    'attribute'=>'status',
                    'content'=>function($data) {
                        return !empty($data->status) ? \common\enum\Realty::Status($data->status) : '';
                    },
                    'filter'=>\common\enum\Realty::Status()
                ],
                [
                    'attribute'=>'type',
                    'content'=>function($data) {
                        return !empty($data->type) ? \common\enum\Realty::Type($data->type) : '';
                    },
                    'filter'=>\common\enum\Realty::Type()
                ],
                [
                    'attribute'=>'user_id',
                    'content'=>function($data) {
                        return \common\enum\User::ListsAdmin($data->user_id);
                    },
                    'filter'=>\common\enum\User::ListsAdmin()
                ],
                [
                    'attribute'=>'vip',
                    'content'=>function($data) {
                        return \common\enum\Realty::Vip($data->vip);
                    },
                    'filter'=>\common\enum\Realty::Vip()
                ],
                'price',
                [
                    'attribute'=>'created_at',
                    'content'=>function($data) {
                        return date('d.m.Y', $data->created_at);
                    },
                    'filter'=>\kartik\widgets\DatePicker::widget([
                        'language' => 'en',
                        'name' => 'RealtySearch[created_at]',
                        'value'=> $searchModel->created_at,
                        'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
                        'layout'=>'{input}{remove}',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ])
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete}',
                    'buttons' => [
                        'view' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', $url, [
                                'class' => 'btn-xs btn-info',
                                'title' => 'Просмотр',
                            ]);
                        },
                        'update' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-pencil-square-o"></i>', $url, [
                                'class' => 'btn-xs btn-warning',
                                'title' => 'Обновить',
                            ]);
                        },
                        'delete' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', $url, [
                                'class' => 'btn-xs btn-danger',
                                'title' => 'Удалить',
                                'data-confirm' => 'Удалить ?'
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>

    <?php \yii\widgets\Pjax::end() ?>

</div>


