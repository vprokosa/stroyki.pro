<?php
$this->title = 'Статистика на '.date('d.m.Y H:i:s', time());

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>
    </div>

    <div class="box-body">

    </div>

</div>
