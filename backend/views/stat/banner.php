<?php

$this->title = 'Баннеров, всего '.$dataProvider->getTotalCount();

$this->params['breadcrumbs'][] = 'Список банеров';

?>
<style>
    .table th{width:150px;}
    #w0-kvdate{width:150px;}
</style>

<div class="box">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>

        <div style="margin-top:25px;">
            <?php $form=\yii\widgets\ActiveForm::begin([
                'method'=>'get'
            ]) ?>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12ccol-xs-12">
                    <?= $form->field($searchModel, 'dateStart')->widget(\kartik\widgets\DatePicker::className(), [
                        'language' => 'en',
                        'value'=> $searchModel->created_at,
                        'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
                        'layout'=>'{input}{remove}',
                        'options'=>[
                            'placeholder' => $searchModel->getAttributeLabel('dateStart'),
                        ],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ])->label(false) ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <?= $form->field($searchModel, 'dateEnd')->widget(\kartik\widgets\DatePicker::className(), [
                        'language' => 'en',
                        'value'=> $searchModel->created_at,
                        'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
                        'layout'=>'{input}{remove}',
                        'options'=>[
                            'placeholder' => $searchModel->getAttributeLabel('dateEnd'),
                        ],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ])->label(false) ?>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <?= $form->field($searchModel, 'city_id')->widget(\kartik\widgets\Select2::className(), [
                        'data' => \common\enum\City::Lists(),
                        'options' => [
                            'placeholder' => $searchModel->getAttributeLabel('city_id'),
                            'class'=>'city',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ])->label(false) ?>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                    <?= \yii\helpers\Html::submitButton('Фильтр', ['class'=>'form-control']) ?>
                </div>
            </div>
            <?php $form=\yii\widgets\ActiveForm::end() ?>
        </div>

    </div>

    <?php \yii\widgets\Pjax::begin([
        'timeout'=>10000,
        'scrollTo'=>new \yii\web\JsExpression('$(".box").position().top')
    ]) ?>

    <div class="box-body">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => false,
            'emptyText' => 'Ничего не найдено',
            'summary'=>'',
            'formatter' => [
                'class' => \yii\i18n\Formatter::className(),
                'nullDisplay' => ''
            ],
            'columns' => [
                'id',
                'title',
                [
                    'attribute'=>'city_id',
                    'content'=>function($data) {
                        return is_numeric($data->city_id) ? \common\enum\City::Lists($data->city_id) : '';
                    }
                ],
                'statShow',
                'statClick',
                [
                    'attribute'=>'crt',
                    'content'=> function($data) {
                        return ($data->statClick/$data->statShow)*100;
                    }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($url, $model){
                            return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', ['banners/view', 'id'=>$model->id], [
                                'class' => 'btn-xs btn-info',
                                'title' => 'Просмотр',
                            ]);
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>

    <?php \yii\widgets\Pjax::end() ?>

</div>
