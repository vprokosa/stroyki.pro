<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<script>
    $(document).ready(function() {
        $("#form-banner").on('submit', function () {
            if($('#form-banner').data('yiiActiveForm').validated == false) {
                $('#form-banner').data('yiiActiveForm').validated = true;
            }
        })
    });
</script>

<?php $form = ActiveForm::begin([
    'id'=>'form-banner',
    'options'=>[
        'enctype'=>'multipart/form-data'
    ]
]); ?>

    <div class="box-body">

        <?= $form->field($model, 'status')->dropDownList(\common\enum\Banner::Status(), ['prompt'=>'']) ?>

        <?php if(empty($model->id)): ?>
            <?= $form->field($model, 'type')->dropDownList(\common\enum\Banner::Type(), ['prompt'=>'']) ?>
        <?php endif ?>

        <?= $form->field($model, 'title')->textInput() ?>

        <?= $form->field($model, 'link')->textInput() ?>

        <?= $form->field($model, 'city_id')->widget(\kartik\widgets\Select2::className(), [
            'data' => \common\enum\City::Lists(),
            'options' => [
                'placeholder' => 'Выберите город ...',
                'class'=>'city',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>

        <?= $form->field($model, 'image')->widget(\kartik\widgets\FileInput::className(), [
//            'disabled'=>is_numeric($model->id) ? true : false,
            'options' => [
                'accept' => 'image/*',
            ],
            'pluginOptions' => [
//                'allowedFileExtensions'=>['gif'],
                'allowedPreviewTypes'=>['image'],
                'showUpload' => false,
                'browseLabel' => '',
                'removeLabel' => '',
                'initialPreview'=>[
                    is_numeric($model->id) ? Html::img('@fronturl' . $model->image, ['class'=>'img-responsive','style'=>'width:300px']) : null
                ],
            ],
        ]) ?>

    </div>

    <div class="box-footer">
        <?= yii\helpers\Html::submitButton(!is_numeric($model->id) ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary']) ?>
        <?= yii\helpers\Html::a('Закрыть', ['banners/index'], ['class'=>'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

