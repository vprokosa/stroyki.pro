<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        '/min/g=backend.css',
        '/min/g=backend.plugins.css',
        '/plugins/font-awesome/css/font-awesome.min.css',
    ];
    public $js = [
        '/min/g=backend.js',
        '/min/g=backend.plugins.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        'frontend\assets\BaseAsset',
   ];

    public function registerAssetFiles($view)
    {
        parent::registerAssetFiles($view);
        $view->registerJsFile('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js', ['condition' => 'lte IE9', 'position'=>\yii\web\View::POS_HEAD]);
        $view->registerJsFile('https://oss.maxcdn.com/respond/1.4.2/respond.min.js', ['condition' => 'lte IE9', 'position'=>\yii\web\View::POS_HEAD]);
    }
}
