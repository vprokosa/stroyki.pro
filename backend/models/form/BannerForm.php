<?php

namespace  backend\models\form;

use common\enum\Banner;
use common\enum\Scenario;
use common\models\Banners;
use yii\base\DynamicModel;
use yii\web\UploadedFile;

class BannerForm extends DynamicModel
{
    public $id = null;
    public $status;
    public $type;
    public $title;
    public $link;
    public $city_id;
    public $image;

    public function attributeLabels()
    {
        return array_merge(
            (new Banners())->attributeLabels(),
            []
        );
    }

    public function rules()
    {
        return [
            ['status', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['status', 'integer','on'=>[Scenario::CREATE, Scenario::UPDATE]],

            ['type', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['type', 'integer','on'=>[Scenario::CREATE, Scenario::UPDATE]],

            ['title', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'filter', 'filter' => 'trim','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'string', 'max' => 254, 'tooLong'=>'{attribute} максимум 254 символов','on'=>[Scenario::CREATE, Scenario::UPDATE]],

            ['link', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['link', 'url', 'message'=>'{attribute} формат не верный','on'=>[Scenario::CREATE, Scenario::UPDATE]],

            ['city_id', 'integer','on'=>[Scenario::CREATE, Scenario::UPDATE]],
        ];
    }

    protected function addImageRule()
    {
        if($this->type == Banner::TYPE_SMALL) {
            $this->addRule('image', 'image', [
                'skipOnEmpty' => false,
//                'extensions'=> ['gif'], 'wrongExtension'=>'{attribute} расширения: gif',
                'minWidth'  => 300, 'underWidth'=>'{attribute}, допустимая ширина: 300px',
                'maxWidth'  => 300, 'overWidth'=>'{attribute}, допустимая ширина: 300px',
                'minHeight' => 115, 'underHeight'=>'{attribute}, допустимая высота: 115px',
                'maxHeight' => 115, 'overHeight'=>'{attribute}, допустимая высота: 115px',
                'on'=>[Scenario::CREATE, Scenario::UPDATE]
            ]);
        }
        elseif ($this->type == Banner::TYPE_MIDDLE) {
            $this->addRule('image', 'image', [
                'skipOnEmpty' => false,
//                'extensions'=> ['gif'], 'wrongExtension'=>'{attribute} расширения: gif',
                'minWidth'  => 300, 'underWidth'=>'{attribute}, допустимая ширина: 300px',
                'maxWidth'  => 300, 'overWidth'=>'{attribute}, допустимая ширина: 300px',
                'minHeight' => 250, 'underHeight'=>'{attribute}, допустимая высота: 250px',
                'maxHeight' => 250, 'overHeight'=>'{attribute}, допустимая высота: 250px',
                'on'=>[Scenario::CREATE, Scenario::UPDATE]
            ]);
        }
        elseif ($this->type == Banner::TYPE_BIG) {
            $this->addRule('image', 'image', [
                'skipOnEmpty' => false,
//                'extensions'=> ['gif'], 'wrongExtension'=>'{attribute} расширения: gif',
                'minWidth'  => 630, 'underWidth'=>'{attribute}, допустимая ширина: 630px',
                'maxWidth'  => 630, 'overWidth'=>'{attribute}, допустимая ширина: 630px',
                'minHeight' => 250, 'underHeight'=>'{attribute}, допустимая высота: 250px',
                'maxHeight' => 250, 'overHeight'=>'{attribute}, допустимая высота: 250px',
                'on'=>[Scenario::CREATE, Scenario::UPDATE]
            ]);
        }
    }

    public function create()
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        $this->addImageRule();
        if (!$this->validate()) {
            return null;
        }

        $transaction = \Yii::$app->db->beginTransaction();
        $banner = new Banners();

        $banner->status = $this->status;
        $banner->type = $this->type;
        $banner->title = $this->title;
        $banner->link = $this->link;
        $banner->city_id = $this->city_id;
        $banner->image = $this->image;

        $banner->scenario = $this->scenario;

        if($banner->save()) {
            $transaction->commit();
            return $banner;
        }

        $transaction->rollBack();
        return null;
    }
    
    public function update(Banners $banner)
    {
        $this->image = UploadedFile::getInstance($this, 'image');
        if (!$this->validate()) {
            return null;
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $banner->status = $this->status;
        $banner->type = $this->type;
        $banner->title = $this->title;
        $banner->link = $this->link;
        $banner->city_id = $this->city_id;
        $banner->image = $this->image;

        $banner->scenario = $this->scenario;
        if($banner->save()) {
            $transaction->commit();
            return $banner;
        }

        $transaction->rollBack();
        return null;
    }
}