<?php

namespace  backend\models\form;

use common\enum\Scenario;
use common\models\Articles;
use yii\base\Model;

class ArticleForm extends Model
{
    public $id = null;
    public $status;
    public $type;
    public $city_id;
    public $position;
    public $title;
    public $alias;
    public $text;

    public function attributeLabels()
    {
        return array_merge(
            (new Articles())->attributeLabels(),
            []
        );
    }

    public function rules()
    {
        return [
            ['status', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['status', 'integer','on'=>[Scenario::CREATE, Scenario::UPDATE]],
			
            ['city_id', 'integer','on'=>[Scenario::CREATE, Scenario::UPDATE]],
			 
            ['position', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['position', 'integer','on'=>[Scenario::CREATE, Scenario::UPDATE]],

           ['title', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'filter', 'filter' => 'trim','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'string', 'max'=>254, 'tooLong'=>'{attribute} максимум 254 символов','on'=>[Scenario::CREATE, Scenario::UPDATE]],

            ['text', 'filter', 'filter' => 'trim','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['text', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process','on'=>[Scenario::CREATE, Scenario::UPDATE]],
        ];
    }

    public function create() 
    {
        if (!$this->validate()) {
            return null;
        }

        $articles = new Articles();
        $articles->scenario = $this->scenario;
        $attributes = $this->getAttributes(null, ['id']);
        $articles->setAttributes($attributes, false);
        return $articles->save() ? $articles : null;
    }
    
    public function update(Articles $articles)
    {
        if (!$this->validate()) {
            return null;
        }

        $attributes = $this->getAttributes(null, ['id']);
        $articles->scenario = $this->scenario;
        $articles->setAttributes($attributes, false);

        return $articles->save() ? $articles : null;
    }
}