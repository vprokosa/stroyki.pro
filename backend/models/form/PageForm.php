<?php

namespace  backend\models\form;

use common\enum\Scenario;
use common\models\Pages;
use yii\base\Model;

class PageForm extends Model
{
    public $id = null;
    public $status;
    public $type;
    public $title;
    public $alias;
    public $text;

    public function attributeLabels()
    {
        return array_merge(
            (new Pages())->attributeLabels(),
            []
        );
    }

    public function rules()
    {
        return [
            ['status', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['status', 'integer','on'=>[Scenario::CREATE, Scenario::UPDATE]],

            ['type', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['type', 'integer','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['type', 'unique',
                'targetClass' => Pages::className(),
                'targetAttribute' => 'type',
                'filter' => $this->id !== null ? ['!=', 'id', $this->id] : null,
                'message' => '{attribute} занято'
                ,'on'=>[Scenario::CREATE, Scenario::UPDATE]
            ],

            ['title', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'filter', 'filter' => 'trim','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['title', 'string', 'max'=>254, 'tooLong'=>'{attribute} максимум 254 символов','on'=>[Scenario::CREATE, Scenario::UPDATE]],

            ['alias', 'required', 'message'=>'{attribute} не заполнен','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['alias', 'filter', 'filter' => 'trim','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['alias', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['alias', 'string', 'max' => 254, 'tooLong'=>'{attribute} максимум 254 символов','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['alias', 'unique',
                'targetClass' => Pages::className(),
                'targetAttribute' => 'alias',
                'filter' => $this->id !== null ? ['!=', 'id', $this->id] : null,
                'message' => '{attribute} занято'
                ,'on'=>[Scenario::CREATE, Scenario::UPDATE]
            ],

            ['text', 'filter', 'filter' => 'trim','on'=>[Scenario::CREATE, Scenario::UPDATE]],
            ['text', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process','on'=>[Scenario::CREATE, Scenario::UPDATE]],
        ];
    }

    public function create()
    {
        if (!$this->validate()) {
            return null;
        }

        $pages = new Pages();
        $pages->scenario = $this->scenario;
        $attributes = $this->getAttributes(null, ['id']);
        $pages->setAttributes($attributes, false);

        return $pages->save() ? $pages : null;
    }
    
    public function update(Pages $pages)
    {
        if (!$this->validate()) {
            return null;
        }

        $attributes = $this->getAttributes(null, ['id']);
        $pages->scenario = $this->scenario;
        $pages->setAttributes($attributes, false);

        return $pages->save() ? $pages : null;
    }
}