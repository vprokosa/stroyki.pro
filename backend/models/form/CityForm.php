<?php

namespace  backend\models\form;

use common\enum\Scenario;
use common\models\City;
use common\models\User;
use yii\base\Model;

class CityForm extends Model
{
    public $id = null;
    public $title;
    public $okrug_id;
    public $default = \common\enum\City::DEFAULT_NO;

    public function attributeLabels()
    {
        return array_merge(
            (new City())->attributeLabels(),
            []
        );
    }

    public function scenarios()
    {
        return [
            Scenario::CREATE => ['title','okrug_id','default'],
            Scenario::UPDATE => ['title','okrug_id','default'],
        ];
    }


    public function rules()
    {
        return [
            ['title', 'required', 'message'=>'{attribute} не заполнен'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['title', 'string', 'max' => 255, 'tooLong'=>'{attribute} максимум 255 символов'],
            ['title', 'unique',
                'targetClass' => City::className(),
                'targetAttribute' => 'title',
                'filter' => $this->id !== null ? ['!=', 'id', $this->id] : null,
                'message' => '{attribute} занято'
            ],

            ['okrug_id', 'required', 'message'=>'{attribute} не заполнен'],
            ['okrug_id', 'integer'],

            ['default', 'required', 'message'=>'{attribute} не заполнен'],
            ['default', 'integer'],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $city = new City();
        $attributes = $this->getAttributes(null, ['id']);
        $city->setAttributes($attributes, false);
        if($city->default == \common\enum\City::DEFAULT_YES) {
            $this->cleanDefault();
        }

        return $city->save() ? $city : null;
    }
    
    public function update(City $city)
    {
        if (!$this->validate()) {
            return null;
        }

        $attributes = $this->getAttributes(null, ['id']);
        $city->setAttributes($attributes, false);
        if($city->default == \common\enum\City::DEFAULT_YES) {
            $this->cleanDefault();
        }

        return $city->save() ? $city : null;
    }

    /**
     * Скинуть все дефолты
     */
    public function cleanDefault()
    {
        $cities = City::find()->where(['default'=>\common\enum\City::DEFAULT_YES])->all();
        foreach($cities as $city) {
            $city->default = \common\enum\City::DEFAULT_NO;
            $city->update(false, ['default']);
        }
    }
}