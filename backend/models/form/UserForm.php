<?php

namespace  backend\models\form;

use common\enum\Scenario;
use common\models\User;
use yii\base\Model;

class UserForm extends Model
{
    public $id = null;
    public $full_name;
    public $username;
    public $buildcompany;
    public $type;
    public $email;
    public $password_hash;

    public function attributeLabels()
    {
        return [
            'full_name'=>'Имя',
            'username'=>'Логин',
            'buildcompany'=>'Застройщик',
            'type'=>'Тип',
            'email'=>'Email',
            'password_hash'=>'Пароль',
        ];
    }

    public function scenarios()
    {
        return [
            Scenario::CREATE => ['full_name', 'username','email','buildcompany','type','password_hash'],
            Scenario::UPDATE => ['full_name', 'username','email','buildcompany','type'],
        ];
    }


    public function rules()
    {
        return [
            ['full_name', 'required', 'message'=>'{attribute} не заполнен'],
            ['full_name', 'filter', 'filter' => 'trim'],
            ['full_name', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['full_name', 'string', 'min' => 3, 'max' => 64, 'tooShort'=>'{attribute} минимум 3 символов', 'tooLong'=>'{attribute} максимум 64 символов'],
			
            ['buildcompany', 'filter', 'filter' => 'trim'],
            ['buildcompany', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['buildcompany', 'string', 'min' => 3, 'max' => 64, 'tooShort'=>'{attribute} минимум 3 символов', 'tooLong'=>'{attribute} максимум 64 символов'],

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['username', 'required', 'message'=>'{attribute} не заполнен'],
            ['username', 'unique',
                'targetClass' => User::className(),
                'targetAttribute' => 'username',
                'filter' => $this->id !== null ? ['!=', 'id', $this->id] : null,
                'message' => '{attribute} занят'
            ],
            ['username', 'string', 'min' => 2, 'max' => 36, 'tooShort'=>'{attribute} минимум 3 символов', 'tooLong'=>'{attribute} максимум 36 символов'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['email', 'required', 'message'=>'{attribute} не заполнен'],
            ['email', 'email', 'message'=>'{attribute} не верный'],
            ['email', 'unique',
                'targetClass' => User::className(),
                'targetAttribute' => 'email',
                'filter' => $this->id !== null ? ['!=', 'id', $this->id] : null,
                'message' => '{attribute} занят'
            ],
            ['email', 'string', 'max' => 80, 'tooLong'=>'{attribute} максимум 80 символов'],

            ['type', 'safe'],

            ['password_hash', 'required', 'message'=>'{attribute} не заполнен'],
            ['password_hash', 'string', 'min' => 6, 'tooShort'=>'{attribute} минимум 6 символов'],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $attributes = $this->getAttributes(null, ['id']);
        $user->scenario = Scenario::CREATE;
        $user->setAttributes($attributes, false);

        return $user->save() ? $user : null;
    }
    
    public function update(User $user)
    {
        if (!$this->validate()) {
            return null;
        }
 
        $attributes = $this->getAttributes(null, ['id','password_hash']);
        $user->scenario = Scenario::UPDATE;
        $user->setAttributes($attributes, false);

//        if(!empty($this->passwordHash)) {
//            $user->setPassword($this->passwordHash);
//            $user->generateAuthKey();
//        }

        return $user->save() ? $user : null;
    }
}