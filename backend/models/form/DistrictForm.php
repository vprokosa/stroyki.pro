<?php

namespace  backend\models\form;

use common\enum\Scenario;
use common\models\District;
use yii\base\Model;

class DistrictForm extends Model
{
    public $id = null;
    public $title;
    public $city_id;

    public function attributeLabels()
    {
        return array_merge(
            (new District())->attributeLabels(),
            []
        );
    }

    public function scenarios()
    {
        return [
            Scenario::CREATE => ['title', 'city_id'],
            Scenario::UPDATE => ['title', 'city_id'],
        ];
    }


    public function rules()
    {
        return [
            ['title', 'required', 'message'=>'{attribute} не заполнен'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['title', 'string', 'max' => 255, 'tooLong'=>'{attribute} максимум 255 символов'],
            ['title', 'unique',
                'targetClass' => District::className(),
                'targetAttribute' => 'title',
                'filter' => $this->id !== null ? ['!=', 'id', $this->id] : null,
                'message' => '{attribute} занято'
            ],

            ['city_id', 'integer'],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $district = new District();
        $attributes = $this->getAttributes(null, ['id']);
        $district->setAttributes($attributes, false);

        return $district->save() ? $district : null;
    }
    
    public function update(District $district)
    {
        if (!$this->validate()) {
            return null;
        }

        $attributes = $this->getAttributes(null, ['id']);
        $district->setAttributes($attributes, false);

        return $district->save() ? $district : null;
    }
}