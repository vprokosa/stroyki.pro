<?php

namespace  backend\models\form;

use common\enum\Scenario;
use common\models\Okrug;
use yii\base\Model;

class OkrugForm extends Model
{
    public $id = null;
    public $area_id;
    public $title;

    public function attributeLabels()
    {
        return array_merge(
            (new Okrug())->attributeLabels(),
            []
        );
    }

    public function scenarios()
    {
        return [
            Scenario::CREATE => ['area_id', 'title'],
            Scenario::UPDATE => ['area_id', 'title'],
        ];
    }


    public function rules()
    {
        return [
            ['area_id', 'filter', 'filter' => 'trim'],
            ['area_id', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['area_id', 'integer'],

            ['title', 'required', 'message'=>'{attribute} не заполнен'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['title', 'string', 'max' => 255, 'tooLong'=>'{attribute} максимум 255 символов'],
            ['title', 'unique',
                'targetClass' => Okrug::className(),
                'targetAttribute' => 'title',
                'filter' => $this->id !== null ? ['!=', 'id', $this->id] : null,
                'message' => '{attribute} занято'
            ],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $okrug = new Okrug();
        $attributes = $this->getAttributes(null, ['id']);
        $okrug->setAttributes($attributes, false);

        return $okrug->save() ? $okrug : null;
    }
    
    public function update(Okrug $okrug)
    {
        if (!$this->validate()) {
            return null;
        }

        $attributes = $this->getAttributes(null, ['id']);
        $okrug->setAttributes($attributes, false);

        return $okrug->save() ? $okrug : null;
    }
}