<?php

namespace backend\models;

use common\models\District;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class DistrictSearch extends District
{
    public function rules()
    {
        return [
            ['title', 'string', 'max'=>255],
            ['city_id', 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = District::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', "$this->title%", false]);
        $query->andFilterWhere(['city_id' => $this->city_id]);

        $query->orderBy(['updated_at'=>SORT_DESC]);

        return $dataProvider;
    }
}
