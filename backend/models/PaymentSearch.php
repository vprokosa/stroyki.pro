<?php

namespace backend\models;

use common\models\City;
use common\models\Payment;
use common\models\PaymentTariff;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PaymentSearch extends Payment
{
    public function rules()
    {
        return [
            ['status', 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Payment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}
