<?php

namespace backend\models;

use common\models\Banners;
use common\models\City;
use common\models\StatBanners;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class StatBannerSearch extends Banners
{
    public $dateStart;
    public $dateEnd;
    public $statShow;
    public $statClick;
    public $crt;

    public function rules()
    {
        return [
            ['city_id', 'integer'],
            [['dateStart','dateEnd'], 'date']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title'=>'Название',
            'dateStart'=>'Дата начала',
            'dateEnd'=>'Дата завершения',
            'city_id'=>'Город',
            'statShow'=>'Просмотры',
            'statClick'=>'Клики',
            'crt'=>'CRT',
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = self::find();
        $query->select([
            'banners.id',
            'banners.title',
            'banners.city_id',
            'count(case when stat_banners.action = 1 then stat_banners.action end) AS statShow',
            'count(case when stat_banners.action = 2 then stat_banners.action end) AS statClick',
        ]);
        $query->innerJoinWith('statBanners');
        $query->groupBy('banners.title');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }


        $query->andFilterWhere(['banners.city_id' => $this->city_id]);
        if($this->dateStart != "") {
            $query->andFilterWhere(['>=', 'stat_banners.created_at', strtotime($this->dateStart)]);
        }
        if($this->dateEnd != "") {
            $query->andFilterWhere(['<=', 'stat_banners.created_at', strtotime($this->dateEnd)]);
        }

        return $dataProvider;
    }
}
