<?php

namespace backend\models;

use common\models\City;
use common\models\Payment;
use common\models\PaymentTariff;
use common\models\SubscriptionSearch;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SubscriptionSearchSearch extends SubscriptionSearch
{
    public function rules()
    {
        return [
//            ['status', 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = SubscriptionSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->orderBy(['created_at'=> SORT_DESC]);

//        $query->andFilterWhere(['status' => $this->status]);

        return $dataProvider;
    }
}
