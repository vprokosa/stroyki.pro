<?php

namespace backend\models;

use common\models\City;
use common\models\PaymentTariff;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PaymentTariffSearch extends PaymentTariff
{
    public function rules()
    {
        return [
            ['type', 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = PaymentTariff::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['type' => $this->type]);

        return $dataProvider;
    }
}
