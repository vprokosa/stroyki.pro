<?php

namespace backend\models;

use common\models\Okrug;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class OkrugSearch extends Okrug
{
    public function rules()
    {
        return [
            ['title', 'string', 'max'=>255],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Okrug::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', "$this->title%", false]);

        $query->orderBy(['updated_at'=>SORT_DESC]);

        return $dataProvider;
    }
}
