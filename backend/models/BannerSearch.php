<?php

namespace backend\models;

use common\models\Banners;
use common\models\City;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class BannerSearch extends Banners
{
    public function rules()
    {
        return [
            [['status', 'type', 'city_id'], 'integer'],
            ['title', 'string', 'max'=>255],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Banners::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['type' => $this->type]);
        $query->andFilterWhere(['city_id' => $this->city_id]);
        $query->andFilterWhere(['like', 'title', "$this->title%", false]);
        $query->orderBy(['updated_at'=>SORT_DESC]);

        return $dataProvider;
    }
}
