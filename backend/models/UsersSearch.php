<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;

class UsersSearch extends User
{
    public $role = \common\enum\User::ROLE_USER;

    public function rules()
    {
        return [
            [['status', 'type'], 'integer'],
            ['username', 'string', 'max'=>36],
            ['email', 'string', 'max'=>80],
            ['created_at', 'string'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['role' => $this->role]);
        $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['type' => $this->type]);
        $query->andFilterWhere(['FROM_UNIXTIME(created_at, "%d.%m.%Y")' => $this->created_at]);

        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        $query->orderBy(['created_at'=>SORT_DESC]);

        return $dataProvider;
    }
}
