<?php

namespace backend\models;

use common\models\Articles;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class ArticleSearch extends Articles
{
    public function rules()
    {
        return [
            [['user_id','city_id','position'], 'integer'], 
            ['title', 'string', 'max'=>255],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Articles::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['status' => $this->status]);
        $query->andFilterWhere(['type' => $this->type]);
        $query->andFilterWhere(['user_id' => $this->user_id]);
        $query->andFilterWhere(['like', 'title', "$this->title%", false]);
        $query->orderBy(['updated_at'=>SORT_DESC]);

        return $dataProvider;
    }
}
