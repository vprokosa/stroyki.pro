<?php

namespace backend\models;

use common\models\City;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CitySearch extends City
{
    public function rules()
    {
        return [
            [['okrug_id', 'default'], 'integer'],
            ['title', 'string', 'max'=>255],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = City::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['okrug_id' => $this->okrug_id]);
        $query->andFilterWhere(['default' => $this->default]);
        $query->andFilterWhere(['like', 'title', "$this->title%", false]);
        $query->orderBy(['updated_at'=>SORT_DESC]);

        return $dataProvider;
    }
}
