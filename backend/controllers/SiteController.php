<?php
namespace backend\controllers;

use backend\components\AppController;
use Yii;
use common\models\form\LoginForm;

class SiteController extends AppController
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($action->id=='error') {
                $this->layout ='@frontend/views/layouts/total';
                $this->disableFooter = true;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'load-district' => [
                'class'=>\common\components\actions\LoadDistrictAction::className()
            ]
        ];
    }


    /**
     * Dashboard
     *
     */
    public function actionIndex()
    {
        $this->layout = 'dashboard';
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }


        $this->layout = 'base';
        $this->bodyClass = 'hold-transition login-page';

        $model = new LoginForm();
        $model->loginAdmin = true;
        
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
