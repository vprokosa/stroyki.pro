<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\SubscriptionSearchSearch;
use common\models\SubscriptionSearch;
use Yii;


class SubscriptionSearchController extends AppController
{
    public function actionIndex()
    {
        $searchModel = new SubscriptionSearchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->getModelOr404(SubscriptionSearch::className(), $id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
