<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\form\ArticleForm;
use backend\models\ArticleSearch;
use common\enum\Scenario;
use common\models\Articles;
use common\traits\ModelTrait;
use Yii;


class ArticleController extends AppController
{
    use ModelTrait;

    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $articles = $this->getModelOr404(Articles::className(), $id);
        return $this->render('view', [
            'model' => $articles,
        ]);
    }

    public function actionCreate()
    {
        $form = new ArticleForm();
        $form->scenario = Scenario::CREATE;

        if ($form->load(Yii::$app->request->post()) && $article = $form->create()) {
            Yii::$app->session->setFlash('success', 'Статья создана');
            return $this->redirect(['view', 'id' => $article->id]);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $pages = $this->getModelOr404(Articles::className(), $id);

        $form = new ArticleForm();
        $form->setAttributes($pages->getAttributes(null, ['created_at','updated_at']), false);
        $form->scenario = Scenario::UPDATE;

        if($form->load(\Yii::$app->request->post()) && $page = $form->update($pages)) {
            Yii::$app->session->setFlash('success', 'Статья обновлена');
            return $this->redirect(['view', 'id' => $page->id]);
        }

        return $this->render('update', ['model'=>$form]);
    }

    public function actionDelete($id)
    {
        $article = $this->getModelOr404(Articles::className(), $id);
        $article->delete();
        return $this->redirect('index');
    } 
}
