<?php
namespace backend\controllers;

use backend\components\AppController;
use common\components\actions\doc\DeleteAction;
use common\components\actions\doc\SortAction;
use common\components\actions\doc\UploadAction;
use common\components\actions\doc\AddNameAction;

class RealtyDocController extends AppController
{
    public function actions()
    {
        return [
            'upload'=>[
                'class'=>UploadAction::className(),
            ],
            'delete'=>[
                'class'=>DeleteAction::className(),
            ],
            'sort'=>[
                'class'=>SortAction::className(),
            ],
            'add-name'=>[
                'class'=>AddNameAction::className(),
            ],
        ];
    }
}
