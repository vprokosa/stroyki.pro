<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\form\OkrugForm;
use backend\models\OkrugSearch;
use common\enum\Scenario;
use common\models\Okrug;
use common\traits\ModelTrait;
use Yii;


class OkrugController extends AppController
{
    use ModelTrait;

    public function actionIndex()
    {
        $searchModel = new OkrugSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $okrug = $this->getModelOr404(Okrug::className(), $id);

        return $this->render('view', [
            'model' => $okrug,
        ]);
    }

    public function actionCreate()
    {
        $form = new OkrugForm();
        $form->scenario = Scenario::CREATE;

        if ($form->load(Yii::$app->request->post()) && $district = $form->save()) {
            Yii::$app->session->setFlash('success', 'Округ создан');
            return $this->redirect(['view', 'id' => $district->id]);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $okrug = $this->getModelOr404(Okrug::className(), $id);

        $form = new OkrugForm();
        $form->setAttributes($okrug->getAttributes(null, ['created_at','updated_at']), false);
        $form->scenario = Scenario::UPDATE;

        if($form->load(\Yii::$app->request->post()) && $okrug = $form->update($okrug)) {
            Yii::$app->session->setFlash('success', 'Район обновлен');
            return $this->redirect(['view', 'id' => $okrug->id]);
        }

        return $this->render('update', ['model'=>$form]);
    }

    public function actionDelete($id)
    {
        return $this->redirect('index');
    }
}
