<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\form\UserForm;
use backend\models\UsersSearch;
use common\enum\Scenario;
use common\models\User;
use common\traits\ModelTrait;
use Yii;


class UsersController extends AppController
{
    use ModelTrait;

    public function actionIndex()
    {
       $searchModel = new UsersSearch();
       $dataProvider = $searchModel->search(Yii::$app->request->get());

       return $this->render('index', [
           'searchModel' => $searchModel,
           'dataProvider' => $dataProvider,
       ]);
    }

    public function actionView($id)
    {
        $user = $this->getModelOr404(User::className(), $id);

        return $this->render('view', [
            'model' => $user,
        ]);
    }

    public function actionCreate()
    {
        $form = new UserForm();
        $form->scenario = Scenario::CREATE;

        if ($form->load(Yii::$app->request->post()) && $user = $form->save()) {
            Yii::$app->session->setFlash('success', 'Пользователь создан');
            return $this->redirect(['view', 'id' => $user->id]);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $user = $this->getModelOr404(User::className(), $id);

        $form = new UserForm();
        $form->setAttributes($user->getAttributes(null, ['password_hash']), false);
        $form->scenario = Scenario::UPDATE;

        if($form->load(\Yii::$app->request->post()) && $user = $form->update($user)) {
            Yii::$app->session->setFlash('success', 'Пользователь обновлен');
            return $this->redirect(['view', 'id' => $user->id]);
        }

        return $this->render('update', ['model'=>$form]);
    }

    public function actionDelete($id)
    {
        return $this->redirect('index');
    }

    public function actionChangeStatus($id, $status=\common\enum\User::STATUS_BLOCK)
    {
        $user = $this->getModelOr404(User::className(), $id);
        $user->status = $status;
        if($user->update(false, ['status'])) {
            $textStatus = $status == \common\enum\User::STATUS_ACTIVE ? 'Активирован' : 'Заблокирован';
            $color = $status == \common\enum\User::STATUS_ACTIVE ? 'success' : 'error';
            \Yii::$app->session->setFlash($color, "Пользователь $textStatus");
        };
        return $this->redirect(['view', 'id' => $user->id]);
    }
}
