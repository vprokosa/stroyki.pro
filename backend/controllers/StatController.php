<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\StatBannerSearch;
use Yii;


class StatController extends AppController
{
    public function actionToday()
    {
        return $this->render('today');
    }

    public function actionBanner()
    {
        $searchModel = new StatBannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('banner', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
