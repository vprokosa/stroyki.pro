<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\BannerSearch;
use backend\models\form\BannerForm;
use common\enum\Scenario;
use common\models\Banners;
use common\models\StatBanners;
use common\traits\ModelTrait;
use Yii;


class BannersController extends AppController
{
    use ModelTrait;

    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $banners = $this->getModelOr404(Banners::className(), $id);

        return $this->render('view', [
            'model' => $banners,
        ]);
    }

    public function actionCreate()
    {
        $form = new BannerForm();
        $form->scenario = Scenario::CREATE;

        if ($form->load(Yii::$app->request->post()) && $banner = $form->create()) {
            Yii::$app->session->setFlash('success', 'Баннер создан');
            return $this->redirect(['view', 'id' => $banner->id]);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $banner = $this->getModelOr404(Banners::className(), $id);

        $form = new BannerForm();

        $form->id = $banner->id;
        $form->status = $banner->status;
        $form->type = $banner->type;
        $form->title = $banner->title;
        $form->link = $banner->link;
        $form->city_id = $banner->city_id;
        $form->image = $banner->getUploadUrl('image');

        $form->scenario = Scenario::UPDATE;

        if($form->load(\Yii::$app->request->post()) && $banner = $form->update($banner)) {
            Yii::$app->session->setFlash('success', 'Баннер обновлен');
            return $this->redirect(['view', 'id' => $banner->id]);
        }

        return $this->render('update', ['model'=>$form]);
    }

    public function actionDelete($id)
    {
        $banner = $this->getModelOr404(Banners::className(), $id);
		StatBanners::deleteAll(['banner_id'=>$banner->id]);
        $banner->delete();
        return $this->redirect('index');
    } 
}
