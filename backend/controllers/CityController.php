<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\CitySearch;
use backend\models\form\CityForm;
use common\enum\Scenario;
use common\models\City;
use common\traits\ModelTrait;
use Yii;


class CityController extends AppController
{
    use ModelTrait;

    public function actionIndex()
    {
        $searchModel = new CitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $city = $this->getModelOr404(City::className(), $id);

        return $this->render('view', [
            'model' => $city,
        ]);
    }

    public function actionCreate()
    {
        $form = new CityForm();
        $form->scenario = Scenario::CREATE;

        if ($form->load(Yii::$app->request->post()) && $city = $form->save()) {
            Yii::$app->session->setFlash('success', 'Город создан');
            return $this->redirect(['view', 'id' => $city->id]);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $city = $this->getModelOr404(City::className(), $id);

        $form = new CityForm();
        $form->setAttributes($city->getAttributes(null, ['created_at','updated_at']), false);
        $form->scenario = Scenario::UPDATE;

        if($form->load(\Yii::$app->request->post()) && $city = $form->update($city)) {
            Yii::$app->session->setFlash('success', 'Город обновлен');
            return $this->redirect(['view', 'id' => $city->id]);
        }

        return $this->render('update', ['model'=>$form]);
    }

    public function actionDelete($id)
    {
        return $this->redirect('index');
    }

}
