<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\PaymentTariffSearch;
use common\models\PaymentTariff;
use Yii;


class TariffController extends AppController
{
    public function actionIndex()
    {
        $searchModel = new PaymentTariffSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->getModelOr404(PaymentTariff::className(), $id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionCreate()
    {
        $model = new PaymentTariff();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Тариф создан');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->getModelOr404(PaymentTariff::className(), $id);

        if($model->load(\Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Тариф обновлен');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', ['model'=>$model]);
    }

    public function actionDelete($id)
    {
        return $this->redirect('index');
    }

}
