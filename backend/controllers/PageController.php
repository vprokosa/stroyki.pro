<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\form\PageForm;
use backend\models\PageSearch;
use common\enum\Scenario;
use common\models\Pages;
use common\traits\ModelTrait;
use Yii;


class PageController extends AppController
{
    use ModelTrait;

    public function actionIndex()
    {
        $searchModel = new PageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSocial()
    {

		if (Yii::$app->request->get('do') == 'save') {

			Yii::$app->db->createCommand("
				update social
				set vk = '".Yii::$app->request->get('vk')."',
					ok = '".Yii::$app->request->get('ok')."',
					fb = '".Yii::$app->request->get('fb')."',
					tw = '".Yii::$app->request->get('tw')."',
					ig = '".Yii::$app->request->get('ig')."'
			")->execute(); 
			
			return $this->redirect('/page/social');
		}
	
        return $this->render('social', []);
	
    }	
	
    public function actionView($id)
    {
        $pages = $this->getModelOr404(Pages::className(), $id);

        return $this->render('view', [
            'model' => $pages,
        ]);
    }

    public function actionCreate()
    {
        $form = new PageForm();
        $form->scenario = Scenario::CREATE;

        if ($form->load(Yii::$app->request->post()) && $banner = $form->create()) {
            Yii::$app->session->setFlash('success', 'Страница создана');
            return $this->redirect(['view', 'id' => $banner->id]);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $pages = $this->getModelOr404(Pages::className(), $id);

        $form = new PageForm();
        $form->setAttributes($pages->getAttributes(null, ['created_at','updated_at']), false);
        $form->scenario = Scenario::UPDATE;

        if($form->load(\Yii::$app->request->post()) && $page = $form->update($pages)) {
            Yii::$app->session->setFlash('success', 'Страница обновлена');
            return $this->redirect(['view', 'id' => $page->id]);
        }

        return $this->render('update', ['model'=>$form]);
    }

    public function actionDelete($id)
    {
        return $this->redirect('index');
    }
}
