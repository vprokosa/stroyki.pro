<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\PaymentSearch;
use common\models\Payment;
use Yii;


class PaymentController extends AppController
{
    public function actionIndex()
    {
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->getModelOr404(Payment::className(), $id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionStatusChange($status=0, $id=0)
    {
        return $this->redirect('index');
    }
}
