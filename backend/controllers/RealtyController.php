<?php
namespace backend\controllers;

use backend\components\AppController;
use common\components\actions\realty\CreateAction;
use common\components\actions\realty\DeleteAction;
use common\components\actions\realty\PaymentAction;
use common\components\actions\realty\UpdateAction;
use common\components\actions\realty\ValidateAction;
use common\models\Realty;
use common\models\search\RealtySearch;
use common\traits\ModelTrait;
use Yii;

class RealtyController extends AppController
{
    use ModelTrait;

    public function actions()
    {
        return [
            'create'=>[
                'class'=>CreateAction::className(),
                'view' => $this,
            ],
            'update'=>[
                'class'=>UpdateAction::className(),
                'view' => $this,
            ],
            'validate'=>[
                'class'=>ValidateAction::className(),
            ],
            'payment'=>[
                'class'=>PaymentAction::className(),
                'view' => $this,
            ],
            'delete'=>[
                'class'=>DeleteAction::className(),
                'view' => $this,
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new RealtySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $realty = $this->getModelOr404(Realty::className(), $id);

        return $this->render('view', [
            'model' => $realty,
        ]);
    }
}
