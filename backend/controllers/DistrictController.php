<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\DistrictSearch;
use backend\models\form\DistrictForm;
use common\enum\Scenario;
use common\models\District;
use common\traits\ModelTrait;
use Yii;


class DistrictController extends AppController
{
    use ModelTrait;

    public function actionIndex()
    {
        $searchModel = new DistrictSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $district = $this->getModelOr404(District::className(), $id);

        return $this->render('view', [
            'model' => $district,
        ]);
    }

    public function actionCreate()
    {
        $form = new DistrictForm();
        $form->scenario = Scenario::CREATE;

        if ($form->load(Yii::$app->request->post()) && $district = $form->save()) {
            Yii::$app->session->setFlash('success', 'Район создан');
            return $this->redirect(['view', 'id' => $district->id]);
        }

        return $this->render('create', [
            'model' => $form,
        ]);
    }

    public function actionUpdate($id)
    {
        $district = $this->getModelOr404(District::className(), $id);

        $form = new DistrictForm();
        $form->setAttributes($district->getAttributes(null, ['created_at','updated_at']), false);
        $form->scenario = Scenario::UPDATE;

        if($form->load(\Yii::$app->request->post()) && $district = $form->update($district)) {
            Yii::$app->session->setFlash('success', 'Район обновлен');
            return $this->redirect(['view', 'id' => $district->id]);
        }

        return $this->render('update', ['model'=>$form]);
    }

    public function actionDelete($id)
    {
        return $this->redirect('index');
    }
}
