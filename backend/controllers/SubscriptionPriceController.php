<?php
namespace backend\controllers;

use backend\components\AppController;
use backend\models\SubscriptionPriceSearch;
use common\models\SubscriptionPrice;
use Yii;


class SubscriptionPriceController extends AppController
{
    public function actionIndex()
    {
        $searchModel = new SubscriptionPriceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->getModelOr404(SubscriptionPrice::className(), $id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
