<?php

namespace common\enum;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Realty
{
    const STATUS_DRAFT = 1; //черновик
    const STATUS_PUBLIC = 2; //опубликовано
    const STATUS_ARCHIVE = 3;//архив

    const TYPE_FLAT = 1;//квартира
    const TYPE_HOUSE = 2;//дом
    const TYPE_COTTAGE = 3;//дача
    const TYPE_APARTMENT_HOUSE= 4;//многоквартирный дом
    
    const VIP_NO = 0;
    const VIP_YES = 1;
    
    const SUBSCRIPTION_PRICE_NO = 0;
    const SUBSCRIPTION_PRICE_YES = 1;

    public static function Status($id=0)
    {
        $data = [
            self::STATUS_DRAFT=>'Черновик',
            self::STATUS_PUBLIC=>'Опубликовано',
            self::STATUS_ARCHIVE=>'Aрхив',
        ];

        if(ArrayHelper::keyExists($id, $data)) {
            return $data[$id];
        } elseif ($id>0) {
            return '';
        }
        
        return $data;
    }
    
    public static function Type($id=0)
    {
        $data = [
            self::TYPE_FLAT=>'Квартира',
            self::TYPE_HOUSE=>'Дом',
            self::TYPE_COTTAGE=>'Дача',
            self::TYPE_APARTMENT_HOUSE=>'Многоквартирный дом',
        ];

        if(ArrayHelper::keyExists($id, $data)) {
            return $data[$id];
        } elseif ($id>0) {
            return '';
        }
        
        return $data;
    }
    
    public static function Vip($id=null, $style=false)
    {
        $data = [
            self::VIP_NO=>'Нет',
            self::VIP_YES=>'Да',
        ];
        $dataStyle = [
            self::VIP_NO=>'<h5><span class="label label-default">Нет</span></h5>',
            self::VIP_YES=>'<h5><span class="label label-success">Да</span></h5>',
        ];

        if(ArrayHelper::keyExists($id, $data)) {
            if($style) {
                return $dataStyle[$id];
            }
            return $data[$id];
        } elseif ($id>0) {
            return '';
        }
        
        return $data;
    }

    public static function MultipleHouse($id=null)
    {
        $data = [];
        if(is_numeric($id)) {
            $realty = Yii::$app->db->createCommand('
            SELECT realty.id, realty_desc.title FROM realty 
            LEFT JOIN realty_desc  ON realty.id=realty_desc.realty_id
            WHERE realty.id=:id AND realty.type=:type')
                ->bindValues([
                    ':id'=>$id,
                    ':type'=>self::TYPE_APARTMENT_HOUSE,
                ])
                ->queryAll();
        } else { //все многоэтажки по пользователю
            $realty = Yii::$app->db->createCommand('
            SELECT realty.id, realty_desc.title FROM realty 
            LEFT JOIN realty_desc  ON realty.id=realty_desc.realty_id
            WHERE realty.user_id=:user_id AND realty.type=:type')
                ->bindValues([
                    ':user_id'=>\Yii::$app->user->id,
                    ':type'=>self::TYPE_APARTMENT_HOUSE,
                ])
                ->queryAll();
        }

        $data = [];
        foreach ($realty as $arr) {
            $data[$arr['id']] = $arr['title'];
        }
        if(ArrayHelper::keyExists($id, $data)) {
            return $data[$id];
        } elseif ($id>0) {
            return '';
        }

        return $data;
    }
}