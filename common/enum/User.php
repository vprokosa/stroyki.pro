<?php

namespace common\enum;


use yii\helpers\ArrayHelper;

class User
{
    //Статусы
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCK = 2;
    const STATUS_DELETED = 3;

    //Типы
    const TYPE_OWNER = 1;
    const TYPE_CONTRACTOR = 2;
    const TYPE_CREATOR = 3;
    const TYPE_AGENCY = 4;

    //Роль
    const ROLE_USER = 1;
    const ROLE_ADMIN = 2;
    
    //Подписка
    const SENDING_NO = 0;
    const SENDING_YES = 1;

    public static function Status($number=0)
    {
        $data = [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_BLOCK => 'Заблокированный',
            self::STATUS_DELETED => 'Удаленный'
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }

    public static function Type($number=0)
    {
        $data = [
            self::TYPE_OWNER => 'собственник',
            self::TYPE_CONTRACTOR => 'подрядчик',
            self::TYPE_CREATOR => 'застройщик',
            self::TYPE_AGENCY => 'агенство'
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }

    public static function Role($number=0)
    {
        $data = [
            self::ROLE_USER => 'Пользователь',
            self::ROLE_ADMIN => 'Админ'
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }

    public static function Subscription($number)
    {
        $data = [
            self::SENDING_NO => 'Не подписан',
            self::SENDING_YES => 'Подписан'
        ];
        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }

    /**
     * Список пользователей
     */
    public static function Lists($id=0)
    {
        $cache_key = \Yii::$app->params['cache']['user_list']['key'];
        $cache_time = \Yii::$app->params['cache']['user_list']['time'];

        $data = [];
        if(\Yii::$app->cache->exists($cache_key)) {
            $data = \Yii::$app->cache->get($cache_key);
        } else {
            $arr = \Yii::$app->db->createCommand(
                'SELECT id, full_name FROM '.\common\models\User::tableName().' WHERE role='.User::ROLE_USER)->queryAll();
            foreach($arr as $item) {
                $data[$item['id']] = $item['full_name'];
            }
            \Yii::$app->cache->set($cache_key, $data, $cache_time);
        }

        if(ArrayHelper::keyExists($id, $data)) {
            return $data[$id];
        } elseif ($id>0) {
            return '';
        }

        return $data;
    }
	
    /**
     * Список пользователей для админки
     */
    public static function ListsAdmin($id=0)
    {
        $cache_key = \Yii::$app->params['cache']['user_list']['key'];
        $cache_time = \Yii::$app->params['cache']['user_list']['time'];

        $data = [];
        if(\Yii::$app->cache->exists($cache_key)) {
            $data = \Yii::$app->cache->get($cache_key);
        } else {
            //$arr = \Yii::$app->db->createCommand(
            //    'SELECT id, full_name FROM '.\common\models\User::tableName().' WHERE role='.User::ROLE_USER)->queryAll();
            $arr = \Yii::$app->db->createCommand(
                'SELECT u.id, u.full_name FROM '.\common\models\User::tableName().' u left join '.\common\models\Realty::tableName().' as r ON r.user_id = u.id WHERE r.id >0  AND u.role='.User::ROLE_USER.' group by u.id' )->queryAll();
            foreach($arr as $item) {
                $data[$item['id']] = $item['full_name'];
            }
            \Yii::$app->cache->set($cache_key, $data, $cache_time);
        }

        if(ArrayHelper::keyExists($id, $data)) {
            return $data[$id];
        } elseif ($id>0) {
            return '';
        }

        return $data;
    }
}