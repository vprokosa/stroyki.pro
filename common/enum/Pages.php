<?php

namespace common\enum;


use yii\helpers\ArrayHelper;

class Pages
{
    //Статусы
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCK = 2;

    //Типы
    const TYPE_CONTACT = 1;
    const TYPE_ABOUT = 2;
    const TYPE_RULE = 3;
    const TYPE_SUPPORT = 4;

    public static function Status($number=0)
    {
        $data = [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_BLOCK => 'Заблокированный',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }

    public static function Type($number=0)
    {
        $data = [
            self::TYPE_CONTACT => 'Контакты',
            self::TYPE_ABOUT => 'О нас',
            self::TYPE_RULE => 'Правила',
            self::TYPE_SUPPORT => 'Поддержка',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }
}