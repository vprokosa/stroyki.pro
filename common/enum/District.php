<?php

namespace common\enum;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class District
{    
    /**
     * Список Районов
     */
    public static function Lists($id=0, $city_id=0)
    {
        $data = [];

        $query = (new Query())->select(['id','title'])->from(\common\models\District::tableName());

        if($city_id > 0 || !empty($_COOKIE['cityID'])) {
            $query->where(['city_id' => $city_id > 0 ? $city_id : $_COOKIE['cityID']]);
        }
        $districts = $query->all();
        foreach($districts as $district) {
            $data[$district['id']] = $district['title'];
        }

        if(ArrayHelper::keyExists($id, $data)) {
            return $data[$id];
        } elseif ($id>0) {
            return '';
        }

        return $data;
    }
}