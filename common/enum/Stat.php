<?php

namespace common\enum;


use yii\helpers\ArrayHelper;

class Stat
{
    const ACTION_SHOW = 1;
    const ACTION_CLICK = 2;

    public static function Action($number=0)
    {
        $data = [
            self::ACTION_SHOW => 'Показы',
            self::ACTION_CLICK => 'Клики',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }
}