<?php

namespace common\enum;


use yii\helpers\ArrayHelper;

class Banner
{
    //Статусы
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCK = 2;

    //Типы
    const TYPE_SMALL = 1;
    const TYPE_MIDDLE = 2;
    const TYPE_BIG = 3;
    const TYPE_BIGGEST = 4;

    public static function Status($number=0)
    {
        $data = [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_BLOCK => 'Заблокированный',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }

    public static function Type($number=0)
    {
        $data = [
            self::TYPE_SMALL => 'Маленький',
            self::TYPE_MIDDLE => 'Средний',
            self::TYPE_BIG => 'Большой',
            self::TYPE_BIGGEST => 'Самый большой',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }

}