<?php

namespace common\enum;


use yii\helpers\ArrayHelper;

class PaymentTariff
{
    const TYPE_VIP = 1;

    public static function Type($number=0)
    {
        $data = [
            self::TYPE_VIP => 'Vip',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }
}