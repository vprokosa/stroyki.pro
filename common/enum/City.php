<?php

namespace common\enum;

use Yii;
use yii\helpers\ArrayHelper;

class City
{
    const DEFAULT_NO = 0;
    const DEFAULT_YES = 1;
    
    /**
     * Список городов
     */
    public static function Lists($id=0)
    {
       // $id = is_int($id) ? $id : 1;

        $cache_key = Yii::$app->params['cache']['city_list']['key'];
        $cache_time = Yii::$app->params['cache']['city_list']['time'];
        
        $data = [];
        if(Yii::$app->cache->exists($cache_key)) {
            $data = Yii::$app->cache->get($cache_key);
        } else {
            $arr = Yii::$app->db->createCommand('SELECT id, title FROM '.\common\models\City::tableName())->queryAll();
            foreach($arr as $item) {
                $data[$item['id']] = $item['title'];
            }
            Yii::$app->cache->set($cache_key, $data, $cache_time);
        }

        if(ArrayHelper::keyExists($id, $data)) {
            return $data[$id];
        } elseif ($id > 0) {
            return '';
        }

        return $data;
    }

    public static function getAll()
    {
        $arr = Yii::$app->db->createCommand('SELECT id, title FROM '.\common\models\City::tableName())->queryAll();
        foreach($arr as $item) {
            $data[$item['id']] = $item['title'];
        }
        return $data;
    }

    public static function Defaults($number=null)
    {
        $data = [
            self::DEFAULT_NO=>'Нет',
            self::DEFAULT_YES=>'Да',
        ];
        
        if(is_numeric($number)) {
            return $data[$number];
        }
        
        return $data;
    }
}