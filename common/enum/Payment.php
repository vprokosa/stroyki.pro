<?php

namespace common\enum;


use yii\helpers\ArrayHelper;

class Payment
{
    const STATUS_SUCCESS = 1;
    const STATUS_PROTECTION = 2;
    const STATUS_CANCEL = 3;
    const STATUS_ERROR = 4;

    const SYSTEM_YANDEX_MONEY = 1;

    public static function Status($number=0)
    {
        $data = [
            self::STATUS_SUCCESS => 'Оплачено',
            self::STATUS_PROTECTION => 'Зачислено с протекцией',
            self::STATUS_CANCEL => 'Отменен',
            self::STATUS_ERROR => 'Ошибка',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }

    public static function System($number=0)
    {
        $data = [
            self::SYSTEM_YANDEX_MONEY => 'Яндекс деньги',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }
}