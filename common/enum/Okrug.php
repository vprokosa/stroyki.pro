<?php

namespace common\enum;

use Yii;
use yii\helpers\ArrayHelper;

class Okrug
{    
    /**
     * Список округов
     */
    public static function Lists($id=0)
    {
        $cache_key = Yii::$app->params['cache']['okrug_list']['key'];
        $cache_time = Yii::$app->params['cache']['okrug_list']['time'];
        
        $data = [];
        if(Yii::$app->cache->exists($cache_key)) {
            $data = Yii::$app->cache->get($cache_key);
        } else {
            $arr = Yii::$app->db->createCommand('SELECT id, title FROM '.\common\models\Okrug::tableName())->queryAll();
            foreach($arr as $item) {
                $data[$item['id']] = $item['title'];
            }
            Yii::$app->cache->set($cache_key, $data, $cache_time);
        }

        if(ArrayHelper::keyExists($id, $data)) {
            return $data[$id];
        } elseif ($id>0) {
            return '';
        }

        return $data;
    }
}