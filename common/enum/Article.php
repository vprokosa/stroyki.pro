<?php

namespace common\enum;


use yii\helpers\ArrayHelper;

class Article
{
    //Статусы
    const STATUS_ACTIVE = 1;
    const STATUS_BLOCK = 2;

    //Позиции статей
    const POSITION_ABOVEFOOTER = 1;
    const POSITION_UNDERTOP = 2;
    const POSITION_MAINPAGE = 3;


    public static function Status($number=0)
    {
        $data = [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_BLOCK => 'Заблокированный',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }
	
    public static function Position($number=0)
    {
        $data = [
            self::POSITION_ABOVEFOOTER => 'Над футером в списке',
            self::POSITION_MAINPAGE => 'Главная страница',
        ];

        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }


}