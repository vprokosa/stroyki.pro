<?php
namespace  common\enum;

class Scenario
{
    const CREATE = 'create';
    const UPDATE = 'update';
}