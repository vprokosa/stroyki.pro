<?php

namespace common\enum;

use Yii;
use yii\helpers\ArrayHelper;

class ObjectSearch
{
    const OPTION_ALL_REALTY = 1; //Все объявления
    const OPTION_VIP = 2; //Vip объявления
    const OPTION_BUILDER = 3; //Застройщики

    public static function Options($number=0)
    {
        $data = [
            self::OPTION_ALL_REALTY=>'Все',
            self::OPTION_VIP=>'Vip объявления',
            self::OPTION_BUILDER=>'Застройщики',
        ];
        if(ArrayHelper::keyExists($number, $data)) {
            return $data[$number];
        }
        return $data;
    }
    
}