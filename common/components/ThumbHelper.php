<?php

namespace common\components;


use yii\helpers\FileHelper;
use yii\helpers\Html;

class ThumbHelper extends \himiklab\thumbnail\EasyThumbnailImage
{
    public static function thumbnailImg($filename, $width, $height, $mode = self::THUMBNAIL_OUTBOUND, $options = [], $tag=true)
    {
        $filename = FileHelper::normalizePath(\Yii::getAlias($filename));
        try {
            $thumbnailFileUrl = self::thumbnailFileUrl($filename, $width, $height, $mode);
        } catch (\Exception $e) {
            return static::errorHandler($e, $filename);
        }

        if($tag) {
            return Html::img(
                $thumbnailFileUrl,
                $options
            );
        }
        return $thumbnailFileUrl;
    }

    protected static function errorHandler($error, $filename)
    {
        return 'notFind';
    }
}