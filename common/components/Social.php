<?php

namespace common\components;

use Yii;

class Social extends \yii\db\ActiveRecord
{
	
	private static $cache;
	
    public static function get($type)
    {

		if (!isset(self::$cache)) {
		
			$data = Yii::$app->db->createCommand('
				select 
				* 
				from social
			')->queryAll();
		
			self::$cache = $data[0];
		
			return static::$cache[$type];
		
		} else {
			
			return self::$cache[$type];
				
		}
	
    }

}