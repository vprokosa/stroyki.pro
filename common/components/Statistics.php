<?php

namespace common\components;

use common\enum\Stat;
use yii\base\Component;
use yii\db\Query;

/**
 * Статистика
 */
class Statistics extends Component
{
    /**
     * Регистрация показов
     */
    public function regShow($type, $id)
    {
        if(\Yii::$app->params['reg-stat']) {
            if($type=='banner') {
                $query = \Yii::$app->db->createCommand()->insert('stat_banners',[
                    'action'=>Stat::ACTION_SHOW,
                    'banner_id'=>$id,
                    'created_at'=>time()
                ])->execute();
            } elseif ($type=='realty') {
                $query = \Yii::$app->db->createCommand()->insert('stat_realty',[
                    'action'=>Stat::ACTION_SHOW,
                    'realty_id'=>$id,
                    'created_at'=>time()
                ])->execute();
            }
        }
    }
    /**
     * Регистрация кликов
     */
    public function regClick($type, $id)
    {
        if(\Yii::$app->params['reg-stat']) {
            if($type=='banner') {
                $query = \Yii::$app->db->createCommand()->insert('stat_banners',[
                    'action'=>Stat::ACTION_CLICK,
                    'banner_id'=>$id,
                    'created_at'=>time()
                ])->execute();
            } elseif ($type=='realty') {
                $query = \Yii::$app->db->createCommand()->insert('stat_realty',[
                    'action'=>Stat::ACTION_CLICK,
                    'realty_id'=>$id,
                    'created_at'=>time()
                ])->execute();
            }
        }
    }
    public function showRealty($id, $today=false)
    {
        $query = (new Query())
            ->select('count(id) as count')
            ->from('stat_realty')
            ->where([
                'realty_id'=>$id,
                'action'=>Stat::ACTION_SHOW,
            ])
            ->groupBy('action');
        
        if($today) {
            $query->andFilterWhere(['FROM_UNIXTIME(created_at, "%d.%m.%Y")' => date('d.m.Y',time())]);
        }
        
        $object = $query->one();
        if(isset($object['count']) && $object['count'] > 0) {
            return $object['count'];
        }
        return '';
    }
}