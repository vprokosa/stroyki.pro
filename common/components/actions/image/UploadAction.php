<?php

namespace common\components\actions\image;


use common\models\form\RealtyForm;
use common\models\RealtyImage;
use yii\base\Action;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\imagine\Image;
use yii\web\UploadedFile;

class UploadAction extends Action
{
    public function run($realtyID='')
    {
        if(!is_numeric($realtyID)) {
            $realtyID = RealtyForm::CreateEmptyRealtyDraft();
        }

        $dir = \Yii::getAlias('@common').'/upload/realty/picture/'.$realtyID;

        if(!file_exists($dir)) {
            FileHelper::createDirectory($dir);
        }
        if (isset($_FILES['file'])) {
            $file = UploadedFile::getInstanceByName('file');
            $name = \Yii::$app->security->generateRandomString().'.'.$file->extension;
            //
            $image = new RealtyImage();
            $image->realty_id = $realtyID;
            $image->image = $name;
            //
            if(($file->saveAs("$dir/$name")) && ($image->save())) {
		//print_r("$dir/$name");
		//die(); 
                return  Json::encode([
                    'realtyID'=>$realtyID,
                    'imageID'=>$image->id
                ]);
            }
        }
        return false;
    }
}