<?php

namespace common\components\actions\image;


use common\models\RealtyImage;
use yii\base\Action;

class DeleteAction extends Action
{
    public function run($imageID=0)
    {
        $image = RealtyImage::findOne($imageID);
        if($image) {
            $name = $image->image;
            $dir = \Yii::getAlias('@common').'/upload/realty/picture/'.$image->realty_id;
            if(file_exists("$dir/$name")) {
                $image->delete();
                unlink("$dir/$name");
            }
        }
        return 1;
    }
}