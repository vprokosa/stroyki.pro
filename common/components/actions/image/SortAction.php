<?php

namespace common\components\actions\image;

use common\models\RealtyImage;
use common\traits\ModelTrait;
use yii\base\Action;
use yii\helpers\Json;

class SortAction extends Action
{
    use ModelTrait;
    
    public function run($data)
    {
        $data = Json::decode($data);
        foreach($data as $index=>$id) {
            $image = $this->getModelOr404(RealtyImage::className(), $id);
            $image->sort = $index;
            $image->update(false, ['sort']);
        }
        return 1;
    }
}