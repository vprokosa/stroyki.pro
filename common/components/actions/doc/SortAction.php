<?php

namespace common\components\actions\doc;


use common\models\RealtyDoc;
use common\traits\ModelTrait;
use yii\base\Action;
use yii\helpers\Json;

class SortAction extends Action
{
    use ModelTrait;

    public function run($data)
    {
        $data = Json::decode($data);
        foreach($data as $index=>$id) {
            $doc = $this->getModelOr404(RealtyDoc::className(), $id);
            $doc->sort = $index;
            $doc->update(false, ['sort']);
        }
        return 1;
    }
}