<?php

namespace common\components\actions\doc;

use common\models\RealtyDoc;
use common\traits\ModelTrait;
use yii\base\Action;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\AssetManager;

class AddNameAction extends Action
{
    use ModelTrait;

    public function run($id=0, $name='')
    {
        $doc = $this->getModelOr404(RealtyDoc::className(), $id);
        $dir = \Yii::getAlias('@common/upload/realty/doc/'.$doc->realty_id);
        $old_file = FileHelper::normalizePath( $dir.'/'.$doc->file );

        $newname = $name;
        if(strpos(php_uname(), 'Windows') >= 0) {
            $newname = iconv( "utf-8", "windows-1251", $name);
        }
        $new_name = $newname.'.'.pathinfo($doc->file, PATHINFO_EXTENSION);
        $new_file = FileHelper::normalizePath( $dir.'/'.$new_name);

        if(file_exists($old_file)) {
            rename($old_file, $new_file);
            $doc->name = $name;
            $doc->file = $name.'.'.pathinfo($doc->file, PATHINFO_EXTENSION);
            $doc->update(true, ['name','file']);
        }
        return 1;
    }
}