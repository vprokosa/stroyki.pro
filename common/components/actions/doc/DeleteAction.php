<?php

namespace common\components\actions\doc;


use common\models\RealtyDoc;
use yii\base\Action;

class DeleteAction extends Action
{
    public function run($docID=0)
    {
        $doc = RealtyDoc::findOne($docID);
        if($doc) {
            $dir = \Yii::getAlias('@common').'/upload/realty/doc/'.$doc->realty_id;

            $new_name = $doc->file;
            if(strpos(php_uname(), 'Windows') >= 0) {
                $new_name = iconv( "utf-8", "windows-1251", $new_name);
            }
            if(file_exists("$dir/{$new_name}")) {
                $doc->delete();
                unlink("$dir/{$new_name}");
            }
        }
        return 1;
    }
}