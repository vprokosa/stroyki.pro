<?php

namespace common\components\actions\doc;


use common\models\form\RealtyForm;
use common\models\RealtyDoc;
use yii\base\Action;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\UploadedFile;

class UploadAction extends Action
{
    public function run($realtyID='')
    {
        if(!is_numeric($realtyID)) {
            $realtyID = RealtyForm::CreateEmptyRealtyDraft();
        }

        $dir = \Yii::getAlias('@common').'/upload/realty/doc/'.$realtyID;
        if(!file_exists($dir)) {
            FileHelper::createDirectory($dir);
        }

        if (isset($_FILES['file'])) {
            $file = UploadedFile::getInstanceByName('file');
            $name = \Yii::$app->security->generateRandomString().'.'.$file->extension;
            //
            $doc = new RealtyDoc();
            $doc->realty_id = $realtyID;
            $doc->file = $name;
            $doc->name = explode('.',$file->name)[0];
            //
            if($doc->save() && $file->saveAs("$dir/{$name}")) {
                return  Json::encode([
                    'realtyID'=>$realtyID,
                    'docID'=>$doc->id,
                    'docIcon'=>RealtyForm::ImgForExtension($doc->file)
                ]);
            }
        }
        return false;
    }
}