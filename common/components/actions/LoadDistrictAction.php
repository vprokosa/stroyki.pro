<?php

namespace common\components\actions;


use common\models\District;
use yii\base\Action;
use yii\db\Query;
use yii\helpers\Json;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Загрузить через город районы
 */
class LoadDistrictAction extends Action
{
    public function run()
    {
        if(Yii::$app->request->isAjax && isset($_POST['cityID'])) {
            $query = (new Query())->select(['id','title as text'])->from(District::tableName());
            if(is_numeric($_POST['cityID'])) {
                $query->where(['city_id'=>$_POST['cityID']]);
            }
            $data = $query->all();
            array_unshift($data, ['id'=>'','text'=>'']);

            return Json::encode($data);
        }
        throw  new NotFoundHttpException(Yii::$app->params['error404']);
    }
}