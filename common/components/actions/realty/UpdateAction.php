<?php

namespace common\components\actions\realty;


use common\components\ThumbHelper;
use common\enum\Scenario;
use common\models\form\RealtyForm;
use common\models\Realty;
use common\traits\ModelTrait;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use yii\web\View;
use Yii;

class UpdateAction extends Action
{
    use ModelTrait;

    /** @var  $view View */
    public $view;
    
    public function run($id)
    {
        /** @var Realty $realty */
        $realty = $this->getModelOr404(Realty::className(), $id);

        if($realty->user_id != Yii::$app->user->id && !Yii::$app->user->identity->isAdmin()) {
            throw new NotFoundHttpException(Yii::$app->params['error404']);
        }

        $form = new RealtyForm();
        $form->setAttributes($realty->getAttributes(['id','vip',  'status','type','price','user_id','geo_lat','geo_lng','alias','deadline','parent_id'],[]), false);
        $form->setAttributes($realty->desc->getAttributes(['title','details'],[]), false);
        $form->setAttributes($realty->attr->getAttributes(['city_id','district_id','street','house','housing','building','around','liter','gkh_name','developer','housing_estate','rooms','floor','floors','area_all','area_living','area_kitchen'],[]), false);
        $i=0;
        foreach($realty->images as $image) {
            $form->images[$i]['id'] = $image->id;
            $form->images[$i]['name'] = $image->image;
            $form->images[$i]['type'] = strpos($image->image, '.') ? explode('.',$image->image)[1] : '';
            $form->images[$i]['thumbnail'] = ThumbHelper::thumbnailImg('@common/upload/realty/picture/'.$image->realty_id.'/'.$image->image, 120, 120, ThumbHelper::THUMBNAIL_OUTBOUND, [], false);

            $form->images_desc[] = $image->desc;
            $i++;
        }

        $i=0;
        foreach($realty->docs as $doc) {
            $form->files[$i]['id'] = $doc->id;
            $form->files[$i]['name'] = $doc->name;
            $form->files[$i]['type'] = strpos($doc->file, '.') ? explode('.',$doc->file)[1] : '';
            $form->files[$i]['thumbnail'] = RealtyForm::ImgForExtension($doc->file);

            $form->files_name[] = $doc->name;
            $i++;
        }

        $form->deadline = date('d.m.Y', $form->deadline);
        $form->scenario = Scenario::UPDATE;

        if(Yii::$app->request->isAjax &&  $form->load(\Yii::$app->request->post()) && $realty = $form->update($realty)) {
            Yii::$app->session->setFlash('success', 'Объявление обновлено');
            return $realty->id;
        }

        return $this->view->render('@common/components/actions/realty/views/update', [
            'model'=>$form
        ]);
    }
}