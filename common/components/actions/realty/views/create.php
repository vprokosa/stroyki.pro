<?php

use yii\helpers\Html;

$this->title = 'Создать';

$this->params['breadcrumbs'][] = ['label' => 'Список объявлений', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';

?>

<div class="box box-success">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>
        <div class="box-tools pull-right" style="top:10px;">
            <span class="label label-warning" style="font-size:100%;">vip</span>
        </div>
    </div>


    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>    
