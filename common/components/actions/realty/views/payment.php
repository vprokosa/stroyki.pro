<?php
$this->title = 'Выбрать способ оплаты';

$this->params['breadcrumbs'][] = ['label' => 'Список объявлений', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';

?>

<form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml">

<input type="hidden" name="receiver" value="<?= Yii::$app->params['yandexMoney']['invoice'] ?>">
<input type="hidden" name="formcomment" value="Сайт «Мозе»">
<input type="hidden" name="short-dest" value="Объявления №<?= $model->id ?>">
<input type="hidden" name="label" value="<?= $model->id ?>">
<input type="hidden" name="quickpay-form" value="shop">
<input type="hidden" name="targets" value="Оплата vip объявления <?= is_numeric($model->id) ? '№'.$model->id : '' ?>">
<input type="hidden" name="sum" value="<?= $model->price ?>" data-type="number">

<div class="box box-success">

    <div class="box-header with-border">
        <strong class="box-title text-blue">
            <?= \yii\helpers\Html::encode($this->title) ?>
        </strong>
    </div>

    <div class="box-body">
        <table class="table table-bordered table-striped" border="0" align="center">
            <tbody>
            <tr>
                <td>Id</td>
                <td><?= $model->id ?></td>
            </tr>
            <tr>
                <td>Объявления</td>
                <td><?= $model->desc->title ?></td>
            </tr>
            <tr>
                <td>Сумма</td>
                <td><?= $tariff ?> руб.</td>
            </tr>
            <tr>
                <td>Способ оплаты</td>
                <td>
                    <?= \yii\helpers\Html::dropDownList('paymentType', 'PC', [
                        'PC'=>'Яндекс.Деньгами',
                        'AC'=>'Банковской картой'
                    ], ['class'=>'form-control']) ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="box-footer">
        <?= \yii\helpers\Html::submitButton('<i class="fa fa-money"> Оплатить</i>', ['class'=>'btn btn-success']) ?>
        <?= \yii\helpers\Html::a('Закрыть', 'index', ['class'=>'btn btn-primary']) ?>
    </div>
</div>

</form>
