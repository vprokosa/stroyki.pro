<?php
/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */

use yii\widgets\ActiveForm;
use frontend\assets\YMapAsset;
use backend\assets\AppAsset;

YMapAsset::register($this);
AppAsset::register($this);
$this->registerJs(
    '$("document").ready(function(){ ready(); });'
);
?>

<style>
    .fa-plus{font-size: 20px !important;}
    .dz-details{display: none}
    #address-object{font-weight:normal;}
    .dz-error-message{top:146px !important;}
    .text-red{color: #a94442; !important;}
    #alert-error{margin-top: 10px; !important;}
</style>

<script>
    /**
     *  Показать адрес в табе карты
     */
    function ShowAddress()
    {
        var city = $.trim( $("#realtyform-city_id option").filter(":selected").text() );
        var street = $.trim( $("#realtyform-street").val() );
        var house = $.trim( $("#realtyform-house").val() );
        var address = "";

        if(city.length) {
            address += "г. " +city;
        }
        if(street.length) {
            if(address.length) {
                address += ", ";
            }
            address += "улица " + street;
        }
        if(house.length) {
            if(address.length) {
                address += ", ";
            }
            address += "дом " + house;
        }
        $("#address-object").text(address);
        ShowObjectOnMap();
    }
    /**
     *  Отобразить адрес на карте, обновить координаты
     */
    function ShowObjectOnMap()
    {
        var zoom = 12;
        var city = $.trim( $("#realtyform-city_id option").filter(":selected").text() );
        var address = "г. ";

        if(city.length) {
            address += city;
            var street = $.trim( $("#realtyform-street").val() );
            var house = $.trim( $("#realtyform-house").val() );

            if(street.length) {
                address += ", улица " + street;
                zoom = 14;
            }
            if(house.length) {
                address += ", дом " + house;
                zoom = 16;
            }
            $("#address-object").text(address);

            // Map settings
            ymaps.ready(function(){
                ymaps.geocode(address).then(
                    function (res) {

                        coords = res.geoObjects.get(0).geometry.getCoordinates();
                        $('#realtyform-geo_lat').val(coords[0]);
                        $('#realtyform-geo_lng').val(coords[1]);

                        map.setCenter(res.geoObjects.get(0).geometry.getCoordinates(), 12, {
                            checkZoomRange: true, duration: 0
                        });

                        var placemark = new ymaps.Placemark(res.geoObjects.get(0).geometry.getCoordinates(), {}, {
                            preset: "twirl#yellowStretchyIcon",
                            draggable: true
                        });
                        map.behaviors.disable("scrollZoom");

                        map.geoObjects.removeAll();
                        map.geoObjects.add(placemark);

                        placemark.events.add('drag', function (e) {
                            coords = this.geometry.getCoordinates();
                            $('#realtyform-geo_lat').val(coords[0]);
                            $('#realtyform-geo_lng').val(coords[1]);
                        }, placemark);
                    },
                    function (err) {
                        console.log('Ошибка загрузки карты !');
                    }
                );
            });
        } else {
            alert("Выберите город !");
        }
    }
    /**
     * Сортировка фоток
     */
    function SortImage()
    {
        var order = $("#tab_1 .dropzone").sortable("toArray");
        data = JSON.stringify(order);
        $.get("/realty-image/sort", {data:data}, function(){});
    }
    /**
     * Сортировка документов
     */
    function SortDoc()
    {
        var order = $("#tab_5 .dropzone").sortable("toArray");
        data = JSON.stringify(order);
        $.get("/realty-doc/sort", {data:data}, function(){});
    }
    /**
     * Проверка если выбран тип квартира
     */
    function isTypeFlat()
    {
        var el = $("#realtyform-type");
        if(el.val() == <?= \common\enum\Realty::TYPE_FLAT ?>) {
            $(".field-realtyform-parent_id").addClass("in");
        } else {
            $(".field-realtyform-parent_id").removeClass("in");
        }
    }
    /**
     * Очистить ошибки на табах
     */
    function CleanErrorTab()
    {
        $("[href='#tab_0']").removeClass("text-red").addClass("text-muted").html("<strong>Общее</strong>");
        $("[href='#tab_2']").removeClass("text-red").addClass("text-muted").html("<strong>Адрес</strong>");
        $("[href='#tab_3']").removeClass("text-red").addClass("text-muted").html("<strong>Параметры</strong>");
        $("[href='#tab_5']").removeClass("text-red").addClass("text-muted").html("<strong>Документы</strong>");
    }
    /**
     * Вывод ошибки на таб
     */
    function ShowErrorTab()
    {
        if($("#tab_0 .has-error").length) {
            $("[href='#tab_0']").removeClass("text-muted").addClass("text-red").html("<strong>Общее</strong> <i class='fa fa-warning text-red'></i>");
        }
        if($("#tab_2 .has-error").length) {
            $("[href='#tab_2']").removeClass("text-muted").addClass("text-red").html("<strong>Адрес</strong> <i class='fa fa-warning text-red'></i>");
        }
        if($("#tab_3 .has-error").length) {
            $("[href='#tab_3']").removeClass("text-muted").addClass("text-red").html("<strong>Параметры</strong> <i class='fa fa-warning text-red'></i>");
        }
        if($("#tab_5 .has-error").length) {
            $("[href='#tab_5']").removeClass("text-muted").addClass("text-red").html("<strong>Документы</strong> <i class='fa fa-warning text-red'></i>");
        }
    }

    function WriteName(el) {
        var id = $(el).attr("id");
        var name = $(el).parent(".dz-preview").find(".dz-image input").val();
        if(name.length) {
            $.get("/realty-doc/add-name", {id:id,name:name}, function(){
                alert("Название добавлено");
            })
        } else {
            alert("Заполните название");
        }
    }

    var coords_lat = '<?= $model->geo_lat ?>';
    var coords_lng = '<?= $model->geo_lng ?>';
    var map;

    function getByCoords()
    {
        var placemark = new ymaps.Placemark([coords_lat, coords_lng], {}, {
            preset: "twirl#yellowStretchyIcon",
            draggable: true
        });
        map.setCenter([coords_lat, coords_lng], 12, {
            checkZoomRange: true, duration: 0
        });
        return placemark;
    }

    function ready(){
        ShowAddress();

        $('#realtyform-is_vip').on('switchChange.bootstrapSwitch', function(event, state) {
            var text = $("[type=submit]").text();
            var bnt = $("[type=submit]");
            if(state) {
                bnt.text(text+" и оплатить");
            } else {
                text = bnt.text().split(" ")[0];
                bnt.text(text);
            }
        });

        ymaps.ready(function(){
            if (coords_lat != "" && coords_lng != "") {
                map = new ymaps.Map("ya-map", {
                    center: [coords_lat, coords_lng],
                    zoom: 16,
                    controls: ["zoomControl"]
                });
                var placemark = getByCoords();
                var fullscreenControl = new ymaps.control.FullscreenControl();
                map.geoObjects.add(placemark);
                map.controls.add(fullscreenControl);

                placemark.events.add('drag', function (e) {
                    var coords = this.geometry.getCoordinates();
                    $('#realtyform-geo_lat').val(coords[0]);
                    $('#realtyform-geo_lng').val(coords[1]);
                }, placemark);

                map.setCenter([coords_lat, coords_lng], 16, {
                    checkZoomRange: true, duration: 0
                });

            } else {
                ymaps.geocode('Москва').then(
                    function (res) {
                        map = new ymaps.Map("ya-map", {
                            center: res.geoObjects.get(0).geometry.getCoordinates(),
                            zoom: 12,
                            controls: ["zoomControl"]
                        });
                        var fullscreenControl = new ymaps.control.FullscreenControl();
                        map.controls.add(fullscreenControl);
                    },
                    function (err) {
                        alert('Ошибка загрузки карты !');
                    }
                );
            }
        });

        isTypeFlat();

        if($("#realtyform-is_composition").is(":checked")) {
            $(".field-realtyform-parent_id").addClass("in");
        }

        $('#realtyform-title').liTranslit({
            elAlias: $('#realtyform-alias')
        });

        $('#form-realty').on('submit',function(o,res){
            CleanErrorTab();
            $("div.box-success, div.box-warning").waitMe({
                effect : 'ios'
            });
            return false;
        });
        $("#form-realty").on("afterValidate", function (event, messages, error){
            if(error.length == 0) {
                var myForm = document.querySelector("#form-realty");
                formData = new FormData(myForm);
                $.ajax({
                    type: "POST",
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function(id) {
                        if( $("[type=submit]").text().search("оплатить")>0 ) {
                            location.href="payment?id="+id;
                        } else {
                            location.href="<?= \yii\helpers\Url::to('index') ?>";
                        }
                    }
                });
            } else {
              $("div.box-success, div.box-warning").waitMe('hide');
              //скроль на верх
              $('body,html').animate({
                  scrollTop: 0
              }, 400);
              //вывод ошибки
              $('#alert-error').removeClass("hidden");
              ShowErrorTab();
            }
        });
  }
</script>

<div id="alert-error" class="alert alert-danger alert-dismissible hidden">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <h5><i class="icon fa fa-warning"></i> Ошибки в форме!</h5>
</div>

<?= \common\components\widgets\VipAlert::widget() ?>

<?php $form = ActiveForm::begin([
    'id'=>'form-realty',
    'validationUrl' => ['validate'],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
]); ?>
      
    <?= \yii\helpers\Html::hiddenInput('scenario', !is_numeric($model->id) ? \common\enum\Scenario::CREATE : \common\enum\Scenario::UPDATE) ?>
    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'geo_lat')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'geo_lng')->hiddenInput()->label(false) ?>

    <div class="box-body">

        <?= $form->field($model, 'is_vip')->widget(\kartik\widgets\SwitchInput::className(), [
            'options'=>[

            ],
            'pluginOptions' => [
                'onText' => 'Да',
                'offText' => 'Нет',
            ]
        ]) ?>

        <?= $form->field($model, 'status')->dropDownList(\common\enum\Realty::Status(), ['value'=> common\enum\Realty::STATUS_PUBLIC,'prompt'=>'']) ?>

        <!-- Тип-->
        <?php //print_r(\common\enum\Realty::Type($model->type)); //die();
		if(Yii::$app->user->identity->isAdmin() && is_numeric($model->id)): ?>
            <div>
                <strong><?= $model->getAttributeLabel('type') ?>:</strong> <?= !empty($model->type) ? \common\enum\Realty::Type($model->type) : \common\enum\Realty::Type(1) ?>
                <?= $form->field($model, 'type')->hiddenInput()->label(false) ?>
            </div>
            <br>
        <?php else: ?>
            <?= $form->field($model, 'type')->dropDownList(\common\enum\Realty::Type(), [
                'prompt'=>'',
                'onchange' => 'isTypeFlat()'
            ]) ?>
        <?php endif ?>
        <!-- #Тип-->

        <!-- В составе -->
        <?php if(Yii::$app->user->identity->isAdmin()): ?>
            <?php if($model->parent_id > 0): ?>
                <div>
                    <strong><?= $model->getAttributeLabel('parent_id') ?>:</strong> <?= \common\enum\Realty::MultipleHouse($model->parent_id) ?>
                    <?= $form->field($model, 'parent_id')->hiddenInput()->label(false) ?>
                </div>
                <br>
            <?php endif ?>
        <?php else: ?>
            <?= $form->field($model, 'parent_id', [
                'options'=>[
                    'class'=>'form-group ield-realtyform-parent_id collapse'
                ]
            ])->dropDownList(\common\enum\Realty::MultipleHouse(), ['prompt'=>'', 'class'=>'form-control']) ?>
        <?php endif ?>
        <!-- #В составе -->

        <?= $form->field($model, 'title')->textInput() ?>

        <?= $form->field($model, 'alias')->textInput() ?>

        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a class="text-muted" data-toggle="tab" href="#tab_0" aria-expanded="true"><strong>Общее</strong></a>
                </li>
                <li class="">
                    <a class="text-muted" data-toggle="tab" href="#tab_1" aria-expanded="true"><strong>Фото</strong></a>
                </li>
                <li class="">
                    <a class="text-muted" data-toggle="tab" href="#tab_2" aria-expanded="true"><strong>Адрес</strong></a>
                </li>
                <li class="">
                    <a class="text-muted" data-toggle="tab" href="#tab_3" aria-expanded="true"><strong>Параметры</strong></a>
                </li>
                <li class="">
                    <a class="text-muted" data-toggle="tab" href="#tab_4" aria-expanded="true"><strong>Объект на карте</strong></a>
                </li>
                <li class="">
                    <a class="text-muted" data-toggle="tab" href="#tab_5" aria-expanded="true"><strong>Документы</strong></a>
                </li>
            </ul>
            <div class="tab-content">

                <!--  Общее  -->
                <div id="tab_0" class="tab-pane active">
                    <?= $form->field($model, 'price')->textInput() ?>

                    <?php //if(Yii::$app->user->identity->isAdmin()): ?>
                        <?= ''
                        // //$form->field($model, 'user_id')->dropDownList(\common\enum\User::Lists(), ['prompt'=>'']) ?>
                    <?php //else: ?>
                        <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
                    <?php //endif ?>
                    
                    <?= $form->field($model, 'deadline')->widget(\kartik\widgets\DatePicker::className(), [
                        'language' => 'en',
                        'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
                        'layout'=>'{input}{remove}',
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]) ?>
                </div>
                <!--  #Общее  -->

                <!--  Фото  -->
                <div id="tab_1" class="tab-pane ">

                    <?= \devgroup\dropzone\DropZone::widget([
                        'name' => 'file',
//                      'url' => '',
                        'message'=>'Загрузить фото',
                        'options' => [
                            'previewTemplate'=>
                                '<div class="dz-preview dz-file-preview">
                                    <div class="dz-image" style="border-radius:5px">
                                        <img class="img-thumbnail" data-dz-thumbnail />
                                    </div>
                                    <div class="dz-details">
                                        <div class="dz-size">
                                            <span data-dz-size></span>
                                        </div>
                                        <div class="dz-filename">
                                            <span data-dz-name></span>
                                        </div>
                                    </div>
                                    <div class="dz-progress">
                                        <span class="dz-upload" data-dz-uploadprogress></span>
                                    </div>
                                    <div class="dz-error-message">
                                        <span data-dz-errormessage></span>
                                    </div>
                                    <div class="dz-success-mark">
                                        <i class="fa fa-check text-green" style="font-size:40px;margin-left:8px;margin-top:-3px;"></i>
                                    </div>
                                    <div class="dz-error-mark">
                                        <i class="fa fa-warning text-red" style="font-size:40px;margin-left:8px; margin-top:-3px;"></i>
                                    </div>
                                </div>',
                            'acceptedFiles'=>'.jpg, .png',
                            'maxFilesize' =>2, // MB
                            'maxFiles'=> 15,
                            'parallelUploads'=>15,
                            'addRemoveLinks'=>true,
                            'dictInvalidFileType'=>'Только jpg и png',
                            'dictMaxFilesExceeded'=>'Вы можете загрузить только до 15 изображений',
                            'dictRemoveFile'=>'Удалить',
                            'dictCancelUploadConfirmation'=>'Вы уверены, в отмене загрузки?',
//                            'accept'=>new \yii\web\JsExpression('function(file, done) {
//                                file.acceptDimensions = done;
//                                file.rejectDimensions = function() { done("Минимальное разрешение 1000x750"); };
//                            }')
                        ],
                        'storedFiles' => count($model->images) ? $model->images : [],
                        'eventHandlers' => [
//                            'thumbnail'=>'function(file) {
//                                if (file.id === undefined) {
//                                    if (file.width < 1000 || file.height < 750) {
//                                        file.rejectDimensions()
//                                    } else {
//                                        file.acceptDimensions();
//                                    }
//                                }
//                            }',
                            'addedfile'=>'function(file) {
                                $(file.previewElement).attr("id", file.id);
                                $(file.previewTemplate).find(".dz-remove").attr("id", file.id);
                            }',
                            'processing'=>'function(file) {
                                var id = $("#realtyform-id").val();
                                this.options.url = "/realty-image/upload?realtyID="+id;
                            }',
                            'success'=> 'function(file, response){
                                response = JSON.parse(response);
                                var realtyID = $("#realtyform-id").val();
                                if(!$.isNumeric(realtyID)) {
                                    $("#realtyform-id").val(response.realtyID)
                                }
                                
                                //ид для блока сортировки
                                $(file.previewTemplate).attr("id", response.imageID)
                                
                                //ид для удаления
                                $(file.previewTemplate).find(".dz-remove").attr("id", response.imageID);
                                $(file.previewElement).addClass("dz-success");
                                
                                //сотрировка
                                SortImage();
                            }',
                            'removedfile' => 'function(file){                              
                                var imageID = $(file.previewTemplate).find(".dz-remove").attr("id");
                                file.previewElement.remove();
                                $.get("/realty-image/delete", {imageID:imageID}, function(){});
                                $(".dz-message").hide();
                                
                                //сотрировка
                                SortImage();
                            }'
                        ],
                        'sortable' => true,
                        'sortableOptions' => [
                            'items'=> '.dz-preview',
                            'cursor'=> 'move',
                            'opacity'=> 0.5,
                            'containment'=> '.dropzone',
                            'distance'=> 20,
                            //'tolerance'=>'pointer',
                            'stop' => new \yii\web\JsExpression('function(event, ui) {
                                SortImage();
                            }')
                        ],
                        'htmlOptions' => [],
                    ]);
                    ?>

                </div>
                <!--  #Фото  -->

                <!--  Адрес  -->
                <div id="tab_2" class="tab-pane">
                    <?= $form->field($model, 'city_id')->widget(\kartik\widgets\Select2::className(), [
                        'data' => \common\enum\City::Lists(),
                        'options' => [
                            'onchange'=>'ShowObjectOnMap();LoadDistricts($(this).val(), "#realtyform-district_id")',
                            'class'=>'city'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]) ?>

                    <?= $form->field($model, 'district_id')->widget(\kartik\widgets\Select2::className(), [
                        'data' => \common\enum\District::Lists(0, $model->city_id),
                        'options' => [
                            'placeholder' => 'Выберите район ...',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]) ?>

                    <?= $form->field($model, 'street')->textInput(['onkeyup'=>'ShowAddress()']) ?>
                    <?= $form->field($model, 'house')->textInput(['onkeyup'=>'ShowAddress()']) ?>
                    <?= $form->field($model, 'housing')->textInput() ?>
                    <?= $form->field($model, 'building')->textInput() ?>
                </div>
                <!--  #Адрес  -->

                <!--  Параметры  -->
                <div id="tab_3" class="tab-pane">
                    <?= $form->field($model, 'around')->textInput() ?>
                    <?= $form->field($model, 'liter')->textInput() ?>
                    <!--<?= $form->field($model, 'gkh_name')->textInput() ?>-->
                    <?= $form->field($model, 'developer')->textInput() ?>
                    <?= $form->field($model, 'housing_estate')->textInput() ?>
                    <?= $form->field($model, 'rooms')->textInput() ?>
                    <?= $form->field($model, 'floor')->textInput() ?>
                    <?= $form->field($model, 'floors')->textInput() ?>
                    <?= $form->field($model, 'area_all')->textInput() ?>
                    <?= $form->field($model, 'area_living')->textInput() ?>
                    <?= $form->field($model, 'area_kitchen')->textInput() ?>
                    <?= $form->field($model, 'details')->textarea(['rows' => '14']) ?>
                </div>
                <!--  #Параметры  -->

                <!--  Объект на карте  -->
                <div id="tab_4" class="tab-pane">
                    <div class="row">
                        <div class="col-md-12">
                            <table>
                                <tr> <th>Адрес: </th> <th>&nbsp;&nbsp;<i id="address-object"></i></th> </tr>
                            </table>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-2">
                            <button class="btn btn-block btn-primary" type="button" onclick="ShowObjectOnMap()">Поиск адреса</button>
                        </div>
                    </div>
                    <br>

                    <div>
                        <div id="ya-map" style="width:100%; max-width:100%; height:400px; position:relative; display:inline-block"></div>
                    </div>
                </div>
                <!--  #Объект на карте  -->

                <!--  Документы  -->
                <style>
                    .dz-remove{display: inline !important;}
                </style>
                <div id="tab_5" class="tab-pane">
                    <?= \devgroup\dropzone\DropZone::widget([
                        'name' => 'file',
//                      'url' => '',
                        'message'=>'Загрузить документы',
                        'options' => [
                            'previewTemplate'=>
                                '<div class="dz-preview dz-file-preview">
                                    <div class="dz-image" style="border-radius:5px;height:190px;background:#FFFFFE">
                                        <img class="img-thumbnail" data-dz-thumbnail />
                                        <br>
                                        <input style="width:120px" type="text" placeholder="Название">
                                    </div>
                                    <div class="dz-details">
                                        <div class="dz-size">
                                            <span data-dz-size></span>
                                        </div>
                                        <div class="dz-filename">
                                            <span data-dz-name></span>
                                        </div>
                                    </div>
                                    <div class="dz-progress">
                                        <span class="dz-upload" data-dz-uploadprogress></span>
                                    </div>
                                    <div class="dz-error-message">
                                        <span data-dz-errormessage></span>
                                    </div>
                                    <div class="dz-success-mark">
                                        <i class="fa fa-check text-green" style="font-size:40px;margin-left:8px;margin-top:-3px;"></i>
                                    </div>
                                    <div class="dz-error-mark">
                                        <i class="fa fa-warning text-red" style="font-size:40px;margin-left:8px; margin-top:-3px;"></i>
                                    </div>
                                    <a class="btn btn-success btn-md dz-writename" onclick="WriteName(this);return false;">
                                        <i class="fa fa-check"></i>
                                    </a>
                                    <a class="btn btn-danger btn-md dz-remove1" data-dz-remove="">
                                        <i class="fa fa-close"></i>
                                    </a>
                                </div>',
                            'maxFilesize' =>50, // MB
                            'maxFiles'=> 10,
                            'parallelUploads'=>10,
                            'dictMaxFilesExceeded'=>'Вы можете загрузить только до 10 документов',
//                            'addRemoveLinks'=>true,
//                            'dictRemoveFile'=>'Удалить',
                            'dictCancelUploadConfirmation'=>'Вы уверены, в отмене загрузки?',
                        ],
                        'storedFiles' => count($model->files) ? $model->files : [],
                        'eventHandlers' => [
                            'addedfile'=>'function(file) {
                                $(file.previewElement).attr("id", file.id);
                                $(file.previewTemplate).find(".dz-writename").attr("id", file.id);
                                $(file.previewTemplate).find(".dz-remove1").attr("id", file.id);
                                $(file.previewTemplate).find(".dz-image input").attr("value", file.name);
                            }',
                            'processing'=>'function(file) {
                                var id = $("#realtyform-id").val();
                                this.options.url = "/realty-doc/upload?realtyID="+id;
                            }',
                            'success'=> 'function(file, response){
                                response = JSON.parse(response);
                                var realtyID = $("#realtyform-id").val();
                                if(!$.isNumeric(realtyID)) {
                                    $("#realtyform-id").val(response.realtyID)
                                }
                                
                                //ид для блока сортировки
                                $(file.previewTemplate).attr("id", response.docID)
                                
                                //ид для удаления
                                $(file.previewTemplate).find(".dz-writename").attr("id", response.docID);
                                $(file.previewTemplate).find(".dz-remove1").attr("id", response.docID);
                                $(file.previewElement).addClass("dz-success");
                                
                                //иконка по типу дока
                                $(file.previewElement).find(".dz-image img").attr("src", response.docIcon);
                                
                                //сотрировка
                                SortDoc();
                            }',
                            'removedfile' => 'function(file){                              
                                var docID = $(file.previewTemplate).find(".dz-remove1").attr("id");
                                file.previewElement.remove();
                                $.get("/realty-doc/delete", {docID:docID}, function(){});
                                $(".dz-message").hide();
                                
                                //сотрировка
                                SortDoc();
                            }',
                            'accept' => 'function(file, done) {
                                if (file.type != "doc" || file.type != "docx" || file.type != "xls" || file.type != "xlsx" || file.type != "ppt" || file.type != "pptx" || file.type != "txt") {
                                    done("Неверный файл");
                                } else {
                                    done();
                                }
                            }'
                        ],
                        'sortable' => true,
                        'sortableOptions' => [
                            'items'=> '.dz-image-preview',
                            'cursor'=> 'move',
                            'opacity'=> 0.5,
//                            'containment'=> '.dropzone',
                            'distance'=> 20,
                            //'tolerance'=>'pointer',
                            'stop' => new \yii\web\JsExpression('function(event, ui) {
                                SortDoc();
                            }')
                        ],
                        'htmlOptions' => [],
                    ]);
                    ?>
                    
                </div>
                <!--  #Документы  -->

            </div>
        </div>
    </div>

    <div class="box-footer">
        <?= yii\helpers\Html::submitButton(!is_numeric($model->id) ? 'Создать' : 'Обновить', ['class' => 'btn btn-primary']) ?>
        <?= yii\helpers\Html::a('Закрыть', Yii::$app->request->referrer, ['class'=>'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

