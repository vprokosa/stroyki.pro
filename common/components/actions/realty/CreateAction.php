<?php

namespace common\components\actions\realty;


use common\enum\Scenario;
use common\enum\User;
use common\models\form\RealtyForm;
use yii\base\Action;
use yii\web\View;
use Yii;

class CreateAction extends Action
{
    /** @var  $view View */
    public $view;
 
    public function run()
    {
        $form = new RealtyForm();

        if (Yii::$app->user->identity->role == User::ROLE_USER || Yii::$app->user->identity->role == User::ROLE_ADMIN) {
            $form->user_id = Yii::$app->user->id;
        }

        //$form->user_id = \Yii::$app->user->identity->role == User::ROLE_USER ? \Yii::$app->user->id : '';

        $form->scenario = Scenario::CREATE;


        if (Yii::$app->request->isAjax && $form->load(Yii::$app->request->post()) && $realty = $form->create()) {
            Yii::$app->session->setFlash('success', 'Объявление создано');
            return $realty->id;
        }

        return $this->view->render('@common/components/actions/realty/views/create', [
            'model' => $form,
        ]);
    }
}