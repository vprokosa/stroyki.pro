<?php

namespace common\components\actions\realty;


use common\models\form\RealtyForm;
use common\traits\ModelTrait;
use yii\base\Action;
use Yii;

class ValidateAction extends Action
{
    use ModelTrait;

    public function run()
    {
        $form = new RealtyForm();
        return $this->validate($form, $_POST['scenario'], Yii::$app->request->post());
    }
}