<?php

namespace common\components\actions\realty;


use common\enum\Scenario;
use common\enum\User;
use common\models\form\RealtyForm;
use common\models\Payment;
use common\models\PaymentTariff;
use common\models\Realty;
use yii\base\Action;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\web\View;
use Yii;

class PaymentAction extends Action
{
    /** @var  $view View */
    public $view;

    public function run($id=0)
    {
        $model = Realty::find()->with('desc')->where(['id'=>$id])->one();

        if(!$model) {
            throw new NotFoundHttpException(Yii::$app->params['error404']);
        }

        $data = (new Query())
            ->select(['tariff'])
            ->from(PaymentTariff::tableName())
            ->where(['type'=>\common\enum\PaymentTariff::TYPE_VIP])
            ->one();
        
        return $this->view->render('@common/components/actions/realty/views/payment', [
            'model' => $model,
            'tariff'=>$data['tariff']
        ]);
    }
}