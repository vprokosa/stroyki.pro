<?php

namespace common\components\actions\realty;


use common\models\Realty;
use common\models\RealtyAttr;
use common\models\RealtyDesc;
use common\models\RealtyImage;
use common\models\RealtyDoc;
use common\models\RealtyFavorite;
use common\models\RealtyVip;
use common\models\StatRealty;
use common\traits\ModelTrait;
use yii\base\Action;
use yii\web\NotFoundHttpException;
use yii\web\View;
use Yii;

class DeleteAction extends Action
{
    use ModelTrait;
    
    /** @var  $view View */
    public $view;
    
    public function run($id)
    {
        /** @var Realty $realty */
        $realty = $this->getModelOr404(Realty::className(), $id);

        if($realty->user_id != Yii::$app->user->id && !Yii::$app->user->identity->isAdmin()) {
            throw new NotFoundHttpException(Yii::$app->params['error404']);
        }
		RealtyAttr::deleteAll(['realty_id'=>$realty->id]);
		RealtyDesc::deleteAll(['realty_id'=>$realty->id]);
		RealtyImage::deleteAll(['realty_id'=>$realty->id]);
		RealtyDoc::deleteAll(['realty_id'=>$realty->id]);
		RealtyFavorite::deleteAll(['realty_id'=>$realty->id]);
		RealtyVip::deleteAll(['realty_id'=>$realty->id]);
		StatRealty::deleteAll(['realty_id'=>$realty->id]);
        $realty->delete();

        return $this->view->redirect('index');
    }
}