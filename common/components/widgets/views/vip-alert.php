<?php if(!Yii::$app->user->identity->isAdmin()): ?>
    <div class="alert alert-info">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h3>Vip объявления - <?= isset($data['tariff']) ? $data['tariff'].'p' : '' ?></h3>
        <p><?= isset($data['desc']) ? $data['desc'] : '' ?></p>
    </div>
<?php endif ?>
