<?php

namespace common\components\widgets;

use common\models\PaymentTariff;
use yii\base\Widget;
use yii\db\Query;

class VipAlert extends Widget
{
    public function run()
    {
        $data = (new Query())
            ->select(['tariff','desc'])
            ->from(PaymentTariff::tableName())
            ->where(['type'=>\common\enum\PaymentTariff::TYPE_VIP])
            ->one();
        
        return $this->render('vip-alert', ['data'=>$data]);
    }

}