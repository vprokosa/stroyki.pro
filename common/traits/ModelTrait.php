<?php

namespace common\traits;

use yii\db\ActiveRecordInterface;
use yii\web\NotFoundHttpException;

trait ModelTrait
{
    /**
     * @param string $modelClass
     * @param integer $id
     * @return ActiveRecordInterface|null
     * @throws NotFoundHttpException
     */
    public function getModelOr404($modelClass, $id)
    {
        if(is_numeric($id)) {
            $model = $modelClass::findOne($id);

            if ($model) {
                return $model;
            }
        }
        throw new NotFoundHttpException();
    }

    /**
     * Универсальная валидация формы
     * 
     * @param $$form модельная форма
     * @param $scenario сценария 
     * @param $data параметры запроса
     * @return array
     */
    public function validate($form, $scenario, $data)
    {
        $form->scenario = $scenario;
        \Yii::$app->response->format = 'json';
        $form->load($data);
        return \yii\widgets\ActiveForm::validate($form);
    }
}