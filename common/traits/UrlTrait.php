<?php

namespace  common\traits;

trait UrlTrait
{
    /**
     * Получить домен из поддомена
     */
    public function getDomain()
    {
        $url = \Yii::$app->urlManager->hostInfo;
        list($subDomain, $domain, $zone) = explode('.', $url);
        return "http://$domain.$zone";
    }
}