<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['user/reset-password', 'token' => $user->password_reset_token]) . 'user/reset-password?token=' . $user->password_reset_token;
?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($user->full_name) ?>,</p>

    <p>Перейдите по ссылке ниже, чтобы поменять пароль:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
