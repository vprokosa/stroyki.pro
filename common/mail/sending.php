<?php
use yii\helpers\Html;
?>

<h2>Здравствуйте <?= Html::encode($full_name) ?>,</h2>
<p>Выполняем ваш запрос, на рассылку объявлений за <?= $date ?></p>

<strong>По выбранным параметрам</strong>
<table>
    <?php if(strlen($rooms)): ?>
        <tr>
            <td>Комнаты</td>
            <td><?= $rooms ?></td>
        </tr>
    <?php endif ?>

    <?php if(is_numeric($start_area)): ?>
        <tr>
            <td>Площадь от</td>
            <td><?= $start_area ?></td>
        </tr>
    <?php endif ?>

    <?php if(is_numeric($end_area)): ?>
        <tr>
            <td>Площадь от</td>
            <td><?= $end_area ?></td>
        </tr>
    <?php endif ?>

    <?php if(is_numeric($start_price)): ?>
        <tr>
            <td>Цена от, руб</td>
            <td><?= $start_price ?></td>
        </tr>
    <?php endif ?>

    <?php if(is_numeric($end_price)): ?>
        <tr>
            <td>Цена до, руб</td>
            <td><?= $end_price ?></td>
        </tr>
    <?php endif ?>

    <?php if(is_string($city)): ?>
        <tr>
            <td>Город</td>
            <td><?= $city ?></td>
        </tr>
    <?php endif ?>
</table>

<br>
<h3>Объявления</h3>
<table>
    <?php foreach($realty as $item): ?>
        <tr>
            <td>
                <a href="<?= Yii::$app->params['sitename'].'/advertisement/'.$item['alias'] ?>"><?= $item['title'] ?></a>
            </td>
        </tr>
    <?php endforeach ?>
</table>
