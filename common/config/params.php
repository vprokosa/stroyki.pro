<?php
return
    \yii\helpers\ArrayHelper::merge([
        'adminEmail' => 'admin@example.com',
        'supportEmail' => 'noreply@stroyki.pro',
        'sitename' => 'stroyki.pro',

        'user.passwordResetTokenExpire' => 3600,

        'error404'=>'Страница не найдена',

        'yandexMoney'=> [
            'invoice'=>'410013689750947',
            'secretWord'=>'EytU3/9IdQmI/CiLrUJUyLDj'
        ]
    ], require 'key_cache.php');


