<?php


return [
    'baseUrl' => '/',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => true,
	'cache' => false,

    'rules' => [
		'/'=>'site/index',
        'login'=>'user/login',
        'logout'=>'user/logout',
        'signup'=>'user/signup',
        'change-city'=>'site/change-city',
        'page'=>'/',
        'page/<alias:(.+)>'=>'site/page',
        'advertisement'=>'/',
        //'novostroyki'=>'site/novostroyki',
        'realty/post'=>'realty/post',
        'realty'=>'realty/index',
        'service/favorite'=>'service/favorite',
        'support' => 'site/request-support',
        //'zastroyshchiki'=>'site/zastroyshchiki',
        //'jilie-kompleksi'=>'site/jilie-kompleksi',
        'all/<user_id:(\d)>'=>'site/all-realty',//все объявления по пользователю
		'article/<id:\d+>' => 'site/article',
        //'search' => 'site/search-filter',
		'<controller:(user)>/<action:(login|logout|signup)>' => '<controller>/<action>',
		'<controller:(site)>/<action:(banner|all-realty)>' => '<controller>/<action>',
		'<controller:(realty)>/<action>' => '<controller>/<action>',
		'<controller:(profile)>/<action>' => '<controller>/<action>',
	//	'<controller>/<maps>' => 'site/search-objects-on-map',
		['class' => \frontend\components\RealtyUrlRule::class,], 
		/* [
        'pattern' => 'site',
        'route' => ['class' => \frontend\components\RealtyUrlRule::class,],
        //'suffix' => '.json',
		], */
        '<controller>' => '<controller>/index',
        '<controller>/<action>' => '<controller>/<action>',
        '<modules>/<controller>/<action>' => '<modules>/<controller>/<action>',
		
		/* 
		//'<controller:(site)>/<action:(article)>' => '<controller>/<action>', 
        'realty/post'=>'realty/post',
                // ['class' => 'frontend\components\RealtyUrlRule'],
       'all/<user_id:(\d)>'=>'site/all-realty',//все объявления по пользователю
        'login'=>'user/login',
        'logout'=>'user/logout',
        'signup'=>'user/signup',
        //'search-objects-on-map'=>'site/search-objects-on-map',
        //'article/<id:(\d)>'=>'article/view', 
        //'advertisement/<id:(.+)>'=>'site/advertisement',
		'<city_id:\w+>' => 'site/search-objects',
		'<city:\w+>/<type:\w+>' => 'site/search-objects',
		'<city:\w+>/<type:\w+>/<rooms:\w+>' => 'site/search-objects',
		'<city:\w+>/<type:\w+>/<rooms:\w+>/<district:\w+>' => 'site/search-objects',

        '/'=>'site/index',
        //'novostroyki/<page:(\d)>/<per-page:(\d)>'=>'site/search-objects',
		//'zastroyshchiki/<ObjectSearch:\w+>' => 'site/search-objects',
        'search-objects'=>'site/search-objects',  */
    ]
];