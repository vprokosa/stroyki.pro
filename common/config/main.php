<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => Yii::getAlias('@common') . '/runtime/cache'
        ],
        'thumbnail' => [
            'class' => \himiklab\thumbnail\EasyThumbnail::className(),
            'cacheAlias' => 'assets/thumbnails',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'sourceLanguage' => '',
                ],
            ],
        ],
        'stat'=>[
            'class'=> \common\components\Statistics::className()
        ],
    ],
];
