<?php

// Время в миллисекундах
//час - 3600
//день - 86400
//неделя - 604800
//месяц - 2629743 
//год - 31556926 

return [
    'cache' => [
        'user_list' => [
            'key'=>'user_list',
            'time'=>2629743
        ],
        'city_list' => [
            'key'=>'city_list',
            'time'=>2629743
        ],
        'okrug_list' => [
            'key'=>'okrug_list',
            'time'=>2629743
        ],
    ]
    
];