<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "realty".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $type
 * @property string $price
 * @property string $price_update
 * @property integer $user_id
 * @property string $geo_lat
 * @property string $geo_lng
 * @property string $alias
 * @property integer $deadline
 * @property integer $vip 
 * @property integer $parent_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Realty $parent
 * @property Realty[] $realties
 * @property User $user
 * @property RealtyAttr[] $realtyAttrs
 * @property RealtyDesc[] $realtyDescs
 * @property RealtyDoc[] $realtyDocs
 * @property RealtyFavorite[] $realtyFavorites
 * @property RealtyImage[] $realtyImages
 * @property RealtyVip[] $realtyVips
 * @property SubscriptionPrice[] $subscriptionPrices
 */
class Realty extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%realty}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord) {
            $this->price_update = date('Y-m-d', time());
        } else {
            $oldPrice = $this->getOldAttribute('price');
            $newPrice = $this->getDirtyAttributes(['price'])['price'];
            if($oldPrice != $newPrice) {
                $subscriptions = SubscriptionPrice::find()->where(['realty_id'=>$this->id])->all();
                foreach ($subscriptions as $subscription) {
                    $subscription->show = \common\enum\Realty::SUBSCRIPTION_PRICE_YES;
                    $subscription->update(false, ['show']);
                }
                $this->price_update = date('Y-m-d', time());
            }
        }
        return parent::beforeSave($insert);
    }


    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'type' => 'Тип',
            'price' => 'Цена',
            'price_update' => 'Обновления цены',
            'user_id' => 'Пользователь',
            'geo_lat' => 'Гео широта',
            'geo_lng' => 'Гео долгота',
            'alias' => 'Алиас',
            'deadline' => 'Срок сдачи',
            'vip' => 'Vip',
            'parent_id' => 'Находиться в составе многоэтажного дома',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * Главное фото недвижимости
     */
    public function mainImage()
    {
        if(isset($this->images[0])) {
            return "@common/upload/realty/picture/{$this->id}/{$this->images[0]->image}";
        } else {
            return '@webroot/image/default/nopic.png';
        }
    }
    public function isApartmentHouse()
    {
        if($this->type == \common\enum\Realty::TYPE_APARTMENT_HOUSE) {
            return true;
        }
        return false;
    }
    public function favoriteClass()
    {
        if(!Yii::$app->user->isGuest && isset($this->favoriteForUser->id)) {
            return 'color-green';
        }
        return 'color-yellow';
    }
    public function subscriptionPriceClass()
    {
        if(!Yii::$app->user->isGuest && isset($this->subscriptionPriceForUser->id)) {
            return 'color-green';
        }
        return 'color-yellow';
    }

    /**
     * Путь к фотке
     */
    public function getUrlImage($name)
    {
        return "/upload/realty/img/{$this->id}/{$name}";
    }
    /**
     * Путь к доку
     */
    public function getUrlDoc($name)
    {
        return "/upload/realty/doc/{$this->id}/{$name}";
    }

    /**
     * Многоквартирный дом
     * @return \yii\db\ActiveQuery
     */
    public function getMultipleHouse()
    {
        return $this->hasOne(Realty::className(), ['id' => 'parent_id']);
    }

    /**
     * Все квартиры в Многоквартирном доме
     * @return \yii\db\ActiveQuery
     */
    public function getFlats()
    {
        return $this->hasMany(Realty::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(RealtyAttr::className(), ['realty_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDesc()
    {
        return $this->hasOne(RealtyDesc::className(), ['realty_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocs()
    {
        return $this->hasMany(RealtyDoc::className(), ['realty_id' => 'id'])->orderBy(['sort'=>SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFavorites()
    {
        return $this->hasMany(RealtyFavorite::className(), ['realty_id' => 'id']);
    }

    public function getFavoriteForUser()
    {
        return $this->hasOne(RealtyFavorite::className(), ['realty_id' => 'id'])->where(['user_id'=>Yii::$app->user->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(RealtyImage::className(), ['realty_id' => 'id'])->orderBy(['id'=>SORT_ASC]);
    }

 /*   public function getImage()
    {
        return $this->hasOne(RealtyImage::className(), ['realty_id' => 'id'])->orderBy(['sort'=>SORT_ASC]);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVips()
    {
        return $this->hasMany(RealtyVip::className(), ['realty_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionPrices()
    {
        return $this->hasMany(SubscriptionPrice::className(), ['realty_id' => 'id']);
    }
    
    public function getSubscriptionPriceForUser()
    {
        return $this->hasOne(SubscriptionPrice::className(), ['realty_id' => 'id'])->where(['user_id'=>Yii::$app->user->id]);
    }
	
}
