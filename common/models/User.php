<?php

namespace common\models;

use common\components\ThumbHelper;
use common\enum\Scenario;
use mongosoft\file\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $status
 * @property integer $role
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $full_name
 * @property string $phone
 * @property string $avatar
 * @property string $header
 * @property string $company
 * @property string $buildcompany
 * @property string $city
 * @property string $street
 * @property string $house
 * @property string $office
 * @property string $address
 * @property integer $sending
 * @property string $logo
 * @property string $site
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Payment[] $payments
 * @property Realty[] $realties
 * @property RealtyFavorite[] $realtyFavorites
 * @property SubscriptionPrice[] $subscriptionPrices
 * @property SubscriptionSearch[] $subscriptionSearches
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    public function scenarios()
    {
        return [
            'editProfile'=>['full_name','username','type','email','phone','site','buildcompany','city','address'],
            'changePass'=>['password'],
            'generateToken'=>['password_reset_token'],
            'resetPass'=>['password'],
            'signup'=>['full_name','username','email','password'],
            'logo' => ['logo'],
            'avatar' => ['avatar'],
            'header' => ['header'],
            'rss'=>['sending'],
            Scenario::CREATE => ['full_name', 'username','email','buildcompany','type','password_hash'],
            Scenario::UPDATE => ['full_name', 'username','email','buildcompany','type'],
        ];
    }

    /**
     * Динамическое подключение загрузки картинок к разным полям
     */
    public function attachUploadImageBehavior($attribute)
    {
        $this->attachBehavior('UploadImage', [
            'class' => UploadImageBehavior::className(),
            'attribute' => $attribute,
            'scenarios' => [$attribute],
            'path' => '@common/upload/user/'.$attribute.'/{id}',
            'url' => '/',
            'createThumbsOnSave'=>false
        ]);
        
        return $this;
    }
    /**
     * Динамическое отключение загрузки картинок к разным полям
     */
    public function detachUploadImageBehavior()
    {
        $this->detachBehavior('UploadImage');
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'status' => 'Статус',
            'role' => 'Роль',
            'username' => 'Логин',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Пароль',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'full_name' => 'Имя',
            'phone' => 'Телефон',
            'avatar' => 'Аватар',
            'header' => 'Фоновая картинка',
            'company' => 'О компании',
            'buildcompany' => 'Застройщик',
            'city' => 'Город',
            'street' => 'Улица',
            'house' => 'Дом',
            'office' => 'Офис',
            'address' => 'Адрес',
            'sending' => 'Подписка на рассылку по поиску',
            'logo' => 'Логотип',
            'site' => 'Официальный сайт',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->cache->delete(Yii::$app->params['cache']['user_list']['key']);
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        Yii::$app->cache->delete(Yii::$app->params['cache']['user_list']['key']);
        parent::afterDelete();
    }

    public function isAdmin()
    {
        if($this->role==\common\enum\User::ROLE_ADMIN) {
            return true;
        }
        return false;
    }

    public function typeName()
    {
        $type = 0;
        $index = $this->type;
        if(is_numeric($index)) {
            $type = \common\enum\User::Type($index);
            $type = mb_strtoupper(mb_substr($type, 0, 1, 'UTF-8'), 'UTF-8') . mb_substr($type, 1, mb_strlen($type), 'UTF-8');
        }
        return $type;
    }

    /**
     * Статус блокировки
     */
    public function lockAndUnlock()
    {
        $title = 'Блокировать';
        $icon = 'fa fa-unlock';
        $class = 'btn-sm btn-primary';
        $status = \common\enum\User::STATUS_BLOCK;
        if($this->status == \common\enum\User::STATUS_BLOCK) {
            $title = 'Активировать';
            $icon = 'fa fa-lock';
            $class = 'btn-sm btn-danger';
            $status = \common\enum\User::STATUS_ACTIVE;
        }
        return Html::a(Html::tag('i', '', ['class'=>$icon]), ['users/change-status', 'id'=>$this->id, 'status'=>$status], [
            'class' => $class,
            'title' => $title
        ]);
    }
    /**
     * Аватарка
     */
    public function avatar($w, $h)
    {
        $this->attachUploadImageBehavior('avatar');
        $file = $this->getUploadPath('avatar');
        if(!empty($file)) {
            $img = ThumbHelper::thumbnailImg($file, $w, $h, ThumbHelper::THUMBNAIL_OUTBOUND, ['class'=>'img-circle']);
            if($img == 'notFind') {
                $img = ThumbHelper::thumbnailImg('@webroot/image/default/nopic.png', $w, $h);
            }
        } else {
            $img = ThumbHelper::thumbnailImg('@webroot/image/default/nopic-user.png', $w, $h, ThumbHelper::THUMBNAIL_OUTBOUND, ['class'=>'img-circle']);
        }
        $this->detachUploadImageBehavior();

        return $img;
    }
    /**
     * Логотип
     */
    public function logo($w, $h)
    {
				
        $this->attachUploadImageBehavior('logo');
        $file = $this->getUploadPath('logo');
        $img = '';
        if(!empty($file)) {
            $img = ThumbHelper::thumbnailImg($file, $w, $h);
            if($img == 'notFind') {
                $img = ThumbHelper::thumbnailImg('@webroot/image/default/nopic.png', $w, $h);
            }
        }
        $this->detachUploadImageBehavior();

        return $img;
    }
    /**
     * Шапка
     */
    public function header($w, $h, $tagImg=true)
    {
        $this->attachUploadImageBehavior('header');
        $file = $this->getUploadPath('header');
        $this->detachBehavior('UploadImage');
        if(!empty($file)) {
            if($tagImg) {
                $img = ThumbHelper::thumbnailImg($file, $w, $h);
                if($img == 'notFind') {
                    $img = ThumbHelper::thumbnailImg('@webroot/image/default/nopic.png', $w, $h);
                }
            } else {
                $img = ThumbHelper::thumbnailImg($file, $w, $h, ThumbHelper::THUMBNAIL_INSET, [], false);
                if($img == 'notFind') {
                    $img = ThumbHelper::thumbnailImg('@webroot/image/default/nopic.png', $w, $h, ThumbHelper::THUMBNAIL_OUTBOUND, [], false);
                }
                if(strlen($this->logo)) {
                    return $img.'|'.$this->publicLogo();
                }
            }
        } else {
            if($tagImg) {
                $img = ThumbHelper::thumbnailImg('@webroot/image/default/nopic-header.png', $w, $h);
            } else {
                $img = ThumbHelper::thumbnailImg('@webroot/image/default/nopic-header.png', $w, $h, ThumbHelper::THUMBNAIL_OUTBOUND, [], false);
                if(strlen($this->logo)) {
                    return $img.'|'.$this->publicLogo();
                }
            }
        }
        $this->detachUploadImageBehavior();

        return $img;
    }

    /**
     * Опубликация логотипа
     */
    public function publicLogo()
    {
        $this->attachUploadImageBehavior('logo');
        $file = $this->getUploadPath('logo');
        $file = ThumbHelper::thumbnailImg($file, 200, 120, ThumbHelper::THUMBNAIL_OUTBOUND, [], false);
        $this->detachBehavior('UploadImage');

        return $file;
    }

    /**
     * Тип пользователя 
     */
    public function type()
    {
        return is_numeric($this->type) ? \common\enum\User::Type($this->type) : ''; 
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealties()
    {
        return $this->hasMany(Realty::className(), ['user_id' => 'id'])->where(['status' => \common\enum\Realty::STATUS_PUBLIC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyFavorites()
    {
        return $this->hasMany(RealtyFavorite::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionPrices()
    {
        return $this->hasMany(SubscriptionPrice::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionSearches()
    {
        return $this->hasMany(SubscriptionSearch::className(), ['user_id' => 'id']);
    }

    public function classSending()
    {
        if($this->sending == \common\enum\User::SENDING_NO) {
            return 'btn-success';
        }
        return 'btn-danger';
    }
    public function textSending()
    {
        if($this->sending == \common\enum\User::SENDING_NO) {
            return 'Включить рассылку';
        }
        return 'Отключить рассылку';
    }






    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id,'status' => \common\enum\User::STATUS_ACTIVE]);
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    }
    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }
    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        list($string, $timestamp) = explode('_', $token);
        $timestamp = (int)$timestamp;
        return $timestamp + $expire >= time();
    }
    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => \common\enum\User::STATUS_ACTIVE,
        ]);
    }
}
