<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "place_okrug".
 *
 * @property integer $id
 * @property integer $area_id
 * @property string $title
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City[] $placeCities
 */
class Okrug extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%place_okrug}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }
    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->cache->delete(Yii::$app->params['cache']['okrug_list']['key']);
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        Yii::$app->cache->delete(Yii::$app->params['cache']['okrug_list']['key']);
        parent::afterDelete();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'Зона',
            'title' => 'Название',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['okrug_id' => 'id']);
    }
}
