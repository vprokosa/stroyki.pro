<?php

namespace common\models;

use common\enum\Scenario;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "pages".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $type
 * @property string $title
 * @property string $alias
 * @property string $text
 * @property integer $created_at
 * @property integer $updated_at
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function scenarios()
    {
        return [
            Scenario::CREATE => ['status','type','title','alias','text'],
            Scenario::UPDATE => ['status','type','title','alias','text'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'type' => 'Тип',
            'title' => 'Название',
            'alias' => 'Алиас',
            'text' => 'Содержание',
            'created_at' => 'Создано',
            'updated_at' => 'Обналено',
        ];
    }
    
    public static function Alias($type)
    {
        $page = Yii::$app->db->createCommand('SELECT alias FROM pages WHERE type=:type')->bindParam(':type', $type)->queryOne();
        if(isset($page['alias'])) {
            return '/page/'.$page['alias'];
        }
        return '';
    }
    
}
