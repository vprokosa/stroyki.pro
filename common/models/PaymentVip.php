<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment_vip".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property integer $realty_vip_id
 * @property integer $period
 *
 * @property Payment $payment
 * @property RealtyVip $realtyVip
 */
class PaymentVip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_vip}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_id' => 'Платеж',
            'realty_vip_id' => 'Недвижимость vip',
            'period' => 'Период',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyVip()
    {
        return $this->hasOne(RealtyVip::className(), ['id' => 'realty_vip_id']);
    }
}
