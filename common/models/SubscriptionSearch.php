<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "subscription_search".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $room1
 * @property integer $room2
 * @property integer $room3
 * @property integer $room4
 * @property integer $start_area
 * @property integer $end_area
 * @property integer $start_price
 * @property integer $end_price
 * @property integer $option
 * @property integer $city_id
 *
 * @property User $user
 */
class SubscriptionSearch extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subscription_search}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'room1' => 'Количество комнат 1',
            'room2' => 'Количество комнат 2',
            'room3' => 'Количество комнат 3',
            'room4' => 'Количество комнат 4+',
            'start_area' => 'Площадь от',
            'end_area' => 'Площадь по',
            'start_price' => 'Цена от',
            'end_price' => 'Цена по',
            'option' => 'Опции',
            'city_id' => 'Город',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * Регистрация параметров
     */
    public static function RegSearch($params)
    {
        if(!Yii::$app->user->isGuest && Yii::$app->user->identity->sending == \common\enum\User::SENDING_YES) {
            $subscriptionSearch = SubscriptionSearch::findOne(['user_id'=>Yii::$app->user->id]);
            if(!$subscriptionSearch) {
                $subscriptionSearch = new SubscriptionSearch();
                $subscriptionSearch->user_id = Yii::$app->user->id;
            }
            if(isset($params['ObjectSearch'])) {
                $subscriptionSearch->setAttributes($params['ObjectSearch'], null);
                if(isset($_COOKIE['cityID'])) {
                    $subscriptionSearch->city_id = $_COOKIE['cityID'];
                }
                $subscriptionSearch->save();
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
