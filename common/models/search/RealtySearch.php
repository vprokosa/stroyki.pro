<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Realty;

/**
 * RealtySearch represents the model behind the search form about `common\models\Realty`.
 */
class RealtySearch extends Realty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'type', 'user_id', 'vip'], 'integer'],
            ['created_at','string'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Realty::find()->with(['attr','desc','images']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		$sort = isset($params['sort']) ? (substr($params['sort'], 0, 1) !== '-' ? $params['sort'] : substr($params['sort'], 1, strlen($params['sort']) + 1)) : 'id';
		$ordering = isset($params['sort']) ? (substr($params['sort'], 0, 1) !== '-' ? SORT_ASC : SORT_DESC) : SORT_DESC;
		//$sort = isset($params['sort']) ? 'id' : $sort;
		//$ordering = isset($params['sort']) ? $ordering : SORT_DESC;
		//print_r($this->user_id);
		//die(); 
        // grid filtering conditions
        $query->andFilterWhere([
            'status' => $this->status,
            'type' => $this->type,
            'price' => $this->price,
            'user_id' => $this->user_id,
            'vip' => $this->vip,
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['FROM_UNIXTIME(created_at, "%d.%m.%Y")' => $this->created_at]);
        $query->orderBy([$sort=>$ordering]);

        return $dataProvider;
    }
}
