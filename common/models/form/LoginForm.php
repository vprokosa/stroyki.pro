<?php
namespace common\models\form;

use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $rememberMe = true;
    public $loginAdmin = false;

    private $_user;

    public function attributeLabels()
    {
        return [
            'username'=>'Логин',
            'email'=>'Email',
            'password'=>'Пароль',
            'rememberMe'=>'Запомнить меня',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = null;

        if ($this->loginAdmin) {
            $rules = [
                // username and password are both required
                [['username', 'password'], 'required', 'message'=>'{attribute} не заполнен'],
                // Validate email
                ['email', 'safe'],
                // rememberMe must be a boolean value
                ['rememberMe', 'boolean'],
                // password is validated by validatePassword()
                ['password', 'validatePassword'],
            ];
        } else {
            $rules = [
                // username and password are both required
                [['password', 'email'], 'required', 'message'=>'{attribute} не заполнен'],
                // Validate email
                ['email', 'email'],
                // rememberMe must be a boolean value
                ['rememberMe', 'boolean'],
                // password is validated by validatePassword()
                ['password', 'validatePassword'],
            ];
        }

        return $rules;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Неверный логин или пароль.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        $where = [];

        if ($this->_user === null) {
            if ($this->loginAdmin) {
               $where = [
                   'username' => $this->username,
                   'status' => \common\enum\User::STATUS_ACTIVE,
                   //'role' => !$this->loginAdmin ? \common\enum\User::ROLE_USER : \common\enum\User::ROLE_ADMIN
               ];
            }else {
                $where = [
                    'email' => $this->email,
                    'status' => \common\enum\User::STATUS_ACTIVE,
                    //'role' => !$this->loginAdmin ? \common\enum\User::ROLE_USER : \common\enum\User::ROLE_ADMIN
                ];
            }
            echo 123;
            $this->_user = User::find()->where($where)->one();
        }

        return $this->_user;
    }
}
