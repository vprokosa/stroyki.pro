<?php

namespace  common\models\form;

use common\enum\Scenario;
use common\models\Realty;
use common\models\RealtyAttr;
use common\models\RealtyDesc;
use common\models\RealtyDoc;
use common\models\RealtyImage;
use common\traits\ModelTrait;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class RealtyForm extends Model
{
    //realty
    public $id = null;
    public $status; //статус zip, public, ...
    public $type; //тип
    public $price; //Цена
    public $user_id; //
    public $geo_lat; //Координаты широты
    public $geo_lng; //Координата долготы
    public $alias;
    public $deadline; //Срок сдачи
    public $vip;
    public $is_vip;
    public $parent_id; //в составе

    //realty_desc
    public $title; //Название
    public $details; //Дополнительная информация

    //realty_attr
    public $city_id; //Город
    public $district_id; //Район
    public $street; //Улица
    public $house; //Дом
    public $housing; //Корпус
    public $building; //Строение
    public $around; //Рядом
    public $liter; //Литер
    public $gkh_name; //ЖКХ
    public $developer; //Застройщик
    public $housing_estate; //Жилой комплекс
    public $rooms; //Комнаты
    public $floor; //Этаж
    public $floors; //Этажей
    public $area_all; //Общая площадь
    public $area_living; //Жилая площадь
    public $area_kitchen; //Площадь кухни

    //realty_doc
    public $files; //доки;
    public $files_name; //название доки;

    //realty_image
    public $images; //фотки
    public $images_desc; //описание фотки

    public function attributeLabels()
    {
        return ArrayHelper::merge(
            (new Realty())->attributeLabels(),
            (new RealtyDesc())->attributeLabels(),
            (new RealtyAttr())->attributeLabels(),
            (new RealtyDoc())->attributeLabels(),
            (new RealtyImage())->attributeLabels(),
            ['is_vip'=> $this->vip==0 ? 'Приобрести статус VIP' : 'Продлить статус VIP']
        );
    }

    public function scenarios()
    {
        return [
            Scenario::CREATE => [
                'id',
                'status', 
                'type', 
                'price',
                'user_id',
                'geo_lat',
                'geo_lng',
                'alias',
                'deadline',
                'is_vip',
                'parent_id',
                'title',
                'details',
                'city_id',
                'district_id',
                'street',
                'house',
                'housing',
                'building',
                'around',
                'liter',
                'gkh_name',
                'developer',
                'housing_estate',
                'rooms',
                'floor',
                'floors',
                'area_all',
                'area_living',
                'area_kitchen',
                'name',
                'desc',
            ],
            Scenario::UPDATE => [
                'id',
                'status',
                'type',
                'price',
                'user_id',
                'geo_lat',
                'geo_lng',
                'alias',
                'deadline',
                'is_vip',
                'parent_id',
                'title',
                'details',
                'city_id',
                'district_id',
                'street',
                'house',
                'housing',
                'building',
                'around',
                'liter',
                'gkh_name',
                'developer',
                'housing_estate',
                'rooms',
                'floor',
                'floors',
                'area_all',
                'area_living',
                'area_kitchen',
                'name',
                'desc',
            ],
        ];
    }

    public function rules()
    {
        return [
            ['id', 'integer'],

            ['status', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнен'],
            ['status', 'integer'],

            ['type', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнен'],
            ['type', 'integer'],

            ['price', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнена'],
            ['price', 'integer', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} только число'],

            ['user_id', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнен'],
            ['user_id', 'integer'],

            ['geo_lat', 'safe'],

            ['geo_lng', 'safe'],

            ['alias', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнен'],
            ['alias', 'filter', 'filter' => 'trim'],
            ['alias', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['alias', 'string', 'max' => 255, 'tooLong'=>'{attribute} максимум 255 символов'],
            ['alias', 'unique',
                'targetClass' => Realty::className(),
                'targetAttribute' => 'alias',
                'filter' => $this->id !== null ? ['!=', 'id', $this->id] : null,
                'message' => '{attribute} занят'
            ],

            ['deadline', 'safe'],

            ['is_vip', 'safe'],

            ['parent_id', 'safe'],
            ['parent_id', 'integer'],

            ['title', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнено'],
            ['title', 'filter', 'filter' => 'trim'],
            ['title', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['title', 'string', 'max' => 255, 'tooLong'=>'{attribute} максимум 255 символов'],

            ['city_id', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнен'],
            ['city_id', 'integer'],

            ['district_id', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнен'],
            ['district_id', 'integer'],

            ['street', 'required', 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'message'=>'{attribute} не заполнена'],
            ['street', 'filter', 'filter' => 'trim'],
            ['street', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['street', 'string', 'max' => 120, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 120 символов'],

            ['house', 'required', 'message'=>'{attribute} не заполнен'],
            ['house', 'filter', 'filter' => 'trim'],
            ['house', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['house', 'string', 'max' => 36, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 36 символов'],

            ['housing', 'filter', 'filter' => 'trim'],
            ['housing', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['housing', 'string', 'max' => 36, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 36 символов'],

            ['building', 'filter', 'filter' => 'trim'],
            ['building', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['building', 'string', 'max' => 36, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 36 символов'],

            ['around', 'filter', 'filter' => 'trim'],
            ['around', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['around', 'string', 'max' => 36, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 36 символов'],

            ['liter', 'filter', 'filter' => 'trim'],
            ['liter', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['liter', 'string', 'max' => 36, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 36 символов'],

            ['gkh_name', 'filter', 'filter' => 'trim'],
            ['gkh_name', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['gkh_name', 'string', 'max' => 36, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 36 символов'],

            ['developer', 'filter', 'filter' => 'trim'],
            ['developer', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['developer', 'string', 'max' => 36, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 36 символов'],

            ['housing_estate', 'filter', 'filter' => 'trim'],
            ['housing_estate', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['housing_estate', 'string', 'max' => 36, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 36 символов'],

            ['rooms', 'integer'],

            ['floor', 'integer'],

            ['floors', 'integer'],

            ['area_all', 'number'],

            ['area_living', 'number'],

            ['area_kitchen', 'number'],

//            ['images', 'each', 'rule' => ['image','minWidth'=>300,'minHeight'=>400,'extensions'=>['png','jpg'],'underWidth'=>'Минимальная ширина 300 пикселей','underHeight'=>'Минимальная высота 400 пикселей','wrongExtension'=>'Только расширения: png, jpg','maxFiles' => 15]],
//            ['images', 'each', 'rule' => ['string', 'max' => 60, 'tooLong'=>'Максимум 60 символа']],

            ['images_desc', 'each', 'rule' => ['string']],
            ['images_desc', 'each', 'rule' => ['filter', 'filter' => 'trim']],
            ['images_desc', 'each', 'rule' => ['filter', '\yii\helpers\HtmlPurifier::process']],

            ['files', 'each', 'rule' => ['file', 'extensions' => 'jpg,png,pdf,doc,docx,xls,xlsx,odf', 'maxFiles' => 8]],
            ['files', 'each', 'rule' => ['string', 'max' => 60, 'tooLong'=>'Максимум 60 символа']],

            ['files_name', 'each', 'rule' => ['filter', 'filter' => 'trim']],
            ['files_name', 'each', 'rule' => ['filter', 'filter'=>'\yii\helpers\HtmlPurifier::process']],
            ['files_name', 'each', 'rule' => ['string', 'max' => 254, 'on'=>[Scenario::CREATE, Scenario::UPDATE], 'tooLong'=>'{attribute} максимум 254 символов']],
        ];
    }

    public function create()
    {
        if (!$this->validate()) {
            return null;
        }

        $this->deadline = strtotime($this->deadline);

        $realty = new Realty();
        $realtyAttr = new RealtyAttr();
        $realtyDesc = new RealtyDesc();
        if(is_numeric($this->id)) {
            $realty = Realty::findOne($this->id);
            $realtyDesc = $realty->desc;
            $realtyAttr = $realty->attr;
            if(!$realty) {
                throw new NotFoundHttpException(\Yii::$app->params['404']);
            }
        }
        $realty->setAttributes($this->getAttributes(['status','type','price','user_id','geo_lat','geo_lng','alias','deadline','parent_id'], []), false);
        $realtyDesc->setAttributes($this->getAttributes(['title','details'], []), false);
        $realtyAttr->setAttributes($this->getAttributes(['city_id','district_id','street','house','housing','building','around','liter','gkh_name','developer','housing_estate','rooms','floor','floors','area_all','area_living','area_kitchen'], []), false);

        $transaction = \Yii::$app->db->beginTransaction();
        $commit = true;

            if($realty->save()) {
                $realtyDesc->link('realty', $realty);
                $realtyAttr->link('realty', $realty);
                if(!$realtyDesc->save() || !$realtyAttr->save()) {
                    $commit = false;
                }
            } else {
                $commit = false;
            }
        if ($commit) {
            $transaction->commit();
            return $realty;
        }
        $transaction->rollBack();
        return null;
    }
    
    public function update(Realty $realty)
    {
        if (!$this->validate()) {
            return null;
        }

        $realty->setAttributes($this->getAttributes(['status','type','price','user_id','geo_lat','geo_lng','alias','deadline','parent_id'], []), false);
        $realty->desc->setAttributes($this->getAttributes(['title','details'], []), false);
        $realty->attr->setAttributes($this->getAttributes(['city_id','district_id','street','house','housing','building','around','liter','gkh_name','developer','housing_estate','rooms','floor','floors','area_all','area_living','area_kitchen'], []), false);

        $transaction = \Yii::$app->db->beginTransaction();
        $commit = true;
        if(!$realty->save() || !$realty->desc->save() || !$realty->attr->save()) {
            $commit = false;
        }
        if ($commit) {
            $transaction->commit();
            return $realty;
        }

        $transaction->rollBack();
        return null;
    }

    /**
     * Пустое объявления в статусе черновик
     */
    public static function CreateEmptyRealtyDraft()
    {
        $realtyId = 0;
        //
        $transaction = \Yii::$app->db->beginTransaction();
        $commit = true;

        $realty = new Realty();
        $realty->status = \common\enum\Realty::STATUS_DRAFT;
        $realty->user_id = \Yii::$app->user->id;
        $realty->parent_id = null;
        if($realty->save()) {
            $realtyDesc = new RealtyDesc();
            $realtyDesc->link('realty', $realty);
            $realtyAttr = new RealtyAttr();
            $realtyAttr->city_id = 1;//Москва
            $realtyAttr->district_id = 1;//Мытище
            $realtyAttr->link('realty', $realty);
            if(!$realtyDesc->save() || !$realtyAttr->save()) {
                $commit = false;
            }
            //
            $realtyId = $realty->id;
        } else {
            $commit = false;
        }

        if ($commit) {
            $transaction->commit();
        }
        $transaction->rollBack();

        return $realtyId;
    }

    public static function ImgForExtension($file='')
    {
        $type = strpos($file, '.') ? explode('.',$file)[1] : '';
        switch ($type) {
            case 'pdf':
                return '/upload/default/doc-pdf.png';
                break;
            case 'doc':
                return '/upload/default/doc-word.png';
                break;
            case 'docx':
                return '/upload/default/doc-word.png';
                break;
            case 'xls':
                return '/upload/default/doc-excel.png';
                break;
            case 'xlsx':
                return '/upload/default/doc-excel.png';
                break;
            case 'ppt':
                return '/upload/default/doc-ppt.png';
                break;
            case 'pptx':
                return '/upload/default/doc-ppt.png';
                break;
            case 'txt':
                return '/upload/default/doc-text.png';
                break;
            default:
                return '/upload/default/doc-not-find-format.png';
                break;
        }
    }
}