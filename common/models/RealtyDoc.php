<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "realty_doc".
 *
 * @property integer $id
 * @property integer $realty_id
 * @property string $name
 * @property string $file
 * @property integer $sort
 *
 * @property Realty $realty
 */
class RealtyDoc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%realty_doc}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'realty_id' => 'Недвижимость',
            'name' => 'Название',
            'file' => 'Файл',
            'sort' => 'Сортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealty()
    {
        return $this->hasOne(Realty::className(), ['id' => 'realty_id']);
    }
}
