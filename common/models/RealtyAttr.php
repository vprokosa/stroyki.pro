<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "realty_attr".
 *
 * @property integer $id
 * @property integer $realty_id
 * @property integer $city_id
 * @property integer $district_id
 * @property string $street
 * @property string $house
 * @property string $housing
 * @property string $building
 * @property string $around
 * @property string $liter
 * @property string $gkh_name
 * @property string $developer
 * @property string $housing_estate
 * @property integer $rooms
 * @property integer $floor
 * @property integer $floors
 * @property string $area_all
 * @property string $area_living
 * @property string $area_kitchen
 *
 * @property City $city
 * @property District $district
 * @property Realty $realty
 */
class RealtyAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%realty_attr}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'realty_id' => 'Недвижимость',
            'city_id' => 'Город',
            'district_id' => 'Район',
            'street' => 'Улица',
            'house' => 'Дом',
            'housing' => 'Корпус',
            'building' => 'Строение',
            'around' => 'Рядом',
            'liter' => 'Литер',
            'gkh_name' => 'ЖКХ',
            'developer' => 'Застройщик',
            'housing_estate' => 'Жилой комплекс',
            'rooms' => 'Комнаты',
            'floor' => 'Этаж',
            'floors' => 'Этажей',
            'area_all' => 'Общая площадь',
            'area_living' => 'Жилая площадь',
            'area_kitchen' => 'Площадь кухни',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(District::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealty()
    {
        return $this->hasOne(Realty::className(), ['id' => 'realty_id']);
    }
}
