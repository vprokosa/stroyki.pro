<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "realty_image".
 *
 * @property integer $id
 * @property integer $realty_id
 * @property string $image
 * @property string $desc
 * @property integer $sort
 *
 * @property Realty $realty
 */
class RealtyImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%realty_image}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'realty_id' => 'Недвижимость',
            'image' => 'Фото',
            'desc' => 'Описание',
            'sort' => 'Сортировка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealty()
    {
        return $this->hasOne(Realty::className(), ['id' => 'realty_id']);
    }
}
