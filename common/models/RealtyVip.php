<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "realty_vip".
 *
 * @property integer $id
 * @property integer $realty_id
 * @property integer $created_at
 * @property integer $end_at
 * @property integer $updated_at
 *
 * @property PaymentVip[] $paymentVips
 * @property Realty $realty
 */
class RealtyVip extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%realty_vip}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'realty_id' => 'Недвижимость',
            'created_at' => 'Создано',
            'end_at' => 'Дата завершения',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentVips()
    {
        return $this->hasMany(PaymentVip::className(), ['realty_vip_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealty()
    {
        return $this->hasOne(Realty::className(), ['id' => 'realty_id']);
    }

    /**
     * срок завершения вип статуса
     */
    public static function FinishDate()
    {

    }
}
