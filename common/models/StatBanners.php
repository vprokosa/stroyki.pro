<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "stat_banners".
 *
 * @property integer $id
 * @property integer $banner_id
 * @property integer $action
 * @property integer $created_at
 *
 * @property Banners $banner
 */
class StatBanners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%stat_banners}}';
    }

    public function behaviors()
    {
        return [
            'Timestamp'=> [
                'class'=>TimestampBehavior::className(),
                'updatedAtAttribute'=>false
            ]

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['banner_id', 'action', 'created_at'], 'integer'],
            [['banner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Banners::className(), 'targetAttribute' => ['banner_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'banner_id' => 'Баннер',
            'action' => 'Действие',
            'created_at' => 'Создано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banners::className(), ['id' => 'banner_id']);
    }
}
