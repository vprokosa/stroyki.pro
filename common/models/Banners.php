<?php

namespace common\models;

use common\enum\Scenario;
use mongosoft\file\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "banners".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $type
 * @property integer $city_id
 * @property string $title
 * @property string $image
 * @property string $link
 * @property integer $sort
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property City $city
 * @property StatBanners[] $statBanners
 */
class Banners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banners}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => \mongosoft\file\UploadImageBehavior::className(),
                'attribute' => 'image',
                'scenarios' => [Scenario::CREATE, Scenario::UPDATE],
                'path' => '@frontend/web/upload/banner/{id}',
                'url' => '@web/upload/banner/{id}',
                'createThumbsOnSave'=>false
            ],
        ];
    }

    public function scenarios()
    {
        return [
            Scenario::CREATE => ['status','type','title','link','city_id','image'],
            Scenario::UPDATE => ['status','title','link','city_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'type' => 'Тип',
            'city_id' => 'Город',
            'title' => 'Название',
            'image' => 'Баннер',
            'link' => 'Ссылка',
            'sort' => 'Сортировка',
            'created_at' => 'Создано',
            'updated_at' => 'Обнавлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatBanners()
    {
        return $this->hasMany(StatBanners::className(), ['banner_id' => 'id']);
    }
}
