<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;

/**
 * This is the model class for table "payment_tariff".
 *
 * @property integer $id
 * @property integer $type
 * @property string $title
 * @property integer $tariff
 * @property string $desc
 * @property integer $created_at
 * @property integer $updated_at
 */
class PaymentTariff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment_tariff}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'tariff', 'title'], 'required', 'message'=>'{attribute} не заполнен'],
            [['type', 'tariff', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 36],
            ['desc', 'filter',  'filter'=>'\yii\helpers\HtmlPurifier::process'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип',
            'title' => 'Название',
            'desc' => 'Описание',
            'tariff' => 'Тариф',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }
    
    public static function Tariff($type=0)
    {
        $res = (new Query())->select('tariff')->from(self::tableName())->where(['type'=>$type])->one();
        if(isset($res['tariff'])) {
            return $res['tariff'];
        }
        return 0;
    }
    public static function Desc($type=0)
    {
        $res = (new Query())->select('desc')->from(self::tableName())->where(['type'=>$type])->one();
        if(isset($res['desc'])) {
            return $res['desc'];
        }
        return 0;
    }
}
