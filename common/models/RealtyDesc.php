<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "realty_desc".
 *
 * @property integer $id
 * @property integer $realty_id
 * @property string $title
 * @property string $details
 *
 * @property Realty $realty
 */
class RealtyDesc extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%realty_desc}}';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'realty_id' => 'Недвижимость',
            'title' => 'Название',
            'details' => 'Дополнительная информация',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealty()
    {
        return $this->hasOne(Realty::className(), ['id' => 'realty_id']);
    }
}
