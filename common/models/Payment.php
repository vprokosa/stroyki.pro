<?php

namespace common\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $system_pay
 * @property string $pay
 * @property integer $user_id
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 * @property PaymentVip[] $paymentVips
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%payment}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'system_pay' => 'Платежная система',
            'pay' => 'Оплата',
            'user_id' => 'Пользователь',
            'comment' => 'Коментарий',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentVip()
    {
        return $this->hasOne(PaymentVip::className(), ['payment_id' => 'id']);
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }


    /**
     * Создать платеж
     */
    public static function Create($realty, $sum, $status=0)
    {
        $transaction = self::getDb()->beginTransaction();
        try{
            $payment = new Payment();
            $payment->status = $status;
            $payment->system_pay = \common\enum\Payment::SYSTEM_YANDEX_MONEY;
            $payment->pay = $sum;
            $payment->user_id = $realty->user_id;
            $payment->comment = 'Оплата vip';
            $payment->save();

            $realtyVip = new RealtyVip();
            $realtyVip->end_at = strtotime("+30 day");
            $realtyVip->link('realty', $realty);
            
            $paymentVip = new PaymentVip();
            $paymentVip->period = 30;
            $paymentVip->realty_vip_id = $realtyVip->id;
            $paymentVip->link('payment', $payment);

            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }
    }
}
