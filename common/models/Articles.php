<?php

namespace common\models;

use common\enum\Scenario;
use mongosoft\file\UploadImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "articles".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $type
 * @property integer $user_id
 * @property integer $city_id
 * @property string $title
 * @property string $alias
 * @property string $text
 * @property integer $created_at
 * @property integer $updated_at
 *
 */
class Articles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%articles}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function scenarios()
    { 
        return [
            Scenario::CREATE => ['title','text','city_id'],
            Scenario::UPDATE => ['title','text','city_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'type' => 'Тип',
            'user_id' => 'Автор',
            'city_id' => 'Город',
            'title' => 'Название',
            'text' => 'Текст',
            'sort' => 'Сортировка',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }
 
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

}
