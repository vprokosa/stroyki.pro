<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "place_city".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property integer $okrug_id
 * @property integer $default
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Okrug $okrug
 * @property District[] $placeDistricts
 * @property RealtyAttr[] $realtyAttrs
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%place_city}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'alias' => 'Псевдоним',
            'okrug_id' => 'Округ',
            'default' => 'По умолчанию',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    public function beforeDelete()
    {
        return parent::beforeDelete();
    }
    public function afterSave($insert, $changedAttributes)
    {
        Yii::$app->cache->delete(Yii::$app->params['cache']['city_list']['key']);
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterDelete()
    {
        Yii::$app->cache->delete(Yii::$app->params['cache']['city_list']['key']);
        parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkrug()
    {
        return $this->hasOne(Okrug::className(), ['id' => 'okrug_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistricts()
    {
        return $this->hasMany(District::className(), ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRealtyAttrs()
    {
        return $this->hasMany(RealtyAttr::className(), ['city_id' => 'id']);
    }
}
