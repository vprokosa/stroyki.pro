<?php
namespace frontend\modules\mortgage_calculation\models;

use yii\base\Model;
use Yii;

/**
 * Ипотечный калькулятор
 */
class CalculatorForm extends Model
{
    public $price;
    public $deposit;
    public $sum;
    public $period;
    public $rate;

    public function attributeLabels()
    {
        return [
            'price'=>'Цена квартиры',
            'deposit'=>'Первоначальный взнос',
            'sum'=>'Сумма кредита',
            'period'=>'Срок кредита ( лет )',
            'rate'=>'Ипотечная ставка в %',
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['price', 'required', 'message'=>'{attribute} не заполнен'],
            ['price', 'number'],

            ['deposit', 'required', 'message'=>'{attribute} не заполнен'],
            ['deposit', 'number'],

            ['sum', 'required', 'message'=>'{attribute} не заполнен'],
            ['sum', 'number'],

            ['period', 'required', 'message'=>'{attribute} не заполнен'],
            ['period', 'number'],

            ['rate', 'required', 'message'=>'{attribute} не заполнен'],
            ['rate', 'number'],
        ];
    }

    /**
     * Рассчитать
     */
    public function calculation()
    {
        $time = ($this->period+1) * 12;
        $interest = ($this->rate+1)/1200;
        $result = $this->sum * ($interest * pow(1+$interest, $time)) / (pow(1+$interest, $time)-1);

        return Yii::$app->formatter->asDecimal($result, 0);
    }
}
