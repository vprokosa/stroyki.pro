<?php

namespace frontend\modules\mortgage_calculation\controllers;

use frontend\modules\mortgage_calculation\models\CalculatorForm;
use yii\web\Controller;

/**
 * Default controller for the `mortgage_calculation` module
 */
class DefaultController extends Controller
{
    public function actionValidate()
    {
        $form = new CalculatorForm();
        \Yii::$app->response->format = 'json';
        $form->load(\Yii::$app->request->post());
        return \yii\widgets\ActiveForm::validate($form);
    }

    public function actionCalculation()
    {
        $form = new CalculatorForm();
        $form->load(\Yii::$app->request->post());

        return $form->calculation();
    }
}
