<?php

namespace frontend\modules\mortgage_calculation\widgets;

use frontend\modules\mortgage_calculation\models\CalculatorForm;
use yii\base\Widget;

class CalculationWidget extends Widget
{
    public $price;
    public $title;
    
    public function run()
    {
        $model = new CalculatorForm();
        $model->price = $this->price;
        $model->deposit = $this->price/2;
        $model->sum = $model->price - $model->deposit;
        $model->period = 11;
        $model->rate = 11;
        $result = $model->calculation();

        return $this->render('form', [
            'model'=>$model,
            'title'=>$this->title,
            'result'=>$result
        ]);
    }
    

}