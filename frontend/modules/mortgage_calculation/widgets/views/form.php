<div class="container">
    <div class="object-page">
        <?php $form = \yii\widgets\ActiveForm::begin([
            'id'=>'form-mortgage-calculator',
            'action'=>['mortgage_calculation/default/calculation'],
            'validationUrl' => ['mortgage_calculation/default/validate'],
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]) ?>
        <div class="s s4">
            <div class="sTitle">
                <h2 class="h2">Ипотечный калькулятор</h2>
            </div>
            <div class="sContent">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form">
                            <div class="input">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="calculationLabel" for="price"><?= $model->getAttributeLabel('price') ?></label>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'price')->textInput(['onkeyup'=>'CalculationI()', 'class' => false])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="calculationLabel" for="deposit"><?= $model->getAttributeLabel('deposit') ?></label>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'deposit')->textInput(['onkeyup'=>'CalculationI()', 'class' => false])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="calculationLabel" for="sumCredit"><?= $model->getAttributeLabel('sum') ?></label>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'sum')->textInput(['onkeyup'=>'CalculationI()', 'class' => false])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="calculationLabel" for="creditYears"><?= $model->getAttributeLabel('period') ?></label>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'period')->dropDownList([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],['prompt'=>'','onchange'=>'CalculationI()'])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="calculationLabel" for="rateIn"><?= $model->getAttributeLabel('rate') ?></label>
                                    </div>
                                    <div class="col-md-6">
                                        <?= $form->field($model, 'rate')->textInput(['onkeyup'=>'CalculationI()', 'class' => false])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="input">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="calculationLabel" for="resultCredit">Ежемесячный платёж (руб)</label>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" value="<?= $result ?>" disabled="disabled" class="resultCredit" id="calculation-result">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-info">
                            <p>Рассчитанная сумма ипотечного
                                кредита с использованием
                                ипотечного калькулятора
                                являются приблизительной и
                                приводятся в исключительно ,
                                справочных целях .
                                И не гарантируют предоставление
                                вам Банком ипотечного кредита
                                на указанных условиях. </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="orange-block">
                <div class="block-content">
                    <div class="text">строго<br/>300 на 250</div>
                </div>
            </div>
        </div>
        <?php \yii\widgets\ActiveForm::end() ?>
    </div>
</div>