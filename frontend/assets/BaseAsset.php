<?php

namespace frontend\assets;


use yii\web\AssetBundle;

class BaseAsset extends AssetBundle
{
    public $jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    /*public $css = [
        '/min/g=base.css',
        '/plugins/font-awesome/css/font-awesome.min.css',
    ];

    public $js = [
        '/min/g=base.js',
    ];*/
}