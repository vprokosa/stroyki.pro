<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/plugins/font-awesome/css/font-awesome.min.css',
        '/css/my.css',
        '/css/site_global.css',
        '/css/front.css',
        '/css/libs.min.css',
        '/css/main.css' 
    ];
    public $js = [
        '/js/jquery.cookie.js',
        '/js/app.js',
        '/js/libs.js',
        '/js/common.js'
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'frontend\assets\YMapAsset',
    ];

    /*public $css = [
        '/min/g=front.plugins.css',
        '/min/g=front.css',
    ];
    public $js = [
        '/min/g=front.plugins.js',
        '/min/g=front.js',
    ];
    public $depends = [
        'frontend\assets\BaseAsset',
    ];*/
}
