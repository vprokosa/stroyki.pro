<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ObjAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '/min/g=front.plugins.css',
        '/min/g=front.css',
		'//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
		'//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css',
		'/css/bootstrap.min.css',
		'/css/styles.css',
    ];
    public $js = [
        '/min/g=front.plugins.js',
        '/min/g=front.js',
		//'//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
		//'//code.jquery.com/jquery-1.12.4.js',
		//'//code.jquery.com/ui/1.12.1/jquery-ui.js',
		//'/js/bootstrap.min.js',
		//'/js/main.js',
    ];
    public $depends = [
        'frontend\assets\BaseAsset',
    ];
}
