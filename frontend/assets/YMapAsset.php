<?php

namespace frontend\assets;


use yii\web\AssetBundle;

class YMapAsset extends AssetBundle
{
    public $jsOptions = [ 'position' => \yii\web\View::POS_HEAD ];

    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];

    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
    ];
}