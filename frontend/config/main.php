<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config =  [
    'id' => 'app-frontend',
    'name' =>'Realty',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'thumbnail'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'mortgage_calculation' => [
            'class' => 'frontend\modules\mortgage_calculation\Module',
        ],
				 'debug' => [
           // 'class' => 'yii\debug\Module',
            //'allowedIPs' => ['37.21.80.160', '::1']
        ]
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'loginUrl'=>'/login',
            'identityCookie' => [
				'name' => '_frontendUser', 
				'path'=>'/frontend/web',
				'httpOnly' => true
			],
        ],
        'session' => [
            'name' => '_frontendSessionId', // unique for frontend
			'savePath' => __DIR__ . '/../runtime', // a temporary folder on frontend
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
		/* 'assetManager' => [
            'appendTimestamp' => true,
			//'linkAssets' => true,
        ], */
		'cache' => [ 
            'class' => 'yii\caching\FileCache',
			//'defaultDuration'=>3600,
        ],/*  */ 
        'urlManager' => require(Yii::getAlias('@common/config/urlManager.php')),
    ],
    'params' => $params,
];
/* if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*'],
    ];
} */
return $config;
