<?php


return [
    'baseUrl' => '/',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => true,
	'cache' => false,

    'rules' => [
        // ['class' => 'frontend\components\RealtyUrlRule'],
        'con' => 'site/test',
        '/'=>'site/index',
        'login'=>'user/login',
        'logout'=>'user/logout',
        'signup'=>'user/signup',
        'page'=>'/',
        'page/<alias:(.+)>'=>'site/page',
        'advertisement'=>'/',
        //'advertisement/<id:(.+)>'=>'site/advertisement',
        'novostroyki'=>'site/search-objects',
        'zastroyshchiki'=>'site/search-objects',
        'jilie-kompleksi'=>'site/search-objects',
        'change-city'=>'site/change-city',
        //'search-objects-on-map'=>'site/search-objects-on-map',
        'all/<user_id:(\d)>'=>'site/all-realty',//все объявления по пользователю
		['class' => \frontend\components\RealtyUrlRule::class],
        '<controller>' => '<controller>/index',
        '<controller>/<action>' => '<controller>/<action>',
        '<modules>/<controller>/<action>' => '<modules>/<controller>/<action>',
		
		/* 
		'<city_id:\w+>' => 'site/search-objects',
		'<city:\w+>/<type:\w+>' => 'site/search-objects',
		'<city:\w+>/<type:\w+>/<rooms:\w+>' => 'site/search-objects',
		'<city:\w+>/<type:\w+>/<rooms:\w+>/<district:\w+>' => 'site/search-objects',

        '/'=>'site/index',
        //'novostroyki/<page:(\d)>/<per-page:(\d)>'=>'site/search-objects',
		//'zastroyshchiki/<ObjectSearch:\w+>' => 'site/search-objects',
        'search-objects'=>'site/search-objects',  */
    ]
];