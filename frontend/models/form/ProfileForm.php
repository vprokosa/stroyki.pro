<?php

namespace frontend\models\form;

use common\models\User;
use yii\base\Model;

class ProfileForm extends Model
{
    public $full_name;
    public $username;
    public $type;
    public $email;
    public $phone;
    public $site;
    public $company;
    public $buildcompany;
    public $city;
    public $street;
    public $house;
    public $office;
    public $address;

    public function attributeLabels()
    {
        return (new User())->attributeLabels();
    }

    public function rules()
    {
        return [ 
            ['full_name', 'required', 'message'=>'{attribute} не заполнен'],
            ['full_name', 'filter', 'filter' => 'trim'],
            ['full_name', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['full_name', 'string', 'min' => 3, 'max' => 64, 'tooShort'=>'{attribute} минимум 3 символов', 'tooLong'=>'{attribute} максимум 64 символов'],

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['username', 'required', 'message'=>'{attribute} не заполнен'],
            ['username', 'unique',
                'targetClass' => User::className(),
                'targetAttribute' => 'username',
                'filter' => \Yii::$app->user->id !== null ? ['!=', 'id', \Yii::$app->user->id] : null,
                'message' => '{attribute} занят'
            ],
            ['username', 'string', 'min' => 2, 'max' => 36, 'tooShort'=>'{attribute} минимум 3 символов', 'tooLong'=>'{attribute} максимум 36 символов'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['email', 'required', 'message'=>'{attribute} не заполнен'],
            ['email', 'email', 'message'=>'{attribute} не верный'],
            ['email', 'unique',
                'targetClass' => User::className(),
                'targetAttribute' => 'email',
                'filter' => \Yii::$app->user->id !== null ? ['!=', 'id', \Yii::$app->user->id] : null,
                'message' => '{attribute} занят'
            ],
            ['email', 'string', 'max' => 80, 'tooLong'=>'{attribute} максимум 80 символов'],

            ['type', 'safe'],

            ['phone', 'string', 'max' => 32, 'tooLong'=>'{attribute} максимум 32 символов'],
            ['phone', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],

            ['site', 'string', 'max' => 32, 'tooLong'=>'{attribute} максимум 64 символов'],
            ['site', 'url'],
            ['site', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],

            ['company', 'string', 'max' => 5000, 'tooLong'=>'{attribute} максимум 5000 символов'],
            ['company', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],

            ['buildcompany', 'string', 'max' => 255, 'tooLong'=>'{attribute} максимум 255 символов'],
            ['buildcompany', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
			
            ['city', 'string', 'max' => 60, 'tooLong'=>'{attribute} максимум 60 символов'],
            ['city', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],

            ['street', 'string', 'max' => 60, 'tooLong'=>'{attribute} максимум 60 символов'],
            ['street', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],

            ['house', 'string', 'max' => 60, 'tooLong'=>'{attribute} максимум 60 символов'],
            ['house', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],

            ['office', 'string', 'max' => 60, 'tooLong'=>'{attribute} максимум 60 символов'],
            ['office', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],

            ['address', 'string', 'max' => 255, 'tooLong'=>'{attribute} максимум 255 символов'],
            ['address', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],


        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }
        $user = \Yii::$app->user->identity;
        $user->setScenario('editProfile');
        $attributes = $this->getAttributes(null);
        $user->setAttributes($attributes, false);
        
        return $user->save() ? $user : null;
    }
}