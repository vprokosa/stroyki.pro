<?php

namespace frontend\models\form;

use common\models\User;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class LogoForm extends Model
{
    public $logo;

    public function attributeLabels()
    {
        return (new User())->attributeLabels();
    }
    
    public function rules()
    {
        return [
            ['logo', 'image',
                'skipOnEmpty' => false,
                'extensions'=> ['png','jpg'], 'wrongExtension'=>'{attribute} расширения: png',
//                'minWidth'  => 200, 'underWidth'=>'{attribute}, допустимая ширина: 200px',
//                'maxWidth'  => 200, 'overWidth'=>'{attribute}, допустимая ширина: 200px',
//                'minHeight' => 120, 'underHeight'=>'{attribute}, допустимая высота: 120px',
//                'maxHeight' => 120, 'overHeight'=>'{attribute}, допустимая высота: 120px',
            ],
        ];
    }

    public function save()
    {
        $this->logo = UploadedFile::getInstance($this, 'logo');
        if (!$this->validate()) {
            return null;
        }
        $user = \Yii::$app->user->identity->attachUploadImageBehavior('logo');
        $user->logo = $this->logo;
        $user->setScenario('logo');
        $user->save();
        $user->detachUploadImageBehavior();

        return true;
    }
}