<?php

namespace frontend\models\form;

use common\models\User;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $old_password;
    public $password;
    public $password_repeat;

    public function attributeLabels()
    {
        return [
            'old_password'=>'Старый пароль',
            'password'=>'Новый пароль',
            'password_repeat'=>'Повторить новый пароль'
        ];
    }


    public function rules()
    {
        return [
            [['old_password','password','password_repeat'], 'required', 'message'=>'{attribute} не заполнен'],
            ['old_password', 'validatePassword'],
            ['password', 'string', 'min' => 6, 'tooShort'=>'{attribute} минимум 6 символов'],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Пароли не совпадают" ],
        ];
    }

    public function validatePassword()
    {
        $user = \Yii::$app->user->identity;
        if (!$user || !$user->validatePassword($this->old_password)) {
            $this->addError('old_password', 'Неверный пароль');
        }
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }
        /**
         * @var User $user
         */
        $user = \Yii::$app->user->identity;
        $user->setScenario('changePass');
        $user->setPassword($this->password);
        $user->generateAuthKey();
        if($user->save()) {
            \Yii::$app->user->identity = $user;
            return $user;
        }
        return null;
    }
}