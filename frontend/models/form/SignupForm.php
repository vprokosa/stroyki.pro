<?php
namespace frontend\models\form;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $full_name;
    public $username;
    public $email;
    public $password;
    public $confirm_password;
    public $accept;
    public $recaptcha;

    public function attributeLabels()
    {
        return [
            'full_name'=>'Имя',
            'username'=>'Логин',
            'email'=>'Email',
            'password'=>'Пароль',

        ];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['full_name', 'safe'],

         /*   ['username', 'filter', 'filter' => 'trim'],
            ['username', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['username', 'required', 'message'=>'{attribute} не заполнен'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => '{attribute} занят'],
            ['username', 'string', 'min' => 2, 'max' => 36, 'tooShort'=>'{attribute} минимум 3 символов', 'tooLong'=>'{attribute} максимум 36 символов'],*/

            ['username', 'safe'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'filter', 'filter'=>'\yii\helpers\HtmlPurifier::process'],
            ['email', 'required', 'message'=>'{attribute} не заполнен'],
            ['email', 'email', 'message'=>'{attribute} не верный'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => '{attribute} занят'],
            ['email', 'string', 'max' => 80, 'tooLong'=>'{attribute} максимум 80 символов'],


            ['password', 'required', 'message'=>'{attribute} не заполнен'],
            ['password', 'string', 'min' => 6, 'tooShort'=>'{attribute} минимум 6 символов'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],
            ['accept', 'required', 'requiredValue' => 1, 'message' => 'Вы должны согласиться с условиями'],

//            ['recaptcha', 'required', 'message'=>'{attribute} не заполнен'],
        //    ['recaptcha', 'captcha', 'message'=>'{attribute} не верный'],
            [['recaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LeCdCMUAAAAALiF-WxZzZLCMJxMtVjo4zHEyUcv', 'uncheckedMessage' => 'Please confirm that you are not a bot.']
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User;
        $user->setScenario('signup');
        $attributes = $this->getAttributes(null);
        $user->setAttributes($attributes, false);
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }
}
