<?php
namespace frontend\models\form;

use common\models\User;
use yii\base\InvalidParamException;
use yii\base\Model;
use Yii;

/**
 * Password reset form
 */
class ResetPasswordForm extends Model
{
    public $password;

    /**
     * @var \common\models\User
     */
    private $_user;

    public function attributeLabels()
    {
        return [
            'password'=>'Пароль'
        ];
    }
    /**
     * Creates a form model given a token.
     *
     * @param  string                          $token
     * @param  array                           $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Маркер сброса пароля не может быть пустым.');
        }
        $this->_user = User::findByPasswordResetToken($token);
        if (!$this->_user) {
            throw new InvalidParamException('Маркер сброса пароля неверный.');
        }
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'required', 'message'=>'{attribute} не заполнен'],
            ['password', 'string', 'min' => 6, 'tooShort'=>'{attribute} минимум 6 символов'],
        ];
    }

    /**
     * Resets password.
     *
     * @return boolean if password was reset.
     */
    public function resetPassword()
    {
        if (!$this->validate()) {
            return null;
        }
        /**
         * @var User $user
         */
        $user = $this->_user;
        $user->setScenario('resetPass');
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->removePasswordResetToken();
        if($user->save()) {
            \Yii::$app->user->identity = $user;
            return $user;
        }
        return null;
    }
}
