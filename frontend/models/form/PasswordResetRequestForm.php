<?php
namespace frontend\models\form;

use common\models\User;
use yii\base\Model;

class PasswordResetRequestForm extends Model
{
    public $email;

    public function attributeLabels()
    {
        return [
            'email'=>'Email',
        ];
    }


    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message'=>'{attribute} не заполнен'],
            ['email', 'email', 'message'=>'{attribute} не верный'],
            ['email', 'exist',
                'targetClass' => User::className(),
                'filter' => ['status' => \common\enum\User::STATUS_ACTIVE],
                'message' => '{attribute} не найден'
            ],
        ];
    }

    public function sendEmail()
    {

        /* @var $user User */
        $user = User::findOne([
            'status' => \common\enum\User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);
        if ($user) {
            if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
                $user->generatePasswordResetToken();
            }
            $user->setScenario('generateToken');

            if ($user->save()) {
                return \Yii::$app->mailer->compose([
                    'html' => 'passwordResetToken',
                ], ['user' => $user])
                    ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->params['sitename'] . ' восстановление пароля'])
                    ->setTo($this->email)
                    ->setSubject('Восстановление пароля на сайте ' . \Yii::$app->params['sitename'])
                    ->send();
            }
        }

        return false;
    }
}
