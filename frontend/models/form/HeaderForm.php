<?php

namespace frontend\models\form;

use common\models\User;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class HeaderForm extends Model
{
    public $header;

    public function attributeLabels()
    {
        return (new User())->attributeLabels();
    }
    
    public function rules()
    {
        return [
            ['header', 'image',
                'skipOnEmpty' => false,
                'extensions'=> ['png','jpg'], 'wrongExtension'=>'{attribute} расширения: png',
//                'minWidth'  => 960, 'underWidth'=>'{attribute}, допустимая ширина: 960px',
//                'maxWidth'  => 960, 'overWidth'=>'{attribute}, допустимая ширина: 960px',
//                'minHeight' => 370, 'underHeight'=>'{attribute}, допустимая высота: 370px',
//                'maxHeight' => 370, 'overHeight'=>'{attribute}, допустимая высота: 370px',
            ],
        ];
    }

    public function save()
    {
        $this->header = UploadedFile::getInstance($this, 'header');
        
        if (!$this->validate()) {
            return null;
        }

        $user = \Yii::$app->user->identity->attachUploadImageBehavior('header');
        $user->header = $this->header;
        $user->setScenario('header');
        $user->save();
        $user->detachUploadImageBehavior();

        return true;
    }
}