<?php

namespace frontend\models\form;

use common\models\User;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

class AvatarForm extends Model
{
    public $avatar;

    public function attributeLabels()
    {
        return (new User())->attributeLabels();
    }
    
    public function rules()
    {
        return [
            ['avatar', 'image',
                'skipOnEmpty' => false,
                'extensions'=> ['png','jpg'], 'wrongExtension'=>'{attribute} расширения: png',
//                'minWidth'  => 512, 'underWidth'=>'{attribute}, допустимая ширина: 512px',
//                'maxWidth'  => 512, 'overWidth'=>'{attribute}, допустимая ширина: 512px',
//                'minHeight' => 512, 'underHeight'=>'{attribute}, допустимая высота: 512px',
//                'maxHeight' => 512, 'overHeight'=>'{attribute}, допустимая высота: 512px',
            ],
        ];
    }

    public function save()
    {
        $this->avatar = UploadedFile::getInstance($this, 'avatar');
        
        if (!$this->validate()) {
            return null;
        }

        $user = \Yii::$app->user->identity->attachUploadImageBehavior('avatar');
        $user->avatar = $this->avatar;
        $user->setScenario('avatar');
        $user->save();
        $user->detachUploadImageBehavior();

        return true;
    }
}