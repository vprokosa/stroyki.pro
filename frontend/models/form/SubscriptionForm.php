<?php

namespace frontend\models\form;

use common\models\User;
use yii\base\Model;

class SubscriptionForm extends Model
{
    public $sending;

    public function attributeLabels()
    {
        return [
            'sending'=>'Рассылка'
        ];
    }


    public function rules()
    {
        return [
            ['sending', 'safe'],
        ];
    }

    public function save()
    {
        if (!$this->validate()) {
            return null;
        }
        /**
         * @var User $user
         */
        $user = \Yii::$app->user->identity;
        $user->sending = $this->sending;
        if($user->update(false, ['sending'])) {
            return $user;
        }
        return null;
    }
}