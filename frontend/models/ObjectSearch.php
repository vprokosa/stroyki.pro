<?php

namespace frontend\models;

use common\enum\User;
use common\models\SubscriptionSearch;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Realty;
use common\models\RealtyAttr;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

/**
 * RealtySearch represents the model behind the search form about `common\models\Realty`.
 */
class ObjectSearch extends Realty
{
    const OPTION_ALL = 1; //все
    const OPTION_VIP = 2; //vip
    const OPTION_CREATOR = 3; //Застройщик

    const SORT_DEFAULT_DESC = 0; //цена по убыванию

    const SORT_PRICE_ASC = 1; //цена по убыванию
    const SORT_PRICE_DESC = 2; //цена по возрастанию
    const SORT_DATE_ASC = 3; //дата по убыванию
    const SORT_DATE_DESC = 4; //дата по возрастанию

    public $user_id;
	
    public $city_id;

    public $deadline1;
    public $deadline2;
    public $deadline3;
    public $deadline4;
    
    public $room1;
    public $room2;
    public $room3;
    public $room4;

    public $start_area;
    public $end_area;

    public $start_price;
    public $end_price;
    public $area_square;
    public $area_total;

    public $district_id;

    public $option;
    public $housing_estate;

    public $sort = self::SORT_DEFAULT_DESC;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id','user_id','start_area','end_area','start_price','end_price','option','housing_estate','sort'], 'integer'],
            [['deadline1','deadline2','deadline3','deadline4','room1','room2','room3','room4','area_square','area_total','district_id'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'deadline1'=>'Сдан',
            'deadline2'=>'2016',
            'deadline3'=>'2017',
            'deadline4'=>'2018+',
            'room1'=>'1',
            'room2'=>'2',
            'room3'=>'3',
            'room4'=>'4+',
            'start_area'=>'От',
            'end_area'=>'До',
            'start_price'=>'От',
            'end_price'=>'До',
            'area_square'=>'За м2',
            'area_total'=>'общая',
            'district_id'=>'Район',
            'option'=>'Тип',
            'housing_estate' => 'Жилой комплекс',
            'sort'=>'Сортировка',
            'user_id'=>'Автор',
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchFilter($filter, $pagination = true, $pageSize = 10)
    {
        $query = Realty::find()->joinWith(['attr', 'user', 'images' ,'attr.city', 'attr.district'], true, 'INNER JOIN');

        $pagination = [];

        if($pageSize) {
            $pagination = [
                'pagination' => [
                    'pageSize' => $pageSize,
                ]
            ];
        }else {
            $pagination = [
                'pagination' => false
            ];
        }

        $dataProvider = new ActiveDataProvider(ArrayHelper::merge(
            ['query' => $query],
            $pagination
        ));


        if(isset($filter['ObjectSearch'])) {
            Yii::$app->response->cookies->add(new Cookie([
                'name' => 'searchFilter',
                'value' => serialize($filter['ObjectSearch']),
                'expire' => time() + 86400 * 365
            ]));
            $this->load($filter);
        } else {
            if(isset($_COOKIE['searchFilter'])) {
                $filter = unserialize(Yii::$app->request->cookies->getValue('searchFilter'));
                // TODO подставлять город из кук
                $this->city_id = !(empty($filter['city_id'])) ? $filter['city_id'] : 1;
                $this->district_id = !empty($params['district_id']) ? $filter['district_id'] : 0;
                $this->sort = !empty($filter['sort']) ? $filter['sort'] : self::SORT_DEFAULT_DESC;

                $this->room4 = !empty($filter['room4']) ? 1 : '';
                $this->room3 = !empty($filter['room3']) ? 1 : '';
                $this->room2 = !empty($filter['room2']) ? 1 : '';
                $this->room1 = !empty($filter['room1']) ? 1 : '';
                $this->deadline1 = !empty($filter['deadline1']) ? 1 : '';
                $this->deadline2 = !empty($filter['deadline2']) ? 1 : '';
                $this->deadline3 = !empty($filter['deadline3']) ? 1 : '';
                $this->deadline4 = !empty($filter['deadline4']) ? 1 : '';
                $this->start_area = !empty($filter['start_area']) ? $filter['start_area'] : '';
                $this->end_area = !empty($filter['end_area']) ? $filter['end_area'] : '';
                $this->start_price = !empty($filter['start_price']) ? $filter['start_price'] : '';
                $this->end_price = !empty($filter['end_price']) ? $filter['end_price'] : '';
                $this->option = !empty($filter['option']) ? $filter['option'] : '';
            }
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        /**
         * Фильтр по комнатам
         */
        $rooms = [];
        if ($this->room4 === 1) {
            $rooms[] = 4;
            $rooms[] = 5;
            $rooms[] = 6;
            $rooms[] = 7;
            $rooms[] = 8;
            $rooms[] = 9;
            $rooms[] = 10;
        }

        if ($this->room3 === 1) $rooms[] = 3;
        if ($this->room2 === 1) $rooms[] = 2;
        if ($this->room1 === 1) $rooms[] = 1;

        $query->andFilterWhere([
            'realty.status' => \common\enum\Realty::STATUS_PUBLIC,
            'realty_attr.rooms' => $rooms
        ]);


        /**
         * Фильтр по сроку сдачи
         * @TODO переделать получение timestamp
         */
        $deadlines = ['or'];

        if ($this->deadline1 === 1) {
            $deadlines[] = ['<=', 'realty.deadline', time()];
        }

        if($this->deadline2 === 1){
            $deadlines[] = ['and', ['<=', 'realty.deadline', 1483228740], ['>=', 'realty.deadline', 1451606400]];
        }
        if($this->deadline3 === 1){
            $deadlines[] = ['and', ['<=', 'realty.deadline', 1514764740], ['>=', 'realty.deadline', 1483228800]];
        }

        if($this->deadline4 === 1){
            $deadlines[] = ['>=', 'realty.deadline', 1514764800];
        }

        if(count($deadlines) > 1){
            $query->andFilterWhere($deadlines);
        }

        /**
         * Фильтр по городу
         */
        if (!empty($this->city_id)){
            $query->andFilterWhere([
                'realty_attr.city_id' => (int)$this->city_id
            ]);
        }


        /**
         * Фильтр по району
         */
        if (isset($this->district_id) && (int)$this->district_id > 0){
            $query->andFilterWhere([
                'realty_attr.district_id' => (int)$this->district_id,
            ]);
        }

        /**
         * Фильтр по общей площади
         */
        $query->andFilterWhere(['>=', 'realty_attr.area_all', $this->start_area]);
        $query->andFilterWhere(['<=', 'realty_attr.area_all', $this->end_area]);

        /**
         * Фильтр по цене
         */
        $query->andFilterWhere(['>=', 'price', $this->start_price]);
        $query->andFilterWhere(['<=', 'price', $this->end_price]);

        /**
         * Сортировка
         */
        $sort = ['vip' => SORT_DESC];
        switch ($this->sort) {
            case self::SORT_PRICE_ASC:
                $sort = ArrayHelper::merge($sort, ['price' => SORT_ASC]);
                break;
            case self::SORT_PRICE_DESC:
                $sort = ArrayHelper::merge($sort, ['price' => SORT_DESC]);
                break;
            case self::SORT_DATE_ASC:
                $sort = ArrayHelper::merge($sort, ['created_at' => SORT_ASC]);
                break;
            case self::SORT_DATE_DESC:
                $sort = ArrayHelper::merge($sort, ['created_at' => SORT_DESC]);
                break;
            case self::SORT_DEFAULT_DESC:
                $sort = ArrayHelper::merge($sort, ['created_at' => SORT_DESC]);
        }

        $query->orderBy($sort);

        //print_r($query->createCommand()->getRawSql());exit;
        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $paginationShow=true, $isOnMap = false, $pageSize = 10)
    {

        //var_dump($params);

        $query = Realty::find()->joinWith(['attr', 'user', 'images' ,'attr.city', 'attr.district'], true, 'INNER JOIN');
		/*  echo'<pre>params';
		print_r($params);
		echo'</pre>';*/

        $pagination = [];
        if($pageSize) {
            $pagination = [
                'pagination' => [
                    'pageSize' => $pageSize,
                ]
            ];
        }else {
            $pagination = [
                'pagination' => false
            ];
        }

        $dataProvider = new ActiveDataProvider(ArrayHelper::merge(
            ['query' => $query],
            $pagination
        ));

        //if(\Yii::$app->request->isGet && isset($_GET['type']) && $_GET['type']==User::TYPE_CREATOR) {
			
            $this->option = isset($params['ObjectSearch']['option']) ? $params['ObjectSearch']['option'] : 0;
			if(!empty($this->option)){
				//$query->andFilterWhere(['users.type'=>$this->option]);
			}
            $this->housing_estate = isset($params['ObjectSearch']['housing_estate']) ? $params['ObjectSearch']['housing_estate'] : 0;
			if(!empty($this->housing_estate)){
				//$query->andFilterWhere(['not', 'realty_attr.housing_estate=""']);
			}
       // }elseif(\Yii::$app->request->isGet && isset($_GET["housing_estate"])){
        //} else {


            if(isset($params['ObjectSearch'])) {
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => 'search_realty',
                    'value' => serialize($params['ObjectSearch']),
                    'expire' => time() + 86400 * 365
                ]));
                $this->load($params);
            } else {
                if(isset($_COOKIE['search_realty'])) {
                    $params = unserialize(Yii::$app->request->cookies->getValue('search_realty'));
                    $this->city_id = isset($params['city_id']) ? $params['city_id'] : '';
                    $this->housing_estate = isset($params['housing_estate']) ? $params['housing_estate'] : 0;
                    $this->room4 = isset($params['room4']) && $params['room4'] == 1 ? 1 : '';
                    $this->room3 = isset($params['room3']) && $params['room3'] == 1 ? 1 : '';
                    $this->room2 = isset($params['room2']) && $params['room2'] == 1 ? 1 : '';
                    $this->room1 = isset($params['room1']) && $params['room1'] == 1 ? 1 : '';
                    $this->deadline1 = isset($params['deadline1']) && $params['deadline1'] == 1 ? 1 : '';
                    $this->deadline2 = isset($params['deadline2']) && $params['deadline2'] == 1 ? 1 : '';
                    $this->deadline3 = isset($params['deadline3']) && $params['deadline3'] == 1 ? 1 : '';
                    $this->deadline4 = isset($params['deadline4']) && $params['deadline4'] == 1 ? 1 : '';
                    $this->start_area = isset($params['start_area']) ? $params['start_area'] : '';
                    $this->end_area = isset($params['end_area']) ? $params['end_area'] : '';
                    $this->start_price = isset($params['start_price']) ? $params['start_price'] : '';
                    $this->end_price = isset($params['end_price']) ? $params['end_price'] : '';
                    $this->option = isset($params['option']) ? $params['option'] : '';
                    $this->sort = isset($params['sort']) ? $params['sort'] : '';
                }
				if(isset($_COOKIE['cityID'])) {
					$this->city_id = $_COOKIE['cityID'];
				}
            }

           

            if (!$this->validate()) {
                // uncomment the following line if you do not want to return any records when validation fails
                // $query->where('0=1');
                return $dataProvider;
            }

            $rooms = [];
            if($this->room4==1) {
                $rooms[] = 4;
                $rooms[] = 5;
                $rooms[] = 6;
                $rooms[] = 7;
                $rooms[] = 8;
                $rooms[] = 9;
                $rooms[] = 10;
            }
            if($this->room3==1) $rooms[] = 3;
            if($this->room2==1) $rooms[] = 2;
            if($this->room1==1) $rooms[] = 1;

            $deadlines = array("or");

            if($this->deadline1==1) {
                $deadlines[] = ['<=', 'realty.deadline', time()];
                //$query->andFilterWhere(['<=', 'realty.deadline', time()]);//deadline < current_time
            }
            
            if($this->deadline2==1){
                $deadlines[] = ['and', ['<=', 'realty.deadline', 1483228740], ['>=', 'realty.deadline', 1451606400]];
               // $query->andFilterWhere(['<=', 'realty.deadline', 1483228740]);//deadline < 2016
                //$query->andFilterWhere(['>=', 'realty.deadline', 1451606400]);//deadline > 2016
            }
            if($this->deadline3==1){
                $deadlines[] = ['and', ['<=', 'realty.deadline', 1514764740], ['>=', 'realty.deadline', 1483228800]];
                //$query->andFilterWhere(['<=', 'realty.deadline', 1514764740]);//deadline < 2017
               // $query->andFilterWhere(['>=', 'realty.deadline', 1483228800]);//deadline > 2017
                //2017 range
            }

            if($this->deadline4==1){
                $deadlines[] = ['>=', 'realty.deadline', 1514764800];
                //$query->andFilterWhere(['>=', 'realty.deadline', 1514764800]);//deadline > 2018
            }

            if(count($deadlines) > 1){
                $query->andFilterWhere($deadlines);
            }


            $query->andFilterWhere([
                'realty.status' => \common\enum\Realty::STATUS_PUBLIC,
                'realty_attr.rooms' => $rooms
            ]);
			if(!empty($this->city_id)){
				$query->andFilterWhere([
					'realty_attr.city_id' => (int)$this->city_id
				]);
			}

            if(isset($this->housing_estate) && (int)$this->housing_estate > 0){
				$query->andFilterWhere([
					'not', 'realty_attr.housing_estate=""'
				]);
            }
			
            if(isset($this->district_id) && (int)$this->district_id > 0){
                $query->andFilterWhere([
                    'realty_attr.district_id' => (int)$this->district_id,
                ]);
            }

            $query->andFilterWhere(['>=', 'realty_attr.area_all', $this->start_area]);
            $query->andFilterWhere(['<=', 'realty_attr.area_all', $this->end_area]);

            $query->andFilterWhere(['>=', 'price', $this->start_price]);
            $query->andFilterWhere(['<=', 'price', $this->end_price]);

            if ($isOnMap) {
                $query->andFilterWhere([
                    'not', 'geo_lat = ""'
                ]);

                $query->andFilterWhere([
                    'not', 'geo_lng = ""'
                ]);
            }

            if($this->option == self::OPTION_VIP) {
                $query->andFilterWhere(['vip'=>\common\enum\Realty::VIP_YES]);
            } elseif($this->option == self::OPTION_CREATOR) {
                $query->andFilterWhere(['users.type'=>User::TYPE_CREATOR]);
            }

            $sort = ['vip'=>SORT_DESC];
            switch ($this->sort) {
                case self::SORT_PRICE_ASC:
                    $sort = ArrayHelper::merge($sort, ['price'=>SORT_ASC]);
                    break;
                case self::SORT_PRICE_DESC:
                    $sort = ArrayHelper::merge($sort, ['price'=>SORT_DESC]);
                    break;
                case self::SORT_DATE_ASC:
                    $sort = ArrayHelper::merge($sort, ['created_at'=>SORT_ASC]);
                    break;
                case self::SORT_DATE_DESC:
                    $sort = ArrayHelper::merge($sort, ['created_at'=>SORT_DESC]);
                    break;
                case self::SORT_DEFAULT_DESC:
                    $sort = ArrayHelper::merge($sort, ['created_at'=>SORT_DESC]);
                    break;
                default:
                    break;
            }

           // var_dump($sort);

            $query->orderBy($sort);
        //}
		//print_r($query->createCommand()->getRawSql());
        return $dataProvider;
    }

    public function getObjectsOnMap() {
        $city_id = 1;
        if(isset($_COOKIE['cityID'])) {
            $city_id = $_COOKIE['cityID'];
        }
        $query = new \yii\db\Query();
        $sql = $query->select('`realty`.`id` AS `id`, `realty_desc`.`title` AS `desc_title`, `geo_lat`, `geo_lng`, `realty_image`.`image`, `realty_attr`.`housing_estate`, `place_district`.`title` AS `district_title`, `place_city`.title as `city_title`, `deadline`, `price`')
            ->from('realty')
            ->join('INNER JOIN', 'realty_attr', '`realty_attr`.`realty_id` = `realty`.`id`')
            ->join('INNER JOIN', 'users', '`realty`.`user_id` = `users`.`id`')
            ->join('INNER JOIN', 'realty_desc', '`realty`.`id` = `realty_desc`.`realty_id`')
            ->join('INNER JOIN', 'realty_image', '`realty`.`id` = `realty_image`.`realty_id`')
            ->join('INNER JOIN', 'place_city', '`realty_attr`.`city_id` = `place_city`.`id`')
            ->join('INNER JOIN', 'place_district', '`realty_attr`.`district_id` = `place_district`.`id`')
            ->where('realty.status = :status')
            ->andWhere('realty_attr.city_id = :city')
            ->andWhere(['not', 'geo_lat = ""'])
            ->andWhere(['not', 'geo_lng = ""'])
            ->andWhere('users.type = :type')
            ->addParams([':status' => 2, ':city' => $city_id, ':type' => 3])
            ->orderBy('vip DESC')
            ->limit(1000);


        return $sql->all();
    }
    public function mainImage($id, $image)
    {
        if(isset($image)) {
            return "@common/upload/realty/picture/{$id}/{$image}";
        } else {
            return '@webroot/image/default/nopic.png';
        }
    }
}

