$(document).ready(function () {
    $('.ico-search').click(function() {
        $(this).find('.ico-search').toggleClass('active');
        $(this).find('.ico-search span.search-open,.ico-search span.search-close').toggle();
        $('#form-search-block').toggle();
    });

    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
        $("#sidebar-wrapper .sidebar-nav").toggleClass("sidebar-nav-open sidebar-nav-close");
    });
    // Opens the sidebar menu
    $("#menu-toggle, .trigger-left-menu").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
        $("#sidebar-wrapper .sidebar-nav").toggleClass("sidebar-nav-close sidebar-nav-open");
    });

    if($.cookie('cityID')) {
        var cityID = $.cookie('cityID');
        $(".city").val(cityID);
        // if(cityID) {
        //     var city = $(".city option").filter("[value="+cityID+"]").text();
        //     $("#site-name span, .breadcrumb #city-name").text(city);
        // }
    }

    $('#form-mortgage-calculator').on('submit',function(o,res){
    //            $("#object-calculation").waitMe({
    //                effect : 'ios'
    //            });
        var price = Number($('#calculatorform-price').val());
        var deposit = Number($('#calculatorform-deposit').val());
        var calcSum = Number($('#calculatorform-sum').val());
        var period = Number($('#calculatorform-period').val());
        var rate = Number($('#calculatorform-rate').val());
        var creditResult = $('#calculation-result');



        var time = (period + 1) * 12;

        var interest = (rate + 1) / 1200;
        var result = calcSum * (interest *  Math.pow(1 + interest, time)) / (Math.pow(1 + interest, time) - 1);

        $.ajax({
            url: '/site/format',
            type: 'POST',
            data : {'resultCalc': result},
            success: function (data) {
                console.log(data);
                creditResult.val(data);
            },

            error: function () {
                alert('Ошибка...')
            }
        })

        return false;
    });
    $("#form-mortgage-calculator").on("afterValidate", function (event, messages, error){
        alert(123);
        if(error.length == 0) {
            var action = $(this).attr('action');
            $.post(action, $(this).serialize(), function (res) {
                $("#calculation-result").val(res);
                $("#object-calculation").waitMe('hide');
                $('#form-mortgage-calculator').data('yiiActiveForm').validated = false;
            });
        } else {
//                $("#object-calculation").waitMe('hide');
        }
    });
});
function CalculationI() {
    $("#form-mortgage-calculator").submit();
}
function SelectCity(el)
{
    var cityID = $(el).val();
    $.cookie('cityID', cityID, { expires: 7, path: '/' });
	location = '/';
    //location.reload();


    // $("#site-name span, .breadcrumb #city-name, .breadcrumb .active span").empty();
    // if(cityID) {
    //     var city = $(el).select2('data')[0]['text'];
    //     $("#site-name span, .breadcrumb #city-name").text(city);
    // }
    //
    // if(!location.pathname.match("search-objects")) {
    //     location.reload();
    // } else {
    //     LoadDistricts(cityID, "#objectsearch-district_id")
    // }
}

function LoadDistricts(cityID, selector) {
    $.post("/site/load-district", {cityID:cityID}, function (data) {
        data = JSON.parse(data);
        $(selector).empty().select2({
            data: data,
            allowClear:true,
            theme:"krajee",
            width:"100%",
            placeholder:"Выберите район ...",
            language:"ru"
        })
    });
}


