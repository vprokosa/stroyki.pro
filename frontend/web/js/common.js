// gallery

$('#imageGallery').lightSlider({
    enableDrag: true,
    gallery: true,
    item: 1,
    auto: false,
    vertical: false,
    adaptiveHeight: true,
    vThumbWidth: 50,
    thumbItem: 7,
    thumbMargin: 40,
    slideMargin: 0,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                thumbItem: 4
            }
        },
        {
            breakpoint: 480,
            settings: {
                thumbItem: 3
            }
        }

    ]
});

// phone button

$('.showPhone').click(function () {
   var phone = $(this).data('phone');
   $(this).text(phone);
});

// select style

$(".form select").SumoSelect();

$(".search-btn").SumoSelect({
    showTitle: false
});

// equalHeights
$(".contacts-height").equalHeights();

$('.user-ads .admy-item .row > div').equalHeights();
// iCheck

$('#accept').iCheck({
    checkboxClass: 'my_checkbox',
    increaseArea: '20%', // optional
    handle: 'checkbox'
});

// nicescroll

$('#search-result-objects-min').niceScroll({
    autohidemode: false,
    horizrailenabled: false
});

// yandex map

if ($('.object-page div').is('#map')) {
    ymaps.ready(init);
    var myMap,
        myPlacemark,
        myPlacemark2,
        CollectionPurple,
        CollectionBlue,
        latitude = $('#map').data('mapLatitude'),
        longitude = $('#map').data('mapLongitude');


    function init() {
        myMap = new ymaps.Map("map", {
            center: [latitude, longitude],
            zoom: 9,
            controls: ["zoomControl"]
        });


        CollectionPurple = new ymaps.GeoObjectCollection({}, {
            iconLayout: 'default#image',
            iconImageHref: '/images/mark_purple.png',
            iconImageSize: [31, 27],
            iconImageOffset: [-18, -15]
        });

        CollectionBlue = new ymaps.GeoObjectCollection({}, {
            iconLayout: 'default#image',
            iconImageHref: '/images/mark_blue.png',
            iconImageSize: [57, 32],
            iconImageOffset: [-30, -20]
        });

        myPlacemark = new ymaps.Placemark([55.76, 37.64], {
            hintContent: 'Метка 1',
            balloonContent: 'Метка 1'
        });

        myPlacemark2 = new ymaps.Placemark([latitude, longitude]);
   //     CollectionPurple.add(myPlacemark);
        CollectionBlue.add(myPlacemark2);
        myMap.geoObjects.add(CollectionBlue).add(CollectionPurple);
    }
}

if ($('.contact-page div').is('#mapContacts')) {
    ymaps.ready(init);
    var myMap,
        myPlacemark2,
        CollectionBlue;

    function init() {
        var myIcon = ymaps.templateLayoutFactory.createClass('<div class="my_icon">' +
            '<div class="mark"></div>' +
            '<div class="captionText">{{ properties.iconCaption }}</div>' +
            '</div>');

        var myGeocoder = ymaps.geocode($('#mapContacts').data('mapAddress'));
        myGeocoder.then(
            function (res) {
                var coordinates = res.geoObjects.properties._data.metaDataProperty.GeocoderResponseMetaData.Point.coordinates;
                myMap = new ymaps.Map("mapContacts", {
                    center: [coordinates[1], coordinates[0]], // 55.76, 37.64
                    zoom: 9,
                    controls: ["zoomControl"]
                });

                CollectionBlue = new ymaps.GeoObjectCollection({}, {
                    iconLayout: myIcon
                });

                myPlacemark2 = new ymaps.Placemark([coordinates[1], coordinates[0]], {
                    iconCaption: $('#mapContacts').data('mapCaption')
                });
                CollectionBlue.add(myPlacemark2);
                myMap.geoObjects.add(CollectionBlue);

            },
            function (err) {
                // обработка ошибки
            }
        );
    }
}



$('.support-form').on('submit', function(e){
    e.preventDefault();
    
    var form = $(this);
    
    $.ajax({
        url:$(this).attr('action'),
        type: 'post',
        data: $(this).serialize(),
        success: function(result) {
            $('form').trigger('reset');
            $('.alert-message').html('<div class="alert alert-success">Ваще сообщение отправленно!</div>').fadeIn().animate({opacity: 1.0}, 3000).fadeOut("slow");
        }
    });
});