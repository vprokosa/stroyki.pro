$(document).ready(function() {
	$("#enter").click(function(){
		$('.enter-form').css("display", "none");
		$('.form-type').removeClass('enabled');

		$('#enter-f').css("display", "block");
		$('#enter').addClass('enabled');
	});

	$("#reg").click(function(){
		$('.enter-form').css("display", "none");
		$('.form-type').removeClass('enabled');

		$('#reg-f').css("display", "block");
		$('#reg').addClass('enabled');
	});
});