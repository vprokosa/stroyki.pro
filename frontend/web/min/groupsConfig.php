<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 *
 *
 **/

return array(
    'base.js'=> [
        '//plugins/jquery/dist/jquery.min.js', //2.2.4
        '//plugins/bootstrap/dist/js/bootstrap.min.js', //3.3.6
        '//total/app.js',
    ],
    'base.css'=> [
        '//plugins/bootstrap/dist/css/bootstrap.css',
        '//total/app.css',
        '//total/footer.css',
        '//total/media.css',
    ],
    
    //Для сайта
    'front.plugins.js' => [
        //Фото
        '//plugins/unitegallery/dist/js/unitegallery.min.js',
        '//plugins/unitegallery/dist/themes/compact/ug-theme-compact.js',
        '//plugins/jquery.cookie/jquery.cookie.js',
        '//plugins/Loading-waitMe/waitMe.min.js',
        '//plugins/liTranslit/js/jquery.liTranslit.js',
    ],
    'front.plugins.css' => [
        //Фото
        '//plugins/unitegallery/dist/css/unite-gallery.css',
        '//plugins/unitegallery/dist/themes/default/ug-theme-default.css',
        '//plugins/Loading-waitMe/waitMe.min.css',
    ],
    'front.js' => [
        '//js/front.js',
    ],
    'front.css' =>[
        '//css/fonts.css',
        '//css/front.css',
        '//css/media.css',
    ],

    //Для админки
    'backend.plugins.js' => [
        '//plugins/jquery-ui/jquery-ui.min.js',
        '//plugins/dropzone/lib/UnoDropZone.js',
        '//plugins/Loading-waitMe/waitMe.min.js',
        '//plugins/liTranslit/js/jquery.liTranslit.js',
        '//plugins/jquery.cookie/jquery.cookie.js',
    ],
    'backend.plugins.css'=> [
        '//plugins/dropzone/lib/UnoDropZone.css',
        '//plugins/Loading-waitMe/waitMe.min.css',
    ],
    'backend.js' => [
        '//js/AdminLTE.js',
        '//js/backend.js',
    ],
    'backend.css' => [
        '//css/AdminLTE.min.css',
        '//css/skin-blue.min.css',
        '//css/backend.css',
    ],

    //Плагины отдельно
);