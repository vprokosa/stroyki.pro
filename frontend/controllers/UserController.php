<?php

namespace frontend\controllers;

use frontend\components\AppController;
use Yii;
use common\models\form\LoginForm;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use frontend\models\ObjectSearch;
use yii\filters\VerbFilter;
use frontend\models\form\PasswordResetRequestForm;
use frontend\models\form\ResetPasswordForm;
use frontend\models\form\SignupForm;
use yii\web\BadRequestHttpException;
use yii\web\Cookie;

class UserController extends AppController
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $searchModel = new ObjectSearch();
 				$this->view->params['bread'] = '_total';
				$this->view->params['r'] = array();
           $this->view->params['searchModelg'] = $searchModel;
            return true;
        } else {
            return false;
        }
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login','signup','logout','request-password-reset','reset-password'],
                'rules' => [
                    [
                        'actions' => ['login','signup','request-password-reset','reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionLogin()
    {


        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'total';
        $this->disableFooter = false;

        $modelSignup = new SignupForm();
        $model = new LoginForm();
        if (isset(Yii::$app->request->post()['SignupForm'])) {
            if ($modelSignup->load(Yii::$app->request->post()) && $user = $modelSignup->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    \Yii::$app->session->setFlash('success', 'Пользователь зарегистрирован на сайте, поздравляем !');
                    return $this->goHome();
                }
            }

        } else {
            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                $referrer = 'realty';
                if (isset($_COOKIE['referrer'])) {
                    $referrer = $_COOKIE['referrer'];
                    Yii::$app->response->cookies->remove('referrer');
                }
                if (!isset($_COOKIE['cityID']) || $_COOKIE['cityID'] == 0 || $_COOKIE['cityID'] == '') {
                    setcookie("cityID", 1);
                }
                return $this->redirect($referrer);
            }
        }
        return $this->render('login', [
            'model' => $model,
            'modelReg' => $modelSignup
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionSignup()
    {
        $this->layout = 'total';
        $this->disableFooter=true;

        echo "<pre>".print_r(Yii::$app->request->post()['SignupForm'], true)."</pre>";
        exit();

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $user = $model->signup()) {
            if (Yii::$app->getUser()->login($user)) {
                \Yii::$app->session->setFlash('success', 'Пользователь зарегистрирован на сайте, поздравляем !');
                return $this->goHome();
            }
        }
        return $this->goBack();
    }

    /**
     * Запрос восстановления пароля
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $this->layout = 'total';
        

        $model = new PasswordResetRequestForm();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Проверьте свою электронную почту для получения дальнейших инструкций.');
            } else {
                Yii::$app->session->setFlash('error', 'К сожалению, мы не можем сбросить пароль для электронной почты.');
            }
            return $this->refresh();
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionLoc() {
        echo 123;
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        $this->layout = 'total';

        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль был сохранен.');
            return $this->redirect(['user/login']);
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}

