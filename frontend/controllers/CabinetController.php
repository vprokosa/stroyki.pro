<?php

namespace frontend\controllers;

use frontend\components\AppController;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use frontend\models\ObjectSearch;

class CabinetController extends AppController
{
    public function init()
    {
        $this->layout = 'cabinet';
        parent::init();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $searchModel = new ObjectSearch();
            $this->view->params['searchModelg'] = $searchModel;
            return true;
        } else {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->render('index');
    }
}