<?php
namespace frontend\controllers;


use common\traits\ModelTrait;
use frontend\components\AppController;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;


use frontend\models\ObjectSearch;
use common\enum\Scenario;
use common\models\Articles;
use Yii;


class ArticleController extends AppController
{

    public function actionIndex()
    {
        //$searchModel = new ArticlesSearch();
       // $dataProvider = $searchModel->search(Yii::$app->request->get());

       // return $this->render('index', [
       //     'searchModel' => $searchModel,
       //     'dataProvider' => $dataProvider,
       // ]); 
    }
   
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $searchModel = new ObjectSearch();
            $this->view->params['searchModelg'] = $searchModel;
			$this->view->params['ObjectSearch'] = [];
            return true;
        } else { 
            return false;
        }
    }

    public function actionView($id)
    {
        $articles = Articles::find()->where(['id'=>$id])->one();
		print_r( $articles); 
		die();
		if($articles) {
			return $this->render('articles', [
				'model' => $articles,
			]);
		}
		throw new NotFoundHttpException(Yii::$app->params['error404']);

    }
}
