<?php

namespace frontend\controllers;


use common\models\form\LoginForm;
use frontend\components\AppController;
use frontend\models\form\AvatarForm;
use frontend\models\form\ChangeAvatarForm;
use frontend\models\form\ChangeBgHeaderForm;
use frontend\models\form\ChangePasswordForm;
use frontend\models\ObjectSearch;
use frontend\models\form\HeaderForm;
use frontend\models\form\LogoForm;
use frontend\models\form\ProfileForm;
use frontend\models\form\SubscriptionForm;
use yii\filters\AccessControl;

class ProfileController extends AppController
{
	public $userProfile;
	
    public function init()
    {
        $this->layout = 'profile';
        parent::init();
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
			$user = \Yii::$app->user->identity;
			$this->userProfile = $user; 
            $searchModel = new ObjectSearch();
 				$this->view->params['bread'] = '_total';
				$this->view->params['r'] = array();
            $this->view->params['searchModelg'] = $searchModel;
            return true;
        } else {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','change-password','change-avatar','subscription','change-bg-header', 'logo'],
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index','change-password','change-avatar','subscription','change-bg-header', 'logo'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Профиль
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index', ['user'=>$this->userProfile]);
    }
    /**
     * Профиль обновить
     * @return string
     */
    public function actionUpdate()
    {
        $user = \Yii::$app->user->identity;
        $profile = new ProfileForm();
        $profile->setAttributes($user->getAttributes(null), false);

        if($profile->load(\Yii::$app->request->post()) && $profile->save())
        {
            \Yii::$app->session->setFlash('success', 'Профиль обновлен');
            return $this->redirect('index');
        }
        
        return $this->render('update', ['model'=>$profile]);
    }
    /**
     * Сменить пароль
     * @return string
     */
    public function actionChangePassword()
    {
        $password = new ChangePasswordForm();

        if($password->load(\Yii::$app->request->post()) && $password->save())
        {
            \Yii::$app->session->setFlash('success', 'Установлен новый пароль');
            return $this->redirect('index');
        }

        return $this->render('change-password', ['model'=>$password]);
    }
    
    /**
     * Подписка
     * @return string
     */
    public function actionSubscription()
    {
        $subscription = new SubscriptionForm();
        $subscription->sending = \Yii::$app->user->identity->sending;

        if($subscription->load(\Yii::$app->request->post()) && $subscription->save())
        {
            if(\Yii::$app->user->identity->sending) {
                \Yii::$app->session->setFlash('success', 'Рассылка добавлена');
            } else {
                \Yii::$app->session->setFlash('warning', 'Рассылка удалена');
            }
            return $this->refresh();
        }

        return $this->render('subscription', ['model'=>$subscription]);
    }

    /**
     * Добавить аватар
     * @return string
     */
    public function actionAvatar()
    {
        $form = new AvatarForm();

        if(isset($_POST['avatar-button']) && $form->save())
        {
            \Yii::$app->session->setFlash('success', 'Установлена новая аватарка');
            return $this->refresh();
        }

        return $this->render('avatar', ['model'=>$form]);
    }
    /**
     * Добавить шапку компании
     * @return string
     */
    public function actionHeader()
    {
        $form = new HeaderForm();

        if(isset($_POST['header-button']) && $form->save())
        {
            \Yii::$app->session->setFlash('success', 'Установлен новый фон шапки');
            return $this->refresh();
        }

        return $this->render('header', ['model'=>$form]);
    }
    /**
     * Добавить лого
     */
    public function actionLogo()
    {
        $form = new LogoForm();

        if(isset($_POST['add-button']) && $form->save())
        {
            \Yii::$app->session->setFlash('success', 'Установлен новый логотип');
            return $this->refresh();
        }
				
        return $this->render('logo', ['model'=>$form]);
    }
}