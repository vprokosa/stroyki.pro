<?php
namespace frontend\controllers;

use common\enum\Realty;
use common\enum\User;
use common\models\RealtyFavorite;
use common\models\SubscriptionPrice;
use common\models\SubscriptionSearch;
use common\traits\ModelTrait;
use frontend\components\AppController;
use himiklab\thumbnail\FileNotFoundException;
use frontend\models\ObjectSearch;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

class ServiceController extends AppController
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $searchModel = new ObjectSearch();
            $this->view->params['searchModelg'] = $searchModel;
            return true;
        } else {
            return false;
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['reg-favorite','favorite','reg-spy-price','spy-price','vip','rss-register','rss','rss-show','rss-delete'],
                'rules' => [
                    [
                        'actions' => [''],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['reg-favorite','favorite','reg-spy-price','spy-price','vip','rss-register','rss','rss-show','rss-delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    /**
     * Регистрация или снятие с регистрации объявления с избранное
     */
    public function actionRegFavorite()
    {
        if(\Yii::$app->request->isAjax) {

            $id = $_GET['id'];

            $count = (new Query())
                ->from('realty_favorite')
                ->where([
                    'user_id'=>\Yii::$app->user->id,
                    'realty_id'=>$id
                ])->count('id');

            //добавить в избранное
            if($count==0) {
                \Yii::$app->db->createCommand()->insert('realty_favorite', [
                    'user_id'=>\Yii::$app->user->id,
                    'realty_id'=>$id,
                    'created_at'=>time()
                ])->execute();
            }
            //удалить из избранного
            elseif($count==1) {
                \Yii::$app->db->createCommand()->delete('realty_favorite', [
                    'user_id'=>\Yii::$app->user->id,
                    'realty_id'=>$id
                ])->execute();
            }

            //итог
            $count = (new Query())
                ->from('realty_favorite')
                ->where([
                    'user_id'=>\Yii::$app->user->id,
                ])->count('id');

            return $count;
        }
        throw new NotFoundHttpException(\Yii::$app->params['error404']);
    }
    /**
     * Список избранных объявлений
     */
    public function actionFavorite()
    {
        $this->layout='total';

        $dataProvider = new ActiveDataProvider([
            'query'=> RealtyFavorite::find()->with(['realty'])->where([
                'user_id'=>\Yii::$app->user->id,
            ])->orderBy(['created_at'=>SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
        $count = $dataProvider->getTotalCount();

        return $this->render('favorite', [
            'dataProvider' => $dataProvider,
            'count' => $count
        ]);
    }
    /**
     * Регистрация или снятие с регистрации объявления с подписки на цену
     */
    public function actionRegSpyPrice()
    {
        if(\Yii::$app->request->isAjax) {
            $id = $_GET['id'];

            $count = (new Query())
                ->from('subscription_price')
                ->where([
                    'user_id'=>\Yii::$app->user->id,
                    'realty_id'=>$id
                ])->count('id');

            //добавить в подписку
            if($count==0) {
                \Yii::$app->db->createCommand()->insert('subscription_price', [
                    'user_id'=>\Yii::$app->user->id,
                    'realty_id'=>$id,
                    'created_at'=>time()
                ])->execute();
            }
            //удалить из подписки
            elseif($count==1) {
                \Yii::$app->db->createCommand()->delete('subscription_price', [
                    'user_id'=>\Yii::$app->user->id,
                    'realty_id'=>$id
                ])->execute();
            }

            //итог
            $count = (new Query())
                ->from('subscription_price')
                ->where([
                    'user_id'=>\Yii::$app->user->id,
                ])->count('id');

            return $count;
        }
        throw new NotFoundHttpException(\Yii::$app->params['error404']);
    }
    /**
     * Список объявления на подписку цены
     */
    public function actionSpyPrice()
    {
        $this->layout='total';

        $dataProvider = new ActiveDataProvider([
            'query'=> SubscriptionPrice::find()->with(['realty'])->where([
                'user_id'=>\Yii::$app->user->id,
            ])->orderBy(['created_at'=>SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
        $count = $dataProvider->getTotalCount();

        return $this->render('spy-price', [
            'dataProvider' => $dataProvider,
            'count' => $count
        ]);
    }
    /**
     * Зарегистрировать рассылку
     * @return int
     * @throws NotFoundHttpException
     */
    public function actionRssRegister()
    {
        if(\Yii::$app->request->isAjax && isset($_POST['param'])) {
            $data = Json::decode($_POST['param']);
            $data = ArrayHelper::map($data, 'name', 'value');

            $subscription = new SubscriptionSearch();
            $validate = false;
            if(isset($data['ObjectSearch[room1]']) && is_numeric($data['ObjectSearch[room1]'])) {
                $subscription->room1 = $data['ObjectSearch[room1]'];
                $validate = true;
            }
            if(isset($data['ObjectSearch[room1]']) && is_numeric($data['ObjectSearch[room2]'])) {
                $subscription->room2 = $data['ObjectSearch[room2]'];
                $validate = true;
            }
            if(isset($data['ObjectSearch[room1]']) && is_numeric($data['ObjectSearch[room3]'])) {
                $subscription->room3 = $data['ObjectSearch[room3]'];
                $validate = true;
            }
            if(isset($data['ObjectSearch[room1]']) && is_numeric($data['ObjectSearch[room4]'])) {
                $subscription->room4 = $data['ObjectSearch[room4]'];
                $validate = true;
            }
            if(isset($data['ObjectSearch[room1]']) && is_numeric($data['ObjectSearch[start_area]'])) {
                $subscription->start_area = $data['ObjectSearch[start_area]'];
                $validate = true;
            }
            if(isset($data['ObjectSearch[room1]']) && is_numeric($data['ObjectSearch[end_area]'])) {
                $subscription->end_area = $data['ObjectSearch[end_area]'];
                $validate = true;
            }
            if(isset($data['ObjectSearch[room1]']) && is_numeric($data['ObjectSearch[start_price]'])) {
                $subscription->start_price = $data['ObjectSearch[start_price]'];
                $validate = true;
            }
            if(isset($data['ObjectSearch[room1]']) && is_numeric($data['ObjectSearch[end_price]'])) {
                $subscription->end_price = $data['ObjectSearch[end_price]'];
                $validate = true;
            }
            if(isset($data['ObjectSearch[room1]']) && isset($_COOKIE['cityID']) && is_numeric($_COOKIE['cityID'])) {
                $subscription->city_id = $_COOKIE['cityID'];
                $validate = true;
            }
            $subscription->user_id = \Yii::$app->user->id;
            $subscription->option = isset($data['ObjectSearch[option]']) ? $data['ObjectSearch[option]'] : '';


            if($validate && $subscription->save()) {
                \Yii::$app->user->identity->sending = User::SENDING_YES;
                \Yii::$app->user->identity->update(false, ['sending']);

                //итог
                $count = (new Query())
                    ->from('subscription_search')
                    ->where([
                        'user_id'=>\Yii::$app->user->id,
                    ])->count('id');
                return $count;
            }
            return 0;
        }
        throw new NotFoundHttpException(\Yii::$app->params['error404']);
    }
    /**
     * Список всех подписок на рассылку
     * @return string
     */
    public function actionRss()
    {
        $this->layout='total';

        $provider = new ActiveDataProvider([
            'query'=>SubscriptionSearch::find()->where(['user_id'=>\Yii::$app->user->id])
        ]);
        
        return $this->render('rss-index', [
            'provider'=>$provider
        ]);
    }
    /**
     * Просмотр одной подписки, что в нее входит
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRssShow($id=0)
    {
        $this->layout='total';
        $subscriptionSearch = SubscriptionSearch::find()->where(['user_id'=>\Yii::$app->user->id, 'id'=>$id])->one();
        if(!$subscriptionSearch) {
            throw new NotFoundHttpException(\Yii::$app->params['error404']);
        }
        return $this->render('rss-show', [
            'model'=>$subscriptionSearch
        ]);
    }
    /**
     * Удалить рассылку
     * @param int $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionRssDelete($id=0)
    {
        $this->layout='total';
        $subscriptionSearch = SubscriptionSearch::find()->where(['user_id'=>\Yii::$app->user->id, 'id'=>$id])->one();
        if(!$subscriptionSearch) {
            throw new NotFoundHttpException(\Yii::$app->params['error404']);
        }
        $subscriptionSearch->delete();
        return $this->redirect('rss');
    }
    
    public function actionVip()
    {
        $this->layout='total';

        $dataProvider = new ActiveDataProvider([
            'query'=> \common\models\Realty::find()->with(['attr','desc'])->where([
                'user_id'=>\Yii::$app->user->id,
                'vip'=>Realty::VIP_YES,
            ])->orderBy(['created_at'=>SORT_DESC]),
            'pagination' => [
                'pageSize' => 10,
            ]
        ]);
        $count = $dataProvider->getTotalCount();

        return $this->render('vip', [
            'dataProvider' => $dataProvider,
            'count' => $count
        ]);
    }
}
