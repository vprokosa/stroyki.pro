<?php

namespace frontend\controllers;


use common\components\actions\realty\CreateAction;
use common\components\actions\realty\DeleteAction;
use common\components\actions\realty\PaymentAction;
use common\components\actions\realty\UpdateAction;
use common\components\actions\realty\ValidateAction;
use common\models\Realty;
use frontend\models\ObjectSearch;
use frontend\components\AppController;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use common\models\Payment;

class RealtyController extends AppController
{
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $searchModel = new ObjectSearch();
            $this->view->params['searchModelg'] = $searchModel;
            return true;
        } else {
            return false;
        }
    }
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','index-draft','index-archive','post','update','validate','payment','delete','show','come-money-yandex'],
                'rules' => [
                    [
                        'actions' => ['come-money-yandex'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index','index-draft','index-archive','post','update','payment','validate','delete','show','come-money-yandex'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    
    public function init()
    {
        $this->layout = 'cabinet';
        parent::init();
    }

    public function actions()
    {
        return [
            'post'=>[
                'class'=>CreateAction::className(),
                'view' => $this,
            ],
            'update'=>[
                'class'=>UpdateAction::className(),
                'view' => $this,
            ],
            'validate'=>[
                'class'=>ValidateAction::className(),
            ],
            'payment'=>[
                'class'=>PaymentAction::className(),
                'view' => $this,
            ],
            'delete'=>[
                'class'=>DeleteAction::className(),
                'view' => $this,
            ],
        ];
    }

    /**
     * Вся недвижимость в публикации
     */
    public function actionIndex()
    {
        $this->layout = 'cabinet_objects';

        $dataProvider = new ActiveDataProvider([
            'query' => Realty::find()->with(['attr','desc','images'])->where([
                'status'=>\common\enum\Realty::STATUS_PUBLIC,
                'user_id'=>\Yii::$app->user->id,
            ])->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 4,
            ],
        ]);

        return $this->render('index', [
            'listDataProvider' => $dataProvider,
            'status' => 'public'
        ]);
    }

    /**
     * Вся недвижимость в черновике
     */
    public function actionIndexDraft()
    {
        $this->layout = 'cabinet_objects';

        $dataProvider = new ActiveDataProvider([
            'query' => Realty::find()->with(['attr','desc','images'])->where([
                'status'=>\common\enum\Realty::STATUS_DRAFT,
                'user_id'=>\Yii::$app->user->id,
            ])->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 4,
            ],
        ]);

        return $this->render('index', [
            'listDataProvider' => $dataProvider,
            'status' => 'draft'
        ]);
    }

    /**
     * Вся недвижимость в архиве
     */
    public function actionIndexArchive()
    {
        $this->layout = 'cabinet_objects';

        $dataProvider = new ActiveDataProvider([
            'query' => Realty::find()->with(['attr','desc','images'])->where([
                'status'=>\common\enum\Realty::STATUS_ARCHIVE,
                'user_id'=>\Yii::$app->user->id,
            ])->orderBy('id DESC'),
            'pagination' => [
                'pageSize' => 4,
            ],
        ]);

        return $this->render('index', [
            'listDataProvider' => $dataProvider,
            'status' => 'archive'
        ]);
    }

    /**
     * Колбек из яндекса при зачислении денег в яндекс кошелек
     */
    public function actionComeMoneyYandex()
    {
//        file_put_contents(\Yii::getAlias('@frontend').'/web/data.txt', var_export($_POST, true));

        if(\Yii::$app->request->isPost &&
            isset($_POST['notification_type']) &&
            isset($_POST['operation_id']) &&
            isset($_POST['amount']) &&
            isset($_POST['currency']) &&
            isset($_POST['datetime']) &&
            isset($_POST['sender']) &&
            isset($_POST['codepro']) &&
            isset($_POST['label']) &&
            isset($_POST['withdraw_amount']) &&
            isset($_POST['sha1_hash'])) {

            //формируем хеш
            $hash = $_POST["notification_type"].'&'.$_POST["operation_id"].'&'.$_POST["amount"].'&'.$_POST["currency"].'&'.$_POST["datetime"].'&'.$_POST["sender"].'&'.$_POST["codepro"].'&'.\Yii::$app->params['yandexMoney']['secretWord'].'&'.$_POST["label"];
            //кодируем в SHA1
            $sha1 = hash("sha1", $hash);
            if($sha1 == $_POST['sha1_hash'])
            {
                $realty = Realty::find()->where(['id'=>$_POST['label']])->one();
                if(!$realty) {
                    throw new NotFoundHttpException(\Yii::$app->params['error404']);
                }
                if($_POST['codepro'] != false)//&& !$_POST['unaccepted']
                {
                    $realty->vip = \common\enum\Realty::VIP_YES;
                    $realty->update(false, ['vip']);

                    //добовляем в платежи
                    Payment::Create($realty, $_POST["amount"], \common\enum\Payment::STATUS_SUCCESS);

                    // уведомление админу об оплате
                    \Yii::$app->mailer->compose(['html' => 'payment'], ['id' => $_POST['label'], 'sum'=>$_POST["amount"]])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->params['sitename'] . ' поступила оплата на яндекс'])
                        ->setTo($this->email)
                        ->setSubject('Поступила оплата на яндекс ' . \Yii::$app->params['sitename'])
                        ->send();
                } else {
                    Payment::Create($realty, $_POST["amount"], \common\enum\Payment::STATUS_PROTECTION);

                    // платеж в протекции {сообщить админу, для мониторинга счета}
                    \Yii::$app->mailer->compose(['html' => 'paymentProtection'], ['id' => $_POST['label'], 'sum'=>$_POST["amount"]])
                        ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->params['sitename'] . ' поступила оплата c протекцией на яндекс'])
                        ->setTo($this->email)
                        ->setSubject('Поступила оплата c протекцией на яндекс ' . \Yii::$app->params['sitename'])
                        ->send();
                }
            }

        } else {
            throw new NotFoundHttpException(\Yii::$app->params['error404']);
        }
    }
}