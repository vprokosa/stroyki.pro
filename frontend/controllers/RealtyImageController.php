<?php
namespace frontend\controllers;

use frontend\components\AppController;
use common\components\actions\image\DeleteAction;
use common\components\actions\image\SortAction;
use common\components\actions\image\UploadAction;

class RealtyImageController extends AppController
{
    public function actions()
    {
        return [
            'upload'=>[
                'class'=>UploadAction::className(),
            ],
            'delete'=>[
                'class'=>DeleteAction::className(),
            ],
            'sort'=>[
                'class'=>SortAction::className(),
            ],
        ];
    }
}
