<?php
namespace frontend\controllers;

use common\components\actions\realty\LoadDistrictAction;
use common\components\ThumbHelper;
use common\models\Banners;
use common\models\Articles;
use common\models\Pages;
use common\models\Realty;
use common\models\User;
use common\models\City;
use common\models\District;
use common\traits\ModelTrait;
use frontend\components\actions\SiteErrorAction;
use frontend\components\AppController;
use frontend\modules\mortgage_calculation\models\CalculatorForm;
use frontend\models\ObjectSearch;
use common\models\RealtyImage;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\HtmlPurifier;
use yii\helpers\Json;
use yii\helpers\Url;
//use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;
use \common\components\TranslitHelper;
use common\enum\User as UserType;
use common\components\actions;

/**
 * Site controller
 */
class SiteController extends AppController
{
    use ModelTrait;
    public $user;
    public $bread;
    public $userRoles = array(
        1 => 'Собственник',
        2 => 'Подрядчик',
        3 => 'Застройщик',
        4 => 'Агенство'
    );
    
    public function beforeAction($action)
    {
       if (parent::beforeAction($action)) {
            if ($action->id=='error') {
                $this->layout ='total';
                $this->disableFooter = false;
            }
            $searchModel = new ObjectSearch();
            $this->view->params['searchModelg'] = $searchModel;
			$this->view->params['ObjectSearch'] = [];
            return true;
        } else {
            return false;
        }
    }
	

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => SiteErrorAction::className(),
            ],
            'captcha' => [
                'class' => \yii\captcha\CaptchaAction::className(),
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'minLength' => 3,
                'maxLength' => 4,
            ],
            'load-district' => [
                'class'=>\common\components\actions\LoadDistrictAction::className()
            ],
            'vip-image' => [
                'class'=>\frontend\components\actions\VipImageAction::className()
            ],
            'logo-header' => [
                'class'=>\frontend\components\actions\LogoHeaderAction::className()
            ]
        ];
    }
    
    public function actionIndex()
    {
        //var_dump(\yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => ['test']]));
        //return $this->redirect('search-objects');
        $this->layout='total';
		$key = 'new_objects'; 

		/*$new_objects = Yii::$app->cache->getOrSet($key, function () {
			return Realty::find()->joinWith(['attr','user'])->andFilterWhere([
				'realty.status' => \common\enum\Realty::STATUS_PUBLIC
			])->limit(3)->orderBy(['id' => SORT_DESC])->all();
		});*/


        $new_objects =  Realty::find()->joinWith(['attr','user'])->andFilterWhere([
            'realty.status' => \common\enum\Realty::STATUS_PUBLIC
        ])->limit(3)->orderBy(['id' => SORT_DESC])->all();

//var_dump($new_objects);
        foreach ($new_objects as $key => $value) {

            $new_objects[$key] = array(
                "obj" => $value,
                "city_name" => "",
                "user_type" => ""
            );
            //var_dump($value->id);
           // var_dump($value->attr);
            if ($value->attr) {
                $city = City::find()->where(["id" => (int)$value->attr->city_id])->one();
                $new_objects[$key]["city_name"] = $city->title;
            }
            /**
             * Если тип юзера застройщик то будем выводить надпись
             */
            if($value->user->type == UserType::TYPE_CREATOR){
                $new_objects[$key]["user_type"] = UserType::Type($value->user->type);
            }
        }
//exit;
		$key = 'new_housing_estate'; 

        /*$new_housing_estate = Yii::$app->cache->getOrSet($key, function () {
			return Realty::find()->joinWith(['attr','user'])->andFilterWhere(['not', 'realty_attr.housing_estate=""'])->limit(3)->orderBy(['id' => SORT_DESC])->all();
		});*/

        $new_housing_estate = Realty::find()->joinWith(['attr','user'])->andFilterWhere(['not', 'realty_attr.housing_estate=""'])->limit(3)->orderBy(['id' => SORT_DESC])->all();


        foreach ($new_housing_estate as $key => $value) {

            $new_housing_estate[$key] = array(
                "obj" => $value,
                "city_name" => "",
                "user_type" => ""
            );

            $city = City::find()->where(["id" => (int)$value->attr->city_id])->one();
            $new_housing_estate[$key]["city_name"] = $city->title;

            $new_housing_estate[$key]["user_type"] = $value->user->type;

           // $new_housing_estate[$key]["user_name"] = $value->user->full_name;
        }

		$key = 'new_zastr'; 
		/*$new_zastr = Yii::$app->cache->getOrSet($key, function () {
			return Realty::find()->joinWith(['attr','user'])
                        ->andFilterWhere(['users.type'=>3])
                        ->limit(3)
                        ->orderBy(['id' => SORT_DESC])
                        ->all();
		});*/

		$new_zastr = Realty::find()->joinWith(['attr','user'])
            ->andFilterWhere(['users.type'=>3])
            ->limit(3)
            ->orderBy(['id' => SORT_DESC])
            ->all();

		//var_dump($new_zastr);
        foreach ($new_zastr as $key => $value) {
            $new_zastr[$key] = array(
                "obj" => $value,
                "city_name" => "",
                "user_type" => ""
            );

            if ($value->attr !== null) {
                $city = City::find()->where(["id" => (int)$value->attr->city_id])->one();
                $new_zastr[$key]["city_name"] = $city->title;
            }
           // var_dump($value->user->company);
            //$new_zastr[$key]["user_type"] = $value->user->full_name;
        }
		$this->view->registerMetaTag(
		['name' => 'keywords','content' => 'Жилые комплексы и Новостройки Москвы и Подмосковья. Продажа квартир в Новостройках и Жилых Комплексах Москвы и Подмосковья',]);
		$this->view->registerMetaTag(['name' => 'description','content' => 'Новостройки Москвы, Новой Москвы и Подмосковья. Купить квартиру в Жилых Комплексах от застройщиков - самая большая и актуальная база Московской недвижимости',]
		);
        return $this->render('new_index', [
            'new_objects' => $new_objects,
            'new_housing_estate' => $new_housing_estate,
            'new_zastr' => $new_zastr
        ]);
    }

    /**
     * Просмотр объявления
     */
    public function actionAdvertisement($id='')
    {
        $id = HtmlPurifier::process($id);
        $realty = Realty::find()->where(['id'=>$id])->with(['attr','desc','images','docs'])->one();
        if($realty) {
			$ObjectSearch = [];
            $this->layout = 'total';
            Yii::$app->stat->regShow('realty', $realty->id);
			$breadCity = (isset($realty->attr['city_id']) && (!empty($realty->attr['city_id']))) ? City::find()->where(["id" => (int)$realty->attr['city_id']])->one() :[] ;
			$breadDistrict = (isset($realty->attr['district_id']) && (!empty($realty->attr['district_id']))) ? District::find()->where(["id" => (int)$realty->attr['district_id']])->one() :[] ;
			if(isset($breadCity) && (!empty($breadCity))){
				$ObjectSearch['city_id'] = $breadCity['id'];
				$this->view->params['breadcrumbs'][] = [
					'label' => $breadCity['title'],
					'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
					'template' => "<li><b>{link}</b></li>\n", // template for this link only
				];
			}
			if(!empty($realty->user->typeName())){
				$ObjectSearch['option'] = \common\enum\User::TYPE_CREATOR;
				$this->view->params['breadcrumbs'][] = [
					'label' => $realty->user->typeName(),
					'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
					'template' => "<li><b>{link}</b></li>\n", // template for this link only
				];
			}
			if(!empty($realty->attr->rooms)){
				$ObjectSearch['room'.$realty->attr->rooms] = 1;
				$this->view->params['breadcrumbs'][] = [
					'label' => Yii::t('app', '{n, number} {n, plural, one{Комната} few{Комнаты} many{Комнаты} other{Комнаты}}', ['n'=>$realty->attr->rooms]),
					'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
					'template' => "<li><b>{link}</b></li>\n", // template for this link only
				];
			}
			if(isset($breadDistrict) && (!empty($breadDistrict))){
				$ObjectSearch['district_id'] = $breadDistrict['id'];
				$this->view->params['breadcrumbs'][] = [
					'label' => $breadDistrict['title'],
					'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
					'template' => "<li><b>{link}</b></li>\n", // template for this link only
				];
			}
			if(!empty($realty->desc->title)){
				$ObjectSearch['room'.$realty->attr->rooms] = 1;
				$this->view->params['breadcrumbs'][] = [
					'label' => \yii\helpers\Html::encode($realty->desc->title),
					'template' => "<li><span style='color: rgb(41, 171, 226);'>{link}</span></li>\n", // template for this link only
				];
			}

			$this->view->registerMetaTag([
				'name' => 'keywords',
				'content' => $realty->desc->title
			]);

            return $this->render('advertisement', ['realty'=>$realty]);
        }

        throw new NotFoundHttpException(Yii::$app->params['error404']);
    }

    public function actionSearchFilter($page = 1)
    {
        if(!empty(Yii::$app->request->post())){
            $searchParams = Yii::$app->request->post();
        } else {
            $searchParams = Yii::$app->request->getQueryParams();
        }

        $searchModel = $this->view->params['searchModelg'];

        $filter = [];
        $rooms = 0;

        /**
         * Обработка колличества комнат
         */
        if(isset($searchParams['ObjectSearch']['room1']) && ($searchParams['ObjectSearch']['room1'] == 1)){
            $rooms = 1;
        }

        if(isset($searchParams['ObjectSearch']['room2']) && ($searchParams['ObjectSearch']['room2'] == 1)){
            $rooms = 2;
        }

        if(isset($searchParams['ObjectSearch']['room3']) && ($searchParams['ObjectSearch']['room3'] == 1)){
            $rooms = 3;
        }

        if(isset($searchParams['ObjectSearch']['room4']) && ($searchParams['ObjectSearch']['room4'] == 1)){
            $rooms = 4;
        }


        if(!empty($rooms)) {
            $filter['room' . $rooms] = 1;
        }


        if(!empty($searchParams['ObjectSearch']['district_id'])) {
            $filter['district_id'] = $searchParams['ObjectSearch']['district_id'];
        }


        /**
         * Обработка города
         */
        if(isset($searchParams['ObjectSearch']['city_id']) && (!empty($searchParams['ObjectSearch']['city_id']))){
            $filter['city_id'] = $searchParams['ObjectSearch']['city_id'];
        }

        $dataProvider = $searchModel->searchFilter($searchParams, true, false, 40);

        $this->view->params['ObjectSearch'] = $filter;

        $bannerMiddle = Json::decode( \frontend\components\widget\BannerMiddle::widget(['count'=>9, 'toJson'=>true]) );

        return $this->render('search-objects', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            //'stringResult' => $stringResult,
            'bannerMiddle' => $bannerMiddle,
            'ObjectSearch' => $filter
        ]);
    }

    public function actionSearchObjects($page=1)
    {
        $this->layout='total';
		$keywords = ['city'=>' Москва ', 'type'=>[' новостройки ',' в новостройке ']];
        $searchModel = $this->view->params['searchModelg'];
		if(!empty(Yii::$app->request->post())){
			$searchParams = Yii::$app->request->post();
		} else {
			$searchParams = Yii::$app->request->getQueryParams();
		}

		//########################
		$ObjectSearch = [];
		$rooms = 0;
		if(isset($searchParams['ObjectSearch']['room1']) && ($searchParams['ObjectSearch']['room1'] == 1)){
			$rooms = 1;
		}
		if(isset($searchParams['ObjectSearch']['room2']) && ($searchParams['ObjectSearch']['room2'] == 1)){
			$rooms = 2;
		}
		if(isset($searchParams['ObjectSearch']['room3']) && ($searchParams['ObjectSearch']['room3'] == 1)){
			$rooms = 3;
		}
		if(isset($searchParams['ObjectSearch']['room4']) && ($searchParams['ObjectSearch']['room4'] == 1)){
			$rooms = 4;
		}
		if(isset($searchParams['ObjectSearch']['city_id']) && (!empty($searchParams['ObjectSearch']['city_id']))){
			$ObjectSearch['city_id'] = $searchParams['ObjectSearch']['city_id'];
			$keywords['city'] = \common\enum\City::Lists($searchParams['ObjectSearch']['city_id']);
			$this->view->params['breadcrumbs'][] = [
				'label' => \common\enum\City::Lists($searchParams['ObjectSearch']['city_id']),
				'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
				'template' => "<li><b>{link}</b></li>\n", // template for this link only
			];
		}

		if(isset($searchParams['ObjectSearch']['novostroyki']) && (!empty($searchParams['ObjectSearch']['novostroyki']))){
			$ObjectSearch['novostroyki'] = 1;
			$keywords['type'][0] = ' новостройки ';
			$keywords['type'][1] = ' в новостройке ';
			$this->view->params['breadcrumbs'][] = [
				'label' => 'Новостройки',
				'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
				'template' => "<li><b>{link}</b></li>\n", // template for this link only
			];
		}
		if(isset($searchParams['ObjectSearch']['housing_estate']) && (!empty($searchParams['ObjectSearch']['housing_estate']))){
			$ObjectSearch['housing_estate'] = 1;
			$keywords['type'][0]  = ' Жилые комплексы ';
			$keywords['type'][1]  = ' в жилом комплексе ';
			$this->view->params['breadcrumbs'][] = [
				'label' => 'Жилые комплексы',
				'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
				'template' => "<li><b>{link}</b></li>\n", // template for this link only
			];
		}
		if(isset($searchParams['ObjectSearch']['option']) && (!empty($searchParams['ObjectSearch']['option']))) {
			$ObjectSearch['option'] = $searchParams['ObjectSearch']['option'];
			if($searchParams['ObjectSearch']['option'] == 3){
				$keywords['type'][0]  = ' квартиры от застройщика ';
				$keywords['type'][1]  = ' от застройщика ';
			}
			$this->view->params['breadcrumbs'][] = [
				'label' => ucfirst(\common\enum\User::Type($searchParams['ObjectSearch']['option'])),
				'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
				'template' => "<li><b>{link}</b></li>\n", // template for this link only
			];
		}
			if(!empty($rooms)){
				$ObjectSearch['room'.$rooms] = 1;
				$this->view->params['breadcrumbs'][] = [
					'label' => Yii::t('app', '{n, number} {n, plural, one{Комната} few{Комнаты} many{Комнаты} other{Комнаты}}', ['n'=>$rooms]),
					'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
					'template' => "<li><b>{link}</b></li>\n", // template for this link only
				];
			}/*  */


        /**
         *  Хлебные крошки если есть район в запросе
         */
        if(isset($searchParams['ObjectSearch']['district_id']) && (!empty($searchParams['ObjectSearch']['district_id']))){
            $ObjectSearch['district_id'] = $searchParams['ObjectSearch']['district_id'];
            $this->view->params['breadcrumbs'][] = [
        		'label' => \common\enum\District::Lists($searchParams['ObjectSearch']['district_id']),
    			//'url' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $ObjectSearch]),
				'template' => "<li><b>{link}</b></li>\n", // template for this link only
            ];
        }

        if(isset($searchParams['ObjectSearch']['maps']) && (!empty($searchParams['ObjectSearch']['maps']))){
    		$ObjectSearch['maps'] = $searchParams['ObjectSearch']['maps'];
			$this->view->params['breadcrumbs'][] = [
                'label' => \yii\helpers\Html::encode('Maps'),
            	'template' => "<li><span style='color: rgb(41, 171, 226);'>{link}</span></li>\n", // template for this link only
        	];
    	}
			/* print_r($this->view->params['breadcrumbs']); */
			
			/* if(!empty($realty->desc->title)){
				$ObjectSearch['room'.$realty->attr->rooms] = 1;
				$this->view->params['breadcrumbs'][] = [
					'label' => \yii\helpers\Html::encode($realty->desc->title),
					'template' => "<li><span style='color: rgb(41, 171, 226);'>{link}</span></li>\n", // template for this link only
				];
			} */
		//########################
		$this->view->registerMetaTag([
			'name' => 'keywords',
			'content' => 'Все '. $keywords['type'][0] .  ' город '. $keywords['city'].'. Купить '. \yii\helpers\Html::encode(' квартиру '). $keywords['type'][1] . ' город '. $keywords['city']
		]);

		$dataProvider = $searchModel->search($searchParams, true, false, 40);
        $stringResult = "$page-{$dataProvider->getCount()} из {$dataProvider->getTotalCount()} результатов";

        $bannerMiddle = Json::decode( \frontend\components\widget\BannerMiddle::widget(['count'=>9, 'toJson'=>true]) );
        $this->view->params['ObjectSearch'] = $ObjectSearch;
        

        return $this->render('search-objects', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'stringResult' => $stringResult,
            'bannerMiddle' => $bannerMiddle,
            'ObjectSearch' => $ObjectSearch
        ]);
    }
    public function actionSearchObjectsOnMap($page=1)
    {
        $this->layout='total';
		if(!empty(Yii::$app->request->post())){
			$searchParams = Yii::$app->request->post();
		} else {
			$searchParams = Yii::$app->request->getQueryParams();
		}
		//print_r($searchParams);

		//########################
		$ObjectSearch = [];
		$rooms = 0;
		if(isset($searchParams['ObjectSearch']['room1']) && ($searchParams['ObjectSearch']['room1'] == 1)){
			$rooms = 1;
		}
		if(isset($searchParams['ObjectSearch']['room2']) && ($searchParams['ObjectSearch']['room2'] == 1)){
			$rooms = 2;
		}
		if(isset($searchParams['ObjectSearch']['room3']) && ($searchParams['ObjectSearch']['room3'] == 1)){
			$rooms = 3;
		}
		if(isset($searchParams['ObjectSearch']['room4']) && ($searchParams['ObjectSearch']['room4'] == 1)){
			$rooms = 4;
		}
		if(isset($searchParams['ObjectSearch']['city_id']) && (!empty($searchParams['ObjectSearch']['city_id']))){
			$ObjectSearch['city_id'] = $searchParams['ObjectSearch']['city_id'];
			$this->view->params['breadcrumbs'][] = [
				'label' => \common\enum\City::Lists($searchParams['ObjectSearch']['city_id']),
				'url' => \yii\helpers\Url::toRoute(['site/search-objects-on-map', 'ObjectSearch' => $ObjectSearch]),
				'template' => "<li><b>{link}</b></li>\n", // template for this link only
			];
		} else {
			if(isset($_COOKIE['cityID']) && (!empty($_COOKIE['cityID']))) {
				$ObjectSearch['city_id'] = $_COOKIE['cityID'];
				$this->view->params['breadcrumbs'][] = [
					'label' => \common\enum\City::Lists($_COOKIE['cityID']),
					'url' => \yii\helpers\Url::toRoute(['site/search-objects-on-map', 'ObjectSearch' => $ObjectSearch]),
					'template' => "<li><b>{link}</b></li>\n", // template for this link only
				];
			}
		}
		if(isset($searchParams['ObjectSearch']['novostroyki']) && (!empty($searchParams['ObjectSearch']['novostroyki']))){
			$ObjectSearch['novostroyki'] = $searchParams['ObjectSearch']['novostroyki'];
			$this->view->params['breadcrumbs'][] = [
				'label' => 'Новостройки',
				'url' => \yii\helpers\Url::toRoute(['site/search-objects-on-map', 'ObjectSearch' => $ObjectSearch]),
				'template' => "<li><b>{link}</b></li>\n", // template for this link only
			];
		}
		if(isset($searchParams['ObjectSearch']['option']) && (!empty($searchParams['ObjectSearch']['option']))) {
			$ObjectSearch['option'] = $searchParams['ObjectSearch']['option'];
			$this->view->params['breadcrumbs'][] = [
				'label' => ucfirst(\common\enum\User::Type($searchParams['ObjectSearch']['option'])),
				'url' => \yii\helpers\Url::toRoute(['site/search-objects-on-map', 'ObjectSearch' => $ObjectSearch]),
				'template' => "<li><b>{link}</b></li>\n", // template for this link only
			];
		}
			if(!empty($rooms)){
				$ObjectSearch['room'.$rooms] = 1;
				$this->view->params['breadcrumbs'][] = [
					'label' => Yii::t('app', '{n, number} {n, plural, one{Комната} few{Комнаты} many{Комнаты} other{Комнаты}}', ['n'=>$rooms]),
					'url' => \yii\helpers\Url::toRoute(['site/search-objects-on-map', 'ObjectSearch' => $ObjectSearch]),
					'template' => "<li><b>{link}</b></li>\n", // template for this link only
				];
			}/*  */
			if(isset($searchParams['ObjectSearch']['district_id']) && (!empty($searchParams['ObjectSearch']['district_id']))){
				$ObjectSearch['district_id'] = $searchParams['ObjectSearch']['district_id'];
				$this->view->params['breadcrumbs'][] = [
					'label' => \common\enum\District::Lists($searchParams['ObjectSearch']['district_id']),
					'url' => \yii\helpers\Url::toRoute(['site/search-objects-on-map', 'ObjectSearch' => $ObjectSearch]),
					'template' => "<li><b>{link}</b></li>\n", // template for this link only
				];
			}
			/* print_r($this->view->params['breadcrumbs']); */
			
			$ObjectSearch['maps'] = $searchParams['ObjectSearch']['maps'];
			$this->view->params['breadcrumbs'][] = [
				'label' => \yii\helpers\Html::encode('Maps'),
				'template' => "<li><span style='color: rgb(41, 171, 226);'>{link}</span></li>\n", // template for this link only
			];
			$this->view->params['ObjectSearch'] = $ObjectSearch;
		//########################

        $searchModel = $this->view->params['searchModelg'];
        $dataProvider = $searchModel->search($searchParams, false, true);

        $objectsMap = $searchModel->search($searchParams, false, true, 15);
        $objs = $searchModel->getObjectsOnMap();
        
//        echo "<pre>".print_r($objs, true)."</pre>";

							/* echo'createUrl<pre>';
					print_r($dataProvider);
					print_r($params);
					echo'</pre>'; */
		/* die(); */

        //$stringResult = "$page-{$dataProvider->getCount()} из {$dataProvider->getTotalCount()} результатов";
        
        //Объекты которые нужно показать на карте
        $objectsForMap = [];

        
        foreach($objs as $object) {

            if (!empty($object['geo_lat']) && !empty($object['geo_lng'])) {
                $objectsForMap[] = [
                    'id' => $object['id'],
                    'url'=> 'https://stroyki.pro/'. \common\components\TranslitHelper::transliteration($object['city_title']) .'/' . $object['id'],
                    'title' => $object['desc_title'],
                    'geo' => [
                        $object['geo_lat'],
                        $object['geo_lng']
                    ],
                    'img'=>ThumbHelper::thumbnailImg($searchModel->mainImage($object['id'], $object['image']), 100, 75, ThumbHelper::THUMBNAIL_OUTBOUND, [], false),
                    'company' => $object['housing_estate'] ? $object['housing_estate'] : '',
                    'district' => $object['district_title'] ? $object['district_title'] : '',
                    'deadline' => intval((date('n', $object['deadline'])+2)/3) . " кв " . date("Y", $object['deadline']),
                    'price' => preg_replace('#(?<=\d)(?=(\d{3})+$)#', ' ', $object['price']) . ' руб',
                    'favoriteClass' => 'color-yellow'
                ];
            }

        }

        //Город для позиционирования на карте или страна
        $positionPlace = 'Россия';
        $zoom = 3;

        if (isset($_COOKIE['cityID']) && is_numeric($_COOKIE['cityID'])) {
            if (!\common\enum\City::Lists($searchParams['ObjectSearch']['city_id']))
                $positionPlace = \common\enum\City::Lists($_COOKIE['cityID']);
            else
                $positionPlace = \common\enum\City::Lists($searchParams['ObjectSearch']['city_id']);
            $zoom = 10;
        }


        return $this->render('search-objects-on-map', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'objectsForMap' => Json::encode($objectsForMap),
            'positionPlace' => $positionPlace,
            'zoom' => $zoom,
            'stringResult' => 5,
            'ObjectSearch' => $ObjectSearch
        ]);
    }
    public function actionPage($alias='')
    {
        $alias = HtmlPurifier::process($alias);
        $page = Pages::findOne(['alias'=>$alias]);
        if($page) {
            $this->layout = 'total';
			$this->view->params['bread'] = '_total';
			$this->view->params['r'] = array();
            return $this->render('page', ['page'=>$page]);
        }
        throw new NotFoundHttpException(Yii::$app->params['error404']);
    }
    
    public function actionBanner($id)
    {
        $banner = $this->getModelOr404(Banners::className(), $id);
        Yii::$app->stat->regClick('banner', $id);
        return $this->redirect($banner->link);
    }
	
    public function actionArticle($id)
    { 
		$bannerMiddle = Json::decode( \frontend\components\widget\BannerMiddle::widget(['count'=>9, 'toJson'=>true]) );
        $article = Articles::findOne(['id'=> $id]);
		/* print_r( $article);
		die(); */
		if($article){
			$this->layout = 'total';
			$this->view->params['bread'] = '_total';
			$this->view->params['r'] = array();
			return $this->render('article', ['bannerMiddle' => $bannerMiddle,'articles'=> $article]);
		} 
		throw new NotFoundHttpException(Yii::$app->params['error404']);
    }

    /**
     * Валидация ипотечного калькулятора
     */
    public function actionValidateMortgageCalculator()
    {
        $form = new CalculatorForm();
        return $this->validate($form, 'default', Yii::$app->request->post());
    }

    /**
     * Ипотечный калькулятор
     */
    public function actionMortgageCalculator()
    {
        $form = new CalculatorForm();
        $form->load(Yii::$app->request->post());

        return $form->calculation();
    }

    /**
     * Все объявления по пользователю
     */
    public function actionAllRealty($user_id=0)
    {
        if(is_numeric($user_id)) {
            $this->layout = 'total';
            $dataProvider = new ActiveDataProvider([
                'query' => Realty::find()->with(['attr','desc','images','user'])->where([
                    'status'=>\common\enum\Realty::STATUS_PUBLIC,
                    'user_id'=>$user_id,
                ])->orderBy('id DESC'),
                'pagination' => [
                    'pageSize' => 3,
                ],
            ]);

            $this->user = User::findOne($user_id);
            $city = '';
            if ($this->user->city) {
               $city = $this->user->city;
            }else {
                $city = City::find()->where(["id" => (int) $_COOKIE['cityID']])->one();
                $city = $city->title;
            }

            $this->view->params['breadcrumbs'][] = [
                'label' => $city,
                'url' => '/'.TranslitHelper::transliteration($city),
            ];

            $this->view->params['breadcrumbs'][] = [
                'label' => 'Застройщики',
                'url' => '/'.TranslitHelper::transliteration($city).'/zastroyshchiki',
            ];

            if ($this->user->buildcompany) {
                $this->view->params['breadcrumbs'][] = [
                    'label' => $this->user->buildcompany,
                    'url' => Url::to(['site/all-realty', 'user_id' => $this->user->id]),
                ];
            }
            $this->view->params['breadcrumbs'][] = [
                'label' => 'Все объявления',
                'template' => '<li class="active">{link}</li>'
            ];



            //echo "<pre>".print_r($this->user, true)."</pre>";
			if($this->user){
			    Yii::$app->session->set('userObj', $this->user);
				return $this->render('all-realty', [
					'listDataProvider' => $dataProvider,
					'user' => $this->user
				]);
	 
			} else {
				throw new NotFoundHttpException(Yii::$app->params['error404']);
			}
        }
        throw new NotFoundHttpException(Yii::$app->params['error404']);
    }

    /*
     * Страница: Контакты
     */

    public function actionContact($user_id = 0) {
        $this->layout = 'total';

        if(is_numeric($user_id)) {
            $this->user = User::findOne($user_id);

            if($this->user){
                // Next code...
                $city = '';
                if ($this->user->city) {
                    $city = $this->user->city;
                }else {
                    $city = City::find()->where(["id" => (int) $_COOKIE['cityID']])->one();
                    $city = $city->title;
                }

                $this->view->params['breadcrumbs'][] = [
                    'label' => $city,
                    'url' => '/'.TranslitHelper::transliteration($city),
                ];

                $this->view->params['breadcrumbs'][] = [
                    'label' => 'Застройщики',
                    'url' => '/'.TranslitHelper::transliteration($city).'/zastroyshchiki',
                ];

                if ($this->user->buildcompany) {
                    $this->view->params['breadcrumbs'][] = [
                        'label' => $this->user->buildcompany,
                        'url' => Url::to(['site/all-realty', 'user_id' => $this->user->id]),
                    ];
                }
                $this->view->params['breadcrumbs'][] = [
                    'label' => 'Контакты',
                    'template' => '<li class="active">{link}</li>'
                ];

                $user = $this->user;
                Yii::$app->session->set('userObj', $user);

                return $this->render('contacts', compact('user'));
            } else {
                throw new NotFoundHttpException(Yii::$app->params['error404']);
            }
        } else {
            throw new NotFoundHttpException(Yii::$app->params['error404']);
        }
    }

    /*
     * Страница: Ипотечный калькулятор
     */

    public function actionCalculator($user_id = 0) {
        $this->layout = 'total';

        if(is_numeric($user_id)) {
            $this->user = User::findOne($user_id);

            if($this->user){
                // Next code...
                $city = '';
                if ($this->user->city) {
                    $city = $this->user->city;
                }else {
                    $city = City::find()->where(["id" => (int) $_COOKIE['cityID']])->one();
                    $city = $city->title;
                }

                $this->view->params['breadcrumbs'][] = [
                    'label' => $city,
                    'url' => '/'.TranslitHelper::transliteration($city),
                ];

                $this->view->params['breadcrumbs'][] = [
                    'label' => 'Застройщики',
                    'url' => '/'.TranslitHelper::transliteration($city).'/zastroyshchiki',
                ];

                if ($this->user->buildcompany) {
                    $this->view->params['breadcrumbs'][] = [
                        'label' => $this->user->buildcompany,
                        'url' => Url::to(['site/all-realty', 'user_id' => $this->user->id]),
                    ];
                }
                $this->view->params['breadcrumbs'][] = [
                    'label' => 'Ипотечный калькулятор',
                    'template' => '<li class="active">{link}</li>'
                ];

                $user = $this->user;
                Yii::$app->session->set('userObj', $user);

                return $this->render('calculator', compact('user'));
            } else {
                throw new NotFoundHttpException(Yii::$app->params['error404']);
            }
        } else {
            throw new NotFoundHttpException(Yii::$app->params['error404']);
        }
    }

    /**
     * Все объявления по пользователю
     */
    public function actionUpdateCities()
    {
		$cities = District::find()->all();
		foreach($cities as $city){
			$city['alias'] = \common\components\TranslitHelper::transliteration($city['title']);
			$city->update();
		}
			/* print_r($city);
			die();
		print_r(count($cities));
		die(); */
		//
    }
	

    /**
     * Страница выбора города
     */
    public function actionChangeCity()
    {
		$this->view->params['breadcrumbs'][] = [
			'label' => 'Выбор города',
			'url' => \yii\helpers\Url::toRoute(['site/change-city', []]),
			'template' => "<li><b>{link}</b></li>\n", // template for this link only
		];
		$this->layout = 'total';
		
		return $this->render('change-city', []);
		throw new NotFoundHttpException(Yii::$app->params['error404']);
	}

    public function actionFormat() {

        echo Yii::$app->formatter->asDecimal($_POST['resultCalc'], 0);

    }


    /**
     *
     */
    public function actionRequestSupport()
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {

            $user = User::findIdentity(4);


            Yii::$app->mailer->compose([
                'html' => 'requestSupport',
            ], $request->post())
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['sitename']])
                ->setTo($user->email)
                ->setSubject('Запрос поддержки')
                ->send();
        }
    }
}
