<?php
$this->title = 'Подписка на рассылку, при смене цены';
?>

<div class="container">
    <h2 class="title">
        <?= $this->title, ', всего '.$count ?>
    </h2>

    <div id="search-result-objects" class="row well">
        <?php \yii\widgets\Pjax::begin([
            'timeout'=>10000,
            'scrollTo'=>new \yii\web\JsExpression('$("#search-result-objects").position().top')
        ]) ?>

        <?= \yii\widgets\ListView::widget([
            'dataProvider' => $dataProvider,
            'layout' => "{items}<div id='paginate'>{pager}</div>",
            'itemView' => function ($model, $key, $index, $widget) {
                return $this->render('_item_spy_price',[
                    'model' => $model,
                    'index' => $index,
                ]);
            },
        ]) ?>

        <?php \yii\widgets\Pjax::end() ?>
    </div>
</div>
