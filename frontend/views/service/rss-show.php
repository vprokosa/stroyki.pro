<?php

/* @var $this yii\web\View */

$this->title = "Просмотр рассылки";

$this->params['breadcrumbs'][] = ['label' => 'Просмотр рассылки'];
?>
<div class="container">
    <h3><?= $this->title ?></h3>
    <?= \yii\widgets\DetailView::widget([
        'model' => $model,
        'formatter' => [
            'class' => \yii\i18n\Formatter::className(),
            'nullDisplay' => ''
        ],
        'attributes' => [
            [
                'attribute'=> 'city_id',
                'value' => $model->city_id>0 ? \common\enum\City::Lists($model->city_id) : '',
            ],
            [
                'attribute'=> 'room1',
                'value' => $model->room1 ? 'Да' : 'Нет',
            ],
            [
                'attribute'=> 'room2',
                'value' => $model->room2 ? 'Да' : 'Нет',
            ],
            [
                'attribute'=> 'room3',
                'value' => $model->room3 ? 'Да' : 'Нет',
            ],
            [
                'attribute'=> 'room4',
                'value' => $model->room4 ? 'Да' : 'Нет',
            ],
            'start_area',
            'end_area',
            'start_price',
            'end_price',
            [
                'attribute'=> 'option',
                'value' => $model->option>0 ? \common\enum\ObjectSearch::Options($model->option) : '',
            ],
        ],
    ]) ?>
</div>
