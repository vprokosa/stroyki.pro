<?php
use common\components\ThumbHelper;
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <a href="<?= \yii\helpers\Url::to(['site/advertisement', 'alias'=>$model->alias]) ?>">
        <div class="box">
            <div class="box-header">
                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 10px">
                    <picture>
                        <source srcset="<?= ThumbHelper::thumbnailImg($model->mainImage(), 1000, 750, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>" media="(min-width: 1000px)">
                        <source srcset="<?= ThumbHelper::thumbnailImg($model->mainImage(), 800, 600, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>" media="(min-width: 800px)">
                        <source srcset="<?= ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>">
                        <img class="img-responsive img-thumbnail" srcset="<?= ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>" alt="…">
                    </picture>
                </div>
            </div>
            <div class="box-body">
                <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                    <div class="text4 pull-right">
                        <?= $this->render('/layouts/_btn_favorite', ['model'=>$model]) ?>
                        <?= $this->render('/layouts/_btn_spy_price', ['model'=>$model]) ?>
                    </div>

                    <h3 class="text-success"><?= $model->price ?> руб</h3>
                    <p><small class="text-muted"><?= is_numeric($model->user->type) ? \common\enum\User::Type($model->user->type) : '' ?></small></p>
                    <p><strong class="text-middle-x"><?= \yii\helpers\Html::encode($model->desc->title) ?></strong></p>
                    <p>
                        <strong class="text-aqua text-middle-x">
                            город: <?= \common\enum\City::Lists($model->attr->city_id) ?>,
                            р-н: <?= \common\enum\District::Lists($model->attr->district_id) ?>,
                            ул.<?= \yii\helpers\Html::encode($model->attr->street) ?>,
                            дом <?= \yii\helpers\Html::encode($model->attr->house) ?>
                        </strong>
                    </p>
                    <?php if(!$model->isApartmentHouse()): ?>
                        <p>
                            комнат: <?= \yii\helpers\Html::encode($model->attr->rooms) ?>,
                            площадь: <?= is_numeric($model->attr->area_all) ? $model->attr->area_all.'<sup>2</sup>' : '' ?>
                        </p>
                    <?php endif ?>

                    <?php /*
                    <p class="text-middle-x"><?= \yii\helpers\StringHelper::truncateWords(\yii\helpers\Html::encode($model->desc->details), 5)  ?></p>
                    */ ?>

                    <?php /*
                    <p class="pull-right">
                        <strong class="text-muted text-middle-x">Срок сдачи: </strong>
                        <strong class="text-aqua text-middle-x">4 кв. 2016</strong>
                    </p>
                    */ ?>

                </div>
            </div>
        </div>
    </a>
</div>