<?php

/* @var $this yii\web\View */

$this->title = "Рассылка по поиску";

$this->params['breadcrumbs'][] = ['label' => 'Рассылка по поиску'];
?>

<div class="container">
    <h3><?= $this->title ?></h3>

    <?php \yii\widgets\Pjax::begin([
        'timeout'=>10000,
        'scrollTo'=>new \yii\web\JsExpression('$(".grid-view").position().top')
    ]) ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider'=>$provider,
        'emptyText' => 'Ничего не найдено',
        'summary'=>'',
        'formatter' => [
            'class' => \yii\i18n\Formatter::className(),
            'nullDisplay' => ''
        ],'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions'=>[
                    'style'=>'width:40px'
                ]
            ],
            [
                'attribute'=>'start_price',
            ],
            [
                'attribute'=>'end_price',
            ],
            [
                'attribute'=>'city_id',
                'content'=>function($data) {
                    return $data->city_id>0 ? \common\enum\City::Lists($data->city_id) : '';
                },
            ],
            [
                'attribute'=>'created_at',
                'content'=>function($data) {
                    return date('d.m.Y', $data->created_at);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
                'buttons' => [
                    'view' => function ($url, $model){
                        return \yii\helpers\Html::a('<i class="fa fa-eye"></i>', \yii\helpers\Url::to(['/service/rss-show', 'id'=>$model->id]), [
                            'class' => 'btn-xs btn-info',
                            'title' => 'Просмотр',
                        ]);
                    },
                    'delete' => function ($url, $model){
                        return \yii\helpers\Html::a('<i class="fa fa-trash"></i>', \yii\helpers\Url::to(['/service/rss-delete', 'id'=>$model->id]), [
                            'class' => 'btn-xs btn-danger',
                            'title' => 'Удалить',
                            'data-confirm' => 'Удалить ?'
                        ]);
                    },
                ]
            ],
        ],
    ]) ?>

    <?php \yii\widgets\Pjax::end() ?>
</div>

