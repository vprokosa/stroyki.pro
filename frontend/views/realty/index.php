<?php
$this->title = 'Мои объявления';
$this->registerJs(
    '$(document).ready(function () {
        $("#tab_'.$status.'").addClass("active");
        $("#'.$status.'").addClass("in").addClass("active");
    });'
);
 ?>

<style>
    #cabinet-header > div > div{padding-left:0px !important;padding-right:15px !important;}
    #object-list .col-lg-12{padding-left:0px;padding-right:17px !important;left:-14px}
    #paginate{margin-left:30px;}
    #banners > div{margin-left:-26px;margin-top:-20px;}
    @media only screen and (max-width:768px) {
        #object-list h2{margin-left:20px !important;}
        .container{padding-right:0px;}
        #object-list{margin-left:0px !important;}
        #object-list .box{right: -14px !important;}
        .list-view .box{left:0px;}
        .list-view .box-body .col-xs-12{margin-top:0px !important;}
    }
</style>

<div id="object-list" class="container" style="padding-right:0px;">
    <div class="row" style="margin-left:0px;">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left: 0px; padding-right: 30px;">

            <div class="box">
                <div class="box-header">
                    <h2><?= $this->title ?></h2>
                </div>
                <div class="box-body">

                    <ul class="nav nav-tabs">
                        <li id="tab_public"><a href="<?= \yii\helpers\Url::to(['index']) ?>"><?= \common\enum\Realty::Status(\common\enum\Realty::STATUS_PUBLIC) ?></a></li>
                        <li id="tab_draft"><a href="<?= \yii\helpers\Url::to(['index-draft']) ?>"><?= \common\enum\Realty::Status(\common\enum\Realty::STATUS_DRAFT) ?></a></li>
                        <li id="tab_archive"><a href="<?= \yii\helpers\Url::to(['index-archive']) ?>"><?= \common\enum\Realty::Status(\common\enum\Realty::STATUS_ARCHIVE) ?></a></li>
                    </ul>

                    <div class="tab-content">

                        <div id="public" class="tab-pane fade">

                            <?php if($status == 'public'): ?>
                                <div class="row well" style="margin-left: 0px;margin-right: 0px;">
                                    <?php \yii\widgets\Pjax::begin([
                                        'timeout'=>10000,
                                        'scrollTo'=>new \yii\web\JsExpression('$("#object-list").position().top')
                                    ]) ?>

                                    <?= \yii\widgets\ListView::widget([
                                        'dataProvider' => $listDataProvider,
                                        'layout' => "{items}<div class='row'><div class='col-lg-12 col-md-12'><div id='paginate'>{pager}</div></div></div>",
                                        'itemView' => function ($model, $key, $index, $widget) {
                                            return $this->render('_item',[
                                                'model' => $model,
                                                'index' => $index,
                                            ]);
                                        },
                                    ]) ?>

                                    <?php \yii\widgets\Pjax::end() ?>
                                </div>
                            <?php endif ?>


                        </div>
                        <div id="draft" class="tab-pane fade">

                            <?php if($status == 'draft'): ?>
                                <div class="row well" style="margin-left: 0px;margin-right: 0px;">
                                    <?php \yii\widgets\Pjax::begin([
                                        'timeout'=>10000,
                                        'scrollTo'=>new \yii\web\JsExpression('$("#object-list").position().top')
                                    ]) ?>

                                    <?= \yii\widgets\ListView::widget([
                                        'dataProvider' => $listDataProvider,
                                        'layout' => "{items}<div class='row'><div class='col-lg-12 col-md-12'><div id='paginate'>{pager}</div></div></div>",
                                        'itemView' => function ($model, $key, $index, $widget) {
                                            return $this->render('_item',[
                                                'model' => $model,
                                                'index' => $index,
                                            ]);
                                        },
                                    ]) ?>

                                    <?php \yii\widgets\Pjax::end() ?>
                                </div>
                            <?php endif ?>

                        </div>
                        <div id="archive" class="tab-pane fade">

                            <?php if($status == 'archive'): ?>
                                <div class="row well" style="margin-left: 0px;margin-right: 0px;">
                                    <?php \yii\widgets\Pjax::begin([
                                        'timeout'=>10000,
                                        'scrollTo'=>new \yii\web\JsExpression('$("#object-list").position().top')
                                    ]) ?>

                                    <?= \yii\widgets\ListView::widget([
                                        'dataProvider' => $listDataProvider,
                                        'layout' => "{items}<div class='row'><div class='col-lg-12 col-md-12'><div id='paginate'>{pager}</div></div></div>",
                                        'itemView' => function ($model, $key, $index, $widget) {
                                            return $this->render('_item',[
                                                'model' => $model,
                                                'index' => $index,
                                            ]);
                                        },
                                    ]) ?>

                                    <?php \yii\widgets\Pjax::end() ?>
                                </div>
                            <?php endif ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
