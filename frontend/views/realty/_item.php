<?php
use common\components\ThumbHelper;
?>

<div class="box">
    fcfr
    <div class="box-header">
        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12" style="padding-left:0px;">
            <a href="<?= \yii\helpers\Url::to(['site/advertisement', 'id'=>$model->id]) ?>">
                <?php if(!$model->vip): ?>
                    <img class="img-responsive img-thumbnail" src="<?= ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>" style="padding:0px;">
                <?php else: ?>
                    <img class="img-responsive img-thumbnail" src="<?= \yii\helpers\Url::to(['site/vip-image', 'url'=>ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false)]) ?>" style="padding:0px;">
                <?php endif ?>
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
            <div style="position:absolute;top:8%;right:1%;z-index:100;width: 80px;">
                <?= \yii\helpers\Html::a('<i class="fa fa-eye"></i>', ['site/advertisement', 'id'=>$model->id], [
                    'class' => 'btn-xs btn-info',
                    'title' => 'Просмотр',
                ]) ?>

                <?= \yii\helpers\Html::a('<i class="fa fa-pencil-square-o"></i>', ['update', 'id'=>$model->id], [
                    'class' => 'btn-xs btn-warning',
                    'title' => 'Обновить',
                ]) ?>

                <?= \yii\helpers\Html::a('<i class="fa fa-trash"></i>', ['delete', 'id'=>$model->id], [
                    'class' => 'btn-xs btn-danger',
                    'title' => 'Удалить',
                    'data-confirm' => 'Удалить ?'
                ]) ?>
            </div> 
            <br>
            <p class="text-green text-bold text-big"><?= $model->desc->title ?> </p>
            <br>
            <p class="text-muted">Цена: <?= $model->price ?>
            <?php if(is_numeric($model->attr->area_all)): ?>
                - <?=  round($model->price/$model->attr->area_all, 0, PHP_ROUND_HALF_EVEN) ?> руб м²
            <?php endif ?>
            </p>
            <p class="text-muted">Город : <?= \common\enum\City::Lists($model->attr->city_id) ?></p>
            <p class="text-muted">Район : <?= \common\enum\District::Lists($model->attr->district_id) ?></p>
            <p class="text-muted">улица : <?= $model->attr->street ?></p>
            <p class="text-muted">дом : <?= $model->attr->house ?></p>
            <p class="text-muted">комнат: <?= $model->attr->rooms ?></p>
            <br>
            <?php /*
                Срок сдачи: 4 кв. 2016
                <span class="fa fa-star-o pull-right color-yellow" style="margin-right:20px;"></span>
                <span class="fa fa-eye pull-right color-yellow"></span>
                */ ?>
            </p>
        </div>
    </div>
</div>