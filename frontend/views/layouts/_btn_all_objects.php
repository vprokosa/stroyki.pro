<script>
    function ShowAllRealtyInCity(id)
    {
        $.cookie('cityID', id, { expires: 7, path: '/' });
        location.href = "/";
    }
</script>


<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="all">
                <div class="d-table">
                    <div class="d-row">
                        <div class="heading-line more">
                            <button type="button" onclick="ShowAllRealtyInCity(<?= $realty->attr->city_id ?>)" >Все новостройки города <?= \common\enum\City::Lists($realty->attr->city_id) ?> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
