<div id="banners" class="container" >
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <picture>
                <source srcset="/image/pik450.jpg" media="(min-width: 1000px)">
                <source srcset="/image/pik450.jpg" media="(min-width: 800px)">
                <source srcset="/image/pik700.jpg" media="(max-width: 768px)">
                <img class="img-responsive img-thumbnail" srcset="/image/pik450.jpg" alt="…">
            </picture>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <picture>
                <source srcset="/image/pik450.jpg" media="(min-width: 1000px)">
                <source srcset="/image/pik450.jpg" media="(min-width: 800px)">
                <source srcset="/image/pik700.jpg" media="(max-width: 768px)">
                <img class="img-responsive img-thumbnail" srcset="/image/pik450.jpg" alt="…">
            </picture>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <picture>
                <source srcset="/image/pik450.jpg" media="(min-width: 1000px)">
                <source srcset="/image/pik450.jpg" media="(min-width: 800px)">
                <source srcset="/image/pik700.jpg" media="(max-width: 768px)">
                <img class="img-responsive img-thumbnail" srcset="/image/pik450.jpg" alt="…">
            </picture>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <picture>
                <source srcset="/image/pik450.jpg" media="(min-width: 1000px)">
                <source srcset="/image/pik450.jpg" media="(min-width: 800px)">
                <source srcset="/image/pik700.jpg" media="(max-width: 768px)">
                <img class="img-responsive img-thumbnail" srcset="/image/pik450.jpg" alt="…">
            </picture>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <picture>
                <source srcset="/image/pik450.jpg" media="(min-width: 1000px)">
                <source srcset="/image/pik450.jpg" media="(min-width: 800px)">
                <source srcset="/image/pik700.jpg" media="(max-width: 768px)">
                <img class="img-responsive img-thumbnail" srcset="/image/pik450.jpg" alt="…">
            </picture>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
            <picture>
                <source srcset="/image/pik450.jpg" media="(min-width: 1000px)">
                <source srcset="/image/pik450.jpg" media="(min-width: 800px)">
                <source srcset="/image/pik700.jpg" media="(max-width: 768px)">
                <img class="img-responsive img-thumbnail" srcset="/image/pik450.jpg" alt="…">
            </picture>
        </div>
    </div>
</div>