<?php $this->beginContent('@frontend/views/layouts/cabinet.php') ?>
<?php 
$this->registerJs(
    '$(document).ready(function () {
            var url = location.pathname;
            $("#profile [href=\'"+url+"\']").parent("li").addClass("active");
        });'
);

 ?>
    <?= $this->render('_alert') ?> 

    <div id="profile">
        <h2><?= $this->title ?></h2>
        <br>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                <ul class="nav nav-pills nav-stacked">
                    <li>
                        <?= \yii\helpers\Html::a('Профиль', ['profile/index']) ?>
                    </li>
                    <li>
                        <?= \yii\helpers\Html::a('Аватарка', ['profile/avatar']) ?>
                    </li>
                    <li>
                        <?= \yii\helpers\Html::a('Логотип', ['profile/logo']) ?>
                    </li>
                    <li>
                        <?= \yii\helpers\Html::a('Фон шапки', ['profile/header']) ?>
                    </li>
                    <li>
                        <?= \yii\helpers\Html::a('Редактировать', ['profile/update']) ?>
                    </li>
                    <li>
                        <?= \yii\helpers\Html::a('Поменять пароль', ['profile/change-password']) ?>
                    </li>
                    <li>
                        <?= \yii\helpers\Html::a('Подписка', ['profile/subscription']) ?>
                    </li>
                </ul>
            </div>
            <div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
                <?= $content ?>
            </div>
        </div>
    </div>

<?php $this->endContent() ?>