<div id="objects-preview" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
                <div class="box">
                    <div class="box-header">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12 ">
                            <picture>
                                <source srcset="/image/o1.jpg" media="(min-width: 1000px)">
                                <source srcset="/image/o1.jpg" media="(min-width: 800px)">
                                <source srcset="/image/o1-750.jpg" media="(max-width: 768px)">
                                <img class="img-responsive img-thumbnail" srcset="/image/o1.jpg" alt="…">
                            </picture>
                        </div>
                    </div>
                    <div class=" box-body">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <h3>Медовый город</h3>
                            <div class="text1">
                                <p>р-н: Заволжский</p>
                                <p>ул. мостецкая стр.107</p>
                                <p>Срок сдачи: 2 кв 2018 г</p>
                            </div>
                            <div class="text2">
                                <p>1 комнатные от ..................100 000 000 руб</p>
                                <p>2 комнатные от ..................200 000 000 руб</p>
                                <p>3 комнатные от ..................300 000 000 руб</p>
                            </div>
                            <div class="text3">
                                <p>Застройщик: Севзаплесзнергосбыт</p>
                            </div>
                            <div class="text4">
                                <span class="fa fa-clock-o"></span>
                                <i>Вчера 19:01</i>
                                <span class="fa fa-star-o pull-right color-yellow"></span>
                                <span class="fa fa-eye pull-right color-yellow"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <!-- banner -->
        <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
            <div class="box-banner">
                <div class="box-header"></div>
                <div class="box-body">
                    <div class="col-sm-6 col-xs-12" style="margin-bottom:20px">
                        <img class="img-responsive img-thumbnail" src="/image/banner.jpg" alt="" >
                    </div>
                    <div class="col-sm-6 col-xs-12" style="margin-bottom:30px">
                        <img class="img-responsive img-thumbnail" src="/image/banner.jpg" alt="" >
                    </div>
                </div>
            </div>
        </div>
        <!-- #banner -->

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
                <div class="box">
                    <div class="box-header">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <picture>
                                <source srcset="/image/o2.jpg" media="(min-width: 1000px)">
                                <source srcset="/image/o2.jpg" media="(min-width: 800px)">
                                <source srcset="/image/o2-750.jpg" media="(max-width: 768px)">
                                <img class="img-responsive img-thumbnail" srcset="/image/o2.jpg" alt="…">
                            </picture>
                        </div>
                    </div>
                    <div class=" box-body">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <h3>2 545 000 руб</h3>
                            <div class="text1">
                                <p>2-к квартира, 66 м², 1/5 эт.</p>
                                <p>р-н: Заволжский</p>
                                <p>ул. мостецкая стр.107</p>
                                <p>Срок сдачи: 2 кв 2018 г</p>
                            </div>
                            <div class="text2">

                            </div>
                            <div class="text3">
                                <p>Собственник:: Евгений</p>
                            </div>
                            <div class="text4">
                                <span class="fa fa-clock-o"></span>
                                <i>Вчера 19:01</i>
                                <span class="fa fa-star-o pull-right color-yellow"></span>
                                <span class="fa fa-eye pull-right color-yellow"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <!-- banner -->
        <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
            <div class="box-banner">
                <div class="box-header"></div>
                <div class="box-body">
                    <div class="col-sm-6 col-xs-12" style="margin-bottom:20px">
                        <img class="img-responsive img-thumbnail" src="/image/banner.jpg" alt="" >
                    </div>
                    <div class="col-sm-6 col-xs-12" style="margin-bottom:30px">
                        <img class="img-responsive img-thumbnail" src="/image/banner.jpg" alt="" >
                    </div>
                </div>
            </div>
        </div>
        <!-- #banner -->

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
                <div class="box">
                    <div class="box-header">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <picture>
                                <source srcset="/image/o3.jpg" media="(min-width: 1000px)">
                                <source srcset="/image/o3.jpg" media="(min-width: 800px)">
                                <source srcset="/image/o3-750.jpg" media="(max-width: 768px)">
                                <img class="img-responsive img-thumbnail" srcset="/image/o3.jpg" alt="…">
                            </picture>
                        </div>
                    </div>
                    <div class=" box-body">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <h3>Медовый город</h3>
                            <div class="text1">
                                <p>р-н: Заволжский</p>
                                <p>ул. мостецкая стр.107</p>
                                <p>Срок сдачи: 2 кв 2018 г</p>
                            </div>
                            <div class="text2">
                                <p>1 комнатные от ..................100 000 000 руб</p>
                                <p>2 комнатные от ..................200 000 000 руб</p>
                                <p>3 комнатные от ..................300 000 000 руб</p>
                            </div>
                            <div class="text3">
                                <p>Застройщик: Севзаплесзнергосбыт</p>
                            </div>
                            <div class="text4">
                                <span class="fa fa-clock-o"></span>
                                <i>Вчера 19:01</i>
                                <span class="fa fa-star-o pull-right color-yellow"></span>
                                <span class="fa fa-eye pull-right color-yellow"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <!-- banner -->
        <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
            <div class="box-banner">
                <div class="box-header"></div>
                <div class="box-body">
                    <div class="col-sm-6 col-xs-12" style="margin-bottom:20px">
                        <img class="img-responsive img-thumbnail" src="/image/banner.jpg" alt="" >
                    </div>
                    <div class="col-sm-6 col-xs-12" style="margin-bottom:30px">
                        <img class="img-responsive img-thumbnail" src="/image/banner.jpg" alt="" >
                    </div>
                </div>
            </div>
        </div>
        <!-- #banner -->

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
                <div class="box">
                    <div class="box-header">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <picture>
                                <source srcset="/image/o4.jpg" media="(min-width: 1000px)">
                                <source srcset="/image/o4.jpg" media="(min-width: 800px)">
                                <source srcset="/image/o4-750.jpg" media="(max-width: 768px)">
                                <img class="img-responsive img-thumbnail" srcset="/image/o4.jpg" alt="…">
                            </picture>
                        </div>
                    </div>
                    <div class=" box-body">
                        <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                            <h3>2 545 000 руб</h3>
                            <div class="text1">
                                <p>2-к квартира, 66 м², 1/5 эт.</p>
                                <p>р-н: Заволжский</p>
                                <p>ул. мостецкая стр.107</p>
                                <p>Срок сдачи: 2 кв 2018 г</p>
                            </div>
                            <div class="text2">

                            </div>
                            <div class="text3">
                                <p>Собственник:: Евгений</p>
                            </div>
                            <div class="text4">
                                <span class="fa fa-clock-o"></span>
                                <i>Вчера 19:01</i>
                                <span class="fa fa-star-o pull-right color-yellow"></span>
                                <span class="fa fa-eye pull-right color-yellow"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <!-- banner -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box-banner">
                <div class="box-header"></div>
                <div class="box-body-banner">
                    <img class="img-responsive img-thumbnail" src="/image/banner-h.jpg" alt="" style="height:255px">
                </div>
            </div>
        </div>
        <!-- #banner -->

    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div id="paginate">
                <ul class="pagination">
                    <li class="paginate_button previous disabled" id="example1_previous">
                        <a href="#">&lt;</a>
                    </li>
                    <li class="paginate_button active">
                        <a href="#">1</a>
                    </li>
                    <li class="paginate_button ">
                        <a href="#">2</a>
                    </li>
                    <li class="paginate_button ">
                        <a href="#">3</a>
                    </li>
                    <li class="paginate_button ">
                        <a href="#">4</a>
                    </li>
                    <li class="paginate_button ">
                        <a href="#">5</a>
                    </li>
                    <li class="paginate_button ">
                        <a href="#">6</a>
                    </li>
                    <li class="paginate_button next" id="example1_next">
                        <a href="#">&gt;</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>