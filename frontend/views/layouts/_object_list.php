<div id="object-list" class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h2>Все новостройки от компании Стройинвестярпром</h2>
                    </div>
                    <div class="box-body">

                        <div class="row well" style="margin-left: 0px;margin-right: 0px;">
                            <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
                                <div class="box">
                                    <div class="box-header">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <picture>
                                                <source srcset="/image/o1.jpg" media="(min-width: 1000px)">
                                                <source srcset="/image/o1.jpg" media="(min-width: 800px)">
                                                <source srcset="/image/o1-750.jpg" media="(max-width: 768px)">
                                                <img class="img-responsive img-thumbnail" srcset="/image/o1.jpg" alt="…">
                                            </picture>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                            <p class="text-green text-bold text-big">от 2 041 200 руб - 54 000 руб.за м²</p>
                                            <p class="text-muted">ЖК : Уютный Дом</p>
                                            <p class="text-muted">Район : Заволжский, Ярославль, Ярославская область</p>
                                            <p class="text-muted">улица : мостецкая</p>
                                            <p class="text-muted">дом : 63</p>
                                            <p class="text-muted">Метро: ВДНХ</p>
                                            <p class="text-muted">Застройщик : Стройинвестярпром</p>
                                            <p class="text-muted text-bold text-big">
                                                Срок сдачи: 4 кв. 2016
                                                <span class="fa fa-star-o pull-right color-yellow" style="margin-right:20px;"></span>
                                                <span class="fa fa-eye pull-right color-yellow"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>

                            <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
                                <div class="box">
                                    <div class="box-header">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <picture>
                                                <source srcset="/image/o2.jpg" media="(min-width: 1000px)">
                                                <source srcset="/image/o2.jpg" media="(min-width: 800px)">
                                                <source srcset="/image/o2-750.jpg" media="(max-width: 768px)">
                                                <img class="img-responsive img-thumbnail" srcset="/image/o2.jpg" alt="…">
                                            </picture>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                            <p class="text-green text-bold text-big">от 2 041 200 руб - 54 000 руб.за м²</p>
                                            <p class="text-muted">ЖК : Уютный Дом</p>
                                            <p class="text-muted">Район : Заволжский, Ярославль, Ярославская область</p>
                                            <p class="text-muted">улица : мостецкая</p>
                                            <p class="text-muted">дом : 63</p>
                                            <p class="text-muted">Метро: ВДНХ</p>
                                            <p class="text-muted">Застройщик : Стройинвестярпром</p>
                                            <p class="text-muted text-bold text-big">
                                                Срок сдачи: 4 кв. 2016
                                                <span class="fa fa-star-o pull-right color-yellow" style="margin-right:20px;"></span>
                                                <span class="fa fa-eye pull-right color-yellow"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>

                            <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
                                <div class="box">
                                    <div class="box-header">
                                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                            <picture>
                                                <source srcset="/image/o3.jpg" media="(min-width: 1000px)">
                                                <source srcset="/image/o3.jpg" media="(min-width: 800px)">
                                                <source srcset="/image/o3-750.jpg" media="(max-width: 768px)">
                                                <img class="img-responsive img-thumbnail" srcset="/image/o3.jpg" alt="…">
                                            </picture>
                                        </div>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                            <p class="text-green text-bold text-big">от 2 041 200 руб - 54 000 руб.за м²</p>
                                            <p class="text-muted">ЖК : Уютный Дом</p>
                                            <p class="text-muted">Район : Заволжский, Ярославль, Ярославская область</p>
                                            <p class="text-muted">улица : мостецкая</p>
                                            <p class="text-muted">дом : 63</p>
                                            <p class="text-muted">Метро: ВДНХ</p>
                                            <p class="text-muted">Застройщик : Стройинвестярпром</p>
                                            <p class="text-muted text-bold text-big">
                                                Срок сдачи: 4 кв. 2016
                                                <span class="fa fa-star-o pull-right color-yellow" style="margin-right:20px;"></span>
                                                <span class="fa fa-eye pull-right color-yellow"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>



                    </div>
                </div>
            </div>
        </div>
</div>
