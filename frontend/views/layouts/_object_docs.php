<style>
    @media only screen and (max-width : 768px) {
        #object-docs{padding-right:0px !important;}
        #object-docs .col-xs-12{padding-right:0px !important;}
        #object-docs .box-header{padding-left:35px !important;}
        #object-docs .box-body{padding-left:25px !important;}
    }
</style>

<?php if(count($realty->docs)): ?>
    <div id="object-docs" class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h2>Документы</h2>
                    </div>
                    <div class="box-body">
                        <div class="row">

                            <?php for($i=0; $i<2; $i++): ?>
                                <?php if(isset($realty->docs[$i])): ?>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="info-item text-muted">
                                            <span class="text-muted"><?= $realty->docs[$i]->name ?></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="info-item text-muted">
                                            <span>
                                                <?php $am = Yii::$app->assetManager->publish('@common/upload/realty/doc/'.$realty->docs[$i]->realty_id)[1] ?>
                                                <?= \yii\helpers\Html::a('Скачать', $am.'/'.$realty->docs[$i]->file, ['download'=>true]) ?>
                                            </span>
                                        </div>
                                    </div>
                                <?php endif ?>

                            <?php endfor; ?>

                            <?php if(count($realty->docs)>2): ?>
                                <div id="other-docs" class="panel-collapse collapse">
                                    <?php for($i=2; $i<count($realty->docs); $i++): ?>
                                        <?php if(isset($realty->docs[$i])): ?>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="info-item text-muted">
                                                    <span class="text-muted"><?= $realty->docs[$i]->name ?></span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="info-item text-muted">
                                                    <span>
                                                        <?= \yii\helpers\Html::a('Скачать', '/upload/realty/doc/'.$realty->docs[$i]->realty_id.'/'.$realty->docs[$i]->file, ['download'=>true]) ?>
                                                    </span>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    <?php endfor; ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="info-item text-muted">
                                        <span><a data-toggle="collapse" href="#other-docs">Показать все документы</a></span>
                                    </div>
                                </div>
                            <?php endif ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                <div class="alert alert-warning alert-dismissible" style="margin-top:20px">
                    <h4><i class="icon fa fa-google"></i> Код гугла</h4>
                    <p>Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код </p>
                    <p>Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код Код </p>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
