<style>
    #breadcrumb-in-object .row{margin-left:-23px;}
    #breadcrumb-in-object .row > div{padding-left:0px;padding-right:0px;}
    @media only screen and (max-width : 940px) {
        .breadcrumb{padding-left:22px;}
    }
    @media only screen and (max-width : 768px) {
        .breadcrumb{padding-left:56px;}
    }
	.breadcrumb>li:last-child>a>span{color:rgb(127,127,127)!important;}
	.breadcrumb>li:last-child>span{color:rgb(127,127,127)!important;}
</style>
<?php 
use yii\widgets\Breadcrumbs;
echo Breadcrumbs::widget([
    'itemTemplate' => "<li><i>{link}</i></li>\n", // template for all links
	'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
?> 
