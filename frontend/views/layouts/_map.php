<div id="map" class="container" >
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 visible-lg visible-md visible-sm">
            <a href="<?= \yii\helpers\Url::to(['site/search-objects-on-map']) ?>">
                <img src="/image/find-map.png" alt="Поиск на карте" width="100%">
            </a>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 visible-lg visible-md visible-sm find">
            <a href="<?= \yii\helpers\Url::to(['site/search-objects']) ?>">
                <picture>
                    <source srcset="/image/find735х100.png" media="(max-width: 991px)">
                    <img class="img-responsive" srcset="/image/find.png" alt="Поиск">
                </picture>
            </a>
        </div>
        <?php /*
        <div class="col-lg-4 col-md-4 col-sm-4 visible-lg visible-md visible-sm">
            <div id="map-search" class="input-group input-group-sm well-lg">
                <input type="text" class="form-control">
                <span class="input-group-btn">
                    <?= \yii\helpers\Html::a('Поиск', ['site/search-objects-on-map'], ['class'=>'btn btn-danger btn-flat']) ?>
                </span>
            </div>

        </div>
        */ ?>
    </div>
</div>