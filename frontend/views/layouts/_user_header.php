<style>
    .jumbotron{background: #e7e7e7 url("/site/logo-header?url=<?= Yii::$app->controller->route=='site/all-realty' ? $this->context->user->header(960, 370, false) : \Yii::$app->user->identity->header(960, 370, false) ?>") no-repeat scroll center center;
                top: 0px; padding-bottom: 320px;}
</style>
<?php if(Yii::$app->controller->route=='realty/post'): ?>
    <style>
        #cabinet-header{padding-right:0px;}
        #cabinet-header > div{margin-left:-20px !important;margin-right:28px !important;}
        .box-success{left:-18px;top:25px;width:99%;margin-bottom:40px;}
        .box-tools{margin-top:-5px;}
        .alert{margin-top:10px;}
        @media only screen and (max-width:768px) {
            .box-success{left:0px;top:0px;width:100%;margin-bottom:0px;}
        }
    </style>
<?php endif ?>
 
<div id="cabinet-header" class="container" style="<?= Yii::$app->controller->route=='realty/post' ? 'padding-right:0px;' : '' ?>">
    <div class="row" style="<?= Yii::$app->controller->route=='realty/post' ? 'margin-left:0px;' : '' ?>">
        <div class="col-lg-12 col-md-12 hidden-sm hidden-xs" style="<?= Yii::$app->controller->route=='realty/post' ? 'padding-right:0px;padding-left:0px;' : 'padding-left:6px;padding-right:7px;' ?>">
            <div class="jumbotron">
                <?php /*
        <h1>Личный кабинет</h1>
        <p>Описание Описание Описание Описание</p>
        */ ?>
            </div>
        </div>
    </div>
</div>
