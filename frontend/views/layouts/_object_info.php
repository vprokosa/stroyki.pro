<?php
    use common\components\ThumbHelper;
    use common\enum\District;
    use	yii\helpers\Html;

    $n = (float) $realty->attr->area_all;
    try {
        $priceM = round($realty->price / $n);
    }catch (yii\base\ErrorException $e) {
        $priceM = 0;
    }

?>

<div class="container">
    <div class="art-title">
        <div class="row">
            <div class="col-md-12">
                <div class="price"><?= price_format($realty->price) ?> <span class="rub">руб</span></div>
                <? if($priceM > 0): ?>
                <div class="price2"><?= price_format($priceM) ?> <span class="rubM">руб/м2</span></div>
                <? endif; ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="slider">
                <ul id="imageGallery">
                    <?php if(count($realty->images)): ?>
                        <?php foreach($realty->images as $image): ?>
                            <li data-thumb="<?= ThumbHelper::thumbnailImg('@common/upload/realty/picture/'.$image->realty_id.'/'.$image->image, 1920, 1080, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>">
                                <?= Html::img(ThumbHelper::thumbnailImg('@common/upload/realty/picture/'.$image->realty_id.'/'.$image->image, 1920, 1080, ThumbHelper::THUMBNAIL_OUTBOUND, [], false), ['alt' => $realty->attr->housing_estate]) ?>
                            </li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="art-description">
        <div class="desc-row">
            <div class="row">
                <?php if ($realty->attr->housing_estate) : ?>
                <div class="col-md-3 col-sm-6">
                    <div class="desc-name"><?= Html::encode($realty->attr->housing_estate) ?></div>
                    <div class="desc-type">жилой комплекс</div>
                </div>
                <?php endif; ?>

                <?php if ($realty->attr->rooms) : ?>
                <div class="col-md-3 col-sm-6">
                    <div class="desc-name"><?= Html::encode($realty->attr->rooms)?></div>
                    <div class="desc-type">комнат</div>
                </div>
                <?php endif; ?>

                <?php if ($realty->attr->floor && $realty->attr->floors) : ?>
                <div class="col-md-3 col-sm-6">
                    <div class="desc-name"><?= Html::encode($realty->attr->floor) ?>/<?= Html::encode($realty->attr->floors) ?></div>
                    <div class="desc-type">этаж</div>
                </div>
                <?php endif; ?>

                <?php if ($realty->attr->area_living && $realty->attr->area_kitchen && $realty->attr->area_all) : ?>
                <div class="col-md-3 col-sm-6">
                    <div class="desc-name"><?= floor($realty->attr->area_all) ?>/<?= floor($realty->attr->area_living) ?>/<?= floor($realty->attr->area_kitchen) ?></div>
                    <div class="desc-type">объщая / жилая / кухня</div>
                </div>
                <?php endif; ?>
                <?php if ($realty->attr->house) : ?>
                    <div class="col-md-3 col-sm-6">
                        <div class="desc-name"><?= $realty->attr->house ?></div>
                        <div class="desc-type">дом</div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="desc-row">
            <div class="row">
                <?php if (District::Lists($realty->attr->district_id)) : ?>
                <div class="col-md-4 col-sm-6 ">
                    <div class="desc-name"><?= Html::encode(District::Lists($realty->attr->district_id)) ?></div>
                    <div class="desc-type">район</div>
                </div>
                <?php endif; ?>

                <?php if ($realty->attr->street) : ?>
                <div class="col-md-4 col-sm-6">
                    <div class="desc-name"><?= Html::encode($realty->attr->street) ?></div>
                    <div class="desc-type">улица</div>
                </div>
                <?php endif; ?>
                <?php if ($realty->attr->house && $realty->attr->building) : ?>
                <div class="col-md-4 col-sm-6 ">
                    <div class="desc-name"><?= Html::encode($realty->attr->house) ?>/<?= Html::encode($realty->attr->building)?></div>
                    <div class="desc-type">дом / строение</div>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="stats">
            <div class="row">
                <div class="col-sm-4">
                    <div class="stats-text"><i class="fa fa-clock-o"></i> Размещено: <?= \common\components\helper\DateTimeHelper::diffFullPeriod($realty->created_at, time()) ?></div>
                </div>
                <div class="col-sm-4 col-xs-7">
                    <div class="stats-text">Просмотров: всего <?= Yii::$app->stat->showRealty($realty->id) ?>, сегодня <?= Yii::$app->stat->showRealty($realty->id, true) ?></div>
                </div>
                <div class="col-sm-4 col-xs-5">
                    <div class="stats-icons pull-right">
                        <?= $this->render('/layouts/_btn_favorite', ['model'=>$realty]) ?>
                        <?= $this->render('/layouts/_btn_spy_price', ['model'=>$realty]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>