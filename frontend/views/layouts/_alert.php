<style>
    .container .alert{margin-bottom:0px;}
</style>
<div class="container"style="display: table;">
    <?php if(Yii::$app->session->hasFlash('info')): ?>
        <?= \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-info',
            ],
            'body' => Yii::$app->session->getFlash('info')
        ]) ?>
    <?php endif ?>

    <?php if(Yii::$app->session->hasFlash('success')): ?>
        <?= \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-success',
            ],
            'body' => Yii::$app->session->getFlash('success')
        ]) ?>
    <?php endif ?>

    <?php if(Yii::$app->session->hasFlash('warning')): ?>
        <?= \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-warning',
            ],
            'body' => Yii::$app->session->getFlash('warning')
        ]) ?>
    <?php endif ?>

    <?php if(Yii::$app->session->hasFlash('error')): ?>
        <?= \yii\bootstrap\Alert::widget([
            'options' => [
                'class' => 'alert-danger',
            ],
            'body' => Yii::$app->session->getFlash('error')
        ]) ?>
    <?php endif ?>
</div>
