<a class="btn btn-dark btn-lg toggle" href="#" id="menu-toggle">
    <i class="fa fa-bars"></i>
</a>

<div class="pull-right">
    <nav id="sidebar-wrapper">
        <div class="sidebar-nav sidebar-nav-close">
            <a class="btn btn-light pull-right toggle" href="#" id="menu-close">
                <i class="fa fa-times"></i>
            </a>
            <br>
            <br>
            <div class="container-fluid" style="padding-left:20px;padding-right:20px;">
                <div class="row">
                    <div class="col-md-7">
                        <div class="sidebar-brand">
                            <a href="/" style="color:white;text-decoration:none">
                                <h2>ZOLOZ</h2>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-5 hidden-sm hidden-xs">
                        <?php if(!Yii::$app->user->isGuest): ?>
                            <table class="pull-right" style="margin-top: 6px;">
                                <tr>
                                    <td>
                                        <span><?= Yii::$app->user->identity->full_name ?>&nbsp;</span>
                                    </td>
                                    <td>
                                        <?= \Yii::$app->user->identity->avatar(50, 50) ?>
                                    </td>
                                </tr>
                            </table>
                        <?php endif ?>
                    </div>
                </div>
                <div class="row">

                    <?php if(!Yii::$app->user->isGuest): ?>
                        <div class="col-md-2 hidden-sm hidden-xs">
                            <a href="/cabinet" style="color:white;text-decoration:none">
                                <h3>Кабинет</h3>
                            </a>
                        </div>
                    <?php endif ?>

                    <div class="col-md-4">
                        <ul class="sidebar-nav">
<!--                            <li>-->
<!--                                --><?//= \yii\helpers\Html::a('Поиск', ['site/search-objects']) ?>
<!--                            </li>-->
                            <li>
                                <?= \yii\helpers\Html::a('Контакты', \common\models\Pages::Alias(\common\enum\Pages::TYPE_CONTACT)) ?>
                            </li>
                            <?php if(Yii::$app->user->isGuest): ?>
                                <li>
                                    <?= \yii\helpers\Html::a('Вход', ['user/login']) ?>
                                </li>
                                <li>
                                    <?= \yii\helpers\Html::a('Регистрация', ['user/signup']) ?>
                                </li>
                            <?php else: ?>
                                <li>
                                    <?= \yii\helpers\Html::a('Мои объявления', ['realty/index']) ?>
                                </li> 
                                <li>
                                    <?= \yii\helpers\Html::a('Подать объявления', ['realty/post']) ?>
                                </li>
                                <li>
                                    <?= \yii\helpers\Html::a('Избраное', ['service/favorite']) ?>
                                </li>
                                <li>
                                    <?= \yii\helpers\Html::a('Профиль', ['profile/index']) ?>
                                </li>
                                <li>
                                    <?= \yii\helpers\Html::a('Выход', ['user/logout']) ?>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>

                    <?php if(!Yii::$app->user->isGuest): ?>
                        <div class="col-md-5 hidden-sm hidden-xs">
                            <img src="/image/o1.jpg" class="img-responsive" alt="">
                        </div>
                    <?php endif ?>

                </div>
            </div>
        </div>
    </nav>
</div>
