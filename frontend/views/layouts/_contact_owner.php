<?php
    use yii\helpers\Html;
?>
<div class="container">
    <section class="s s5">
        <div class="sTitle">
            <h2 class="h2">Документация</h2>
        </div>
        <div class="sContent">
            <div class="row">
                <div class="col-sm-5">
                    <span class="doc-hiding">Проектная декларация</span>
                </div>
                <div class="col-sm-7">
                    <a href="#" class="download">Скачать</a>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <span class="doc-hiding">Разрешение на строительство</span>
                </div>
                <div class="col-sm-7">
                    <a href="#" class="download">Скачать</a>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="docs"><a href="#">Показать все документы</a></div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="container">
    <div class="s s6">
        <div class="sTitle">
            <h2 class="h2">Контакты</h2>
        </div>
        <div class="sContent">
            <div class="contacts">
                <div class="row">
                    <div class="col-md-4">
                        <div class="contacts-height">
                            <?= Html::img($user->publicLogo(), ['alt' => $realty->user->buildcompany]) ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contacts-height">
                            <ul class="contacts-list">
                                <?php if ($realty->user->buildcompany) : ?>
                                <li>Застройщик: <?= Html::a($realty->user->buildcompany, ['site/all-realty', 'user_id'=>$user->id], ['target'=>'_blank']) ?></li>
                                <?php endif; ?>

                                <?php if ($realty->user->city) : ?>
                                <li>город: <?= $realty->user->city ?></li>
                                <?php endif; ?>

                                <?php if ($realty->user->street) : ?>
                                <li>ул: <?= $realty->user->street ?></li>
                                <?php endif; ?>

                                <?php if ($realty->user->house) : ?>
                                <li>дом: <?= $realty->user->house ?></li>
                                <?php endif; ?>
                                <?php if ($realty->user->office) : ?>
                                <li>офис: <?= $realty->user->office ?></li>
                                <?php endif; ?>
                                <?php if ($realty->user->address) : ?>
                                <li><?= $realty->user->address ?></li>
                                <?php endif; ?>
                                <li class="small">Все объявления: <?= Html::a(count($realty->user->realties), \yii\helpers\Url::toRoute(['site/all-realty', 'user_id' => $realty->user->id]), ['target' => '_blank'])?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="contacts-height">
                            <div class="avatar">
                                <?= $user->avatar(100, 100) ?>
                            </div>
                            <div class="name">
                                <div class="d-table">
                                    <div class="d-row">
                                        <div class="text"><?= $realty->user->full_name ? $realty->user->full_name : '' ?>  </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <button class="showPhone" data-phone="<?= $user->phone ?>">Показать телефон</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>