<div class="container">
    <div class="s s2">
        <div class="sTitle">
            <div class="row">
                <div class="sTitle-container">
                    <div class="col-sm-3 col-xs-4">
                        <h2 class="h2">На карте</h2>
                    </div>
                    <div class="col-sm-9 col-xs-8">
                        <div class="markers">
                            <span class="mark-blue">Это предложение</span>
                            <span class="mark-purple">Новостройки рядом</span>
                            <span class="cork"><a href="#">Пробки</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="sContent">
            <div id="map" data-map-latitude="<?= $realty->geo_lat ?>" data-map-longitude="<?= $realty->geo_lng ?>"></div>
        </div>
    </div>
</div>