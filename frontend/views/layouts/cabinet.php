<?php $this->beginContent('@frontend/views/layouts/base.php') ?>

    <div class="content">
        <?= $this->render('_header_cabinet') ?>

        <?//= $this->render('_user_header') ?>

        <div class="container" style="padding-right: 0px;">
            <?= $content ?>
        </div>

    </div>

<?php $this->endContent() ?>
