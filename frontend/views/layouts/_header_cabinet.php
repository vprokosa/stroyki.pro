<?
use yii\helpers\Url;
$this->params['ObjectSearch'] = isset($this->params['ObjectSearch']) ? $this->params['ObjectSearch'] : [];
$city = $_SERVER["REQUEST_URI"] != "/" && isset($_COOKIE['cityID']) && is_numeric($_COOKIE['cityID']) ? ' '.\common\enum\City::Lists($_COOKIE['cityID']) : '';
?>

<header>
    <div class="container">
        <div class="row top-link">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <a title="Сменить город" href="<?= \yii\helpers\Url::toRoute(['site/change-city', []]);?>">
                    <?php if(isset($_COOKIE['cityID']) && (!empty($_COOKIE['cityID']))) {
                        echo \common\enum\City::Lists($_COOKIE['cityID']);
                    } else {
                        echo 'Выбрать город';
                    }
                    ?>
                </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-5 text-center">
                <?= \yii\helpers\Html::a('Добавить объявление', ['realty/post']) ?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-3 text-right">
                <? if (Yii::$app->user->isGuest) { ?>
                    <?= \yii\helpers\Html::a('Вход', ['user/login']) ?>
                <? } else { ?>
                    <a style="position: relative;right: 10px;display:inline-block;" href="/logout" class="green_link">Выход</a>
                <? } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-10 col-xs-10">
                <a href="/" class="pull-left"><img src="/images/logo-ava.jpg" /></a>
                <div class="pull-left header-title">
                    <p><a href="/" class="site-name">STROYKI.PRO</a></p>
                    <h1<?= ($city)?' class="city-choosen"':''; ?>>НОВОСТРОЙКИ<?= $city; ?></h1>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-2 col-xs-2">
                <div class="pull-right trigger-left-menu">
                    <div class="ico-menu"></div>
                </div>
                <?php if(!Yii::$app->user->isGuest): ?>
                    <?= \frontend\components\widget\Favorite::widget() ?>
                    <?= \frontend\components\widget\SpyPrice::widget() ?>
                    <?= \frontend\components\widget\Rss::widget() ?>
                <?php else: ?>
                <div class="pull-right hidden-xs hidden-sm state-icons">
                    <div class="ico-autosearch"><span class="count">0</span></div>
                    <p>автопоиск</p>
                </div>
                <div class="pull-right hidden-xs hidden-sm state-icons">
                    <div class="ico-monitor"><span class="count">0</span></div>
                    <p>мониторинг</p>
                </div>
                <div class="pull-right hidden-xs hidden-sm state-icons">
                    <div class="ico-favorite"><span class="count">0</span></div>
                    <p>избранное</p>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="blue">
        <div class="container">
            <div class="pull-right trigger-search">
                <div class="ico-search"><span class="search-open">Поиск</span><span class="search-close">Закрыть</span></div>
                <?= $this->render('_form_search.php') ?>
            </div>
            <div class="pull-left home-link">
                <a href="/"></a>
            </div>
            <div class="sm-menu text-center"><span>Меню</span></div>
            <nav class="main_menu">
                <ul>
                    <li><a href="<?
                        unset($this->params['ObjectSearch']['option']);
                        unset($this->params['ObjectSearch']['housing_estate']);

                        echo \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch'=> array_merge($this->params['ObjectSearch'], ['novostroyki'=>1])]);?>">Новостройки</a></li>
                    <li><a href="<?
                        unset($this->params['ObjectSearch']['novostroyki']);
                        unset($this->params['ObjectSearch']['housing_estate']);
                        echo \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => array_merge($this->params['ObjectSearch'], ['option'=>\common\enum\User::TYPE_CREATOR])]);?>">Застройщики</a></li>
                    <li><a href="<?
                        unset($this->params['ObjectSearch']['novostroyki']);
                        unset($this->params['ObjectSearch']['option']);
                        echo \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => array_merge($this->params['ObjectSearch'], ['housing_estate'=>1])]);?>">Жилые комплексы</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
