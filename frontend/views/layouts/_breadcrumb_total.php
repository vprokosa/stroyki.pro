<?
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
?>
<?php 
$title = [];
if(!empty($this->params['breadcrumbs'])){
	foreach($this->params['breadcrumbs'] as $b){
		if(!empty($b['label'])){
			$title[] = $b['label'];
		}
	}
}
if(count($title) > 2){
	$type=$title[1];
	$city=$title[0];
	$title[0] = $type;
	$title[1] = $city;
	
}
$this->title = empty($this->title) ? (!empty(implode(' ', $title)) ? implode(' ', $title) : '«Все Новостройки» - База новостроек Москвы, Подмосковья и Новой Москвы от застройщиков') : $this->title;

echo Breadcrumbs::widget([
    'itemTemplate' => "<li>{link}</li>\n",
	'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]);
?> 
