<?php if(strlen($this->context->user->company)): ?>
    <div class="row" id="company">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="text-muted">О компании</h2>
                </div>
                <div class="box-body">
                    <p class="text-muted">
                        <?= $this->context->user->company ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>