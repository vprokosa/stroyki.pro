<?php
use common\components\Social;
?>
<?php if(!$this->context->disableFooter): ?>
    <footer class="footer">
        <nav class="footer-menu">
            <ul>
                <li><a href="/page/support">Техподдержка</a></li>
                <li><a href="/article/8">Стать партнёром</a></li>
                <li><a href="/article/9">Реклама</a></li>
            </ul>
        </nav>
		<div class="footer__center">
			<div class="liveinternet">
				<!--LiveInternet counter--><script type="text/javascript">
					document.write("<a href='//www.liveinternet.ru/click' "+
						"target=_blank><img src='//counter.yadro.ru/hit?t44.5;r"+
						escape(document.referrer)+((typeof(screen)=="undefined")?"":
							";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
								screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
						";"+Math.random()+
						"' alt='' title='LiveInternet' "+
						"border='0' width='31' height='31'><\/a>")
				</script><!--/LiveInternet-->
			</div>
			<div class="social">
				<ul>
					<li>
						<a target="_blank" href="<?= Social::get('vk'); ?>">
							<i class="socico socico__vk"></i>
						</a>
					</li>
					<li>
						<a target="_blank" href="<?= Social::get('ok'); ?>">
							<i class="socico socico__od"></i>
						</a>
					</li>
					<li>
						<a target="_blank" href="<?= Social::get('fb'); ?>">
							<i class="socico socico__fb"></i>
						</a>
					</li>
					<li>
						<a target="_blank" href="<?= Social::get('tw'); ?>">
							<i class="socico socico__tw"></i>
						</a>
					</li>
					<li>
						<a target="_blank" href="<?= Social::get('ig'); ?>">
							<i class="socico socico__ig"></i>
						</a>
					</li>					
				</ul>
			</div>
		</div>
    </footer>
<?php endif ?>

