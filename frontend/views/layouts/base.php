<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use frontend\assets\YMapAsset;

AppAsset::register($this);
YMapAsset::register($this);
//$this->registerCssFile('/css/css032017.css');

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <div class="container-fluid">
            <?php $this->beginBody() ?>

            <?= $content ?>

            <?php $this->endBody() ?>
            <script>
                function isLogin()
                {
                    if(<?= !Yii::$app->user->isGuest ? 1 : 0 ?>) {
                        return true;
                    }
                    $.cookie('referrer', '/', { expires: 1, path: '/' });
                    return false;
                }
            </script>

            <?= $this->render('_footer') ?>

        </div>
        <?php
        $t=time();
        $t1=time();
        ?>
        <div class="m" time="<?= $t1-$t ?>">
            <?= $this->render('_menu') ?>
        </div>
    </body>
    <?php flush(); ?>
</html>
<?php $this->endPage() ?>
    