<div id="search-result-objects" class="row well">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
            <div class="box">
                <div class="box-header">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 10px">
                        <picture>
                            <source srcset="/image/207x171.jpg" media="(min-width: 1000px)">
                            <source srcset="/image/207x171.jpg" media="(min-width: 800px)">
                            <source srcset="/image/950x400.jpg" media="(max-width: 768px)">
                            <img class="img-responsive img-thumbnail" srcset="/image/207x171.jpg" alt="…">
                        </picture>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                        <div class="text4 pull-right">
                            <span class="fa fa-star-o pull-right color-yellow"></span>
                            <span class="fa fa-eye pull-right color-yellow"></span>
                        </div>

                        <h3 class="text-success">2 545 000 руб</h3>
                        <p><small class="text-muted">54 265 - 75 424 РУБ.за m<sup>2</sup><s</small></p>
                        <p><strong class="text-middle-x">ЖК "Успенский", ООО "ЭкоСтрой-Инвест"</strong></p>
                        <p><strong class="text-aqua text-middle-x">участок № 47, 48 ЖК "Успенский", Горки-10, Московская область</strong></p>
                        <p class="text-middle-x">Жилой комплекс "Успенский" представляет собой два 17-этажных монолитных здания комфорт</p>
                        <p class="pull-right">
                            <strong class="text-muted text-middle-x">Срок сдачи: </strong>
                            <strong class="text-aqua text-middle-x">4 кв. 2016</strong>
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
            <div class="box">
                <div class="box-header">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 10px">
                        <picture>
                            <source srcset="/image/207x171.jpg" media="(min-width: 1000px)">
                            <source srcset="/image/207x171.jpg" media="(min-width: 800px)">
                            <source srcset="/image/950x400.jpg" media="(max-width: 768px)">
                            <img class="img-responsive img-thumbnail" srcset="/image/207x171.jpg" alt="…">
                        </picture>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                        <div class="text4 pull-right">
                            <span class="fa fa-star-o pull-right color-yellow"></span>
                            <span class="fa fa-eye pull-right color-yellow"></span>
                        </div>

                        <h3 class="text-success">2 545 000 руб</h3>
                        <p><small class="text-muted">54 265 - 75 424 РУБ.за m<sup>2</sup><s</small></p>
                        <p><strong class="text-middle-x">ЖК "Успенский", ООО "ЭкоСтрой-Инвест"</strong></p>
                        <p><strong class="text-aqua text-middle-x">участок № 47, 48 ЖК "Успенский", Горки-10, Московская область</strong></p>
                        <p class="text-middle-x">Жилой комплекс "Успенский" представляет собой два 17-этажных монолитных здания комфорт</p>
                        <p class="pull-right">
                            <strong class="text-muted text-middle-x">Срок сдачи: </strong>
                            <strong class="text-aqua text-middle-x">4 кв. 2016</strong>
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
            <div class="box">
                <div class="box-header">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 10px">
                        <picture>
                            <source srcset="/image/207x171.jpg" media="(min-width: 1000px)">
                            <source srcset="/image/207x171.jpg" media="(min-width: 800px)">
                            <source srcset="/image/950x400.jpg" media="(max-width: 768px)">
                            <img class="img-responsive img-thumbnail" srcset="/image/207x171.jpg" alt="…">
                        </picture>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                        <div class="text4 pull-right">
                            <span class="fa fa-star-o pull-right color-yellow"></span>
                            <span class="fa fa-eye pull-right color-yellow"></span>
                        </div>

                        <h3 class="text-success">2 545 000 руб</h3>
                        <p><small class="text-muted">54 265 - 75 424 РУБ.за m<sup>2</sup><s</small></p>
                        <p><strong class="text-middle-x">ЖК "Успенский", ООО "ЭкоСтрой-Инвест"</strong></p>
                        <p><strong class="text-aqua text-middle-x">участок № 47, 48 ЖК "Успенский", Горки-10, Московская область</strong></p>
                        <p class="text-middle-x">Жилой комплекс "Успенский" представляет собой два 17-этажных монолитных здания комфорт</p>
                        <p class="pull-right">
                            <strong class="text-muted text-middle-x">Срок сдачи: </strong>
                            <strong class="text-aqua text-middle-x">4 кв. 2016</strong>
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
            <div class="box">
                <div class="box-header">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 10px">
                        <picture>
                            <source srcset="/image/207x171.jpg" media="(min-width: 1000px)">
                            <source srcset="/image/207x171.jpg" media="(min-width: 800px)">
                            <source srcset="/image/950x400.jpg" media="(max-width: 768px)">
                            <img class="img-responsive img-thumbnail" srcset="/image/207x171.jpg" alt="…">
                        </picture>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                        <div class="text4 pull-right">
                            <span class="fa fa-star-o pull-right color-yellow"></span>
                            <span class="fa fa-eye pull-right color-yellow"></span>
                        </div>

                        <h3 class="text-success">2 545 000 руб</h3>
                        <p><small class="text-muted">54 265 - 75 424 РУБ.за m<sup>2</sup><s</small></p>
                        <p><strong class="text-middle-x">ЖК "Успенский", ООО "ЭкоСтрой-Инвест"</strong></p>
                        <p><strong class="text-aqua text-middle-x">участок № 47, 48 ЖК "Успенский", Горки-10, Московская область</strong></p>
                        <p class="text-middle-x">Жилой комплекс "Успенский" представляет собой два 17-этажных монолитных здания комфорт</p>
                        <p class="pull-right">
                            <strong class="text-muted text-middle-x">Срок сдачи: </strong>
                            <strong class="text-aqua text-middle-x">4 кв. 2016</strong>
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a href="<?= \yii\helpers\Url::to(['advertisement/show']) ?>">
            <div class="box">
                <div class="box-header">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12" style="margin-bottom: 10px">
                        <picture>
                            <source srcset="/image/207x171.jpg" media="(min-width: 1000px)">
                            <source srcset="/image/207x171.jpg" media="(min-width: 800px)">
                            <source srcset="/image/950x400.jpg" media="(max-width: 768px)">
                            <img class="img-responsive img-thumbnail" srcset="/image/207x171.jpg" alt="…">
                        </picture>
                    </div>
                </div>
                <div class="box-body">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">

                        <div class="text4 pull-right">
                            <span class="fa fa-star-o pull-right color-yellow"></span>
                            <span class="fa fa-eye pull-right color-yellow"></span>
                        </div>

                        <h3 class="text-success">2 545 000 руб</h3>
                        <p><small class="text-muted">54 265 - 75 424 РУБ.за m<sup>2</sup><s</small></p>
                        <p><strong class="text-middle-x">ЖК "Успенский", ООО "ЭкоСтрой-Инвест"</strong></p>
                        <p><strong class="text-aqua text-middle-x">участок № 47, 48 ЖК "Успенский", Горки-10, Московская область</strong></p>
                        <p class="text-middle-x">Жилой комплекс "Успенский" представляет собой два 17-этажных монолитных здания комфорт</p>
                        <p class="pull-right">
                            <strong class="text-muted text-middle-x">Срок сдачи: </strong>
                            <strong class="text-aqua text-middle-x">4 кв. 2016</strong>
                        </p>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>