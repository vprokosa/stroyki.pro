<style>
    @media only screen and (max-width : 768px) {
        #object-desc{padding-right:0px !important;}
        #object-desc .col-xs-12{padding-right:0px !important;}
        #object-desc .box-header{padding-left:35px !important;}
        #object-desc .box-body{padding-left:25px !important;}
    }
</style>
<?php if(strlen($realty->desc->details)): ?>
    <div class="container">
        <div class="s s1">
            <div class="sTitle">
                <h2 class="h2">Дополнильная информация</h2>
            </div>
            <div class="sContent">
                <p><?= \yii\helpers\Html::encode($realty->desc->details) ?></p>
            </div>
        </div>
    </div>
<?php endif ?>

