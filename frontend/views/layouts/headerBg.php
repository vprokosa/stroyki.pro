<?php

    use yii\helpers\Html;

?>

<div class="contact-slider" style="background-image: url(<?= $src ?>)">
    <div class="container slider-container">
        <div class="row">
            <div class="col-xs-12">
                <div class="slider-info">
                    <?= Html::img($user->publicLogo(), ['alt' => $user->buildcompany]) ?>
                    <ul>
                        <li><?= $user->full_name?></li>
                        <li>Застройщик <?= $user->buildcompany ?></li>
                        <li><?= $user->phone ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>