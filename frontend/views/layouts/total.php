<?php $this->beginContent('@frontend/views/layouts/base.php') ?>
    <?= $this->render('_header') ?>
    <!-- biggest banner -->
    <?php

        $actions = ['contact', 'calculator', 'all-realty'];

        if (array_search(Yii::$app->controller->action->id, $actions) !== false) {
            echo \frontend\components\widget\HeaderBackground::run();
        }else
            echo \frontend\components\widget\BannerBiggest::run(array('city_id' => ((isset($_COOKIE['cityID'])) && (!empty($_COOKIE['cityID']))) ? $_COOKIE['cityID'] : 0 ));?>
    <!-- biggest banner -->
    <?php if(isset($this->params['breadcrumbs'])): ?>
    <div class="breadcrumb-block">
        <div class="container">
            <ol class="breadcrumb">
                <?= $this->render('_breadcrumb_total') ?>
            </ol>
        </div>
    </div>
    <?php endif; ?>
    <?= $this->render('_alert') ?>

    <?= $content ?>

<?php $this->endContent() ?>
    