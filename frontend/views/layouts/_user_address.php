<?php
use frontend\assets\YMapAsset;

YMapAsset::register($this);
?>

<script>
    var coords_lat = '<?= 0.4546 ?>';
    var coords_lng = '<?= 0.454643 ?>';
    var map;

    function getByCoords()
    {
        var placemark = new ymaps.Placemark([coords_lat, coords_lng], {}, {
            preset: "twirl#yellowStretchyIcon",
//            draggable: true
        });
        map.setCenter([coords_lat, coords_lng], 12, {
            checkZoomRange: true, duration: 0
        });
        return placemark;
    }

    $(document).ready(function () {
        ymaps.ready(function(){
            if (coords_lat != "" && coords_lng != "") {
                map = new ymaps.Map("ya-map", {
                    center: [coords_lat, coords_lng],
                    zoom: 16,
                    controls: ["zoomControl"],
                    behaviors: []
                });
                map.behaviors.disable("scrollZoom");
                map.behaviors.enable('drag');
                var placemark = getByCoords();
                var fullscreenControl = new ymaps.control.FullscreenControl();

                map.controls.add(fullscreenControl);
                map.geoObjects.add(placemark);

//                placemark.events.add('drag', function (e) {
//                    var coords = this.geometry.getCoordinates();
//                    $('#realtyform-geo_lat').val(coords[0]);
//                    $('#realtyform-geo_lng').val(coords[1]);
//                }, placemark);

                map.setCenter([coords_lat, coords_lng], 16, {
                    checkZoomRange: true, duration: 0
                });

            } else {
                ymaps.geocode('Москва').then(
                    function (res) {
                        map = new ymaps.Map("ya-map", {
                            center: res.geoObjects.get(0).geometry.getCoordinates(),
                            zoom: 12,
                            controls: ["zoomControl"]
                        });
                    },
                    function (err) {
                        alert('Ошибка загрузки карты !');
                    }
                );
            }
        });
    });
</script>

<div id="user-address" class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="text-muted">Адрес компании</h2>
                </div>
                <div class="box-body">
                    <div id="ya-map" style="width:100%; max-width:100%; height:400px; position:relative; display:inline-block"></div>
                </div>
            </div>
        </div>
    </div>
</div>