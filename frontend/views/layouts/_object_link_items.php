<style>
    @media only screen and (max-width : 768px) {
        #object-link-items{padding-right:0px !important;}
        #object-link-items .col-xs-12{padding-right:0px !important;}
        #object-link-items .box-header{padding-left:35px !important;}
        #object-link-items .box-body{padding-left:25px !important;}
    }
</style>
<div class="container">
<?php if(count($realty->flats)): ?>
    <div class="s s3">
        <div class="sTitle">
            <h2 class="h2">Планировки</h2>
        </div>
        <div class="sContent">
            <ul>
             <?php foreach($realty->flats as $flat): ?>
                <li>
                    <a href="<?= \yii\helpers\Url::to(['site/advertisement', 'alias'=> $flat->alias]) ?>">
                        <div class="row">
                            <div class="col-sm-5 col-xs-6">
                                <div class="d-table">
                                    <div class="d-row">
                                        <div class="items">
                                            <div class="area"><?= $flat->attr->rooms ?> ком. <?= floor($flat->attr->area_all) ?> / <?= floor($flat->attr->area_living) ?> / <?= floor($flat->attr->area_kitchen) ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-4">
                                <div class="d-table">
                                    <div class="d-row">
                                        <div class="items">
                                            <div class="price"><?= price_format($flat->price) ?> руб</div>
                                            <div class="priceM"><?= price_format(floor($flat->price/$flat->attr->area_all)) ?> руб м2</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-xs-2">
                                <div class="icons">
                                    <div class="d-table">
                                        <div class="d-row">
                                            <div class="items">
                                                <div class="stats-icons">
                                                    <?= $this->render('/layouts/_btn_favorite', ['model'=>$realty]) ?>
                                                    <?= $this->render('/layouts/_btn_spy_price', ['model'=>$realty]) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a>
                </li>
            <?php endforeach; ?>

            </ul>
            <div class="row">
                <div class="col-xs-12">
                    <div class="more">
                        <a href="#">Показать все планировки</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>
</div>