<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="block_box">
				 <div class="row">
						 <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
							<div class="box">
									<div class="box-header">
											<h2>Документы</h2>
									</div>
									<div class="in_1">
											<div><p class="l_50">Проектная декларация</p><a href="javascript:void(0);" class="l_50">Скачать</a></div>
											<div><p class="l_50">Разрешение на строительство</p><a href="javascript:void(0);" class="l_50">Скачать</a></div>
											<div><a href="javascript:void(0);">Показать все документы</a></div>
											
									</div>
							</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
							<div class="box_strong">                
							</div>
					</div>
				 </div>
					
			</div>			
		</div>
	</div>
</div>

<div id="contact-owner" class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box sh_box">
                <div class="box-header">
                    <h2>Контакты <?= $user->type() ?> </h2>
                </div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="str_img"></div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<?= !empty($user->buildcompany) ? '<p>Застройщик: <span>'.$user->buildcompany.'</span></p>' : '' ?>
					<?= !empty($user->buildcompany) ? '<p>Город: <span>'.$user->city.'</span></p>' : '' ?>
					<?= !empty($user->address) ? '<p>Адрес: <span>'.$user->address.'</span></p>' : '' ?>
					<?= count($user->realties) > 0 ? '<p>Все объявления: <span><a target="_blank" href="'.\yii\helpers\Url::toRoute(['site/all-realty', 'user_id' => $user->id]).'">'.count($user->realties).'</a></span></p>' : '' ?>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<div class="rad_str_img" <?= !empty($user->avatar) ? 'style="background:url(/img/'.$user->avatar.')"' : '' ?>></div>
					<div class="l_cont">
						<h3>Контактное лицо</h3>
						<?= !empty($user->full_name) ? '<p><span>'.$user->full_name.'</span></p>' : '' ?>
						<p class="tel_sm">Показать телефон</p>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>