 <?php
$this->registerJs(
    ' $(document).ready(function () {
        $(".item-room").on("click", function () {
           var el = $(this);
           if(el.hasClass("active")) {
               el.removeClass("btn-danger").addClass("btn-default");
               el.find("input").val(0)
           } else {
               el.removeClass("btn-default").addClass("btn-danger");
               el.find("input").val(1)
           }
        });
        $(".item-area").on("click", function () {
           var el = $(this);
           if(el.hasClass("active")) {
               el.find("i").removeClass("fa-square").addClass("fa-square-o").css("color","black");
               el.find("input").val(0)
           } else {
               el.find("i").removeClass("fa-square-o").addClass("fa-square").css("color","red");
               el.find("input").val(1)
           }
        });
        if(Number($("#objectsearch-room4").val())) {
            $("#objectsearch-room4").parent("label").removeClass("btn-default").addClass("btn-danger").addClass("active");
        }
        if(Number($("#objectsearch-room3").val())) {
            $("#objectsearch-room3").parent("label").removeClass("btn-default").addClass("btn-danger").addClass("active");
        }
        if(Number($("#objectsearch-room2").val())) {
            $("#objectsearch-room2").parent("label").removeClass("btn-default").addClass("btn-danger").addClass("active");
        }
        if(Number($("#objectsearch-room1").val())) {
            $("#objectsearch-room1").parent("label").removeClass("btn-default").addClass("btn-danger").addClass("active");
        }
        $("[href=\'"+location.pathname+"\']").removeClass("btn-default").addClass("active").addClass("btn-info");
    });'
);

?>
<style>
    @media only screen and (max-width : 1200px) {
        .submits{padding-left: 0px !important;}
    }
    @media only screen and (max-width : 940px) {
        .col-price{padding-left: 8px !important;}
    }
    @media only screen and (max-width : 768px) {
        .col-type{padding-left: 18px !important;}
        .col-period{padding-left: 18px !important;}
        .col-room{padding-left: 18px !important;}
        .col-area{padding-left: 18px !important;}
        .col-price{padding-left: 18px !important;}
        .col-region{padding-left: 18px !important;}
        .submits{padding-left: 11px !important;}
        #search-form .container{padding-right: 0px!important;}
        #search-form #objectsearch-end_price {
            width: 78px !important;
        }
    }
</style>
<?php
 $form = \yii\widgets\ActiveForm::begin([
    'id'=>'form-search'
]);
$noMapObjectSearch = $ObjectSearch;
unset($noMapObjectSearch['maps']);
?>

    <?php if(\Yii::$app->controller->route == 'site/search-objects'): ?>
        <?= \frontend\components\widget\BannerSmall::widget(['dop_class'=>'bntype1']) ?>
    <?php endif ?>

 <div class="container">
     <div class="listAds">
         <div class="searchButtons">
             <div class="row">
                 <div class="col-md-4">
                     <?= $form->field($searchModel, 'sort')->dropDownList([
                         '0' => 'Цена по убыванию',
                         '1' => 'Цена по возрастанию',
                         '2' => 'Дата по убыванию',
                         '3' => 'Дата по возрастанию'
                     ], ['prompt' => 'Сортировка', 'class' => 'search-btn', 'onchange' => '$("#form-search").submit()',])->label(false) ?>
                 </div>
                 <div class="col-md-4">
                     <?php
                     $cityId = !empty($_COOKIE['cityID']) ? $_COOKIE['cityID'] : 1;
                     $cityName = \common\enum\City::Lists($cityId);
                     //var_dump($cityName); exit;
                     ?>

                     <a href="/<?= \common\components\TranslitHelper::transliteration($cityName) ?>/zastroyshchiki"
                        class="showOnMap search-btn">Посмотреть списком</a>
                 </div>
                 <div class="col-md-4">
                     <a href="#" class="offers search-btn">Спец предложения</a>
                 </div>
             </div>
         </div>
     </div>
 </div>
<!--<div class="container btn-search-mini">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="box_result_search row">
						 <div class="block_spisok">
								<a href="<?/*= \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $noMapObjectSearch]) */?>"><img src="/img/list_icon_red.png">список</a>
                         </div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="box_result_search row">
						 <div class="block_spisok">
								<a href="<?/*= \yii\helpers\Url::toRoute(['site/search-objects-on-map', 'ObjectSearch' => $ObjectSearch]) */?>"><img src="/img/map_point_icon_red.png"><span class="name_map1">просмотр на карте</span><span class="name_map2">карта</span></a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left:15px;">
					<div class="row">
						<?/*= $form->field($searchModel, 'sort',[
//                'template'=>'<strong >'.$searchModel->getAttributeLabel('sort').'</strong>{input}',
                'options'=>[
											'style' => '    margin: 0px auto 10px;width:100%;max-width:298px;'
//                    'tag'=>false
                ]
                ])->widget(\kartik\widgets\Select2::className(), [
                    'data' => [
                        'Цена по убыванию',
                        'Цена по возрастанию',
                        'Дата по убыванию',
                        'Дата по возрастанию',
                    ],
                    'options' => [
                        'placeholder' => 'Сортировка ...',
                        'onchange'=>'$("#form-search").submit()',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumResultsForSearch'=>'Infinity'
                    ],
                ])->label(false) */?>
					</div>
				</div>
			</div>

        <!--  #Сортировка и переключатель -->


<!--</div>

-->

<?php \yii\widgets\ActiveForm::end(); ?>