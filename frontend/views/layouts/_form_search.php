<div id="form-search-block">
    <?php $form = \yii\widgets\ActiveForm::begin([
        'id'=>'form-search',
        'action' => \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => $this->params['ObjectSearch']])
    ]); ?>

        <div class="control-item">
            <?$searchModel = $this->params['searchModelg'];?>
            <?= $form->field($searchModel, 'city_id')->widget(\kartik\widgets\Select2::className(), [
                'data' => \common\enum\City::Lists(),
                'options' => [
                    'placeholder' => "Выберите город...",
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                    //'minimumResultsForSearch'=>'Infinity'
                ],
            ])->label(false) ?>
        </div>
        <div class="control-item">
            <?= $form->field($searchModel, 'district_id')->widget(\kartik\widgets\Select2::className(), [
                'data' => \common\enum\District::Lists(),
                'options' => [
                    'placeholder' => 'Выберите район ...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label(false) ?>
        </div>
        <div class="control-item">
            <span class="label">Комнат</span>
            <?
            $checkbox = [];
            if(isset($searchModel->room4)){$checkbox = ['value'=>$searchModel->room4,'uncheck'=>null]; }
            ?>
            <?= $form->field($searchModel, 'room4', [
                'template'=>'<div class="input checkbox">{input}{label}</div>',
                'options'=>[
                    'tag'=>false
                ]
            ])->checkbox([
                $checkbox
            ], false) ?>
            <?
            $checkbox = [];
            if(isset($searchModel->room3)){$checkbox = ['value'=>$searchModel->room3,'uncheck'=>null]; }?>
            <?= $form->field($searchModel, 'room3', [
                'template'=>'<div class="input checkbox">{input}{label}</div>',
                'options'=>[
                    'tag'=>false
                ]
            ])->checkbox([
                $checkbox
            ], false) ?>
            <?
            $checkbox = [];
            if(isset($searchModel->room2)){$checkbox = ['value'=>$searchModel->room2,'uncheck'=>null]; }?>
            <?= $form->field($searchModel, 'room2', [
                'template'=>'<div class="input checkbox">{input}{label}</div>',
                'options'=>[
                    'tag'=>false
                ]
            ])->checkbox([
                $checkbox
            ], false) ?>

            <?
            $checkbox = [];
            if(isset($searchModel->room1)){$checkbox = ['value'=>$searchModel->room1,'uncheck'=>null]; }?>
            <?= $form->field($searchModel, 'room1', [
                'template'=>'<div class="input checkbox">{input}{label}</div>',
                'options'=>[
                    'tag'=>false
                ]
            ])->checkbox([
                $checkbox
            ], false) ?>
        </div>
        <div class="control-item">
            <span class="label">Срок сдачи</span>
            <?
            $checkbox = [];
            if(isset($searchModel->deadline1)){$checkbox = ['value'=>$searchModel->deadline1,'uncheck'=>null]; }
            ?>
            <?= $form->field($searchModel, 'deadline1', [
                'template'=>'<div class="input checkbox">{input}{label}</div>',
                'options'=>[
                    'tag'=>false
                ]
            ])->checkbox([
                $checkbox
            ], false) ?>
            <?
            $checkbox = [];
            if(isset($searchModel->deadline2)){$checkbox = ['value'=>$searchModel->deadline2,'uncheck'=>null]; }
            ?>
            <?= $form->field($searchModel, 'deadline2', [
                'template'=>'<div class="input checkbox">{input}{label}</div>',
                'options'=>[
                    'tag'=>false
                ]
            ])->checkbox([
                $checkbox
            ], false) ?>
            <?
            $checkbox = [];
            if(isset($searchModel->deadline3)){$checkbox = ['value'=>$searchModel->deadline3,'uncheck'=>null]; }
            ?>
            <?= $form->field($searchModel, 'deadline3', [
                'template'=>'<div class="input checkbox">{input}{label}</div>',
                'options'=>[
                    'tag'=>false
                ]
            ])->checkbox([
                $checkbox
            ], false) ?>
            <?
            $checkbox = [];
            if(isset($searchModel->deadline4)){$checkbox = ['value'=>$searchModel->deadline4,'uncheck'=>null]; }
            ?>
            <?= $form->field($searchModel, 'deadline4', [
                'template'=>'<div class="input checkbox">{input}{label}</div>',
                'options'=>[
                    'tag'=>false
                ]
            ])->checkbox([
                $checkbox
            ], false) ?>
        </div>
    <div class="control-item">
        <span class="label">Площадь</span>
        <div class="ipgroup">
            <?= $form->field($searchModel, 'start_area',[
                'template'=>'{input}',
                'options'=>[
                    'tag'=>false
                ]
            ])->textInput([
                'placeholder'=>$searchModel->getAttributeLabel('start_area'),
                'class'=>'form-control',
                'style'=>'margin-right:5px;'
            ]) ?>
            <?= $form->field($searchModel, 'end_area',[
                'template'=>'{input}',
                'options'=>[
                    'tag'=>false
                ]
            ])->textInput([
                'placeholder'=>$searchModel->getAttributeLabel('end_area'),
                'class'=>'form-control',
                'style'=>''
            ]) ?>
        </div>
    </div>
    <div class="control-item">
        <span class="label">Цена</span>
        <div class="ipgroup">
            <?= $form->field($searchModel, 'start_price',[
                'template'=>'{input}',
                'options'=>[
                    'tag'=>false
                ]
            ])->textInput([
                'placeholder'=>$searchModel->getAttributeLabel('start_price'),
                'class'=>'form-control',
            ]) ?>

            <?= $form->field($searchModel, 'end_price',[
                'template'=>'{input}',
                'options'=>[
                    'tag'=>false
                ]
            ])->textInput([
                'placeholder'=>$searchModel->getAttributeLabel('end_price'),
                'class'=>'form-control',
            ]) ?>
        </div>
    </div>
    <?= $form->field($searchModel, 'sort')->hiddenInput(['class' => 'onsort']) ?>
    <div class="control-item">
        <button type="submit" class="action-search" title=" найти">
            <!-- <img class="" alt=" найти" width="133" height="22" src="/image/new_header/u60387-5.png">>-->
        </button>

        <a href="<?= \yii\helpers\Url::toRoute(['site/search-objects-on-map', 'ObjectSearch' => $this->params['ObjectSearch']])?>" class="action-map">
            <!--<img  alt=" на карте" src="image/new_header/u60393.png">-->
        </a>
    </div>
    <div class="control-item">
        <button onclick="RegSending()" class="action-subsribe">
            <!--<img  alt=" подписаться на поиск" src="/image/new_header/u60404.png">-->
        </button>
    </div>
<?php \yii\widgets\ActiveForm::end(); ?>
</div>