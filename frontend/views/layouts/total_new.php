<?php $this->beginContent('@frontend/views/layouts/base_new.php') ?>

<div class="content" >
    <?= $this->render('_header') ?>
    
    <div class="container-fluid">
        <div class="row">
            <?= $this->render('_alert') ?>

            <?= $content ?>
        </div>
    </div>

</div>

<?php $this->endContent() ?>
    