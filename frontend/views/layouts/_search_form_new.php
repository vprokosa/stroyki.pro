 <?php
$this->registerJs(
    '$(document).ready(function () {
        $(".item-room").on("click", function () {
           var el = $(this);
           if(el.hasClass("active")) {
               el.removeClass("btn-danger").addClass("btn-default");
               el.find("input").val(0)
           } else {
               el.removeClass("btn-default").addClass("btn-danger");
               el.find("input").val(1)
           }
        });
        $(".item-area").on("click", function () {
           var el = $(this);
           if(el.hasClass("active")) {
               el.find("i").removeClass("fa-square").addClass("fa-square-o").css("color","black");
               el.find("input").val(0)
           } else {
               el.find("i").removeClass("fa-square-o").addClass("fa-square").css("color","red");
               el.find("input").val(1)
           }
        });
        if(Number($("#objectsearch-room4").val())) {
            $("#objectsearch-room4").parent("label").removeClass("btn-default").addClass("btn-danger").addClass("active");
        }
        if(Number($("#objectsearch-room3").val())) {
            $("#objectsearch-room3").parent("label").removeClass("btn-default").addClass("btn-danger").addClass("active");
        }
        if(Number($("#objectsearch-room2").val())) {
            $("#objectsearch-room2").parent("label").removeClass("btn-default").addClass("btn-danger").addClass("active");
        }
        if(Number($("#objectsearch-room1").val())) {
            $("#objectsearch-room1").parent("label").removeClass("btn-default").addClass("btn-danger").addClass("active");
        }
        $("[href=\'"+location.pathname+"\']").removeClass("btn-default").addClass("active").addClass("btn-info");
    });'
);

?>
<style>
    @media only screen and (max-width : 1200px) {
        .submits{padding-left: 0px !important;}
    }
    @media only screen and (max-width : 940px) {
        .col-price{padding-left: 8px !important;}
    }
    @media only screen and (max-width : 768px) {
        .col-type{padding-left: 18px !important;}
        .col-period{padding-left: 18px !important;}
        .col-room{padding-left: 18px !important;}
        .col-area{padding-left: 18px !important;}
        .col-price{padding-left: 18px !important;}
        .col-region{padding-left: 18px !important;}
        .submits{padding-left: 11px !important;}
        #search-form .container{padding-right: 0px!important;}
        #search-form #objectsearch-end_price {
            width: 78px !important;
        }
    }
</style>
<div class="container">
    

    <div id="search-panel" class="row" style="margin-top:10px;">
        <!--  Результат  -->
        <!--<div class="col-lg-4 col-md-4 col-sm-4 ">
            <div class="col1">
                <span id="result" class="color-grey2"></span>
            </div>
        </div>-->
        <!--  #Результат  -->

        <!--  #Сортировка и переключатель  -->
		
        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12" >

            <div class="mybl">
			<?php $form = \yii\widgets\ActiveForm::begin([
				'id'=>'form-search'
				]); ?>
                <?= $form->field($searchModel, 'sort',[
//                'template'=>'<strong >'.$searchModel->getAttributeLabel('sort').'</strong>{input}',
//                'options'=>[
//                    'tag'=>false
//                ]
                ])->widget(\kartik\widgets\Select2::className(), [
                    'data' => [
                        'Цена по убыванию',
                        'Цена по возрастанию',
                        'Дата по убыванию',
                        'Дата по возрастанию',
                    ],
                    'options' => [
                        'placeholder' => 'Сортировка ...',
                        'onchange'=>'$("#form-search").submit()',
			
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumResultsForSearch'=>'Infinity'
                    ],
                ])->label(false) ?>

               
           
		<?php \yii\widgets\ActiveForm::end(); ?>
        </div>
		</div>
		<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <a href="/search-objects-on-map"><img src="/img/map_point_icon_red.png">проcмотр на карте</a>
                </div>
        <!--  #Сортировка и переключатель -->
    
</div>



