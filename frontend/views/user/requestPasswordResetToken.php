<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\form\ResetPasswordForm */

$this->title = 'Поменять пароль';
?>
<style>
    .login-box{margin-bottom: 20px;}
    @media only screen and (max-width : 1200px) {
        .login-box{padding-left: 20px !important;}
    }
    @media only screen and (max-width : 768px) {
        .login-box h3, .login-box p, .login-box form{padding-left: 55px !important;}
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 login-box">
            <h3><?= $this->title ?></h3>
            <p class="m_t_20">
                Пожалуйста, введите адрес электронной почты, указанный в параметрах вашей учётной записи. На него будет
                отправлен специальный проверочный код.
                После его получения вы сможете ввести новый пароль для вашей учетной записи.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 login-box">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'email', [
                    'options' => [
                        'class' => 'form-group has-feedback',
                    ],
                    'template' => '{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>{error}'
                ])->textInput([
                    'class' => 'form-control',
                    'placeholder' => $model->getAttributeLabel('email'),
                ]) ?>

                <div class="row">
                    <div class="col-xs-6">
                        <?= Html::submitButton('Восстановить', [
                            'class' => 'btn btn-primary btn-block',
                            'name' => 'request-password-reset-token-button'
                        ]) ?>
                    </div>
                    <div class="col-xs-6">
                        <?= Html::a('Отмена', ['user/login'], [
                            'class' => 'btn btn-primary btn-block btn-flat',
                        ]) ?>
                    </div>
                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

