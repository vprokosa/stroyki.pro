<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\form\ResetPasswordForm */

$this->title = 'Сброс пароля';
?>

<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="reset-password-box">

                <h2><?= $this->title ?></h2>

                <div class="reset-password-box-body">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                    <p>Пожалуйста, выберите ваш новый пароль:</p>
                    <?= $form->field($model, 'password', [
                        'options' => [
                            'class' => 'form-group has-feedback',
                        ],
                        'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
                    ])->passwordInput([
                        'class' => 'form-control',
                        'placeholder' => $model->getAttributeLabel('password'),
                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

    </div>
</div>