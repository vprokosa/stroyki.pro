<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\form\SignupForm */

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .signup-box{margin-bottom: 20px;}
    @media only screen and (max-width : 1200px) {
        .signup-box{padding-left: 20px !important;}
    }
    @media only screen and (max-width : 768px) {
        .signup-box h3, .signup-box form{padding-left: 55px !important;}
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 signup-box">
            <h3><?= $this->title ?> / <a href="<?= \yii\helpers\Url::to(['user/login']) ?>">Вход</a></h3>

            <div class="signup-box-body">
                <?php $form = ActiveForm::begin() ?>

                <?= $form->field($model, 'full_name', [
                    'options' => [
                        'class' => 'form-group has-feedback',
                    ],
                    'template' => '{input}<span class="glyphicon glyphicon-sunglasses form-control-feedback"></span>{error}'
                ])->textInput([
                    'class' => 'form-control',
                    'placeholder' => $model->getAttributeLabel('full_name'),
                ]) ?>

                <?= $form->field($model, 'username', [
                    'options' => [
                        'class' => 'form-group has-feedback',
                    ],
                    'template' => '{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}'
                ])->textInput([
                    'class' => 'form-control',
                    'placeholder' => $model->getAttributeLabel('username'),
                ]) ?>

                <?= $form->field($model, 'email', [
                    'options' => [
                        'class' => 'form-group has-feedback',
                    ],
                    'template' => '{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span>{error}'
                ])->textInput([
                    'class' => 'form-control',
                    'placeholder' => $model->getAttributeLabel('email'),
                ]) ?>

                <?= $form->field($model, 'password', [
                    'options' => [
                        'class' => 'form-group has-feedback',
                    ],
                    'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
                ])->passwordInput([
                    'class' => 'form-control',
                    'placeholder' => $model->getAttributeLabel('password'),
                ]) ?>

                <div>
                    <?= $form->field($model, 'captcha')->widget(\yii\captcha\Captcha::className(), [
                        'template' => '<div class="d_inline_block">{image}</div><div class="d_inline_block w90 v_middle">{input}</div>',
                        'options'=>[
                            'class'=>'form-control',
                            'placeholder' => $model->getAttributeLabel('captcha'),
                        ]
                    ])->label(false) ?>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <?= Html::submitButton('Регистрация', [
                            'class' => 'btn btn-primary btn-block',
                            'name' => 'signup-button'
                        ]) ?>
                    </div>
                    <div class="col-xs-6">
                        <?= Html::a('Отмена', ['site/index'], [
                            'class' => 'btn btn-primary btn-block btn-flat',
                        ]) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

                <!-- <a href="#">I forgot my password</a><br>-->
                <!-- <a href="register.html" class="text-center">Register a new membership</a>-->

            </div>
        </div>

    </div>
</div>