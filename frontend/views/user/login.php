<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
///* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile(Yii::getAlias('@web') .'js/common_0517.js', ['depends' => [yii\web\JqueryAsset::className()]], \yii\web\View::POS_END);
$this->registerJsFile(Yii::getAlias('@web') .'js/jquery.mousewheel.min.js', ['depends' => [yii\web\JqueryAsset::className()]], \yii\web\View::POS_END);
 
?>
	<link rel="stylesheet" href="css/bootstrap-grid-3.3.1.min.css" />
	<link rel="stylesheet" href="css/font-awesome.min.css" />
	<link rel="stylesheet" href="css/fonts.css" />
	<link rel="stylesheet" href="css/main_0517.css" />
	<link rel="stylesheet" href="css/media_0517.css" />


	<div class="container">
		<div class="row">
			<div class="col-sm-3 hidden-xs"></div>
			<div class="col-sm-6 form-container">
				<div class="form-type enabled" id="enter">Вход</div>
				<div class="form-type" id="reg">Регистрация</div>
				<div class="enter-form" id="reg-f" style="display: none;">
                    <?php $formRegister = ActiveForm::begin() ?>
                    <?= $formRegister->field($modelReg, 'email')->textInput(['placeholder' => 'Email', 'class' => false])->label(false) ?>
                    <?= $formRegister->field($modelReg, 'password')->passwordInput(['placeholder' => 'Пароль не менее 6 символов', 'class' => false])->label(false) ?>
                    <?= $formRegister->field($modelReg, 'confirm_password')->passwordInput(['placeholder' => 'Пароль повторно', 'class' => false])->label(false) ?>
					<div class="check">
                        <?= $formRegister->field($modelReg, 'accept')->checkbox([
                            'id' => 'accept',
                            'label' => 'Я согласен с <a href="#" class="terms-a">условиями сайта</a>',
                            'template' => '<div class="checkbox">{beginLabel}{input}{labelTitle}<br/>{error}{endLabel}{hint}</div>'
                        ]) ?>
<!--						<label for="accept">Я согласен с <a href="#" class="terms-a">условиями сайта</a></label>-->
					</div>
                    <div class="clearfix"></div>
					<div class="captcha">
                        <?= $formRegister->field($modelReg, 'recaptcha')->widget(
                            \himiklab\yii2\recaptcha\ReCaptcha::className(),
                            ['siteKey' => '6LeCdCMUAAAAANwK2oSh1w-ddN6auFIDCreHu_FV']
                        )->label(false) ?>
                    </div>
                    <?= Html::submitInput('Зарегестрироваться', ['name' => 'register'])?>
                    <?php ActiveForm::end(); ?>
                </div>
				<div class="enter-form" id="enter-f">
                    <?php $form = ActiveForm::begin() ?>
                    <?= $form->field($model, 'email',[
						'options'=>[
							
						],
						'template'=>'{input}{error}'
					])->textInput([
						'type'=>'email',
						'class'=>'l-email',
						'placeholder'=>$model->getAttributeLabel('Email'),
					]) ?>
					<?= $form->field($model, 'password',[
						'options'=>[
							'class'=>'l-password',
						],
						'template'=>'{input}{error}'
					])->passwordInput([
						'type'=>'password',
						'class'=>'l-email',
						'placeholder'=>$model->getAttributeLabel('Ваш пароль'),
					]) ?>

                    <div class="resetBlock">
                        <a href="/user/request-password-reset" class="resetPass">Забыли пароль?</a>
                    </div>
					<?= Html::submitInput('Вход') ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
	<!--[if lt IE 9]>
	<script src="libs/html5shiv/es5-shim.min.js"></script>
	<script src="libs/html5shiv/html5shiv.min.js"></script>
	<script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
	<script src="libs/respond/respond.min.js"></script>
	<![endif]-->

<style>
    .login-box{margin-bottom: 20px;}
    @media only screen and (max-width : 1200px) {
        .login-box{padding-left: 20px !important;}
    }
    @media only screen and (max-width : 768px) {
        .login-box h3, .login-box form{padding-left: 55px !important;}
    }
</style>
<!--
<div class="container">
    <div class="row">

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 login-box">

            <h3><?/*= $this->title */?> / <a href="<?/*= \yii\helpers\Url::to(['user/signup']) */?>">Регистрация</a></h3>
            
            <?/*= $form->field($model, 'username',[
                'options'=>[
                    'class'=>'form-group has-feedback',
                ],
                'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}'
            ])->textInput([
                'class'=>'form-control',
                'placeholder'=>$model->getAttributeLabel('username'),
            ]) */?>

            --><?/*= $form->field($model, 'password', [
                'options'=>[
                    'class'=>'form-group has-feedback',
                ],
                'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
            ])->passwordInput([
                'class'=>'form-control',
                'placeholder'=>$model->getAttributeLabel('password'),
            ]) */?>

           <!-- <div class="row">
                <div class="col-xs-8">
                    <?/*= $form->field($model, 'rememberMe',[
                        'template' => "<div class=\"checkbox icheck\"><label>{input} {label}</label></div>{error}",
                    ])->checkbox() */?>
                    <?/*= Html::a('Забыли пароль?', ['user/request-password-reset']) */?>
                </div>
                <div class="col-xs-4" style="padding-right:0px;">
<input type="submit" value="Зарегестрироваться">
                </div>
            </div>-->
            <!-- <a href="#">I forgot my password</a><br>-->
            <!-- <a href="register.html" class="text-center">Register a new membership</a>-->


<!--            <form action="" method="post">-->
<!--                <div class="form-group">-->
<!--                    <input type="text" class="form-control" placeholder="Identifiant ou Email">-->
<!--                </div>-->
<!---->
<!--                <div class="form-group">-->
<!--                    <input type="password" class="form-control" placeholder="********">-->
<!--                </div>-->
<!---->
<!--                <div class="form-group">-->
<!--                    <input type="checkbox" id="s"> <label for="s" >Se souvenir de moi</label >-->
<!--                </div>-->
<!---->
<!--                <div class="form-group">-->
<!--                    <input type="submit" class="btn btn-success" value="Se connecter">-->
<!--                </div>-->
<!---->
<!--            </form>-->
  <!--      </div>
    </div>
</div>-->
</div>

<?php /*
<div class="container">

            <div class="login-box">
                <h3><?= $this->title ?> / <a href="<?= \yii\helpers\Url::to(['user/signup']) ?>">Регистрация</a></h3>
                <!-- /.login-logo -->
                <div class="login-box-body">
                    <?php $form = ActiveForm::begin() ?>

                    <?= $form->field($model, 'username',[
                        'options'=>[
                            'class'=>'form-group has-feedback',
                        ],
                        'template'=>'{input}<span class="glyphicon glyphicon-user form-control-feedback"></span>{error}'
                    ])->textInput([
                        'class'=>'form-control',
                        'placeholder'=>$model->getAttributeLabel('username'),
                    ]) ?>

                    <?= $form->field($model, 'password', [
                        'options'=>[
                            'class'=>'form-group has-feedback',
                        ],
                        'template'=>'{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
                    ])->passwordInput([
                        'class'=>'form-control',
                        'placeholder'=>$model->getAttributeLabel('password'),
                    ]) ?>

                    <div class="row">
                        <div class="col-xs-8">
                            <?= $form->field($model, 'rememberMe',[
                                'template' => "<div class=\"checkbox icheck\"><label>{input} {label}</label></div>{error}",
                            ])->checkbox() ?>
                            <?= Html::a('Забыли пароль?', ['user/request-password-reset']) ?>
                        </div>
                        <div class="col-xs-4">
                            <?= Html::submitButton('Вход', [
                                'class' => 'btn btn-primary btn-block btn-flat',
                                'name' => 'login-button'
                            ]) ?>

                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                    <!-- <a href="#">I forgot my password</a><br>-->
                    <!-- <a href="register.html" class="text-center">Register a new membership</a>-->

                </div>
            </div>

</div>
 */ ?>
