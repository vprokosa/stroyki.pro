<?php
    use yii\helpers\Html;
    use yii\helpers\Url;

    $this->title = 'Контакты, ' . $user->full_name;

?>

<div class="contact-page object-page">
    <div class="container">
        <div class="row">
            <div class="user-nav">
                <div class="col-sm-4">
                    <a href="<?= Url::to(['site/all-realty', 'user_id' => $user->id]); ?>">Все объявления</a>
                </div>
                <div class="col-sm-4">
                    <a href="/site/calculator?user_id=<?= $user->id?>">Ипотечный калькулятор</a>
                </div>
                <div class="col-sm-4">
                    <a href="/site/contact?user_id=<?= $user->id?>" class="active">Контакты</a>
                </div>
            </div>
        </div>

        <div class="contacts-content">
            <div class="s s6">
                <div class="sTitle">
                    <h2 class="h2">Контакты</h2>
                </div>
                <div class="sContent">
                    <div class="contacts">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="contacts-height">
                                    <?= Html::img($user->publicLogo(), ['alt' => $user->buildcompany]) ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contacts-height">
                                    <ul class="contacts-list">
                                        <?php if ($user->buildcompany) : ?>
                                            <li>Застройщик: <?= Html::a($user->buildcompany, ['site/all-realty', 'user_id'=>$user->id], ['target'=>'_blank']) ?></li>
                                        <?php endif; ?>

                                        <?php if ($user->city) : ?>
                                            <li>город: <?= $user->city ?></li>
                                        <?php endif; ?>

                                        <?php if ($user->street) : ?>
                                            <li>ул: <?= $user->street ?></li>
                                        <?php endif; ?>


                                        <?php if ($user->house) : ?>
                                            <li>дом: <?= $user->house ?></li>
                                        <?php endif; ?>

                                        <?php if ($user->office) : ?>
                                            <li>офис: <?= $user->office ?></li>
                                        <?php endif; ?>

                                        <?php if ($user->address) : ?>
                                            <li><?= $user->address ?></li>
                                        <?php endif; ?>
                                        <li class="small">Все объявления: <?= Html::a(count($user->realties), \yii\helpers\Url::toRoute(['site/all-realty', 'user_id' => $user->id]), ['target' => '_blank'])?></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="contacts-height">
                                    <div class="avatar">
                                        <?= $user->avatar(100, 100) ?>
                                    </div>
                                    <div class="name">
                                        <div class="d-table">
                                            <div class="d-row">
                                                <div class="text"><?= $user->full_name ? $user->full_name : '' ?>  </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if ($user->phone) : ?>
                                    <div class="clearfix"></div>
                                    <button class="showPhone" data-phone="<?= $user->phone ?>">Показать телефон</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /.s6 -->
            <?php if ($user->city) : ?>
            <div class="s s2">
                <div class="sTitle">
                    <div class="row">
                        <div class="sTitle-container">
                            <div class="col-xs-12">
                                <h2 class="h2">На карте</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sContent">
                    <div id="mapContacts" data-map-address="<?= $user->city.', '.$user->street ?>" data-map-caption="<?= $user->buildcompany ?>"></div>
                </div>
            </div> <!-- /.s2 -->
            <?php endif; ?>
            <div class="s s1">
                <div class="sTitle">
                    <h2 class="h2">О компании</h2>
                </div>
                <div class="sContent">
                    <p><?= $user->company?></p>
                </div>
            </div> <!-- /.s1 -->
            <div class="sGroup">
                <div class="row">
                    <div class="col-md-8">
                        <div class="s s5">
                            <div class="sTitle">
                                <h2 class="h2">Документация</h2>
                            </div>
                            <div class="sContent">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="doc-hiding">Проектная декларация</span>
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="#" class="download">Скачать</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <span class="doc-hiding">Разрешение на строительство</span>
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="#" class="download">Скачать</a>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="docs"><a href="#">Показать все документы</a></div>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- /.s5 -->
                    </div>
                    <div class="col-md-4">
                        <div class="banner-block banner-right">300 на 250 пикс строго</div>
                    </div>
                </div>
            </div> <!-- /.sGroup -->

        </div> <!-- /.contacts-content -->
        <div class="clearfix"></div>
    </div>
    <?= \frontend\components\widget\ObjectsNear::widget([
        'geo_lat'=> 55.70270300,
        'geo_lng'=> 37.02356100
    ]) ?>
    <div class="container">
        <div class="d-table">
            <div class="d-row">
                <div class="heading-line">
                    <h2 class="h2">Новостройки от других застройщиков</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-newBuildings">
        <?php
        echo \frontend\components\widget\BannerBiggest::run(array('city_id' => ((isset($_COOKIE['cityID'])) && (!empty($_COOKIE['cityID']))) ? $_COOKIE['cityID'] : 0 ));
        ?>
    </div>

    <?= \frontend\components\widget\BannerSmall::widget() ?>
    <?= \frontend\components\widget\BannerMiddleHorizontal::widget() ?>
    <div class="container">
        <?= \frontend\components\widget\BannerBig::widget() ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="all">
                    <div class="d-table">
                        <div class="d-row">
                            <div class="heading-line more">
                                <a href="/">Посмотреть всех застройщиков</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
