<?php

    use yii\helpers\Url;

    $this->title = 'Ипотечный калькулятор, ' . $user->full_name;

?>

<div class="calculator-page object-page">
    <div class="container">

        <div class="row">
            <div class="user-nav">
                <div class="col-sm-4">
                    <a href="<?= Url::to(['site/all-realty', 'user_id' => $user->id]); ?>">Все объявления</a>
                </div>
                <div class="col-sm-4">
                    <a href="/site/calculator?user_id=<?= $user->id?>" class="active">Ипотечный калькулятор</a>
                </div>
                <div class="col-sm-4">
                    <a href="/site/contact?user_id=<?= $user->id?>">Контакты</a>
                </div>
            </div>
        </div>
    </div>

        <?= \frontend\modules\mortgage_calculation\widgets\CalculationWidget::widget([
            'price' => 5000000,
            'title' =>  $user->full_name
        ]) ?>

    <div class="container">

        <div class="clearfix"></div>
        <div class="d-table">
            <div class="d-row">
                <div class="heading-line">
                    <h2 class="h2">Новостройки от других застройщиков</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-newBuildings">
        <?php
        echo \frontend\components\widget\BannerBiggest::run(array('city_id' => ((isset($_COOKIE['cityID'])) && (!empty($_COOKIE['cityID']))) ? $_COOKIE['cityID'] : 0 ));
        ?>
    </div>

    <?= \frontend\components\widget\BannerSmall::widget() ?>
    <?= \frontend\components\widget\BannerMiddleHorizontal::widget() ?>
    <div class="container">
        <?= \frontend\components\widget\BannerBig::widget() ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="all">
                    <div class="d-table">
                        <div class="d-row">
                            <div class="heading-line more">
                                <a href="/">Посмотреть всех застройщиков</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
