<?php
use common\components\ThumbHelper;
use common\enum\District;

?>
<div class="row">
    <div class="col-xs-12">
        <div class="content-list">
            <div class="admy-item">
                <a href="<?= \yii\helpers\Url::to(['site/advertisement', 'id'=>$model->id]) ?>">
                    <div class="row">
                        <div class="col-md-4">
                            <?php if(!$model->vip): ?>
                                <div class="preview" style="background-image: url(<?= ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>)"></div>
                            <?php else: ?>
                                <div class="preview" style="background-image: url(<?= \yii\helpers\Url::to(['site/vip-image', 'url'=>ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false)]) ?>"></div>
                            <?php endif ?>
                        </div>
                        <div class="col-md-8">
                            <div class="price-list"><?= preg_replace('#(?<=\d)(?=(\d{3})+$)#', ' ', $model->price) ?> руб</div>
                            <div class="clearfix"></div>
                            <div class="builder"><?= $model->attr->housing_estate ? $model->attr->housing_estate : '' ?></div>
                            <div class="address-wrap">
                                <?php if ($model->attr->district->title) : ?> <div class="address-text">рн: <?= $model->attr->district->title ?></div> <?php endif; ?>
                                <?php if ($model->attr->street) : ?> <div class="address-text">ул: Зои космедемьянской</div> <?php endif ?>
                            </div>
                        </div>
                    </div>
                    <div class="toBottom">
                        <div class="deadline"><?= intval((date('n', $model->deadline)+2)/3) . " кв " . date("Y", $model->deadline) ?></div>
                    </div>
                    <div class="userButtons">
                        <?= $this->render('/layouts/_btn_favorite', ['model'=>$model]) ?>
                        <?= $this->render('/layouts/_btn_spy_price', ['model'=>$model]) ?>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>