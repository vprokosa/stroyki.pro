<style>
    #breadcrumb-total .row{margin-left:-23px;}
    #breadcrumb-total .row > div{padding-left:0px;padding-right:0px;}
    @media only screen and (max-width : 940px) {
        .breadcrumb{padding-left:22px;}
    }
    @media only screen and (max-width : 768px) {
        .breadcrumb{padding-left:56px;}
    }
</style>
<?php $href = '/';?> 
<div class="container">
    <div id="breadcrumb-total">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="<?= $href ?>"><span style="color:red">Главная</span></li></a>
                    <?php $href .= 'search-objects';
					?>
                    <?php if(!empty($this->params['city'])){ 
						$href .= '?ObjectSearch[city_id]='.$this->params['city']->id;
					?>
					<li><a href="<?= $href ?>"><span id="city-name"><?= $this->params['city']->title ?></span></a></li>
                    <?php } ?>
                </ol>
            </div>
        </div>
    </div>
</div>

