<?php
use frontend\assets\YMapAsset;

YMapAsset::register($this);
$this->title = 'Поиск объектов на карте';
?>

<?= $this->render('@frontend/views/layouts/_search_form_map', [
    'searchModel'=>$searchModel,
    'stringResult'=>$stringResult,
    'dataProvider'=>$dataProvider,
	'ObjectSearch' => $ObjectSearch
]) ?>

    <?php \yii\widgets\Pjax::begin([
        'timeout'=>10000,
        'scrollTo'=>new \yii\web\JsExpression('$("#search-map").position().top-150')
    ]) ?>

    <?php

    $this->registerJs(
        '
            $("#result").text("'.$stringResult.'");

            ymaps.ready(function(){
                ymaps.geocode("'.$positionPlace.'").then(
                    function (res) {
                        glMap = new ymaps.Map("ya-map", {
                            center: res.geoObjects.get(0).geometry.getCoordinates(),
                            zoom: '.$zoom.',
                            controls: [],
                            behaviors: []
                        });
                        glMap.behaviors.disable("scrollZoom");
                        glMap.behaviors.enable(\'drag\');

                        var zoomControl = new ymaps.control.ZoomControl({
                            options: {
                                position: {
                                    top:170,
                                    left:15
                                }
                            }
                        });
                        glMap.controls.add(zoomControl);
                        
                         var MyIconLayout = ymaps.templateLayoutFactory.createClass(
                         \'<div class="iconLayout"><div class="iconContent">$[[cluster#iconContent]]</div><div class="caption">$[properties.geoObjects]</div></div>\');
                         
                      var MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
                         \'{% for geoObject in properties.geoObjects %} $[geoObject.properties.balloonContentBody|raw] {% endfor %}\');



                        siteClusterer = new ymaps.Clusterer(
                            {clusterDisableClickZoom: true,
                             clusterBalloonContentLayout: MyIconContentLayout,
                           //  clusterIconLayout: MyIconLayout,
                           //  clusterIconContentLayout: MyIconContentLayout
                             //preset: \'islands#invertedVioletClusterIcons\',
                             }
                        );
                        siteClusterer.add(getObjects());
                        var fullscreenControl = new ymaps.control.FullscreenControl();

                        glMap.controls.add(fullscreenControl);
                        glMap.geoObjects.add(siteClusterer);
                    },
                    function (err) {
                        alert(\'Ошибка загрузки карты !\');
                    }
                );
            });
       '
    );
    ?>

    <script>
        var glMap;
        var geoObjects = [];
        var siteClusterer;

        function mapShowObject(event, lat, lng)
        {
            $('body,html').animate({
                scrollTop: $("#search-map").position().top
            }, 400);
            //
            event.preventDefault();
            glMap.setCenter([lat, lng], 16);
            var circle = new ymaps.Circle([[lat, lng], 10]);
            glMap.geoObjects.add(circle);
            var objectsInsideCircle = ymaps.geoQuery(geoObjects).searchInside(circle);
            var object = objectsInsideCircle.get(0);
            object.balloon.open();
        }
        function mapAddObjects(objects)
        {
            console.log('Adding objects...');
            var n = 0;
            var m = geoObjects.length + objects.length;
            for (var i = geoObjects.length; i < m; i++) {

                geoObjects[i] = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: objects[n]['geo']
                    },
                    properties: {
                        clusterCaption: '<img src="'+objects[n]['img']+'" width="100"/>'+ objects[n]['title']+'<a class="btn f12 btn_blue" target="_blank" href="'+ objects[n]['url'] +'">Открыть</a>',
                        balloonContentBody:  '<div class="yamap_balloonContentBody_img"><img src="'+ objects[n]['img'] +'" /></div>'+ objects[n]['title'],
                        balloonContentFooter: '<a class="btn f12 btn_blue" target="_blank" href="'+ objects[n]['url'] +'">Открыть</a>'
                    }
                });
                n++;
            }
            siteClusterer.removeAll();
            siteClusterer.add(geoObjects);
        }
        function getObjects()
        {
            var objectsForMap = <?= $objectsForMap ?>;
            console.log(objectsForMap);
            for (var i=0; i<objectsForMap.length; i++) {
                geoObjects[i] = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: objectsForMap[i]['geo']
                    },
                    properties: {
                        iconCaption: 'TEXT',
                        clusterCaption: '<img src="'+ objectsForMap[i]['img'] +'" />' + objectsForMap[i]['title'] + '<a class="btn f12 btn_blue" target="_blank" href="'+ objectsForMap[i]['url'] +'">Открыть</a>',
                        balloonContentBody:  '<div class="clusterContent"><a href="'+ objectsForMap[i]['url'] +'" target="_blank"><div class="yamap_balloonContentBody_img"><div class="balloonRight toH"><img src="'+ objectsForMap[i]['img'] +'" alt="' + objectsForMap[i]['title'] + '" /></div>'
                            + '<div class="balloonLeft toH">'
                                + '<div class="price">' + objectsForMap[i]['price'] +'</div>'
                                + '<div class="company">'+  objectsForMap[i]['company'] +'</div>'
                                + '<div class="district">'+  objectsForMap[i]['district'] +'</div>'
                                + '<div class="deadline">'+  objectsForMap[i]['deadline'] +'</div>'
                              + '<i onclick="if(isLogin()) { RegFavorite(this, '+ objectsForMap[i]['id'] +'); } else { location.href = \'/login\'; } return false;"  class="fa fa-star-o pull-right '+  objectsForMap[i]['favoriteClass'] +' ?>" style="cursor: pointer"></i>'
                            + '</div>'
                        + '</div></a></div>',
                        balloonContentFooter: ''
                    }
                });
            }
            return geoObjects;
        }

    </script>


    <div id="search-map" class="row well">

        <div id="search-result-objects-min" class="col-lg-3 hidden-md hidden-sm hidden-xs">
            <?= \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items} <div id='paginate'>{pager}</div>",
                'itemView' => function ($model, $key, $index, $widget) {
                    return $this->render('_item_object_map',[
                        'model' => $model,
                        'index' => $index,
                    ]);
                },

                'pager' => [
                    'maxButtonCount' => 5
                ]
            ]) ?>,
        </div>
        <div id="search-result-on-map" class="col-lg-9 col-md-12">
            <div class="s s2">
                <div class="sTitle">
                    <div class="row">
                        <div class="sTitle-container">
                            <div class="col-xs-12">
                                <div class="markers">
                                    <span class="mark-blue">Все новостройки</span>
                                    <span class="mark-green">От застройщика</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ya-map" style="width:101%; max-width:101%; height:900px; position:relative; display:inline-block"></div>
        </div>

    </div>


    <?php \yii\widgets\Pjax::end() ?>

<?= \frontend\components\widget\BannerSmall::widget() ?>
<?= \frontend\components\widget\BannerMiddleHorizontal::widget() ?>

<div class="object-page">
    <div class="d-table">
        <div class="d-row">
            <div class="heading-line">
                <h2 class="h2">Новостройки от застройщиков</h2>
            </div>
        </div>
    </div>
</div>

<div class="bg-newBuildings">
    <?php echo \frontend\components\widget\BannerBiggest::run(array('city_id' => ((isset($_COOKIE['cityID'])) && (!empty($_COOKIE['cityID']))) ? $_COOKIE['cityID'] : 0 )) ?>
</div>
<div class="onMap">
    <div class="row">
        <div class="col-xs-12">
            <div class="all">
                <div class="d-table">
                    <div class="d-row">
                        <div class="heading-line more">
                            <?php
                                $cityId = !empty($_COOKIE['cityID']) ? $_COOKIE['cityID'] : 1;
                                $cityname = \common\enum\City::Lists($cityId);
                            ?>
                            <a href="/<?= \common\components\TranslitHelper::transliteration($cityname) ?>/zastroyshchiki">Все
                                новостройки города </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>