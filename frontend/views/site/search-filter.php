<?php

//$this->title = 'Поиск объектов';
$block='';
$this->registerJs(
    'function  Resize() {
        if ($(window).width() < 992) {
            var paginate = $("#paginate > .pagination").clone();
            $("#paginate").hide();
            $("#paginate-footer").empty().html(paginate);
        } else {
            $("#paginate").show();
            $("#paginate-footer").empty();
        }
    }
    $(document).ready(function () {
        Resize();
        $(window).on("resize", function () {
            Resize();
        });
        $(document).on(\'pjax:complete\', function() {
            Resize();
        });
    });
	$(document).ready(function () {
                $("#result").text("<?= $stringResult ?>");
            });
	'
);

?>
<?php /*
<h2 class="title">
    <?= $this->title ?>
</h2>
 */

// echo "<pre>".print_r($this, true)."</pre>";

var_dump(\Yii::$app->controller->route);
?>

<div class="listAds">

    <?= $this->render('@frontend/views/layouts/_search_form', [
        'searchModel'=>$searchModel,
        //'stringResult'=>$stringResult,
        'dataProvider'=>$dataProvider,
        'ObjectSearch'=>$ObjectSearch
    ]) ?>

    <div class="container">
        <div id="search-result-objects" class="row">
            <?php \yii\widgets\Pjax::begin([
                'timeout' => 10000,
                // 'scrollTo'=>new \yii\web\JsExpression('$("#search-panel").position().top-150')
            ]) ?>

            <script>

            </script>

            <?php if (\Yii::$app->controller->route == 'site/search-filter'): ?>
                <?php
                $block = '<div style="float: left;margin-left: -9px;margin-right: -9px; " class="bntype2">'
                    . frontend\components\widget\BannerSmall::widget(['dop_class' => ''])
                    . \frontend\components\widget\BannerMiddleHorizontal::widget()
                    . \frontend\components\widget\BannerBig::widget()
                    . '</div>';
                ?>
            <?php endif ?>
            <div class="col-md-8">
                <div class="content-list">
                    <?= \yii\widgets\ListView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}<div class=\"block_pagination\">{pager}</div>$block",
                        'itemView' => function ($model, $key, $index, $widget) use ($dataProvider) {
                            return $this->render('_item_object', [
                                'model' => $model,
                                'index' => $index,
                                'position' => $model->vip ? 'position:relative' : ''
                            ]);
                        },
                    ]) ?>
                </div>

                <div id="paginate-footer" style="margin-left:20px;margin-bottom:15px;"></div>
            </div>
            <div class="col-md-4">
                <div class="banners-list">
                    <?php foreach ($bannerMiddle as $banner): ?>
                        <div class="banner-item">
                            <a href="<?= \yii\helpers\Url::to(['site/banner', 'id' => $banner['id']]) ?>"
                               target="_blank">
                                <?= \yii\helpers\Html::img('/upload/banner/' . $banner['id'] . '/' . $banner['image'], [
                                    'class' => 'img-responsive banner',
                                    'alt' => $banner['title'],
                                ]) ?>
                            </a>
                        </div>
                    <?php endforeach ?>
                </div>

            </div>

            <?php \yii\widgets\Pjax::end() ?>
            <!--	--><? /*= \frontend\components\widget\ShowArticle::run(array('pos' => 1, 'city_id' => (isset($_COOKIE['cityID']) && !empty($_COOKIE['cityID'])) ? $_COOKIE['cityID'] : 0 ));*/ ?>

        </div>

    </div>
</div>
