<?php 
use common\components\ThumbHelper;
use common\enum\User;
$this->title='«Все Новостройки» - База новостроек Москвы, Подмосковья и Новой Москвы от застройщиков';
?>

<div class="general">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <h2><a href="<?
                    unset($this->params['ObjectSearch']['option']);
                    unset($this->params['ObjectSearch']['housing_estate']);
                    echo \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch'=> array_merge($this->params['ObjectSearch'], ['novostroyki'=>1])]);?>" class="" >Все новостройки</a></h2>
            </div>
        </div>
        <div class="row">
            <?
            foreach ($new_objects as $key => $value) {
                ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 stroyki_main_item">
                    <div class="box_house">
                        <div class="strong_size bg_1" style="background-image:url(<?= ThumbHelper::thumbnailImg($value["obj"]->mainImage(), 300, 250, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>);">
                            <a href="<?= \yii\helpers\Url::to(['site/advertisement', 'id'=>$value["obj"]->id]) ?>">
                                <p class="city_p"><?=$value["city_name"]?><br>
                                    <?=$value["obj"]->attr->rooms?> комнатная</p>
                                <p class="price_p"><?=number_format($value["obj"]->price, 0, ","," ")?> <span>руб</span></p>

                                <p class="developer_p"><?=$value["obj"]->user->buildcompany?></p>

                            </a>
                        </div>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <h2><a href="<?
                    unset($this->params['ObjectSearch']['novostroyki']);
                    unset($this->params['ObjectSearch']['option']);
                    echo \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => array_merge($this->params['ObjectSearch'], ['housing_estate'=>1])]);?>">Все жилые комплексы</a></h2>
            </div>
        </div>
        <div class="row">
            <?
            foreach ($new_housing_estate as $key => $value) {
                ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 stroyki_main_item">
                    <div class="box_house">
                        <div class="strong_size bg_4" style="background-image:url(<?= ThumbHelper::thumbnailImg($value["obj"]->mainImage(), 300, 250, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>);">
                            <a href="<?= \yii\helpers\Url::to(['site/advertisement', 'id'=>$value["obj"]->id]) ?>">
                                <p class="city_p"><?=$value["city_name"]?></p>
                                <!--<p class="jk_p"><?=($value["obj"]->attr->gkh_name)?$value["obj"]->attr->gkh_name: 'жк Солнечный берег' ?></p>-->
                                <p class="jk_p"><?=($value["obj"]->attr->housing_estate)?$value["obj"]->attr->housing_estate: '' ?></p>
                                <p class="sm_price">От :  <?=number_format($value["obj"]->price, 0, ","," ")?>  руб  м2</p>

                                <?php if ($value["user_type"] == User::TYPE_CREATOR): ?>
                                    <p class="developer_p">Застройщик : <?=$value["obj"]->user->buildcompany?></p>
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                <h2><a href="<?
                    unset($this->params['ObjectSearch']['novostroyki']);
                    unset($this->params['ObjectSearch']['housing_estate']);
                    echo \yii\helpers\Url::toRoute(['site/search-objects', 'ObjectSearch' => array_merge($this->params['ObjectSearch'], ['option'=>\common\enum\User::TYPE_CREATOR])]);?>">Все застройщики</a></h2>
            </div>
        </div>
        <div class="row">
            <?
            foreach ($new_zastr as $key => $value) {
                ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 stroyki_main_item">
                    <div class="box_house">
                        <div class="strong_size bg_7" style="background-image:url(<?= ThumbHelper::thumbnailImg($value["obj"]->mainImage(), 300, 250, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>);">
                            <a href="<?= \yii\helpers\Url::to(['site/advertisement', 'id'=>$value["obj"]->id]) ?>">
                                <p class="city_p last_city_p"><?=$value["city_name"]?></p>
                                <p class="developer_p">Застройщик:</p>
                                <p class="big_devel_p"><?=$value["obj"]->user->buildcompany?></p>
                                <?php if($value["obj"]->user->publicLogo() !== 'notFind'): ?>
                                <div style="background-image:url(<?=$value["obj"]->user->publicLogo()?>);"></div>
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>
                </div>
                <?
            }
            ?>
        </div>
        <div class="row" style="margin-left:-15px;">
            <?= frontend\components\widget\BannerSmall::widget(['dop_class' => '']) ?>
            <?= frontend\components\widget\BannerMiddleHorizontal::widget() ?>
            <?= frontend\components\widget\BannerBig::widget() ?>
        </div>
        <div class="row main-text">
            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                    <?= \frontend\components\widget\ShowArticle::run(array('pos' => 3, 'city_id' => (isset($_COOKIE['cityID']) && !empty($_COOKIE['cityID'])) ? $_COOKIE['cityID'] : 0 ));?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <div class="banners-right">
                    <!--<div class="banner-right col-lg-12 col-md-12 col-sm-6 col-xs-12 br1">баннер 300на 250 пикс<br>жёстко</div>
                    <div class="banner-right col-lg-12 col-md-12 col-sm-6 col-xs-12 br2">баннер 300на 250 пикс<br>жёстко</div>-->

                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- авто -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-9616416554119262"
                         data-ad-slot="1682961932"
                         data-ad-format="auto"></ins>
                    <script>
                        (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
