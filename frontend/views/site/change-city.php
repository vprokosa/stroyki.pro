<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"></div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="changeCity">
            <?= \kartik\select2\Select2::widget([
                'name' => 'city',
                'data' => \common\enum\City::Lists(),
                'options' => [
                    'placeholder' => 'Найти город',
                    'onchange' => 'SelectCity(this)',
                    'class' => 'city',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>
</div>