<?php
use common\components\ThumbHelper;
use common\components\TranslitHelper;
use common\enum\District;
use common\enum\City;
use yii\helpers\Html;

?>
<div class="admy-item">
    <a href="<?= \yii\helpers\Url::to(['advertisement', 'id'=>$model->id]) ?>">
        <div class="row">
            <div class="col-md-4">
                <?php if(!$model->vip): ?>
                    <div class="preview" style="background-image: url(<?= ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>)"></div>
                <?php else: ?>
                    <div class="preview" style="background: url(<?= \yii\helpers\Url::to(['site/vip-image', 'url'=>ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false)]) ?>)">
                <?php endif ?>
            </div>
            <div class="col-md-8">
                <?php if (file_exists('/upload/citesLogo/.png')) : ?>
                    <img src="/upload/citesLogo/.png" alt="<?= City::Lists($model->attr->city_id)?>" class="cityLogo">
                <?php endif; ?>
                <?php if (!empty($model->flats)) : ?>
                    <div class="builder"><?= $model->attr->housing_estate ?></div>
                    <div class="clearfix"></div>
                    <div class="address-wrap">
                        <div class="address-text">Застройщик: <?= $model->user->buildcompany ?></div>
                    </div>
                    <ul class="prices-list">
                        <?php
                        $icount = 1;
                        foreach ($model->flats as $flat) :
                            ?>
                            <li><span class="rooms"><?= $flat->attr->rooms?></span> комн от <span class="price"><?= preg_replace('#(?<=\d)(?=(\d{3})+$)#', ' ', $flat->price) ?> <span class="rub">руб</span></span></li>
                            <?php
                            if ($icount == 3)
                                break;
                            $icount++;
                        endforeach;
                        ?>
                    </ul>
                <?php else: ?>
                    <div class="decs-list">
                        <ul class="houseInfo">
                            <?php if ($model->attr->rooms) : ?>
                                <li><span><?= $model->attr->rooms?></span>:комн,</li>
                            <?php endif; ?>
                            <?php if ($model->attr->area_living) : ?>
                                <li>50/37/10,</li>
                            <?php endif; ?>
                            <?php if ($model->attr->floor) : ?>
                                <li>эт:<?= $model->attr->floor ?>/<?= $model->attr->floors ?></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="address-wrap">
                        <?php if (District::Lists($model->attr->district_id)) : ?>
                            <div class="address-text">рн: <?= District::Lists($model->attr->district_id) ?></div>
                        <?php endif; ?>
                        <?php if ($model->attr->street) : ?>
                            <div class="address-text">ул: <?= $model->attr->street ?></div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="toBottom">
            <div class="deadline">срок сдачи<span><?= intval((date('n', $model->deadline)+2)/3) . " кв " . date("Y", $model->deadline) ?></span></div>
            <?php if (empty($model->flats)) : ?>
                <div class="price-list"><?= preg_replace('#(?<=\d)(?=(\d{3})+$)#', ' ', $model->price) ?> руб</div>
            <?php endif; ?>
        </div>

    </a>
    <div class="userButtons">
        <?= $this->render('/layouts/_btn_favorite', ['model'=>$model]) ?>
        <?= $this->render('/layouts/_btn_spy_price', ['model'=>$model]) ?>
    </div>
</div>