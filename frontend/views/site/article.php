<?php


use yii\helpers\Html;


$this->title = \yii\helpers\Html::encode($articles->title);
$block='';
//use Yii;
//$this->registerCssFile("/css/css032017.css");

?>
<?php $href = '/';?>
<div class="container">
    <div id="breadcrumb-total">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="<?= $href ?>"><?= Html::encode('Главная') ?></li></a>
                    <?php $href .= 'article/' .  $articles->id; ?>

                    <li><?= $this->title ?></li>


                </ol>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div id="search-result-objects" class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-left:9px;padding-right:9px;">
		<div id="pu24759" class="clearfix grpelem"><!-- column -->
           <div data-content-guid="u24759_content" id="u24759" class="clearfix colelem shared_content"><!-- group -->
            <!-- m_editable region-id="editable-static-tag-U25393-BP_infinity" template="index.html" data-type="html" data-ice-options="disableImageResize,link" -->
            <div data-content-guid="u25393-4_content" data-muse-type="txt_frame" data-muse-uid="U25393" id="u25393-4" class="clearfix grpelem shared_content"><!-- content -->
             <p><?=$articles->title?></p>
            </div>
            <!-- /m_editable -->
           </div>
           <div data-content-guid="u25396_content" id="u25396" class="clearfix colelem shared_content"><!-- group -->
            <!-- m_editable region-id="editable-static-tag-U45178-BP_infinity" template="index.html" data-type="html" data-ice-options="disableImageResize,link" -->
            <div data-content-guid="u45178-21_content" data-muse-type="txt_frame" data-muse-uid="U45178" id="u45178-21" class="clearfix grpelem shared_content"><!-- content -->
             <?=$articles->text?>
            </div>
            <!-- /m_editable -->
           </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 visible-lg visible-md" style="padding-left:9px;">


        </div>
    </div>


</div>
