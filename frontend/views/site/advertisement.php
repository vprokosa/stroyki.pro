<?php
$this->title = 'Просмотр объекта';
//echo \common\enum\City::Lists($realty->attr->city_id);

function price_format($price) {
    return preg_replace('#(?<=\d)(?=(\d{3})+$)#', ' ', $price);
}

?>
<style>
    #banner-big img{margin-top:-20px;margin-bottom:-10px;}
    #banners1 .row .col-lg-4{padding-left:8px;padding-right:8px;}
    @media only screen and (max-width:768px) {
        /*#banners1 .row .col-lg-4{left:26px !important;}*/
        #banner-big .box-banner{left: 17px !important;}
        #btn-all-objects button{margin-left:28px !important;margin-bottom:25px !important;}

        #contact-owner{padding-right:0px !important;}
        #contact-owner .col-xs-12{padding-right:0px !important;}
        #contact-owner .box-header{padding-left:35px !important;}
        #contact-owner .box-body{padding-left:30px !important;}
        #contact-owner img{margin-left:0px !important;}
    }
</style>

<div class="object-page">

<?= $this->render('@frontend/views/layouts/_object_info', ['realty'=>$realty]) ?>

<?= $this->render('@frontend/views/layouts/_object_desc', ['realty'=>$realty]) ?>

<?= $this->render('@frontend/views/layouts/_object_on_map', ['realty'=>$realty]) ?>

<?= $this->render('@frontend/views/layouts/_object_link_items', ['realty'=>$realty]) ?>

<?php 
if($realty->type != \common\enum\Realty::TYPE_APARTMENT_HOUSE): ?>
    <?= \frontend\modules\mortgage_calculation\widgets\CalculationWidget::widget([
        'price' => $realty->price,
        'title' => $realty->desc->title
    ]) ?>
<?php endif ?>

<?= $this->render('@frontend/views/layouts/_object_docs', ['realty'=>$realty]) ?>

<?= $this->render('@frontend/views/layouts/_contact_owner', ['user'=>$realty->user, 'realty'=>$realty]) ?>

<?= \frontend\components\widget\ObjectsNear::widget([
    'geo_lat'=>$realty->geo_lat,
    'geo_lng'=>$realty->geo_lng
]) ?>

</div>

<div class="container">
    <div class="object-page">
        <div class="d-table">
            <div class="d-row">
                <div class="heading-line">
                    <h2 class="h2">Новостройки от застройщика <?= \common\enum\City::Lists($realty->attr->city_id)?></h2>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bg-newBuildings">
    <?php echo \frontend\components\widget\BannerBiggest::run(array('city_id' => ((isset($_COOKIE['cityID'])) && (!empty($_COOKIE['cityID']))) ? $_COOKIE['cityID'] : 0 )) ?>
</div>

<?= \frontend\components\widget\BannerSmall::widget() ?>
<?= \frontend\components\widget\BannerMiddleHorizontal::widget() ?>
<div class="container">
    <?= \frontend\components\widget\BannerBig::widget() ?>
</div>

<?= $this->render('@frontend/views/layouts/_btn_all_objects', ['realty'=>$realty]) ?>
