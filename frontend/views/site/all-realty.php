<?php
    use yii\helpers\Url;

    $this->title = 'Все объявления, ' . $user->full_name;
?>


<div class="all-ads listAds">
    <div class="container">
        <div class="row">
            <div class="user-nav">
                <div class="col-sm-4">
                    <a href="<?= Url::to(['site/all-realty', 'user_id' => $user->id]); ?>" class="active">Все объявления</a>
                </div>
                <div class="col-sm-4">
                    <a href="/site/calculator?user_id=<?= $user->id?>">Ипотечный калькулятор</a>
                </div>
                <div class="col-sm-4">
                    <a href="/site/contact?user_id=<?= $user->id?>">Контакты</a>
                </div>
            </div>
        </div>

        <!-- List ads -->

        <div class="row">
            <div class="col-xs-12">
                <div class="content-list">
                    <?= \yii\widgets\ListView::widget([
                        'dataProvider' => $listDataProvider,
                        'layout' => "{items}<div class=\"row\"><div class=\"col-xs-12\"><div class=\"pagination-block\">{pager}</div></div></div>",
                        'itemView' => function ($model, $key, $index, $widget) {
                            return $this->render('_item_realty', [
                                'model' => $model,
                                'index' => $index,
                            ]);
                        },
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="d-table">
            <div class="d-row">
                <div class="heading-line">
                    <h2 class="h2">Новостройки от других застройщиков</h2>
                </div>
            </div>
        </div>

    </div>

    <div class="bg-newBuildings">
        <?php
        echo \frontend\components\widget\BannerBiggest::run(array('city_id' => ((isset($_COOKIE['cityID'])) && (!empty($_COOKIE['cityID']))) ? $_COOKIE['cityID'] : 0 ));
        ?>
    </div>

    <?= \frontend\components\widget\BannerSmall::widget() ?>
    <?= \frontend\components\widget\BannerMiddleHorizontal::widget() ?>
    <div class="container">
        <?= \frontend\components\widget\BannerBig::widget() ?>

        <div class="row">
            <div class="col-xs-12">
                <div class="all">
                    <div class="d-table">
                        <div class="d-row">
                            <div class="heading-line more">
                                <a href="/">Все новостройки застройщика</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>