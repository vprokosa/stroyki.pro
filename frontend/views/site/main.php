<?php

$this->title = '«Все Новостройки» - База новостроек Москвы, Подмосковья и Новой Москвы от застройщиков';
?>

<?php $this->beginContent('@frontend/views/layouts/base.php') ?>

<div class="content">
    
    <?= $this->render('_header') ?>

    <?= $this->render('_alert') ?>
    
    <?= $this->render('_breadcrumb', ['option_show'=>'visible-xs']) ?>

    <?= $this->render('_menu_min') ?>

    <?= \frontend\components\widget\BannerSmall::widget() ?>

    <?= $this->render('_breadcrumb', ['option_show'=>'hidden-xs']) ?>

    <?= $this->render('_map') ?>

    <div class="container">
        <div class="row">

            <?= \frontend\components\widget\ObjectsMain::widget() ?>
            
            <?= \frontend\components\widget\BannerMiddle::widget(['count'=>5]) ?>
            
        </div>
    </div>
    
</div>

<?php $this->endContent() ?>
