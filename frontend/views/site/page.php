<?php

use yii\helpers\Html;

$this->title = \yii\helpers\Html::encode($page->title);

?>

<?php $href = '/';?>
<div class="container">
    <div id="breadcrumb-total">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="<?= $href ?>"><?= Html::encode('Главная') ?></li></a>
                    <?php $href .= 'pages/' .  $page->alias; ?>

                    <li><a href="<?= $href ?>"><?= $this->title ?></a></li>


                </ol>
            </div>
        </div>
    </div>
</div>

<div class="container page">

    <h1><?= $this->title ?></h1>
    <br>
    <div>
        <?= \yii\helpers\HtmlPurifier::process($page->text) ?>
    </div>


    <div class="row form">
        <?php
        $route = Yii::$app->urlManager->parseRequest(Yii::$app->request);

        ?>

        <?php  if(isset($route[1]['alias']) && $route[1]['alias'] === 'support'): ?>

            <?= Html::beginForm('/support', 'post', ['class' => 'support-form']); ?>
            <div class="col-sm-6">
                <div class="form-group">
                    <?= Html::label('Имя') ?>
                    <?= Html::input('text', 'name', null, ['class' => 'form-control input-sm', 'required' => true]) ?>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group">
                    <?= Html::label('Почта (E-mail)') ?>
                    <?= Html::input('email', 'email', null, ['class' => 'form-control input-sm']) ?>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <?= Html::label('Сообщение') ?>
                    <?= Html::textarea('message', '', ['rows' => 6, 'class' => 'form-control', 'required' => true]) ?>
                </div>
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary btn-sm']) ?>
                </div>
            </div>

            <?= Html::endForm() ?>

        <?php endif; ?>
    </div>
</div>

