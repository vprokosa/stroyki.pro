<?php
$this->title = 'Поиск объектов';
?>

<script>
    function  Resize() {
        if ($(window).width() < 992) {
            var paginate = $("#paginate > .pagination").clone();
            $("#paginate").hide();
            $("#paginate-footer").empty().html(paginate);
        } else {
            $("#paginate").show();
            $("#paginate-footer").empty();
        }
    }
    $(document).ready(function () {
        Resize();
        $(window).on("resize", function () {
            Resize();
        });
        $(document).on('pjax:complete', function() {
            Resize();
        });
    });
</script>

<?php /*
<h2 class="title">
    <?= $this->title ?>
</h2>
 */ ?>

<section class="content">
    <div class="container">
        <div class="row">
            <div id="banners">
			
			<?php if(\Yii::$app->controller->route == 'site/search-objects'): ?>
			<?= \frontend\components\widget\BannerSmall::widget() ?>
			<?php endif ?>
			
               
            </div>
        </div>    
            <div class="box_result_search">
           <div class="block_spisok row">
		   
			<?= $this->render('@frontend/views/layouts/_search_form_new', [
				'searchModel'=>$searchModel,
				'stringResult'=>$stringResult,
				'dataProvider'=>$dataProvider
]) ?>
                
                
            </div>
        </div>
		
        <div id="search-result-objects" class="row">
        
            
			<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
			 <?= \yii\widgets\ListView::widget([
                'dataProvider' => $dataProvider,
                'layout' => "{items}<div class='block_pagination'>{pager}</div>",
                'itemView' => function ($model, $key, $index, $widget) use($dataProvider) {
                    return $this->render('_item_object',[
                        'model' => $model,
                        'index' => $index,
                        'position' => $model->vip ? 'position:relative' : ''
                    ]);
                },
				
            ]) ?>
                
            </div>
            <div class="col-lg-4 col-md-5 visible-lg visible-md" >
			
			<?= \frontend\components\widget\BannerBig::widget() ?>
                          
                                        
        </div>
           
        
      


    <div class="hidden">

    </div>
	
</div>
</div>
</section>
<section class="advertising">
    <div class="container">
        <div class="row">
            <div class="block_942">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="#"><p>Реклама на сайте</p></a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="#"><p>Стать партнёром</p></a>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <a href="#"><p>Техническая поддержка</p></a>
                </div>
            </div>
        </div>
        </div>
</section>



