<?php

use common\models\User;

$this->title = 'Добавить логотип';
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6" style="padding-left: 30px; padding-right: 0px;">
            <div class="change-avatar-box">
                <div class="change-avatar-box-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'options'=>[
                            'enctype'=>'multipart/form-data'
                        ]
                    ]) ?>
												
                    <?= \Yii::$app->user->identity->logo(200, 150) ?>
                    <?= $form->field($model, 'logo', [
                        'options'=>[
                            'style'=>'padding-bottom:0px;margin-bottom:0px;'
                        ]
                    ])->widget(\kartik\widgets\FileInput::classname(), [
                        'options' => [
                            'class'=>'app-file-input',
                            'accept' => 'image/*',
                        ],
                        'pluginOptions' => [
                            'allowedFileExtensions'=>['png','jpg'],
                            'allowedPreviewTypes'=>['image'],
                            'showUpload' => false,
                            'browseLabel' => '',
                            'removeLabel' => '',
                            'browseIcon' => '<i class="fa fa-folder-open"></i>',
                            'removeIcon' => '<i class="fa fa-trash"></i>',                         
                        ]
                    ]) ?>

                    <?= \yii\helpers\Html::submitButton(\yii\helpers\Html::tag('i', '', ['class'=>'fa fa-save']).' Сохранить', [
                        'class' => 'btn btn-primary btn-block',
                        'name' => 'add-button',
                    ]) ?>

                    <?php \yii\widgets\ActiveForm::end() ?>

                </div>
            </div>
        </div>

    </div>
</div>