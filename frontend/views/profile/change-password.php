<?php
$this->title = 'Новый пароль';
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="change-password-box">

                <div class="change-password-box-body">
                    <?php $form = \yii\widgets\ActiveForm::begin() ?>

                        <?= $form->field($model, 'old_password', [
                            'options' => [
                                'class' => 'form-group has-feedback',
                            ],
                            'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
                        ])->passwordInput([
                            'class' => 'form-control',
                            'placeholder' => $model->getAttributeLabel('old_password'),
                        ]) ?>


                        <?= $form->field($model, 'password', [
                            'options' => [
                                'class' => 'form-group has-feedback',
                            ],
                            'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
                        ])->passwordInput([
                            'class' => 'form-control',
                            'placeholder' => $model->getAttributeLabel('password'),
                        ]) ?>


                        <?= $form->field($model, 'password_repeat', [
                            'options' => [
                                'class' => 'form-group has-feedback',
                            ],
                            'template' => '{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span>{error}'
                        ])->passwordInput([
                            'class' => 'form-control',
                            'placeholder' => $model->getAttributeLabel('password_repeat'),
                        ]) ?>

                        <?= \yii\helpers\Html::submitButton(\yii\helpers\Html::tag('i', '', ['class'=>'fa fa-save']).' Сохранить', [
                            'class' => 'btn btn-primary btn-block',
                            'name' => 'change-password-button',
                        ]) ?>

                    <?php \yii\widgets\ActiveForm::end() ?>

                </div>
            </div>
        </div>

    </div>
</div>