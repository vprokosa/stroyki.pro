<?php
$this->title = 'Обновить профиль';
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="profile-box">

                <div class="profile-box-body">
                    <?php $form = \yii\widgets\ActiveForm::begin() ?>


                    <?= $form->field($model, 'full_name')->textInput() ?>

                    <?= $form->field($model, 'username')->textInput() ?>

                    <?= $form->field($model, 'type')->dropDownList(\common\enum\User::Type(), ['prompt'=>'']) ?>

                    <?= $form->field($model, 'email')->textInput() ?>

                    <?= $form->field($model, 'phone')->textInput() ?>

                    <?= $form->field($model, 'site')->textInput() ?>

                    <?= $form->field($model, 'buildcompany')->textInput() ?>

                    <?= $form->field($model, 'city')->textInput() ?>
					
                    <?= $form->field($model, 'street')->textInput() ?>
					
                    <?= $form->field($model, 'house')->textInput() ?>
					
                    <?= $form->field($model, 'office')->textInput() ?>
					
                    <?= $form->field($model, 'address')->textarea() ?>
					
                    <?= $form->field($model, 'company')->textarea(['rows' => 6,'style'=>'width: 570px; height: 80px;resize: both;']) ?>

                    <?= \yii\helpers\Html::submitButton(\yii\helpers\Html::tag('i', '', ['class'=>'fa fa-save']).' Сохранить', [
                        'class' => 'btn btn-primary btn-block',
                        'name' => 'profile-button',
                    ]) ?>

                    <?php \yii\widgets\ActiveForm::end() ?>

                </div>
            </div>
        </div>

    </div>
</div>