<?php
$this->title = 'Новый аватар';
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6" style="padding-left: 30px; padding-right: 0px;">
            <div class="change-avatar-box">

                <div class="change-avatar-box-body">
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'options'=>[
                            'enctype'=>'multipart/form-data'
                        ]
                    ]) ?>
                        <?= \Yii::$app->user->identity->avatar(75, 75) ?>
                        <?= $form->field($model, 'avatar', [
                            'options'=>[
                                'style'=>'padding-bottom:0px;margin-bottom:0px;'
                            ]
                        ])->widget(\kartik\widgets\FileInput::classname(), [
                            'options' => [
                                'class'=>'app-file-input',
                                'accept' => 'image/*',
                            ],
                            'pluginOptions' => [
                                'allowedFileExtensions'=>['jpg', 'png'],
                                'allowedPreviewTypes'=>['image'],
                                'showUpload' => false,
                                'browseLabel' => '',
                                'removeLabel' => '',
                                'browseIcon' => '<i class="fa fa-folder-open"></i>',
                                'removeIcon' => '<i class="fa fa-trash"></i>',
    //                                'initialPreview'=>[
    //                                    Yii::$app->user->identity->avatar()
    //                                ],
                            ]
                        ]) ?>

                        <?= \yii\helpers\Html::submitButton(\yii\helpers\Html::tag('i', '', ['class'=>'fa fa-save']).' Сохранить', [
                            'class' => 'btn btn-primary btn-block',
                            'name' => 'avatar-button',
                        ]) ?>

                    <?php \yii\widgets\ActiveForm::end() ?>

                </div>
            </div>
        </div>

    </div>
</div>