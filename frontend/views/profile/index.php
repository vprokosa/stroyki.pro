<?php

$this->title = 'Профиль';
?>
 
<?= \yii\widgets\DetailView::widget([
    'model' => $user,
    'formatter' => [
        'class' => \yii\i18n\Formatter::className(),
        'nullDisplay' => 'Пусто'
    ],
    'attributes' => [
        [
            'label'=> $user->getAttributeLabel('avatar'),
            'format'=>'html',
            'value' => $user->avatar(50, 50)
        ],
        'full_name',
        'username',
        [
            'label'=> $user->getAttributeLabel('type'),
            'value' => is_numeric($user->type) ? \common\enum\User::Type($user->type) : 'Пусто'
        ],
        [
            'label'=> $user->getAttributeLabel('status'),
            'value' => \common\enum\User::Status($user->status)
        ],
        'email',
        'phone',
        'site',
        'company',
        'buildcompany',
        'city',
        'street', 
        'house', 
        'office', 
        'address', 
    ]
]);

?>