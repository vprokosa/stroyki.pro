<?php
$this->title = 'Подписка';
?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="subscription-box">

                <div class="subscription-box-body">
                    <?php $form = \yii\widgets\ActiveForm::begin() ?>
                    

                    <?= $form->field($model, 'sending')->checkbox() ?>

                    <?= \yii\helpers\Html::submitButton(\yii\helpers\Html::tag('i', '', ['class'=>'fa fa-save']).' Сохранить', [
                        'class' => 'btn btn-primary btn-block',
                        'name' => 'subscription-button',
                    ]) ?>

                    <?php \yii\widgets\ActiveForm::end() ?>

                </div>
            </div>
        </div>

    </div>
</div>