<?php

namespace frontend\components\actions;

use yii\base\Action;
use yii\web\View;
use Yii;

class SiteErrorAction extends Action
{
    /** @var  $view View */
    public $view;

    public function run()
    {
        $exception = Yii::$app->errorHandler->exception;
//var_dump($exception);
        $this->view = new View();

        return $this->view->render('@frontend/components/actions/site/error', [
            'exception' => $exception
        ]);
    }
}