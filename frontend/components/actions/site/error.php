<?php

use Yii;
use \yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ошибка <?= $exception->statusCode; ?>. <?= $exception->getMessage() ?></title>
 </head>
<style>
    *{
        padding: 0;
        margin: 0;
    }
    html {
        min-height: 100%;
    }
    body {

        width: 100%;
        height: 100%;
        background: url("/img/404.jpg");
        background-repeat: no-repeat;
        background-position: 50% 50%;
        background-size: cover;
    }
    h1 {
        font-size: 36px;
        color: #fff;
    }
    .message {
        text-align: center;
        width: 400px;
        padding: 25px;
        position: absolute;
        top:40%;
        left: 50%;
        margin-left: -225px;
    }
    a {
        /*text-decoration: none;*/
        color: #fff;
    }
</style>
<body>

    <div class="message">
        <h1>Ошибка <?= $exception->statusCode; ?>. <?= $exception->getMessage() ?></h1>
        <br/><br/>
        <a href="<?= Url::home() ?>">Перейти на главную</a>
    </div>
</body>
</html>
