<?php

namespace frontend\components\actions;


use yii\base\Action;

class LogoHeaderAction extends Action
{
    public function run($url='')
    {
//    header('Content-Description: File Transfer');
//    header('Content-Disposition: attachment; filename=banner.png');
//    header('Content-Type: application/octet-stream');
        header('Content-type: image/png');
        if(strpos($url, '|')) {
            list($header, $logo) = explode('|', $url);
            $img = \WideImage\WideImage::load(\yii\helpers\Url::base(true).$header);
            $img_logo = \WideImage\WideImage::load(\yii\helpers\Url::base(true).$logo);

            return $img->merge($img_logo, 'left+20', 'bottom-25')->output('png');
        }
        return \WideImage\WideImage::load(\yii\helpers\Url::base(true).$url)->output('png');
    }
}