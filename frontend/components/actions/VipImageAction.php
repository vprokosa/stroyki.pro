<?php

namespace frontend\components\actions;


use yii\base\Action;

class VipImageAction extends Action
{
    public function run($url='')
    {
//        $url = substr($url, 0, -1);
//    header('Content-Description: File Transfer');
//    header('Content-Disposition: attachment; filename=banner.png');
//    header('Content-Type: application/octet-stream');
        header('Content-type: image/png');

        $img = \WideImage\WideImage::loadFromFile(\Yii::getAlias("@webroot{$url}"));
        $img_vip = \WideImage\WideImage::loadFromFile(\Yii::getAlias('@webroot/image/vip3.png'));

        return $img->merge($img_vip, 'right', 'bottom')->output('png');
    }
}