<?php
namespace frontend\components;

use Yii;
use yii\caching\DbDependency;
use yii\web\CompositeUrlRule;
use yii\web\UrlRuleInterface;
use yii\base\InvalidConfigException;
use common\models\Realty;
use common\models\City;
use common\models\District;
use yii\base\Model;

class RealtyUrlRule implements UrlRuleInterface
{
    public $cacheComponent = 'cache';

    public $cacheID = 'RealtyUrlRule';

    public $ruleConfig = ['class' => 'yii\web\UrlRule'];

    public function createUrl($manager, $route, $params)
    {

        //die();
		/* echo'createUrl<pre>'; */
		//print_r($route);
		/* print_r($params);
		echo'</pre>';
		*/
		$return = [];
        if ($route === 'site/advertisement') {
			$breadRealty = (isset($params['id']) && (!empty($params['id']))) ? Realty::find()->where(['id'=>(int)$params['id']])->with(['attr','desc'])->one() :[] ;
			if($breadRealty){
				if(!empty($breadRealty->attr->city_id)){
					$return[] =  \common\components\TranslitHelper::transliteration(\common\enum\City::Lists($breadRealty->attr->city_id));
				}
				if(!empty($breadRealty->user->type)){
					if($breadRealty->user->type == \common\enum\User::TYPE_CREATOR){
						$return[] ='zastroyshchiki';
					}
				}
				if(!empty($breadRealty->attr->rooms)){
					$return[] = $breadRealty->attr->rooms. '-komnatnie';
				}
				if(!empty($breadRealty->attr->district_id)){
					$return[] =  \common\components\TranslitHelper::transliteration(\common\enum\District::Lists($breadRealty->attr->district_id));
				}
				if(!empty($breadRealty->id)){
					$return[] =  $breadRealty->id;
				}
				if(!empty($breadRealty->desc->title)){
					//$return .=  '_'.$breadRealty->desc->title;
				}
				
			}
			//$return .= $params['id'];

			return implode('/', $return);
		}
        if (($route === 'site/search-objects') || ($route === 'site/search-objects-on-map')) {
			//$routeArr = explode('/', $route);
			//$routing = (isset($routeArr[1]) && !empty($routeArr[1])) ? $routeArr[1] : 'search-objects' ;
            if (isset($params['ObjectSearch'])) {
				if(isset($params['ObjectSearch']['city_id']) && (!empty($params['ObjectSearch']['city_id']))){
					$return[] =  \common\components\TranslitHelper::transliteration(\common\enum\City::Lists($params['ObjectSearch']['city_id']));
				} else {
					if(isset($_COOKIE['cityID']) && (!empty($_COOKIE['cityID']))) {
						$return[] =  \common\components\TranslitHelper::transliteration(\common\enum\City::Lists($_COOKIE['cityID']));
					}
				}
				if(isset($params['ObjectSearch']['option']) && (!empty($params['ObjectSearch']['option']))){
					if($params['ObjectSearch']['option'] == \common\enum\User::TYPE_CREATOR){
						$return[] ='zastroyshchiki';
					}
				}
				if (isset($params['ObjectSearch']['novostroyki'])) {
					$return[] ='novostroyki';
				}
				if(isset($params['ObjectSearch']['housing_estate']) && (!empty($params['ObjectSearch']['housing_estate']))){
					$return[] = 'jilie-kompleksi';
				}
				if(isset($params['ObjectSearch']['room1']) && (!empty($params['ObjectSearch']['room1']))){
					$return[] = '1-komnatnie';
				}
				if(isset($params['ObjectSearch']['room2']) && (!empty($params['ObjectSearch']['room2']))){
					$return[] = '2-komnatnie';
				}
				if(isset($params['ObjectSearch']['room3']) && (!empty($params['ObjectSearch']['room3']))){
					$return[] = '3-komnatnie';
				}
				if(isset($params['ObjectSearch']['room4']) && (!empty($params['ObjectSearch']['room4']))){
					$return[] = '4-komnatnie';
				}
				if(isset($params['ObjectSearch']['district_id']) && (!empty($params['ObjectSearch']['district_id']))){
					$return[] =  \common\components\TranslitHelper::transliteration(\common\enum\District::Lists($params['ObjectSearch']['district_id']));
				}
			}
			if ($route === 'site/search-objects-on-map') {
				$return[] ='maps';
			} else {
				if (isset($params['ObjectSearch']['maps'])) {
					$return[] ='maps';
				}
			}
        }
		/* echo'returnUrl<pre>';
		print_r($return);
		echo'</pre>'; */

			if(isset($params['page'])){
				$return[] = 'p-'.$params['page'];
			}
			if(isset($params['per-page'])){
				$return[] = 'pp-'.$params['per-page'];
			}
		return implode('/', $return);
    }

    public function parseRequest($manager, $request)
    {
		#/moskva/zastroyshchiki/3-komnatnie/mitishe

        $pathInfo = $request->getPathInfo();
		$pathElems = explode('/', $pathInfo);
		/* print_r($pathInfo);
		print_r($pathElems); */

        /**
         * Контроллеры и экшены, которые необходимо исключить
         */
		$controllers = array('article', 'page', 'banner', 'user', 'realty', 'realty-image', 'profile', 'service', 'cabinet');
		$actions = array('contact', 'calculator', 'format', 'support', 'upload');

		$params = array();

		if(in_array($pathElems[0], $controllers)){
			return false;
		}

		if (isset($pathElems[1]) && in_array($pathElems[1], $actions))
            return false;
		/* if (preg_match('%^(\w+)(/(\w+))?$%', $pathInfo, $matches)) {
			print_r($matches); 
		} */
		$returnPatch = 'site/search-objects';
		$cityObj = '';
		$distrObj = '';

		foreach($pathElems as $elem){

		    /* echo'elem<pre>';
			print_r($elem);
			echo'</pre>'; */

			if(is_numeric($elem)){

				$params['id'] = $elem;
				return ['site/advertisement', $params];
			}
			if(empty($cityObj)){
				$cityObj = City::find()->where(["alias" => $elem])->one();
			}

            /**
             * если есть район в запросе прим.
             * yaroslavl/zastroyshchiki/yaroslavskijr-n
             */
			if(empty($distrObj)){
				$distrObj = District::find()->where(["alias" => $elem])->one();
			}

			//$distrObj = empty($distrObj) ? District::find()->where(["alias" => $elem])->one() : '';
			/* echo'cityObj<pre>';
			print_r($cityObj);
			echo'</pre>'; */


			switch($elem){
				case 'zastroyshchiki':
					$returnPatch = 'site/search-objects';
					$params['ObjectSearch']['option'] =  3;
				break;
				case 'jilie-kompleksi':
					$returnPatch = 'site/search-objects';
					$params['ObjectSearch']['housing_estate'] =  1;
				break;
				case 'novostroyki':
					$returnPatch = 'site/search-objects';
					$params['ObjectSearch']['novostroyki'] =  1;
				break;
				case 'maps':
					$returnPatch = 'site/search-objects-on-map';
					$params['ObjectSearch']['maps'] =  1;
				break;
			}



			$exploded = explode('-', $elem);
			if(count($exploded) > 1){
				switch($exploded[0]){
					case 'p':
						$params['page'] =  $exploded[1];
					break;
					case 'pp':
						$params['per-page'] =  $exploded[1];
					break;
					default:
						$returnPatch = 'site/search-objects';
						$room = substr($elem, 0, 1);
						if($room == 1) {$params['ObjectSearch']['room1'] = 1;}
						if($room == 2) {$params['ObjectSearch']['room2'] = 1;}
						if($room == 3) {$params['ObjectSearch']['room3'] = 1;}
						if(in_array($room, array(4,5,6,7,8,9,10))) {$params['ObjectSearch']['room4'] = 1;}
				}
			}
		}

		//$returnPatch = 'site/search-objects';
		//if(count($pathElems) <= 2)
		/* $city = isset($pathElems[0]) ? $pathElems[0] : '';
		$type = isset($pathElems[1]) ? $pathElems[1] : '';
		$rooms = isset($pathElems[2]) ? $pathElems[2] : '';
		$district = isset($pathElems[3]) ? $pathElems[3] : '';
		$id = isset($pathElems[4]) ? $pathElems[4] : ''; */
		/* if(isset($id) && ($id>0)){
			$params['id'] = $id;
			return ['site/advertisement', $params];
		} */

		$params['ObjectSearch']['city_id'] =  !empty($cityObj) ? $cityObj['id']: '';
		if(!empty($cityObj)){
			if(!isset($_COOKIE['cityID']) || empty($_COOKIE['cityID'])) {
				unset($_COOKIE["cityID"]);
				setcookie("cityID", $params['ObjectSearch']['city_id'], time() + (1000 * 120),'/','',false,false);
				$_COOKIE['cityID'] = $params['ObjectSearch']['city_id'];
			}

			if(isset($pathElems[1]) && !in_array($pathElems[1], array('zastroyshchiki', 'jilie-kompleksi', 'novostroyki', 'maps'))){
				return false;
			}
		}

        	/* echo'params<pre>';
			print_r($params);
			echo'</pre>';
			die(); */

		$params['ObjectSearch']['district_id'] = !empty($distrObj) ? $distrObj['id']: '';


			
/* 		if(!empty($city)){
			$cityObj = City::find()->where(["alias" => $city])->one();
		} else {
			if(isset($_COOKIE['cityID']) && (!empty($_COOKIE['cityID']))) {
				$params['ObjectSearch']['city_id'] =  $_COOKIE['cityID'];
			}
		}
		if(!empty($type)){
			switch($type){
				case 'zastroyshchiki':
					$params['ObjectSearch']['option'] =  3;
				break;
				case 'jilie-kompleksi':
					$params['ObjectSearch']['housing_estate'] =  1;
				break;
				case 'novostroyki':
					$params['ObjectSearch']['novostroyki'] =  1;
				break;
			}
		}
		if(!empty($rooms)){
			$room = substr($rooms, 0, 1);
			if($room == 1) {$params['ObjectSearch']['room1'] = 1;}
			if($room == 2) {$params['ObjectSearch']['room2'] = 1;}
			if($room == 3) {$params['ObjectSearch']['room3'] = 1;}
			if(in_array($room, array(4,5,6,7,8,9,10))) {$params['ObjectSearch']['room4'] = 1;}
		}
		if(!empty($district)){
			$distrObj =  District::find()->where(["alias" => $district])->one();
			$params['ObjectSearch']['district_id'] = !empty($distrObj) ? $distrObj['id']: '';
		}
		if(end($pathElems) == 'maps'){
			$returnPatch = 'site/search-objects-on-map';
			$params['ObjectSearch']['maps'] =  1;
		} */
		/* echo'parseRequest<pre>';
		print_r($returnPatch);
		print_r($params);
		echo'</pre>'; */

		if(!empty($params)){
			return [$returnPatch, $params];
		}
        return false; // this rule does not apply
    }



}