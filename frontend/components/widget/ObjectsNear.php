<?php

namespace frontend\components\widget;


use common\enum\Banner;
use yii\base\Widget;
use yii\db\Query;

//Новостройки рядом
class ObjectsNear extends Widget
{
    public $geo_lat;
    public $geo_lng;
    
    public function run()
    {
        $data = (new Query())
            ->select('realty.id, realty.alias, realty.price, realty_attr.rooms, realty_attr.area_all, realty_attr.area_living, realty_attr.area_kitchen, realty_image.image')
            ->from('realty')
            ->leftJoin('realty_attr', 'realty_attr.realty_id = realty.id')
            ->leftJoin('realty_image', 'realty_image.realty_id = realty.id')
            ->where(['between', 'geo_lat', $this->geo_lat-1, $this->geo_lat+1])
            ->andWhere(['between', 'geo_lng', $this->geo_lng-1, $this->geo_lng+1])
            ->limit(3)
            ->orderBy(['realty_image.sort'=>SORT_ASC])
            ->orderBy('RAND()')
            ->all();
        
        return $this->render('objects-near', ['data'=>$data]);
    }
}