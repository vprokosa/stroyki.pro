<?php

namespace frontend\components\widget;


use common\enum\Article;
use yii\base\Widget;
use yii\db\Query;

class ShowArticle extends Widget
{
    public function run($params = null)
    {
		if(is_array($params) && !empty($params)){
			$position = isset($params['pos']) ? $params['pos'] : 0 ;
			$city_id = isset($params['city_id']) ? $params['city_id'] : 0 ;
			$q = new Query();
			$q->select(['id','text','title']);
			$q->from('articles');
			if(!empty($position)){
				$q->where(['position'=>$position]);
			}
			if(!empty($position) && ($position == 3)){ // главная
			} else {
				if(!empty($city_id)){
					$q->andWhere(['city_id'=>$city_id]);
				}
			}
			
			$q->andWhere(['status'=>\common\enum\Article::STATUS_ACTIVE]);
			$article = $q->one();
			if(!empty($article)){
				return $this->render('articles_item', ['article'=>$article]);
			}
		}
    }
}