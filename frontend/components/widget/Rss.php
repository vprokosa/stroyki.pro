<?php

namespace frontend\components\widget;


use yii\base\Widget;
use yii\db\Query;
use yii\web\View;

class Rss extends Widget
{
    public function run()
    {
        $count = (new Query())
            ->from('subscription_search')
            ->where([
                'user_id'=>\Yii::$app->user->id
            ])->count('id');
        $this->regJS();
        return $this->render('rss', ['count'=>$count]);
    }
    public function regJS()
    {
        $view = $this->getView();
        $view->registerJs("
            function RegSending() {
                if(isLogin()) {
                    var param = $('#form-search').serializeArray();
                    param = JSON.stringify(param);
                    $.post(\"/service/rss-register\",{param:param}, function (count) {
                        if(count > 0){
                            $('#u38663-4').parent().parent().addClass('active');
                            var src = $('#u38663-4').parent().find('img').attr('src');
                            if(src.indexOf('_active.png') == -1){
                                src = src.replace('.png', '_active.png');
                                $('#u38663-4').parent().find('img').attr('src', src);
                            }
                            
                        }else{
                             $('#u38663-4').parent().parent().removeClass('active');
                            var src = $('#u38663-4').parent().find('img').attr('src');
                            src = src.replace('_active.png', '.png');
                            $('#u38663-4').parent().find('img').attr('src', src);
                        }
                        if(count>=1) {
                            alert('Рассылка оформлена');
                            $('#u38663-4 p').text(count);
                        } else {
                            alert('Рассылка не оформлена. Ваша форма поиска возможно не заполнена');
                        }
                    });
                } else {
                    location.href = '/login';
                }
            }  
        ", View::POS_END);
    }
    
}