<?php

namespace frontend\components\widget;

use yii\base\Widget;
use yii\data\ActiveDataProvider;
use common\models\Realty;
use yii\db\Query;
use yii\helpers\Json;

class ObjectsMain extends Widget
{
    public function run()
    {
        $op = '>';
        $city_id = 0;
        if(isset($_COOKIE['cityID']) && is_numeric($_COOKIE['cityID'])) {
            $op = '=';
            $city_id = $_COOKIE['cityID'];
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Realty::find()
                ->with(['attr','user','desc','images','favoriteForUser'])
                ->leftJoin('realty_attr', 'realty_attr.realty_id=realty.id')
                ->where([
                    'realty.status' => \common\enum\Realty::STATUS_PUBLIC,
                ])
                ->andWhere([$op, 'realty_attr.city_id', $city_id])
                ->orderBy('realty.id DESC'),
            'pagination' => [
                'pageSize' => 4,
            ],
        ]);

        $json = BannerMiddle::widget(['toJson'=>true,'count'=>4]);
        $banners = Json::decode($json);
        $banners = array_chunk($banners, 2);
        
        return $this->render('objects-main/list', [
            'listDataProvider' => $dataProvider,
            'banners'=>$banners
        ]);
    }
    

}