<?php

namespace frontend\components\widget;

use yii\base\Widget;
use Yii;

class HeaderBackground extends Widget {

    public function run() {

        $user = Yii::$app->session->get('userObj');
      //  echo "<pre>".print_r($user, true)."</pre>";
        $src = $user->header(1920, 1080, false);
        $src = strpos($src, '|') ? explode('|', $src)[0] : $src;

        return $this->render('headerBg', compact('user', 'src'));
    }
}
