<?php

namespace frontend\components\widget;


use common\enum\Banner;
use yii\base\Widget;
use yii\db\Query;

class BannerBig extends Widget
{
    public function run()
    {
        $op = 'IS';
        $city_id = NULL;
        if(isset($_COOKIE['cityID']) && is_numeric($_COOKIE['cityID'])) {
            $countBanners =  (new Query())
                ->select('id')
                ->from('banners')
                ->andWhere(['status'=>Banner::STATUS_ACTIVE])
                ->where(['city_id'=>$_COOKIE['cityID']])
                ->andWhere(['type'=>Banner::TYPE_BIG])
                ->count();
            if($countBanners) {
                $op = '=';
                $city_id = $_COOKIE['cityID'];
            }
        }

        $banner = (new Query())
            ->select(['id','image','title'])
            ->from('banners')
            ->where([$op, 'city_id', $city_id])
            ->andWhere(['status'=>Banner::STATUS_ACTIVE])
            ->andWhere(['type'=>Banner::TYPE_BIG])
            ->orderBy('RAND()')
            ->one();

        return $this->render('banner-big', ['banner'=>$banner]);
    }
}