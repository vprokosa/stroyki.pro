<?php

namespace frontend\components\widget;


use yii\base\Widget;
use yii\db\Query;
use yii\web\View;

class Favorite extends Widget
{
    public function run()
    {
        $count = (new Query())
            ->from('realty_favorite')
            ->where([
                'user_id'=>\Yii::$app->user->id
            ])->count('id');
        $this->regJS();
        return $this->render('favorite', ['count'=>$count]);
    }

    public function regJS()
    {
        $view = $this->getView();
        $view->registerJs("
            function RegFavorite(el, id) {
                $.get('/service/reg-favorite', {id:id}, function(count){
                    var currentCount = new Number( $('#u38729-4 p').text() );
                    $('#u38729-4 p').text(count);
                    if(count > 0){
                        $('#u38729-4').parent().parent().addClass('active');
                        var src = $('#u38729-4').parent().find('img').attr('src');
                        if(src.indexOf('_active.png') == -1){
                            src = src.replace('.png', '_active.png');
                            $('#u38729-4').parent().find('img').attr('src', src);
                        }
                        
                    }else{
                         $('#u38729-4').parent().parent().removeClass('active');
                        var src = $('#u38729-4').parent().find('img').attr('src');
                        src = src.replace('_active.png', '.png');
                        $('#u38729-4').parent().find('img').attr('src', src);
                    }
                    if(currentCount < count) {
                        $(el).removeClass('color-yellow').addClass('color-green');
                    } 
                    else if(currentCount > count) {
                        $(el).removeClass('color-green').addClass('color-yellow');
                    }
                });
            }
        ", View::POS_END);
    }
    
}