<div id="banners1" class="container" >
    <div class="row">
        <?php foreach($banners as $banner): ?>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                <a href="<?= \yii\helpers\Url::to(['site/banner', 'id'=>$banner['id']]) ?>" target="_blank">
                    <img class="img-responsive" src="/upload/banner/<?= $banner['id'] ?>/<?= $banner['image'] ?>" alt="<?= $banner['title'] ?>">
                </a>
            </div>
            <?php Yii::$app->stat->regShow('banner', $banner['id']); ?>
        <?php endforeach ?>
    </div>
</div>