<div id="_banners" class="container <?php echo $dop_class; ?>" >
    <div class="row">
        <?php foreach($banners as $banner): ?>
            <div class="col-md-4">
                <div class="banners">
                    <a href="<?= \yii\helpers\Url::to(['site/banner', 'id'=>$banner['id']]) ?>" target="_blank">
                        <img style="width:300px;height:115px;display:block;margin: 0 auto;" class="img-responsive" src="/upload/banner/<?= $banner['id'] ?>/<?= $banner['image'] ?>" alt="<?= $banner['title'] ?>">
                    </a>
                </div>
            </div>
            <?php Yii::$app->stat->regShow('banner', $banner['id']); ?>
        <?php endforeach ?>
    </div>
</div>

<!--<div id="banners" class="container <?php /*echo $dop_class; */?>" >
    <div class="row">
        <?php /*foreach($banners as $banner): */?>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <a href="<?/*= \yii\helpers\Url::to(['site/banner', 'id'=>$banner['id']]) */?>" target="_blank">
                    <img style="width:300px;height:115px;display:block;margin: 0 auto;" class="img-responsive" src="/upload/banner/<?/*= $banner['id'] */?>/<?/*= $banner['image'] */?>" alt="<?/*= $banner['title'] */?>">
                </a>
            </div>
            <?php /*Yii::$app->stat->regShow('banner', $banner['id']); */?>
        <?php /*endforeach */?>
    </div>
</div>-->