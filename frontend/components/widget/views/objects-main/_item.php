<?php 
use common\components\ThumbHelper;
?>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
    <a href="<?= \yii\helpers\Url::to(['site/advertisement', 'alias'=>$model->alias]) ?>">
        <div class="box">
            <div class="box-header">
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <picture>
                        <source srcset="<?= ThumbHelper::thumbnailImg($model->mainImage(), 1000, 750, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>" media="(min-width: 1000px)">
                        <source srcset="<?= ThumbHelper::thumbnailImg($model->mainImage(), 800, 600, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>" media="(min-width: 800px)">
                        <source srcset="<?= ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>" media="(min-width: 768px)">
                        <img class="img-responsive img-thumbnail" srcset="<?= ThumbHelper::thumbnailImg($model->mainImage(), 768, 576, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>" alt="Фото">
                    </picture>
                </div>
            </div>
            <div class=" box-body">
                <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                    <h3><?= \yii\helpers\Html::encode($model->desc->title) ?></h3>
                    <div class="text1">
                        <p>
                            город: <?= \common\enum\City::Lists($model->attr->city_id) ?>,
                            р-н: <?= \common\enum\District::Lists($model->attr->district_id) ?>
                        </p>
                        <p>ул.<?= $model->attr->street ?>, дом <?= $model->attr->house ?></p>
                        <?php if(!$model->isApartmentHouse()): ?>
                            <p>
                                комнат: <?= \yii\helpers\Html::encode($model->attr->rooms) ?>,
                                площадь: <?= is_numeric($model->attr->area_all) ? $model->attr->area_all.'<sup>2</sup>' : '' ?>
                            </p>
                        <?php endif ?>
                    </div>

                    <?php /*
                    <div class="text2">
                        <p>1 комнатные от ..................100 000 000 руб</p>
                        <p>2 комнатные от ..................200 000 000 руб</p>
                        <p>3 комнатные от ..................300 000 000 руб</p>
                    </div>
                    */ ?>

                    <section>
                        <div class="text3">
                            <p><?= is_numeric($model->user->type) ? \common\enum\User::Type($model->user->type) : '' ?></p>
                            <?php /*
                        <p>Застройщик: Севзаплесзнергосбыт</p>
                        */ ?>
                        </div>
                        <div class="text4">
                            <span class="fa fa-clock-o"></span>
                            <i><?= \common\components\helper\DateTimeHelper::diffFullPeriod($model->created_at, time()) ?>  </i>
                            <?= $this->render('/layouts/_btn_favorite', ['model'=>$model]) ?>
                            <?= $this->render('/layouts/_btn_spy_price', ['model'=>$model]) ?>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </a>
</div>

<?php if($index==1 || $index==3): ?>
    <!-- banner -->
    <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
        <div class="box-banner">
            <div class="box-header"></div>
            <div class="box-body">
                <?php if(isset($banners[$index==1 ? 0 : 1])): ?>
                    <?php foreach($banners[$index==1 ? 0 : 1] as $banner): ?>
                        <div class="col-sm-6 col-xs-12" style="margin-bottom:20px">
                            <a href="<?= \yii\helpers\Url::to(['site/banner', 'id'=>$banner['id']]) ?>" target="_blank">
                                <img class="img-responsive img-thumbnail" src="/upload/banner/<?= $banner['id'] ?>/<?= $banner['image'] ?>" alt="<?= $banner['title'] ?>">
                            </a>
                        </div>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
        </div>
    </div>
    <!-- #banner -->
<?php endif ?>

<?php if($index == 3): ?>
    <!-- banner -->
    <?= \frontend\components\widget\BannerBig::widget() ?>
    <!-- #banner -->
<?php endif ?>




