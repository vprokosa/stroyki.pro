<style>
    .empty{font-size: x-large;margin-left: 13px;}
</style>
<?php
 $this->registerJs('
    $(document).ready(function () {
        if($(".empty").length) {
            $("#banner-right").remove();
        }
        $("#breadcrumb .active a").text("<?= $listDataProvider->getTotalCount() ?> объявлений");
    });');
?>
<div id="objects-preview" class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <div class="row">
        <?php \yii\widgets\Pjax::begin([
            'timeout'=>10000,
            'scrollTo'=>new \yii\web\JsExpression('$("#objects-preview").position().top')
        ]) ?>
            <?= \yii\widgets\ListView::widget([
                'dataProvider' => $listDataProvider,
                'layout' => "{items}<div id='paginate'>{pager}</div>",
                'itemView' => function ($model, $key, $index, $widget) use ($banners) {
                    return $this->render('_item',[
                        'model' => $model,
                        'index' => $index,
                        'banners' => $banners
                    ]);
                },
            ]) ?>

        <?php \yii\widgets\Pjax::end() ?>
    </div>
</div>
