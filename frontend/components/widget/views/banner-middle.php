<div id="banner-right" class="visible-lg visible-md">
    <div class="col-lg-4 col-md-4">
        <?php foreach($banners as $banner): ?>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <a href="<?= \yii\helpers\Url::to(['site/banner', 'id'=>$banner['id']]) ?>" target="_blank">
                        <img class="img-responsive img-thumbnail" src="/upload/banner/<?= $banner['id'] ?>/<?= $banner['image'] ?>" alt="<?= $banner['title'] ?>" style="width:370px;height:210px">
                    </a>
                </div>
            </div>
            <?php Yii::$app->stat->regShow('banner', $banner['id']); ?>
        <?php endforeach ?>
    </div>
</div>