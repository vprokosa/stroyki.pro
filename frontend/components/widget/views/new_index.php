<?php
$this->title = 'Главная';
use common\components\ThumbHelper;
?>


<section class="contentnd">
    <div class="container boxnd">
        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden_max_600">
                <div class="block_line_box">
                    <a class="new_l" href="/search-objects"><p>Новые объявления</p></a>
                    <a class="add_advert add_in_sm" href="/realty/post"><p>Добавить объявление</p></a>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible_min_600">
                <div class="block_line_box first_20">
                    <a class="new_l" href="/search-objects"><p>Новые объявления</p></a>
                    <a class="add_advert add_in_sm add_advert add_in_sm_sm" href="/realty/post"><p>+</p></a>
                </div>
            </div>
            <div class="block_house">
            	<?
				foreach ($new_objects as $key => $value) {
				?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box_house">
                        <div class="strong_size bg_1" style="background-image:url(<?= ThumbHelper::thumbnailImg($value["obj"]->mainImage(), 300, 250, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>);">
                            <a href="/site/advertisement?id=<?=$value["obj"]->id?>">
                                <p class="city_p"><?=$value["city_name"]?><br>
                                <?=$value["obj"]->attr->rooms?> комнатная</p>
                                <p class="price_p"><?=number_format($value["obj"]->price, 0, ","," ")?> <span>руб</span></p>
                                <p class="developer_p"><?=$value["user_type"]?></p>
                            </a>
                        </div>
                    </div>
                </div>
				<?
				}
				?>
            </div>
			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden_max_600">
	            <div class="block_line_box">
	                <a class="new_l" href="/search-objects?housing_estate=1"><p>Новые жилые комплексы</p></a>
	                <a class="add_advert add_in_sm add__2" href="/realty/post"><p>Добавить жилой комплекс</p></a>
	            </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible_min_600">
	            <div class="block_line_box ">
	                <a class="new_l" href="/search-objects?housing_estate=1"><p>Новые жилые комплексы</p></a>
	                <a class="add_advert add_in_sm add__2 add_advert add_in_sm_sm" href="/realty/post"><p>+</p></a>
	            </div>
            </div>

            <div class="block_house">
	            <?
				foreach ($new_housing_estate as $key => $value) {
				?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
	                <div class="box_house">
	                    <div class="strong_size bg_4" style="background-image:url(<?= ThumbHelper::thumbnailImg($value["obj"]->mainImage(), 300, 250, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>);">
	                        <a href="/site/advertisement?id=<?=$value["obj"]->id?>">
	                        	<p class="city_p"><?=$value["city_name"]?></p>
	                            <p class="jk_p"><?=$value["obj"]->attr->gkh_name ?></p>
	                            <p class="sm_price">От :  <?=number_format($value["obj"]->price, 0, ","," ")?>  руб  м2</p>
	                            <p class="developer_p">Застройщик : <?=$value["user_type"]?></p>
	                        </a>
	                    </div>
	                </div>
	            </div>
				<?
				}
				?>
        	</div>

        	

        	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden_max_600">
	            <div class="block_line_box">
	                <a class="new_l" href="/search-objects?type=3"><p>Новые застройщики</p></a>
	                <a class="add_advert add_in_sm add__3" href="/realty/post"><p>Добавить застройщика</p></a>
	            </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible_min_600">
	            <div class="block_line_box ">
	                <a class="new_l" href="/search-objects?type=3"><p>Новые застройщики</p></a>
	                <a class="add_advert add_in_sm add__3 add_advert add_in_sm_sm" href="/realty/post"><p>+</p></a>
	            </div>
            </div>

            <div class="block_house">
            	<?
				foreach ($new_zastr as $key => $value) {
				?>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
	                <div class="box_house">
	                    <div class="strong_size bg_7" style="background-image:url(<?= ThumbHelper::thumbnailImg($value["obj"]->mainImage(), 300, 250, ThumbHelper::THUMBNAIL_OUTBOUND, [], false) ?>);">
	                        <a href="/site/advertisement?id=<?=$value["obj"]->id?>">
	                        	<p class="city_p last_city_p"><?=$value["city_name"]?></p>
	                            <p class="developer_p">Застройщик:</p>
	                            <p class="big_devel_p"><?=$value["user_type"]?></p>
	                            <div style="background-image:url(<?=$value["obj"]->user->publicLogo()?>);"></div>
	                        </a>
	                    </div>
	                </div>
	            </div>
				<?
				}
				?>
        	</div>
			
			<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			                <div class="block_cities">
			                    <div class="head_block">
			                       <p>Найти город</p>
			                       <div class="box_form_search">
			                           <form id="form_c" action="" method="post">
			                               <input id="search_city" type="search">
			                               <a href="javascript:void(0);" class="button" type="submit"><img alt="" src="/image/search.png"></a>
			                           </form>
			                       </div>
			                   </div>
			                    <div class="cities_list">
			                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
			                            <a href="javascript:void(0);">Адыгея</a>
			                            <a href="javascript:void(0);">Алтайский край</a>
			                            <a href="javascript:void(0);">Амурская область</a>
			                            <a href="javascript:void(0);">Архангельская область</a>
			                            <a href="javascript:void(0);">Астраханская область</a>
			                            <a href="javascript:void(0);">Башкортостан</a>
			                            <a href="javascript:void(0);">Белгородская область</a>
			                        </div>
			                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hidden-xs">
			                            <a href="javascript:void(0);">Адыгея</a>
			                            <a href="javascript:void(0);">Алтайский край</a>
			                            <a href="javascript:void(0);">Амурская область</a>
			                            <a href="javascript:void(0);">Архангельская область</a>
			                            <a href="javascript:void(0);">Астраханская область</a>
			                            <a href="javascript:void(0);">Башкортостан</a>
			                            <a href="javascript:void(0);">Белгородская область</a>
			                        </div>
			                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hidden-xs">
			                            <a href="javascript:void(0);">Адыгея</a>
			                            <a href="javascript:void(0);">Алтайский край</a>
			                            <a href="javascript:void(0);">Амурская область</a>
			                            <a href="javascript:void(0);">Архангельская область</a>
			                            <a href="javascript:void(0);">Астраханская область</a>
			                            <a href="javascript:void(0);">Башкортостан</a>
			                            <a href="javascript:void(0);">Белгородская область</a>
			                        </div>
			                    </div>
			                    <div class="cities_list hidden_area">
			                        <div class="col-xs-6">
			                            <a href="javascript:void(0);">Адыгея</a>
			                            <a href="javascript:void(0);">Алтайский край</a>
			                            <a href="javascript:void(0);">Амурская область</a>
			                            <a href="javascript:void(0);">Архангельская область</a>
			                            <a href="javascript:void(0);">Астраханская область</a>
			                            <a href="javascript:void(0);">Башкортостан</a>
			                            <a href="javascript:void(0);">Белгородская область</a>
			                        </div>
			                        <div class="col-xs-6">
			                            <a href="javascript:void(0);">Адыгея</a>
			                            <a href="javascript:void(0);">Алтайский край</a>
			                            <a href="javascript:void(0);">Амурская область</a>
			                            <a href="javascript:void(0);">Архангельская область</a>
			                            <a href="javascript:void(0);">Астраханская область</a>
			                            <a href="javascript:void(0);">Башкортостан</a>
			                            <a href="javascript:void(0);">Белгородская область</a>
			                        </div>
			                    </div>
			                    <div class="box_partners">
			                        <p>Все города</p>
			                        <a class="add_advert add_in_sm add__4" href="javascript:void(0);"><p>Стать партнёром</p></a>
			                    </div>
			                </div>
			            </div> -->

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:20px;">
                <div class="block_about">
					<?= \frontend\components\widget\ShowArticle::run(array('pos' => 3, 'city_id' => $_COOKIE['cityID'])) ?>  
                    <div class="box_banner">
                        <div></div>
                    </div>
                </div>
            </div>

            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden_max_600">
                <div class="block_line_box">
                    <a class="new_l" href="javascript:void(0);"><p>Последние новости</p></a>
                    <a class="add_advert add_in_sm add__5" href="javascript:void(0);"><p>Добавить</p></a>
                </div>
            </div> -->

            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 visible_min_600">
                <div class="block_line_box ">
                    <a class="new_l" href="javascript:void(0);"><p>Последние новости</p></a>
                    <a class="add_advert add_in_sm add__5 add_advert add_in_sm_sm" href="javascript:void(0);"><p>+</p></a>
                </div>
            </div> -->

            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="article">
                   <div class="box_img_art">
                       <img alt="" src="/image/news_city.jpg">
                   </div>
                    
                    <div class="box_article">
                        <h4>Выходит новый закон о новостройках.</h4>
                        <p>Последние новости  Последние</p>
                        <p>Последние новости  Последние новости ...</p>
                        
                    </div>
                    <div class="box_read_more">
                        <a href="javascript:void(0);">Читать дальше</a>
                    </div>
                    
                </div>
                <div class="box_list_news">
                    <div class="list_new">
                        <img alt="" src="/image/img_list_new.jpg"> 
                        <p>Новый дом на таганке</p>
                    </div>
                    <div class="list_new">
                        <img alt="" src="/image/img_list_new.jpg"> 
                        <p>Новый дом на таганке</p>
                    </div>
                    <div class="list_new">
                        <img alt="" src="/image/img_list_new.jpg"> 
                        <p>Новый дом на таганке</p>
                    </div>
                    <div class="list_new">
                        <img alt="" src="/image/img_list_new.jpg"> 
                        <p>Новый дом на таганке</p>
                    </div>
                    <div class="list_new">
                        <img alt="" src="/image/img_list_new.jpg"> 
                        <p>Новый дом на таганке</p>
                    </div>
                    <div class="box_btn_news">
                        <a class="plus_news" href="javascript:void(0);"><p>+Новость</p></a>
                        <a class="all_news" href="javascript:void(0);"><p>Все новости</p></a>
                    </div>
                </div>
            </div> -->

        </div>
    </div>
</section>