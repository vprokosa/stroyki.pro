<?php foreach($subscriptions as $subscription): ?>
    <?= \kartik\widgets\Growl::widget([
        'type' => \kartik\widgets\Growl::TYPE_INFO,
        'icon' => 'fa fa-eye',
        'title' => 'Изменения цены в объявлении',
        'showSeparator' => true,
        'body' => $subscription->realty->desc->title
    ]) ?>
<?php endforeach ?>

