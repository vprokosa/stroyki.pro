<?php
use common\components\ThumbHelper;
use yii\helpers\Html;

function floatToInt ($str) {
    return substr($str,  strpos('.', $str), -3);
}
?>

<?php if(count($data)): ?>

<div class="container">
    <div class="object-page">
        <div class="s s7">
            <div class="sTitle">
                <h2 class="h2">Новостройки рядом</h2>
            </div>
            <div class="sContent">
                <div class="row">
                <?php foreach($data as $item): ?>
                    <div class="item-wrapper">
                        <div class="col-md-4">
                            <div class="item">
                                <div class="popular-img">
                                    <?php if(strlen($item['image'])): ?>
                                        <img src="<?= ThumbHelper::thumbnailImg('@common/upload/realty/picture/'.$item['id'].'/'.$item['image'], 300, 225, ThumbHelper::THUMBNAIL_OUTBOUND, ['class' => false], false) ?>" alt="Новостройки рядом">
                                    <?php else: ?>
                                        <?= \common\components\ThumbHelper::thumbnailImg('@webroot/image/default/nopic.png', 300, 225, \common\components\ThumbHelper::THUMBNAIL_OUTBOUND, ['class'=> false]) ?>
                                    <?php endif ?>
                                </div>
                                <h3 class="h3"><a href="<?= \yii\helpers\Url::to(['site/advertisement', 'id'=>$item['id']]) ?>"><?= $item['rooms'] ?> комнатная: <?= floatToInt($item['area_all']) ?>/<?=  floatToInt($item['area_living']) ?>/<?=  floatToInt($item['area_kitchen']) ?></a></h3>
                                <span class="price"><?= preg_replace('#(?<=\d)(?=(\d{3})+$)#', ' ', $item['price']) ?> руб</span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif; ?>