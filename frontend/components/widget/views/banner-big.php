<?php if(isset($banner['id'])): ?>
    <div id="banner-big">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-xs">
            <div class="box-banner">
                <div class="box-header"></div>
                <div class="box-body-banner" style="">
                    <a href="<?= \yii\helpers\Url::to(['site/banner', 'id'=>$banner['id']]) ?>" target="_blank">
                        <img class="img-responsive" src="/upload/banner/<?= $banner['id'] ?>/<?= $banner['image'] ?>" alt="<?= $banner['title'] ?>" style="height:250px">
                    </a>
                </div>
            </div>
        </div>
        <?php Yii::$app->stat->regShow('banner', $banner['id']); ?>
    </div>
    
<?php endif ?>
