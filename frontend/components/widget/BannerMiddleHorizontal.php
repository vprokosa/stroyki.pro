<?php

namespace frontend\components\widget;


use common\enum\Banner;
use yii\base\Widget;
use yii\db\Query;
use yii\helpers\Json;

class BannerMiddleHorizontal extends Widget
{
    public $toJson=false;
    public $count;

    public function run()
    {
        $bannerMiddle = Json::decode( \frontend\components\widget\BannerMiddle::widget(['count'=>6, 'toJson'=>true]) );

        return $this->render('banner-middle-horizontal', ['banners'=>$bannerMiddle]);
    }
}