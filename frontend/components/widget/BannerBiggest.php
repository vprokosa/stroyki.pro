<?php

namespace frontend\components\widget;


use common\enum\Banner;
use yii\base\Widget;
use yii\db\Query;

class BannerBiggest extends Widget
{

    public function run($params = null)
    {
        $city_id = NULL;
		//if(is_array($params) && !empty($params)){
			$city_id = isset($params['city_id']) ? $params['city_id'] : 0 ;
		//}
			$q = new Query();
        
            $q->select(['id','image','title']);
            $q->from('banners');
            $q->where(['status'=>Banner::STATUS_ACTIVE]);
            $q->andWhere(['type'=>Banner::TYPE_BIGGEST]);
            //$q->orderBy('RAND()');
            //->limit($this->count) 
			//if(!empty($city_id)){
				$q->andWhere(['city_id'=> $city_id]);
			//}
        $banners = $q->one();
        return $this->render('banner-biggest', ['banners'=>$banners]);
    }
}