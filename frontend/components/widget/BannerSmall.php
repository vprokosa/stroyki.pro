<?php

namespace frontend\components\widget;


use common\enum\Banner;
use yii\base\Widget;
use yii\db\Query;

class BannerSmall extends Widget
{
    public $dop_class;

    public function run()
    {
        $op = 'IS';
        $city_id = NULL;
        if(isset($_COOKIE['cityID']) && is_numeric($_COOKIE['cityID'])) {
            $countBanners =  (new Query())
                ->select('id')
                ->from('banners')
                ->andWhere(['status'=>Banner::STATUS_ACTIVE])
                ->where(['city_id'=>$_COOKIE['cityID']])
                ->andWhere(['type'=>Banner::TYPE_SMALL])
                ->count();
            if($countBanners) {
                $op = '=';
                $city_id = $_COOKIE['cityID'];
            }
        }

        $banners = (new Query())
            ->select(['id','image','title'])
            ->from('banners')
            ->where([$op, 'city_id', $city_id])
            ->andWhere(['status'=>Banner::STATUS_ACTIVE])
            ->andWhere(['type'=>Banner::TYPE_SMALL])
            ->orderBy('RAND()')
            ->limit(6)
            ->all();
        
        return $this->render('banner-small', ['banners'=>$banners,'dop_class'=>$this->dop_class]);
    }
}