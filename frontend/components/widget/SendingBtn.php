<?php

namespace frontend\components\widget;

use yii\base\Widget;

class SendingBtn extends Widget
{
    public function run()
    {
        return $this->render('sending-btn', []);
    }    
}