<?php

namespace frontend\components\widget;


use common\models\SubscriptionPrice;
use yii\base\Widget;

class NotyChangePrice extends Widget
{
    public function run()
    {
        $subscriptions = SubscriptionPrice::find()->with('realty.desc')->where([
            'user_id'=>\Yii::$app->user->id,
            'show'=>\common\enum\Realty::SUBSCRIPTION_PRICE_YES
        ])->all();
        foreach ($subscriptions as $subscription) {
            $subscription->show = \common\enum\Realty::SUBSCRIPTION_PRICE_NO;
            $subscription->update(false, ['show']);
        }
        return $this->render('noty-change-price', ['subscriptions'=>$subscriptions]);
    }    
}